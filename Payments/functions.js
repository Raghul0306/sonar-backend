const { makeExtendSchemaPlugin, gql } = require("graphile-utils");
const fetch = require("node-fetch");
import {runTransaction, processPayment} from "phunnel-payments/functions";
import {getCustomerByStripeId, isBuyerCustomer, saveCardForPhunnelUsage, retrieveStripeCustomerCard, createStripeAuthCharge, finalizeStripeAuthCharge } from "phunnel-payments/payment-utilities";

export const CreateTransaction = makeExtendSchemaPlugin(build => {
    return {
      typeDefs: gql`
        type StripeTransaction {
         retail_price: Float
         available_quantity: Int
         buyerID: String
         buyerEmail: Int
        }
        type StripeCharge {
          amount: Float
          id: String
          currency: String
          source: String
          description: String
        }
        type PhunnelPayment {
          transaction: StripeTransaction
          charge: StripePaymentIntent
        }
        extend type Query {
          runStripeTransaction(listingTevoId: Int, quantity: Int, buyerEmail: String, currencyType: String): PhunnelPayment
        }
      `,
      resolvers: {
        Query: {
           runStripeTransaction: async (obj, args, context, info) => {
            return runTransaction(args.listingTevoId, args.quantity, args.buyerEmail, "usd").then(
              res => {return res}
            );
          }
        }
      }
    };
  });

  export const ChargePayment = makeExtendSchemaPlugin(build => {
    return {
      typeDefs: gql`
        extend type Query {
          chargePayment(tevoListing: TevoListingInput, buyerEmail: String, currencyType: String, quantity:Int, unTokenizedCreditCard: StripeSourceInput, stripeSourceId: String): PhunnelPayment
        }
      `,
      resolvers: {
        Query: {
            chargePayment: async (obj, args, context, info) => {
            return processPayment(args.tevoListing, args.buyerEmail, "usd", args.quantity,  args.unTokenizedCreditCard, args.stripeSourceId).then(
              res => { return res}
            );
          }
        }
      }
    };
  });

  

  export const GetStripeUser = makeExtendSchemaPlugin(build => {
    return {
      typeDefs: gql`
        type StripeSource{
          id: String
          object: String
          fingerprint: String
          exp_month: String
          exp_year: String
          last4: String
          funding: String
          brand: String
        }
        type StripeSources {
          object: String
          data: [StripeSource]
          url: String
        }
        type StripeUser {
         id: String
         email: String
         default_source: String
         object: String
         sources: StripeSources
        }
        extend type Query {
          getStripeUser(stripeId: String): StripeUser
        }
      `,
      resolvers: {
        Query: {
          getStripeUser: async (obj, args, context, info) => {
            return getCustomerByStripeId(args.stripeId).then(
              res => {return res}
            );
          }
        }
      }
    };
  });


  export const GetStripeUserByEmail = makeExtendSchemaPlugin(build => {
    return {
      typeDefs: gql`
        extend type Query {
          getStripeUserByEmail(email: String): StripeUser
        }
      `,
      resolvers: {
        Query: {
          getStripeUserByEmail: async (obj, args, context, info) => {
            return isBuyerCustomer(args.email).then(
              res => {return res}
            );
          }
        }
      }
    };
  });

  export const SaveCardForFutureUse = makeExtendSchemaPlugin(build => {
    return {
      typeDefs: gql`
        input StripeSourceInput{
          id: String
          number: String,
          exp_month: String,
          exp_year: String,
          cvc: String,
        }
        input StripeUserInput{
          id: String
        }
        extend type Query {
          saveCardForFutureUse(card:StripeSourceInput, user: StripeUserInput): StripeSource
        }
      `,
      resolvers: {
        Query: {
          saveCardForFutureUse: async (obj, args, context, info) => {
            return saveCardForPhunnelUsage(args.card, args.user).then(
              res => {return res}
            );
          }
        }
      }
    };
  });

  export const GetStripeToken = makeExtendSchemaPlugin(build => {
    return {
      typeDefs: gql`
        extend type Query {
          getStripeToken(token:String, customer:String): StripeSource
        }
      `,
      resolvers: {
        Query: {
          getStripeToken: async (obj, args, context, info) => {
            return retrieveStripeCustomerCard(args.customer, args.token).then(
              res => {return res}
            );
          }
        }
      }
    };
  });

export const CreateStripeAuthCharge = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      type StripePaymentIntent {
        id: String
      }
      input StripePaymentIntentInput {
        id: String!
      }
      extend type Query {
        createStripeAuthCharge(
          # This is in cents, because Stripe charges in cents
          amount: Int!,
          paymentMethod: String!,
          stripeID: String
        ): StripePaymentIntent
      }
    `,
    resolvers: {
      Query: {
        createStripeAuthCharge: async (obj, args, context, info) => {
          return createStripeAuthCharge(args.amount, args.paymentMethod, args.stripeID).then(
            res => {return res}
          );
        }
      }
    }
  };
});

export const FinalizeStripeAuthCharge = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      extend type Query {
        finalizeStripeAuthCharge(
          paymentIntent: StripePaymentIntentInput!
        ): StripePaymentIntent
      }
    `,
    resolvers: {
      Query: {
        finalizeStripeAuthCharge: async (obj, args, context, info) => {
          return finalizeStripeAuthCharge(args.paymentIntent).then(
            res => {return res}
          );
        }
      }
    }
  };
});