const dotenv = require('dotenv');
dotenv.config();
const schedule = require('node-schedule');
const venueTimeZone = require('./venueTimeZoneSync');
schedule.scheduleJob('*/5 * * * *', venueTimeZone.run);// for every 5 min call for getting venue timezone 
schedule.scheduleJob('*/15 * * * *', venueTimeZone.completedEvents);// for every 15 min call for change events to completed status