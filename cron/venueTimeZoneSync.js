const venueTimeZone = require('../helpers/cron_event_helper');
const job = async () => {
    await venueTimeZone.updateVenueTimeZone();
}

const run = async () => {
    console.info(`${new Date()} ->  venue time zone update cron start `);
    try {
        await job();
        console.info(`${new Date()} -> venue time zone update cron end `);
    } catch (e) {
        console.info(`${new Date()} -> venue time zone update cron error => `, e);
    }
}

const jobEvents = async () => {
    await venueTimeZone.changeEventStatus();
}
const completedEvents = async () => {
    console.info(`${new Date()} ->  Completed events status update cron start `);
    try {
        await jobEvents();
        console.info(`${new Date()} -> Completed events status update cron end `);
    } catch (e) {
        console.info(`${new Date()} -> Completed events status update cron error => `, e);
    }
}

module.exports = {
    completedEvents,
    run
};
