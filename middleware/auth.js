const jwt = require('jsonwebtoken');

exports.verifyAuthToken = (req, res, next) => {
    try {
        
      if (req.header("authorization")) {
        req.user = jwt.verify(
          req.header("authorization").split(" ")[1],
          process.env.APP_JWT_KEY
        );
        next();
      }
      else {
        res.status(401).json({
          status: "failed",
          message: "Invalid Access"
        });
  
      }
    } catch (err) {
      res.status(401).json({
        status: "failed",
        message: "Invalid Access",
      });
    }
  };


exports.verifyOpenAPIAuthToken = (req, res, next) => {
  
    try {
      if (req.header("authorization")) {
        req.user = jwt.verify(
          req.header("authorization").split(" ")[1],
          process.env.APP_OPEN_JWT_KEY
        );
        next();
      }
      else {
        res.status(401).json({
          status: "failed",
          message: "Invalid Access",
          norefreshtoken:true
        });
  
      }
    } catch (err) {
      res.status(401).json({
        status: "failed",
        message: "Invalid Access",
        norefreshtoken:true
      });
    }

};

exports.verifyWebhookToken = (req, res, next) => {
  try {
    if (req.header("authorization") == process.env.EMAIL_PARSER_TOKEN) {
      next();
    }
    else {
      res.status(401).json({
        status: "failed",
        message: "Invalid Access", userrole
      });

    }
  } catch (err) {
    res.status(401).json({
      status: "failed",
      message: "Invalid Access",
    });
  }
};

