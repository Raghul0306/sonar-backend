'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    // create schema if it doesn't exist already
    await queryInterface.createSchema('ticket_exchange', { logging: true })
    await queryInterface.createSchema('ticket_exchange_private', { logging: true })


  },
  down: async (queryInterface, Sequelize) => {
  }
};