'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('user_account', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      user_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "user id stored in this field"
      },

      email: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "user email address stored in this field"
      },
      password_hash: {
        allowNull: false, 
        type: Sequelize.TEXT,
        comment: "user password stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the user account field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the user account field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange_private'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('user_account');
  }
};