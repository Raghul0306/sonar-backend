'use strict';
const table = { schema: 'ticket_exchange', tableName: 'user' };

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(table, 'verification_token', {
        type: Sequelize.STRING,
        allowNull: true,
        after: "active"
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([queryInterface.removeColumn(table, 'verification_token')]);
  }
};
