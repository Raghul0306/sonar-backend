'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('genre', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      name: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "genre name are stored in this field"
      },

      parent_category: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "parent_category are stored in this field"
      },

      child_category: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "child_category are stored in this field"
      },

      tevo_id: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "tevo_id are stored in this field"
      },

      mercury_id: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "mercury_id are stored in this field"
      },
      
      full_name:{
        type: Sequelize.STRING,
        comment: "Full name of the sub/child category in this field"
      },

      image: {
        type: Sequelize.TEXT, 
        allowNull: true, 
        comment: "image path will be stored in this field"
      },
      
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the genre field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the gentre field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('genre');
  }
};
