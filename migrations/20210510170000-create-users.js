'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('user', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      first_name: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "user first name are stored in this field"
      },
      last_name: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "user last name are stored in this field"
      },
      phonecode: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'countries',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "user phone number country id are stored in this field"
      },
      phone: {
        type: Sequelize.BIGINT,
        allowNull: true,
        comment: "user phone number are stored in this field"
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "user email address are stored in this field"
      },

      user_role: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user_roles',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "user role id are stored in this field"
      },

      mercuryid: {
        allowNull: true, 
        type: Sequelize.STRING,
        unique: true,
        comment: "User mercury id are stored in this field"
      },
      stripe_id: {
        allowNull: true, 
        type: Sequelize.STRING,
        unique: true,
        comment: "User stripe id are stored in this field"
      },
      last_login: {
        allowNull: true, 
        type: Sequelize.DATE,
        comment: "user last login time is stored in this field"
      },

      company_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'companies',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "company id are stored in this field"
      },
      parent_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        comment: "User parent id are stored in this field"
      },
      street_address_1: {
        allowNull: true, 
        type: Sequelize.TEXT,
        comment: "user street address are stored in this field"
      },
      street_address_2: {
        allowNull: true, 
        type: Sequelize.TEXT,
        comment: "user street address2 are stored in this field"
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "user city are stored in this field"
      },
      state: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "user state are stored in this field"
      },
      zip_code: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "user zip code are stored in this field"
      },
      country: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "user country are stored in this field"
      },


      needsToChangePassword: {
        allowNull: true, 
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: true,
        comment: "user change password status are stored in this field"
      },
      needsToAcceptTerms: {
        allowNull: true, 
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: true,
        comment: "user terms & condtions status stored in this field"
      },
      needsToAcceptPrivacy: {
        allowNull: true, 
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: true,
        comment: "user privacy policy status stored in this field"
      },
      needsToAcceptSeller: {
        allowNull: true, 
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: true,
        comment: "user seller agreement status stored in this field"
      },
      needsToSyncBank: {
        allowNull: true, 
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: true,
        comment: "user bank account sync status stored in this field"
      },
      
      notes: {
        allowNull: true, 
        type: Sequelize.TEXT,
        comment: "user notes are stored in this field"
      },

      verification_token: {
        type: Sequelize.STRING,
        allowNull: true,
        comment: "verification token are stored in this field"
      },

      active: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        defaultValue: 1,
        comment: "user active status stored in this field"
      },


      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the user field is created the timestamp is stored in this field"
      },
      created_by: {
        type: Sequelize.INTEGER,
        allowNull: false,
        comment: "The user who made the create"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the user field is updated the timestamp is stored in this field"
      },
      updated_by: {
        type: Sequelize.INTEGER,
        allowNull: true,
        comment: "The user who made the update"
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('user');
  }
};