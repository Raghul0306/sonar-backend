'use strict';
const table = { schema: 'ticket_exchange', tableName: 'purchase_order' };

module.exports = {
  up: (queryInterface, Sequelize) => {
      return Promise.all([
          queryInterface.changeColumn(table, 'purchase_date', {
              type: Sequelize.DATEONLY,
              allowNull: false,
          })
      ])
  },

  down: (queryInterface, Sequelize) => {
      return Promise.all([
          queryInterface.changeColumn(table, 'purchase_date', {
              type: Sequelize.DATE,
              allowNull: false,
          })
      ])
  }
};