'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('event', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      name: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "event name are stored in this field"
      },

      category_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'category',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "category id stored in this field"
      },

      performer_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'performer',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "performer id stored in this field"
      },


      opponent_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'performer',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "performer id stored in this field"
      },


      venue_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'venue',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "venue id stored in this field"
      },
      

      date: {
        type: Sequelize.DATE,
        allowNull: false, 
        defaultValue: Sequelize.NOW,
        comment: "Event date  is stored in this field"
      },

      tevo_id: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        comment: "ticket evolution event id  is stored in this field"
      },

      mercury_id: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        comment: "mercury event id is stored in this field"
      },

      popularity_score: {
        allowNull: true, 
        type: Sequelize.FLOAT,
        comment: "ticket evolution popularity score  is stored in this field"
      },

      eventdate_day: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "event date day is stored in this field"
      },

      tickettype_id: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        comment: "event ticket type id is stored in this field"
      },

      genre_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'genre',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "genre id stored in this field"
      },

      disclosures: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "disclosures are stored in this field"
      },

      notes: {
        allowNull: true, 
        type: Sequelize.TEXT,
        comment: "notes are stored in this field"
      },

      event_status_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'event_status',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "Event status is stored in this field"
      },

      ticket_system_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'ticketing_systems',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "Ticket system are stored in this field"
      },

      event_link:{
        type: Sequelize.TEXT,
        allowNull: true,
        comment: "Event link url are stored in this field"
      },

      image: {
        type: Sequelize.TEXT, 
        allowNull: true, 
        comment: "image path will be stored in this field"
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the event field is created the timestamp is stored in this field"
      },

      created_by: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "user id stored in this field"
      },
      
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the event field is updated the timestamp is stored in this field"
      }

    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('event');
  }
};