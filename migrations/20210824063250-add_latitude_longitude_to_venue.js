'use strict';
const table = { schema: 'ticket_exchange', tableName: 'venue' };

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    return Promise.all([
      queryInterface.addColumn(
        table,
        'latitude',
        {
          allowNull: true,
          type: Sequelize.FLOAT,
          comment: "venue latitude is stored in this field"
        }
      ),
      queryInterface.addColumn(
        table,
        'longitude',
        {
          allowNull: true,
          type: Sequelize.FLOAT,
          comment: "venue longitude is stored in this field"
        }
      ),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(table, 'latitude'),
      queryInterface.removeColumn(table, 'longitude')
    ]);
  }
};
