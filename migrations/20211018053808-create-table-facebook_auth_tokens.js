'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('facebook_auth_tokens', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      buyer_user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'buyerUsers',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "Buyer User Id stored in this field"
      },
      facebook_id: {
        type: Sequelize.TEXT,
        allowNull: false,
        comment: "Facebook account id"
      },
      access_token: {
        type: Sequelize.TEXT,
        allowNull: false,
        comment: "Facebook sign in access token"
      },
      revoked: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
        comment: "Token revoked status"
      },
      scope: {
        type: Sequelize.TEXT,
        allowNull: false,
        comment: "Facebook sign in scope"
      },
      expires_in:{
        type: Sequelize.INTEGER,
        allowNull: false,
        comment: "Token expires in"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "The date and time token data instered"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "The date and time token data updated"
      },
    }, {
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('facebook_auth_tokens');
  }
};
