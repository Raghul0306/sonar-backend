'use strict';
const table = { schema: 'ticket_exchange', tableName: 'channels' };
module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     *           
     */
    return Promise.all([
      queryInterface.addColumn(table, 'active', {
        type: Sequelize.BOOLEAN,
        after: "logo_url",
        defaultValue: true,
        comment: "channels will show based on active status"
      },
      )
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return Promise.all([queryInterface.removeColumn(table, 'active')]);
  }
};
