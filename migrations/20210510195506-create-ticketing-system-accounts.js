'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ticketing_system_accounts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ticketing_system_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'ticketing_systems',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "country id stored in this field"
      },

      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "user id stored in this field"
      },

      parent_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "parent id stored in this field"
      },

      account_name: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "Account name is stored in this field"
      },

      active: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        defaultValue: 1,
        comment: "Account active status are stored in this field"
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the fees field is created the timestamp is stored in this field"
      },
      
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the fees field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ticketing_system_accounts');
  }
};
