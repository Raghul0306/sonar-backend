'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('disclosures', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      disclosure_name: {
        allowNull: false, 
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT,
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        }
      },
      description: {
        type: Sequelize.TEXT,
        after: "disclosure_name",
        allowNull: true
      },
      active: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    },{
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('disclosures');
  }
};
