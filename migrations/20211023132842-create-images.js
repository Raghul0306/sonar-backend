'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('images', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "name is stored in this field"
      },
      category_id: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'category',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "category id is stored in this field"
      }, 
      genre_id: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'genre',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "genre id is stored in this field"
      },
      performer_id: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'performer',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "performer id is stored in this field"
      },
      event_id: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'event',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "event id is stored in this field"
      },
      image_url: {
        allowNull: false, 
        type: Sequelize.TEXT,
        comment: "image url is stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "create date and time is stored in this field"
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "created person id is stored in this field"
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "update date and time is stored in this field"
      }, 
      updated_by: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "updated person id is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('images');
  }
};
