'use strict';
const table = { schema: 'ticket_exchange', tableName: 'listing' };
module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     *           
     */
    return Promise.all([
      queryInterface.addColumn(table, 'listing_status_id', {
        type: Sequelize.INTEGER,
        after: "active",
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'listing_status',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        defaultValue: 1,
        comment: "listing status id is stored in this field"
      },
      )
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return Promise.all([queryInterface.removeColumn(table, 'listing_status_id')]);
  }
};
