'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('buyerUsers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      first_name: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "customer first name is stored in this field"
      },

      last_name: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "customer last name is stored in this field"
      },

      email: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "customer email is stored in this field"
      },

      phone_number: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "customer phone number is stored in this field"
      },

      user_role_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user_roles',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "customer user role id is stored in this field"
      },

      stripeID: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        comment: "customer user stripe id is stored in this field"
      },

      promo_code_used: {
        allowNull: true, 
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: false,
        comment: "promo code status are stored in this field"
      },

      address1: {
        allowNull: false,
        type: Sequelize.TEXT,
        comment: "customer address1 is stored in this field"
      },

      address2: {
        allowNull: true, 
        type: Sequelize.TEXT,
        comment: "customer address2 is stored in this field"
      },

      city: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "customer city is stored in this field"
      },

      state: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "customer state is stored in this field"
      },

      country: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "customer county is stored in this field"
      },

      zip_code: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "customer zip code is stored in this field"
      },

      notes: {
        allowNull: true, 
        type: Sequelize.TEXT,
        comment: "customer internal notes are stored in this field"
      },
      password_hash: {
        allowNull: true, 
        type: Sequelize.TEXT,
        comment: "user password stored in this field"
      },
      token: {
        type: Sequelize.STRING,
        allowNull: true
      },
      channel_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'channels',
            schema: 'ticket_exchange'
          },
          key: 'id'
        }
      },
      phone_number: {
        type: Sequelize.TEXT,
        allowNull: true,
        comment: "customer phone number is stored in this field"
      },
      is_google_login: {
        allowNull: true, 
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: false,
        comment: "User register with Google"
      },
      is_facebook_login: {
        allowNull: true, 
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: false,
        comment: "User register with Facebook"
      },
      is_email_login: {
        allowNull: true, 
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: false,
        comment: "User register with Email"
      },
      facebook_id: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "User FacbookId"
      },
      google_id: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "User GoogleId"
      },
      is_user_registered: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        comment: "User registered or not registered details"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the buyer user field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the buyer user field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('buyerUsers');
  }
};