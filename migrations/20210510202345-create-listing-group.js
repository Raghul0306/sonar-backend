'use strict'; 
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('listing_group', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      seller_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "user id stored in this field"
      },

      event_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'event',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "event id stored in this field"
      },

      in_hand: {
        allowNull: true, 
        type: Sequelize.DATE,
        comment: "In-hand date are stored in this field"
      },
      
      ticket_type_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'ticket_types',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "ticket_type id stored in this field"
      },

      splits_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'splits',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "splits id stored in this field"
      },

      seat_type_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'seat_type',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "seat_type id stored in this field"
      },

      ticket_system_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'ticketing_systems',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "Ticket system are stored in this field"
      },

      ticket_system_account: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'ticketing_system_accounts',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "Ticket system account id are stored in this field"
      },

      hide_seat_numbers: {
        allowNull: true, 
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        comment: "hide seat number are stored in this field"
      },

      section: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "section are stored in this field"
      },

      row: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "row are stored in this field"
      },

      quantity: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        defaultValue: 0,
        comment: "quantity are stored in this field"
      },

      seat_start: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "seat start are stored in this field"
      },

      seat_end: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "seat end are stored in this field"
      },

      face_value: {
        allowNull: false, 
        type: Sequelize.FLOAT,
        comment: "face value are stored in this field"
      },

      unit_cost: {
        allowNull: false, 
        type: Sequelize.FLOAT,
        comment: "Unit cost are stored in this field"
      },

      group_cost: {
        allowNull: false, 
        type: Sequelize.FLOAT,
        comment: "group cost are stored in this field"
      },

      group_price: {
        allowNull: true, 
        type: Sequelize.FLOAT,
        comment: "group price are stored in this field"
      },

      last_price: {
        allowNull: true, 
        type: Sequelize.FLOAT,
        comment: "last price are stored in this field"
      },

      last_price_change: {
        allowNull: true, 
        type: Sequelize.DATE,
        comment: "last price change date are stored in this field"
      },

      PO_ID: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "Purchase Order ID are stored in this field"
      },

      internal_notes: {
        allowNull: true, 
        type: Sequelize.TEXT,
        comment: "internal notes  are stored in this field"
      },

      external_notes: {
        allowNull: true, 
        type: Sequelize.TEXT,
        comment: "external notes  are stored in this field"
      },

      zone_seating: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "zone seating are stored in this field"
      },

      is_attachments: {
        allowNull: true, 
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        comment: "attachments available status are stored in this field"
      },

      tevoid: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        comment: "tevo id are stored in this field"
      },

      mercuryid: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        comment: "mercury id are stored in this field"
      },

      delivery_type: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "delivery type are stored in this field"
      },

      tags: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "tags are stored in this field"
      },

      inactive: {
        allowNull: false, 
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        comment: "inactive are stored in this field"
      },
      
      is_removed: {
        allowNull: false, 
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        comment: "is_removed are stored in this field"
      },

      qc_hold: {
        allowNull: true, 
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        comment: "qc_hold are stored in this field"
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the listing field is created the timestamp is stored in this field"
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER, 
        defaultValue: 0
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the listing field is updated the timestamp is stored in this field"
      }, 
      updated_by: {
        allowNull: true, 
        type: Sequelize.INTEGER, 
        defaultValue: 0
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('listing_group');
  }
};
