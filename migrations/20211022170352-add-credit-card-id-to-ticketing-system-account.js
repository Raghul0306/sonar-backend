'use strict';
const table = { schema: 'ticket_exchange', tableName: 'ticketing_system_accounts' };
module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn(
      table,
      'credit_card_id',
      {
        type: Sequelize.INTEGER,
        after: "ticketing_system_id",
        allowNull: true,
        onDelete: 'cascade',
        references: {
        model: {
        tableName: 'credit_card',
        schema: 'ticket_exchange'
        },
        key: 'id'
        },
        comment: "credit card id are stored in this field"
      }
    )
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeColumn(table, 'credit_card_id')
  }
};
