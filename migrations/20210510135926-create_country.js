'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    await queryInterface.createTable('countries', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      abbreviation: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "country abbreviation are stored in this field"
      },

      name: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "country name are stored in this field"
      },

      phonecode: {
        type: Sequelize.STRING,
        allowNull: true,
        comment: "country phone number code are stored in this field"
      },

      sorting: {
        type: Sequelize.INTEGER,
        allowNull: true, 
        defaultValue: 0,
        comment: "country sorting value are stored in this field"
      },

      active: {
        type: Sequelize.INTEGER,
        allowNull: false, 
        defaultValue: 1,
        comment: "country active status are stored in this field"
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        comment: "When the country field is created the timestamp is stored in this field"

      },
      
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        comment: "When the country field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('countries');
  }
};
