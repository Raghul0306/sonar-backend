'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('seat_type', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "seat type name is stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the seat type field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the seat type field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('seat_type');
  }
};