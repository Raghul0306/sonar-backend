'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('venue', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      name: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "venue name is stored in this field"
      },

      address: {
        type: Sequelize.TEXT,
        allowNull: false,
        comment: "venue street is stored in this field"
      },

      address2: {
        type: Sequelize.TEXT,
        allowNull: true,
        comment: "venue street2 is stored in this field"
      },

      city: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "venue city is stored in this field"
      },

      zip_code: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "venue zip code are stored in this field"
      },

      state: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "venue state are stored in this field"
      },

      country: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "venue country is stored in this field"
      },

      default_category: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        comment: "default_category is stored in this field"
      },

      tevoid: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        comment: "tevoid is stored in this field"
      },

      mercuryid: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        comment: "merucryid is stored in this field"
      },

      popularity_score: {
        allowNull: true, 
        type: Sequelize.FLOAT,
        comment: "venue popularity is stored in this field"
      },

      image: {
        type: Sequelize.TEXT, 
        allowNull: true, 
        comment: "image path will be stored in this field"
      },

      latitude: {
        allowNull: true,
        type: Sequelize.FLOAT,
        comment: "venue latitude is stored in this field"
      },

      longitude: {
        allowNull: true,
        type: Sequelize.FLOAT,
        comment: "venue longitude is stored in this field"
      },

      timezone: {
        allowNull: true,
        type: Sequelize.STRING,
        comment: "Time zone based on city state"
      },
      
      
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the venue field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the venue field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('venue');
  }
};