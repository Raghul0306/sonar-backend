'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('fees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      amount: {
        allowNull: false, 
        type: Sequelize.FLOAT,
        comment: "amount is stored in this field"
      },
      postive: {
        allowNull: false, 
        type: Sequelize.BOOLEAN,
        comment: "postive is stored in this field"
      },
      description: {
        allowNull: true, 
        type: Sequelize.TEXT,
        comment: "description is stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the fees field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the fees field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('fees');
  }
};