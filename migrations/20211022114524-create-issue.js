'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('issues', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      issue_type_id: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'issue_types',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "issue types id is stored in this field"
      }, 
      assigner: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "assigner id is stored in this field"
      },
      listing_group_id: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'listing_group',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "listing group id is stored in this field"
      },
      event_id: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'event',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "event id is stored in this field"
      },
      assignee: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "assignee id is stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "create date and time is stored in this field"
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "created person id is stored in this field"
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "update date and time is stored in this field"
      }, 
      updated_by: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "updated person id is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('issues');
  }
};
