'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('favorites', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      favorite_type: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      buyer_user_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'buyerUsers',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "the unique MyTickets user_id"
      },
      event_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'event',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "unique event id"
      },
      listing_group_id: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'listing_group',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "listing_group id"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "the date and time the order as created"
      },
      created_by: {
        allowNull: true,
        type: Sequelize.INTEGER,
        comment: "who created the order"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "the date and time the order was last updated"
      },
      updated_by: {
        allowNull: true,
        type: Sequelize.INTEGER,
        comment: "the user who made the update"
      }
    }, {
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('favorites');
  }
};
