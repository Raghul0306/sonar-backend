'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ticketing_systems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ticketing_system_name: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "ticket system name is stored in this field"
      },
      country_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'countries',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "country id stored in this field"
      },
      url:{
        type: Sequelize.STRING,
        allowNull: true,
        comment: "ticket system URL"
      },
      active: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        defaultValue: 1,
        comment: "ticket system active status are stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the fees field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the fees field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ticketing_systems');
  }
};
