'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('payout', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "user id stored in this field"
      }, 
      transaction_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'transaction',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "transaction id stored in this field"
      },
      order_status_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'order_status',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "order status id stored in this field"
      },
      cancelled_order_type_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'cancelled_order_type',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "cancelled order type id stored in this field"
      },
      payout_date: {
        allowNull: false,
        type: Sequelize.DATE
      }, 
      payout_amount: {
        allowNull: false,
        type: Sequelize.FLOAT
      }, 
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER, 
        defaultValue: 0
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      }, 
      updated_by: {
        allowNull: true, 
        type: Sequelize.INTEGER, 
        defaultValue: 0
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('payout');
  }
};