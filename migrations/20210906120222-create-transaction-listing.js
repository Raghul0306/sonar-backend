'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('transaction_listing', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      transaction_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'transaction',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "transaction id stored in this field"
      },
      listing_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'listing',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "listing id stored in this field"
      }
    }, {
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('transaction_listing');
  }
};