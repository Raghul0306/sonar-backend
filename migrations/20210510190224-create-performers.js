'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('performer', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      
      name: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "performer name stored in this field"
      },
      abv:{
        type: Sequelize.STRING(4),
        comment: "A shorthand reference to team name"
      },
      category_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'category',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "category id stored in this field"
      },

      tevoid: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        comment: "tevo id stored in this field"
      },

      mercuryid: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        comment: "mercury id stored in this field"
      },

      popularity_score: {
        allowNull: true, 
        type: Sequelize.FLOAT,
        comment: "popularity score id stored in this field"
      },

      genre_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'genre',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "genre id is stored in this field"
      },

      league_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
        model: {
        tableName: 'leagues',
        schema: 'ticket_exchange'
        },
        key: 'id'
        },
        comment: "league id are stored in this field"
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the performer field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the performer field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('performer');
  }
};
