'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('vendor', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        allowNull: false, 
        type: Sequelize.STRING
      },
      address: {
        allowNull: true, 
        type: Sequelize.TEXT
      }, 
      address2: {
        allowNull: true, 
        type: Sequelize.TEXT
      }, 
      zip: {
        allowNull: true, 
        type: Sequelize.STRING
      }, 
      city: {
        allowNull: true, 
        type: Sequelize.STRING
      }, 
      state: {
        allowNull: true, 
        type: Sequelize.STRING
      }, 
      country: {
        allowNull: true, 
        type: Sequelize.STRING
      }, 
      country_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        // onDelete: 'cascade',
        // references: {
        //   model: {
        //     tableName: 'countries',
        //     schema: 'ticket_exchange'
        //   },
        //   key: 'id'
        // },
        comment: "country id stored in this field"
      },
      email: {
        allowNull: true, 
        type: Sequelize.STRING
      }, 
      phone: {
        allowNull: true, 
        type: Sequelize.STRING
      }, 
      url:{
        type: Sequelize.STRING,
        comment: "URL"
      },
      notes: {
        allowNull: true, 
        type: Sequelize.TEXT
      }, 
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        defaultValue: 0
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      }, 
      updated_by: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        defaultValue: 0
      }
    }, {
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('vendor');
  }
};
