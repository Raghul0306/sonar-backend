'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ticketing_system_user_login', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      buyer_user_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'buyerUsers',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "buyer user id stored in this field"
      },
      ticketing_system_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'ticketing_systems',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "ticketing system id stored in this field"
      },
      user_name: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "user email address are stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    }, {
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ticketing_system_user_login');
  }
};
