'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    await queryInterface.createTable('cities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      name: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "city name are stored in this field"
      },

      country_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'countries',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "country id are stored in this field"
      },

      state_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'states',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "state id are stored in this field"
      },

      active: {
        type: Sequelize.INTEGER,
        allowNull: false, 
        defaultValue: 1,
        comment: "city active status are stored in this field"
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        comment: "When the city field is created the timestamp is stored in this field"

      },

      created_by: {
        type: Sequelize.INTEGER,
        allowNull: false,
        comment: "The user who made the create"
      },
      
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        comment: "When the city field is updated the timestamp is stored in this field"
      },

      updated_by: {
        type: Sequelize.INTEGER,
        allowNull: true,
        comment: "The user who made the update"
      }
    },{
      schema: 'ticket_exchange'
    });

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('cities');
  }
};
