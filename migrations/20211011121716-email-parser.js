'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('email_parser', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ticketing_system_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'ticketing_systems',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "ticketing system id stored in this field"
      },
      message_id:{
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      parsed_data:{
        type: Sequelize.TEXT,
        allowNull: false,
        unique: true
      },
      sync_status:{
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      sync_at:{
        type: Sequelize.DATE,
        allowNull: true,
      },
      no_of_attempts :{
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      status:{
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      message:{
        type: Sequelize.TEXT,
        allowNull: true,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      }
    },{
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('email_parser');
  }
};
