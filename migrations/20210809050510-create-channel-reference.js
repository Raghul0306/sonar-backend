'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('channel_reference', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      channel_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'channels',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "channel id stored in this field"
      },
      external_channel_reference_id: {
        type: Sequelize.STRING, 
        allowNull: false, 
        comment: "external channel reference id is stored in this field"
      }, 
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER, 
        defaultValue: 0
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      }, 
      updated_by: {
        allowNull: true, 
        type: Sequelize.INTEGER, 
        defaultValue: 0
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('channel_reference');
  }
};