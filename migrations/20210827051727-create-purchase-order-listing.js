'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('purchase_order_listing', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      purchase_order_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'purchase_order',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "purchase order id stored in this field"
      },
      listing_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'listing',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "listing id stored in this field"
      }
    }, {
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('purchase_order_listing');
  }
};