'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('event_tags', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      event_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'event',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "Event id is stored in this field"
      },
      tag_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'tags',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "Tag id is stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the fees field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the fees field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('event_tags');
  }
};
