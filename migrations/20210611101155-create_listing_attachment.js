'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('listing_attachments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },     

      listing_group_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'listing_group',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "listing group id stored in this field"
      },

      attachment_name: {
        type: Sequelize.STRING,
        allowNull: true,
        comment: "listing  attachment name is stored in this field"
      },

      attachment_url: {
        type: Sequelize.TEXT,
        allowNull: false,
        comment: "listing  attachment url is stored in this field"
      },
      
      active: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        defaultValue: 1,
        comment: "markup active status are stored in this field"
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        comment: "When the channel markup types field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        comment: "When the channel markup types field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('listing_attachments');
  }
};
