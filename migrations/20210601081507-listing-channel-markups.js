'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('listing_channel_markups', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      listing_group_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'listing_group',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "listing group id stored in this field"
      },

      user_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "user id stored in this field"
      },

      channel_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'channels',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "channel id stored in this field"
      },

      channel_markup_type_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'channel_markup_types',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "channel markup type id stored in this field"
      },

      markup_amount: {
        type: Sequelize.INTEGER,
        allowNull: true,
        comment: "markup amount is stored in this field"
      },
      
      markup_active: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        defaultValue: 1,
        comment: "markup active status are stored in this field"
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the channel markup types field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the channel markup types field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('listing_channel_markups');
  }
};
