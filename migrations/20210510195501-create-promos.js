'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('promos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      name: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "promos name are stored in this field"
      },

      amount: {
        allowNull: false, 
        type: Sequelize.FLOAT,
        comment: "amount are stored in this field"
      },

      postive: {
        allowNull: false, 
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        comment: "postive are stored in this field"
      },

      description: {
        allowNull: true, 
        type: Sequelize.TEXT,
        comment: "description are stored in this field"
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the promo field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the promo field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('promos');
  }
};