'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('purchase_order_invoice_attachment', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      }, 
      purchase_order_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'purchase_order',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "purchase order id stored in this field"
      },
      attachment_name: {
        type: Sequelize.STRING,
        allowNull: true,
        comment: "attachment name is stored in this field"
      },
      attachment_url: {
        type: Sequelize.TEXT,
        allowNull: false,
        comment: "attachment url is stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE, 
        defaultValue: Sequelize.NOW,
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER, 
        defaultValue: 0
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE, 
        defaultValue: Sequelize.NOW,
      }, 
      updated_by: {
        allowNull: true, 
        type: Sequelize.INTEGER, 
        defaultValue: 0
      }
    },{
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('purchase_order_invoice_attachment');
  }
};
