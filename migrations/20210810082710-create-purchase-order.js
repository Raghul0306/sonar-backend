'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('purchase_order', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "user id stored in this field"
      }, 
      vendor_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'vendor',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "vendor id stored in this field"
      },
      purchase_date: {
        type: Sequelize.DATEONLY,
        allowNull: false,
        comment: "purchase order date stored in this field"
      }, 
      purchase_amount: {
        allowNull: false, 
        type: Sequelize.FLOAT,
        comment: "purchase order amount stored in this field"
      }, 
      currency_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'currency',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "currency id stored in this field"
      }, 
      listing_group_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'listing_group',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "listing group id stored in this field"
      },
      payment_method_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'payment_method',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "payment method id stored in this field"
      },
      payment_type_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'payment_type',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "payment type id stored in this field"
      },
      payment_last4_digits: {
        allowNull: true, 
        type: Sequelize.STRING
      }, 
      ticketing_system_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'ticketing_systems',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "ticketing system id stored in this field"
      },
      account_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'ticketing_system_accounts',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "ticketing system account id stored in this field"
      },
      broker_order_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
        model: {
        tableName: 'broker_order',
        schema: 'ticket_exchange'
        },
        key: 'id'
        },
        comment: "broker order id are stored in this field"
      },
      external_reference: {
        allowNull: true, 
        type: Sequelize.STRING
      }, 
      active: {
        allowNull: false, 
        type: Sequelize.BOOLEAN
      }, 
      sales_tax: {
        allowNull: true, 
        type: Sequelize.FLOAT
      }, 
      shipping_and_handling_fee: {
        allowNull: true, 
        type: Sequelize.FLOAT
      },
      fees: {
        allowNull: true, 
        type: Sequelize.FLOAT
      },
      notes: {
        allowNull: true, 
        type: Sequelize.TEXT
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        defaultValue: 0
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      }, 
      updated_by: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        defaultValue: 0
      }
    }, {
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('purchase_order');
  }
};