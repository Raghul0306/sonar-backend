'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('event_status', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      event_status: {
        type: Sequelize.STRING,
        allowNull: false
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER, 
        defaultValue: 0
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      }, 
      updated_by: {
        allowNull: true, 
        type: Sequelize.INTEGER, 
        defaultValue: 0
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('event_status');
  }
};