'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('company_user', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      company_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'companies',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "company id are stored in this field"
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "user id are stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the user field is created the timestamp is stored in this field"
      },
      created_by: {
        type: Sequelize.INTEGER,
        allowNull: false,
        comment: "The user who made the create"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the user field is updated the timestamp is stored in this field"
      },
      updated_by: {
        type: Sequelize.INTEGER,
        allowNull: true,
        comment: "The user who made the update"
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('company_user');
  }
};