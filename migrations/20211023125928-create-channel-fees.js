'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('channel_fees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "user id is stored in this field"
      }, 
      channel_id: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'channels',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "channel id is stored in this field"
      },
      fee: {
        allowNull: false, 
        type: Sequelize.FLOAT,
        comment: "fees is stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "create date and time is stored in this field"
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "created person id is stored in this field"
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "update date and time is stored in this field"
      }, 
      updated_by: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "updated person id is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('channel_fees');
  }
};
