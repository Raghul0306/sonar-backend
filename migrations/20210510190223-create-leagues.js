'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('leagues', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        after: "name",
        allowNull: true,
        comment: "league name is stored in this field"
      },
      category_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'category',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "category id stored in this field"
      },
      country_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'countries',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "country id stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "create date and time is stored in this field"
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "created person id is stored in this field"
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "update date and time is stored in this field"
      }, 
      updated_by: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "updated person id is stored in this field"
      }
    }, {
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('leagues');
  }
};
