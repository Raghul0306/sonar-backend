'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('listing', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      listing_group_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'listing_group',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "listing group id stored in this field"
      },

      buyer_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "buyer user id stored in this field"
      },

      in_hand: {
        allowNull: true, 
        type: Sequelize.DATE,
        comment: "In-hand date are stored in this field"
      },

      seat_number: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "seat number are stored in this field"
      },

      price: {
        allowNull: true, 
        type: Sequelize.FLOAT,
        comment: "price are stored in this field"
      },

      unit_cost: {
        allowNull: false, 
        type: Sequelize.FLOAT,
        comment: "cost are stored in this field"
      }, 

      sold: {
        allowNull: true, 
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        comment: "sold are stored in this field"
      },

      face_value: {
        allowNull: false, 
        type: Sequelize.FLOAT,
        comment: "face value are stored in this field"
      },

      delivered: {
        allowNull: true, 
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        comment: "delivered status are stored in this field"
      },

      inactive: {
        allowNull: true, 
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        comment: "inactive are stored in this field"
      },

      hold: {
        allowNull: true, 
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        comment: "hold are stored in this field"
      },

      pdf: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "pdf are stored in this field"
      },

      last_price: {
        allowNull: true, 
        type: Sequelize.FLOAT,
        comment: "last price are stored in this field"
      },

      last_price_change: {
        allowNull: true, 
        type: Sequelize.DATE,
        comment: "last price change date are stored in this field"
      },

      data_type: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'ticket_types',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "Ticket Type id stored in this field"
      },

      active: {
        allowNull: true, 
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        comment: "listing active status  are stored in this field"
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the listing field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the listing field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('listing');
  }
};