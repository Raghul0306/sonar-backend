'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ticket_types', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "ticket type name is stored in this field"
      },
      delivery_type_description: {
        type: Sequelize.TEXT,
        allowNull: true,
        comment: "Ticket delivery type description"
      },
      alternative_name: {
        type: Sequelize.TEXT,
        allowNull: true,
        comment: "Ticket type alternative name"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the ticket type field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the ticket type field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ticket_types');
  }
};