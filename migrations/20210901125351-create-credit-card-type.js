'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('credit_card_type', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      type_name: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "name of the credit card types"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "the date and time the order as created"
      },
      created_by: {
        allowNull: true,
        type: Sequelize.INTEGER,
        comment: "who created the order"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "the date and time the order was last updated"
      },
      updated_by: {
        allowNull: true,
        type: Sequelize.INTEGER,
        comment: "the user who made the update"
      }
    }, {
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('credit_card_type');
  }
};
