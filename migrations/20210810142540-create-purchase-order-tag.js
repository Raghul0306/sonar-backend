'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('purchase_order_tag', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      purchase_order_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'purchase_order',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "Purchase order id is stored in this field"
      },
      tag_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'tags',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "Tag id is stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER,
        defaultValue: 0
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      }, 
      updated_by: {
        allowNull: true, 
        type: Sequelize.INTEGER,
        defaultValue: 0
      }
    }, {
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('purchase_order_tag');
  }
};