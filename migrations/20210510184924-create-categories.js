'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('category', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        allowNull: false, 
        type: Sequelize.STRING,
        comment: "category name is stored in this field"
      },
      tevoid: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "tevoid category name is stored in this field"
      },
      mercuryid: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "mercury category name  is stored in this field"
      },

      parent_category_id: {
        allowNull: false, 
        type: Sequelize.INTEGER
      },
      child_category_id: {
        allowNull: false, 
        type: Sequelize.INTEGER
      },
      grand_child_category_id: {
        allowNull: false, 
        type: Sequelize.INTEGER
      },
      image: {
        type: Sequelize.TEXT, 
        allowNull: true, 
        comment: "image path will be stored in this field"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "When the category field is created the timestamp is stored in this field"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "When the category field is updated the timestamp is stored in this field"
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('category');
  }
};