'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('cancelled_order_type', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        allowNull: false, 
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER, 
        defaultValue: 0
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      }, 
      updated_by: {
        allowNull: true, 
        type: Sequelize.INTEGER, 
        defaultValue: 0
      }
    }, {
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('cancelled_order_type');
  }
};