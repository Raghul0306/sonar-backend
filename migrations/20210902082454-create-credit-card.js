'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('credit_card', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,       
      },
      user_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
        comment: "the unique MyTickets user_id"
      },
      type_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'credit_card_type',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
      },
      last_4_digits: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      creditcard_name: {
        type: Sequelize.STRING,
        allowNull: false,
        comment: "the date and time the order as created"
      },

      address1: {
        type: Sequelize.TEXT,
        allowNull: true,
        comment: "company name is stored in this field"
      },
      address2: {
        type: Sequelize.TEXT,
        allowNull: true,
        comment: "company name is stored in this field"
      },
      city: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
        model: {
        tableName: 'cities',
        schema: 'ticket_exchange'
        },
        key: 'id'
        },
        comment: "company city id are stored in this field"
      },
      state: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
        model: {
        tableName: 'states',
        schema: 'ticket_exchange'
        },
        key: 'id'
        },
        comment: "company state id are stored in this field"
      },
      zip_code: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "company zip code are stored in this field"
      },
      country: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
        model: {
        tableName: 'countries',
        schema: 'ticket_exchange'
        },
        key: 'id'
        },
        comment: "company country id are stored in this field"
      },
      security_code: {
        allowNull: true, 
        type: Sequelize.STRING,
        comment: "security code are stored in this field"
      },
      ticketing_system_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
        model: {
        tableName: 'ticketing_systems',
        schema: 'ticket_exchange'
        },
        key: 'id'
        },
        comment: "ticketing system id are stored in this field"
      },
      ticketing_system_account_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
        model: {
        tableName: 'ticketing_systems',
        schema: 'ticket_exchange'
        },
        key: 'id'
        },
        comment: "ticketing system account id are stored in this field"
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        comment: "the date and time the order as created"
      },
      created_by: {
        allowNull: true,
        type: Sequelize.INTEGER,
        comment: "who created the order"
      },
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE,
        comment: "the date and time the order was last updated"
      },
      updated_by: {
        allowNull: true,
        type: Sequelize.INTEGER,
        comment: "the user who made the update"
      }
    }, {
      schema: 'ticket_exchange'
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     await queryInterface.dropTable('credit_card');
  }
};
