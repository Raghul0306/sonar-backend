'use strict';
const table = { schema: 'ticket_exchange', tableName: 'currency' };
module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn(
      table,
      'symbol',
      {
        type: Sequelize.STRING,
        after: "iso_code",
        allowNull: true,
        onDelete: 'cascade',
        comment: "currency symbol id are stored in this field"
      }
    )
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeColumn(table, 'symbol')
  }
};