'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('price_log', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      event_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'event',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
      }, 
      listing_group_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'listing_group',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
      }, 
      listing_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'listing',
            schema: 'ticket_exchange'
          },
          key: 'id'
        },
      }, 
      price: {
        allowNull: false, 
        type: Sequelize.FLOAT,
        comment: 'Price is stored in this field'
      }, 
      new_price: {
        allowNull: false, 
        type: Sequelize.FLOAT,
        comment: 'Updated Price is stored in this field'
      }, 
      confirmed_price_change: {
        allowNull: false, 
        type: Sequelize.BOOLEAN
      }, 
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      created_by: {
        allowNull: false, 
        type: Sequelize.INTEGER
      }, 
      updated_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    },{
      schema: 'ticket_exchange'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('price_log');
  }
};