'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'channel_reference', schema: 'ticket_exchange' },
    [{
      channel_id:1,
      external_channel_reference_id:1,
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date(),
      updated_by: 1
    },
    {
      channel_id:2,
      external_channel_reference_id:1,
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date(),
      updated_by: 1
    },
    {
      channel_id:3,
      external_channel_reference_id:1,
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date(),
      updated_by: 1
    },
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    
  }
};
