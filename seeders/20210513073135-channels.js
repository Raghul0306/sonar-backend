'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'channels', schema: 'ticket_exchange' }, [{
      channel_name:"Stubhub",
      logo_url:"s3://dev-mytickets/uploads/channels/1/stubhub.png",
      active: true,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      channel_name:"Viagogo",
      logo_url:"s3://dev-mytickets/uploads/channels/2/viagogo-logo.png",
      active: true,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      channel_name:"Seatgeek",
      logo_url:"s3://dev-mytickets/uploads/channels/3/seatgeek-logo.png",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      channel_name:"Vivid Seats",
      logo_url:"s3://dev-mytickets/uploads/channels/4/vividseats-logo.png",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      channel_name:"Gametime",
      logo_url:"s3://dev-mytickets/uploads/channels/5/gametime-logo.png",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      channel_name:"Ticket Network",
      logo_url:"s3://dev-mytickets/uploads/channels/6/ticketnetwork-logo.png",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },
    
    {
      channel_name:"Ticket Evolution",
      logo_url:"s3://dev-mytickets/uploads/channels/7/ticketevolution-logo.png",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    }, 

    {
      channel_name:"Yadara",
      active: true,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      channel_name:"Tixstock",
      logo_url:"s3://dev-mytickets/uploads/channels/9/tixstock-logo.png",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      channel_name:"Ticketmaster Resale",
      logo_url:"s3://dev-mytickets/uploads/channels/10/ticketmaster-logo.png",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      channel_name:"ViaSeat",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      channel_name:"Ticombo",
      logo_url:"s3://dev-mytickets/uploads/channels/12/ticombo-logo.png",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      channel_name:"Live Football Tickets",
      logo_url:"s3://dev-mytickets/uploads/channels/13/livefootballtickets-logo.png",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      channel_name:"Gigsberg",
      logo_url:"s3://dev-mytickets/uploads/channels/14/gigsberg-logo.jpg",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      channel_name:"Vibetickets",
      logo_url:"s3://dev-mytickets/uploads/channels/15/vibetickets-logo.png",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      channel_name:"Fanpass",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      channel_name:"Stubhub International",
      logo_url: "s3://dev-mytickets/uploads/channels/1/stubhub.png",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      channel_name:"Beeyay",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      channel_name:"Seatsnet",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      channel_name:"Biletwise",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      channel_name:"Ticketpad",
      active: false,
      created_at: new Date(),
      updated_at: new Date()
    },
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('channels', null, {});
  }
};
