'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert({ tableName: 'countries', schema: 'ticket_exchange' }, [
      {
          id: "1",
          name: "Afghanistan",
          abbreviation: "AF",
          phonecode: "93",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "2",
          name: "Aland Islands",
          abbreviation: "AX",
          phonecode: "+358-18",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "3",
          name: "Albania",
          abbreviation: "AL",
          phonecode: "355",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "4",
          name: "Algeria",
          abbreviation: "DZ",
          phonecode: "213",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "5",
          name: "American Samoa",
          abbreviation: "AS",
          phonecode: "+1-684",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "6",
          name: "Andorra",
          abbreviation: "AD",
          phonecode: "376",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "7",
          name: "Angola",
          abbreviation: "AO",
          phonecode: "244",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "8",
          name: "Anguilla",
          abbreviation: "AI",
          phonecode: "+1-264",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "9",
          name: "Antarctica",
          abbreviation: "AQ",
          phonecode: "672",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "10",
          name: "Antigua And Barbuda",
          abbreviation: "AG",
          phonecode: "+1-268",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "11",
          name: "Argentina",
          abbreviation: "AR",
          phonecode: "54",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "12",
          name: "Armenia",
          abbreviation: "AM",
          phonecode: "374",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "13",
          name: "Aruba",
          abbreviation: "AW",
          phonecode: "297",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "14",
          name: "Australia",
          abbreviation: "AU",
          phonecode: "61",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "15",
          name: "Austria",
          abbreviation: "AT",
          phonecode: "43",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "16",
          name: "Azerbaijan",
          abbreviation: "AZ",
          phonecode: "994",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "17",
          name: "Bahamas The",
          abbreviation: "BS",
          phonecode: "+1-242",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "18",
          name: "Bahrain",
          abbreviation: "BH",
          phonecode: "973",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "19",
          name: "Bangladesh",
          abbreviation: "BD",
          phonecode: "880",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "20",
          name: "Barbados",
          abbreviation: "BB",
          phonecode: "+1-246",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "21",
          name: "Belarus",
          abbreviation: "BY",
          phonecode: "375",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "22",
          name: "Belgium",
          abbreviation: "BE",
          phonecode: "32",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "23",
          name: "Belize",
          abbreviation: "BZ",
          phonecode: "501",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "24",
          name: "Benin",
          abbreviation: "BJ",
          phonecode: "229",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "25",
          name: "Bermuda",
          abbreviation: "BM",
          phonecode: "+1-441",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "26",
          name: "Bhutan",
          abbreviation: "BT",
          phonecode: "975",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "27",
          name: "Bolivia",
          abbreviation: "BO",
          phonecode: "591",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "155",
          name: "Bonaire, Sint Eustatius and Saba",
          abbreviation: "BQ",
          phonecode: "599",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "28",
          name: "Bosnia and Herzegovina",
          abbreviation: "BA",
          phonecode: "387",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "29",
          name: "Botswana",
          abbreviation: "BW",
          phonecode: "267",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "30",
          name: "Bouvet Island",
          abbreviation: "BV",
          phonecode: "0055",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "31",
          name: "Brazil",
          abbreviation: "BR",
          phonecode: "55",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "32",
          name: "British Indian Ocean Territory",
          abbreviation: "IO",
          phonecode: "246",
          active: "0",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "33",
          name: "Brunei",
          abbreviation: "BN",
          phonecode: "673",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "34",
          name: "Bulgaria",
          abbreviation: "BG",
          phonecode: "359",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "35",
          name: "Burkina Faso",
          abbreviation: "BF",
          phonecode: "226",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "36",
          name: "Burundi",
          abbreviation: "BI",
          phonecode: "257",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "37",
          name: "Cambodia",
          abbreviation: "KH",
          phonecode: "855",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "38",
          name: "Cameroon",
          abbreviation: "CM",
          phonecode: "237",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "39",
          name: "Canada",
          abbreviation: "CA",
          phonecode: "1",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "40",
          name: "Cape Verde",
          abbreviation: "CV",
          phonecode: "238",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "41",
          name: "Cayman Islands",
          abbreviation: "KY",
          phonecode: "+1-345",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "42",
          name: "Central African Republic",
          abbreviation: "CF",
          phonecode: "236",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "43",
          name: "Chad",
          abbreviation: "TD",
          phonecode: "235",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "44",
          name: "Chile",
          abbreviation: "CL",
          phonecode: "56",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "45",
          name: "China",
          abbreviation: "CN",
          phonecode: "86",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "46",
          name: "Christmas Island",
          abbreviation: "CX",
          phonecode: "61",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "47",
          name: "Cocos (Keeling) Islands",
          abbreviation: "CC",
          phonecode: "61",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "48",
          name: "Colombia",
          abbreviation: "CO",
          phonecode: "57",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "49",
          name: "Comoros",
          abbreviation: "KM",
          phonecode: "269",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "50",
          name: "Congo",
          abbreviation: "CG",
          phonecode: "242",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "52",
          name: "Cook Islands",
          abbreviation: "CK",
          phonecode: "682",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "53",
          name: "Costa Rica",
          abbreviation: "CR",
          phonecode: "506",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "54",
          name: "Cote D'Ivoire (Ivory Coast)",
          abbreviation: "CI",
          phonecode: "225",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "55",
          name: "Croatia",
          abbreviation: "HR",
          phonecode: "385",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "56",
          name: "Cuba",
          abbreviation: "CU",
          phonecode: "53",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "249",
          name: "Cura ao",
          abbreviation: "CW",
          phonecode: "599",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "57",
          name: "Cyprus",
          abbreviation: "CY",
          phonecode: "357",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "58",
          name: "Czech Republic",
          abbreviation: "CZ",
          phonecode: "420",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "51",
          name: "Democratic Republic of the Congo",
          abbreviation: "CD",
          phonecode: "243",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "59",
          name: "Denmark",
          abbreviation: "DK",
          phonecode: "45",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "60",
          name: "Djibouti",
          abbreviation: "DJ",
          phonecode: "253",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "61",
          name: "Dominica",
          abbreviation: "DM",
          phonecode: "+1-767",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "62",
          name: "Dominican Republic",
          abbreviation: "DO",
          phonecode: "+1-809 and 1-829",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "63",
          name: "East Timor",
          abbreviation: "TL",
          phonecode: "670",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "64",
          name: "Ecuador",
          abbreviation: "EC",
          phonecode: "593",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "65",
          name: "Egypt",
          abbreviation: "EG",
          phonecode: "20",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "66",
          name: "El Salvador",
          abbreviation: "SV",
          phonecode: "503",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "67",
          name: "Equatorial Guinea",
          abbreviation: "GQ",
          phonecode: "240",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "68",
          name: "Eritrea",
          abbreviation: "ER",
          phonecode: "291",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "69",
          name: "Estonia",
          abbreviation: "EE",
          phonecode: "372",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "70",
          name: "Ethiopia",
          abbreviation: "ET",
          phonecode: "251",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "71",
          name: "Falkland Islands",
          abbreviation: "FK",
          phonecode: "500",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "72",
          name: "Faroe Islands",
          abbreviation: "FO",
          phonecode: "298",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "73",
          name: "Fiji Islands",
          abbreviation: "FJ",
          phonecode: "679",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "74",
          name: "Finland",
          abbreviation: "FI",
          phonecode: "358",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "75",
          name: "France",
          abbreviation: "FR",
          phonecode: "33",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "76",
          name: "French Guiana",
          abbreviation: "GF",
          phonecode: "594",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "77",
          name: "French Polynesia",
          abbreviation: "PF",
          phonecode: "689",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "78",
          name: "French Southern Territories",
          abbreviation: "TF",
          phonecode: "262",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "79",
          name: "Gabon",
          abbreviation: "GA",
          phonecode: "241",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "80",
          name: "Gambia The",
          abbreviation: "GM",
          phonecode: "220",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "81",
          name: "Georgia",
          abbreviation: "GE",
          phonecode: "995",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "82",
          name: "Germany",
          abbreviation: "DE",
          phonecode: "49",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "83",
          name: "Ghana",
          abbreviation: "GH",
          phonecode: "233",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "84",
          name: "Gibraltar",
          abbreviation: "GI",
          phonecode: "350",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "85",
          name: "Greece",
          abbreviation: "GR",
          phonecode: "30",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "86",
          name: "Greenland",
          abbreviation: "GL",
          phonecode: "299",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "87",
          name: "Grenada",
          abbreviation: "GD",
          phonecode: "+1-473",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "88",
          name: "Guadeloupe",
          abbreviation: "GP",
          phonecode: "590",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "89",
          name: "Guam",
          abbreviation: "GU",
          phonecode: "+1-671",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "90",
          name: "Guatemala",
          abbreviation: "GT",
          phonecode: "502",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "91",
          name: "Guernsey and Alderney",
          abbreviation: "GG",
          phonecode: "+44-1481",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "92",
          name: "Guinea",
          abbreviation: "GN",
          phonecode: "224",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "93",
          name: "Guinea-Bissau",
          abbreviation: "GW",
          phonecode: "245",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "94",
          name: "Guyana",
          abbreviation: "GY",
          phonecode: "592",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "95",
          name: "Haiti",
          abbreviation: "HT",
          phonecode: "509",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "96",
          name: "Heard Island and McDonald Islands",
          abbreviation: "HM",
          phonecode: "672",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "97",
          name: "Honduras",
          abbreviation: "HN",
          phonecode: "504",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "98",
          name: "Hong Kong S.A.R.",
          abbreviation: "HK",
          phonecode: "852",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "99",
          name: "Hungary",
          abbreviation: "HU",
          phonecode: "36",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "100",
          name: "Iceland",
          abbreviation: "IS",
          phonecode: "354",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "101",
          name: "India",
          abbreviation: "IN",
          phonecode: "91",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "102",
          name: "Indonesia",
          abbreviation: "ID",
          phonecode: "62",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "103",
          name: "Iran",
          abbreviation: "IR",
          phonecode: "98",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "104",
          name: "Iraq",
          abbreviation: "IQ",
          phonecode: "964",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "105",
          name: "Ireland",
          abbreviation: "IE",
          phonecode: "353",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "106",
          name: "Israel",
          abbreviation: "IL",
          phonecode: "972",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "107",
          name: "Italy",
          abbreviation: "IT",
          phonecode: "39",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "108",
          name: "Jamaica",
          abbreviation: "JM",
          phonecode: "+1-876",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "109",
          name: "Japan",
          abbreviation: "JP",
          phonecode: "81",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "110",
          name: "Jersey",
          abbreviation: "JE",
          phonecode: "+44-1534",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "111",
          name: "Jordan",
          abbreviation: "JO",
          phonecode: "962",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "112",
          name: "Kazakhstan",
          abbreviation: "KZ",
          phonecode: "7",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "113",
          name: "Kenya",
          abbreviation: "KE",
          phonecode: "254",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "114",
          name: "Kiribati",
          abbreviation: "KI",
          phonecode: "686",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "248",
          name: "Kosovo",
          abbreviation: "XK",
          phonecode: "383",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "117",
          name: "Kuwait",
          abbreviation: "KW",
          phonecode: "965",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "118",
          name: "Kyrgyzstan",
          abbreviation: "KG",
          phonecode: "996",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "119",
          name: "Laos",
          abbreviation: "LA",
          phonecode: "856",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "120",
          name: "Latvia",
          abbreviation: "LV",
          phonecode: "371",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "121",
          name: "Lebanon",
          abbreviation: "LB",
          phonecode: "961",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "122",
          name: "Lesotho",
          abbreviation: "LS",
          phonecode: "266",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "123",
          name: "Liberia",
          abbreviation: "LR",
          phonecode: "231",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "124",
          name: "Libya",
          abbreviation: "LY",
          phonecode: "218",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "125",
          name: "Liechtenstein",
          abbreviation: "LI",
          phonecode: "423",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "126",
          name: "Lithuania",
          abbreviation: "LT",
          phonecode: "370",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "127",
          name: "Luxembourg",
          abbreviation: "LU",
          phonecode: "352",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "128",
          name: "Macau S.A.R.",
          abbreviation: "MO",
          phonecode: "853",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "129",
          name: "Macedonia",
          abbreviation: "MK",
          phonecode: "389",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "130",
          name: "Madagascar",
          abbreviation: "MG",
          phonecode: "261",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "131",
          name: "Malawi",
          abbreviation: "MW",
          phonecode: "265",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "132",
          name: "Malaysia",
          abbreviation: "MY",
          phonecode: "60",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "133",
          name: "Maldives",
          abbreviation: "MV",
          phonecode: "960",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "134",
          name: "Mali",
          abbreviation: "ML",
          phonecode: "223",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "135",
          name: "Malta",
          abbreviation: "MT",
          phonecode: "356",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "136",
          name: "Man (Isle of)",
          abbreviation: "IM",
          phonecode: "+44-1624",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "137",
          name: "Marshall Islands",
          abbreviation: "MH",
          phonecode: "692",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "138",
          name: "Martinique",
          abbreviation: "MQ",
          phonecode: "596",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "139",
          name: "Mauritania",
          abbreviation: "MR",
          phonecode: "222",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "140",
          name: "Mauritius",
          abbreviation: "MU",
          phonecode: "230",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "141",
          name: "Mayotte",
          abbreviation: "YT",
          phonecode: "262",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "142",
          name: "Mexico",
          abbreviation: "MX",
          phonecode: "52",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "143",
          name: "Micronesia",
          abbreviation: "FM",
          phonecode: "691",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "144",
          name: "Moldova",
          abbreviation: "MD",
          phonecode: "373",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "145",
          name: "Monaco",
          abbreviation: "MC",
          phonecode: "377",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "146",
          name: "Mongolia",
          abbreviation: "MN",
          phonecode: "976",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "147",
          name: "Montenegro",
          abbreviation: "ME",
          phonecode: "382",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "148",
          name: "Montserrat",
          abbreviation: "MS",
          phonecode: "+1-664",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "149",
          name: "Morocco",
          abbreviation: "MA",
          phonecode: "212",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "150",
          name: "Mozambique",
          abbreviation: "MZ",
          phonecode: "258",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "151",
          name: "Myanmar",
          abbreviation: "MM",
          phonecode: "95",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "152",
          name: "Namibia",
          abbreviation: "NA",
          phonecode: "264",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "153",
          name: "Nauru",
          abbreviation: "NR",
          phonecode: "674",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "154",
          name: "Nepal",
          abbreviation: "NP",
          phonecode: "977",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "156",
          name: "Netherlands",
          abbreviation: "NL",
          phonecode: "31",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "157",
          name: "New Caledonia",
          abbreviation: "NC",
          phonecode: "687",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "158",
          name: "New Zealand",
          abbreviation: "NZ",
          phonecode: "64",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "159",
          name: "Nicaragua",
          abbreviation: "NI",
          phonecode: "505",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "160",
          name: "Niger",
          abbreviation: "NE",
          phonecode: "227",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "161",
          name: "Nigeria",
          abbreviation: "NG",
          phonecode: "234",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "162",
          name: "Niue",
          abbreviation: "NU",
          phonecode: "683",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "163",
          name: "Norfolk Island",
          abbreviation: "NF",
          phonecode: "672",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "115",
          name: "North Korea",
          abbreviation: "KP",
          phonecode: "850",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "164",
          name: "Northern Mariana Islands",
          abbreviation: "MP",
          phonecode: "+1-670",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "165",
          name: "Norway",
          abbreviation: "NO",
          phonecode: "47",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "166",
          name: "Oman",
          abbreviation: "OM",
          phonecode: "968",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "167",
          name: "Pakistan",
          abbreviation: "PK",
          phonecode: "92",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "168",
          name: "Palau",
          abbreviation: "PW",
          phonecode: "680",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "169",
          name: "Palestinian Territory Occupied",
          abbreviation: "PS",
          phonecode: "970",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "170",
          name: "Panama",
          abbreviation: "PA",
          phonecode: "507",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "171",
          name: "Papua new Guinea",
          abbreviation: "PG",
          phonecode: "675",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "172",
          name: "Paraguay",
          abbreviation: "PY",
          phonecode: "595",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "173",
          name: "Peru",
          abbreviation: "PE",
          phonecode: "51",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "174",
          name: "Philippines",
          abbreviation: "PH",
          phonecode: "63",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "175",
          name: "Pitcairn Island",
          abbreviation: "PN",
          phonecode: "870",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "176",
          name: "Poland",
          abbreviation: "PL",
          phonecode: "48",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "177",
          name: "Portugal",
          abbreviation: "PT",
          phonecode: "351",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "178",
          name: "Puerto Rico",
          abbreviation: "PR",
          phonecode: "+1-787 and 1-939",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "179",
          name: "Qatar",
          abbreviation: "QA",
          phonecode: "974",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "180",
          name: "Reunion",
          abbreviation: "RE",
          phonecode: "262",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "181",
          name: "Romania",
          abbreviation: "RO",
          phonecode: "40",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "182",
          name: "Russia",
          abbreviation: "RU",
          phonecode: "7",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "183",
          name: "Rwanda",
          abbreviation: "RW",
          phonecode: "250",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "184",
          name: "Saint Helena",
          abbreviation: "SH",
          phonecode: "290",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "185",
          name: "Saint Kitts And Nevis",
          abbreviation: "KN",
          phonecode: "+1-869",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "186",
          name: "Saint Lucia",
          abbreviation: "LC",
          phonecode: "+1-758",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "187",
          name: "Saint Pierre and Miquelon",
          abbreviation: "PM",
          phonecode: "508",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "188",
          name: "Saint Vincent And The Grenadines",
          abbreviation: "VC",
          phonecode: "+1-784",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "189",
          name: "Saint-Barthelemy",
          abbreviation: "BL",
          phonecode: "590",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "190",
          name: "Saint-Martin (French part)",
          abbreviation: "MF",
          phonecode: "590",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "191",
          name: "Samoa",
          abbreviation: "WS",
          phonecode: "685",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "192",
          name: "San Marino",
          abbreviation: "SM",
          phonecode: "378",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "193",
          name: "Sao Tome and Principe",
          abbreviation: "ST",
          phonecode: "239",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "194",
          name: "Saudi Arabia",
          abbreviation: "SA",
          phonecode: "966",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "195",
          name: "Senegal",
          abbreviation: "SN",
          phonecode: "221",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "196",
          name: "Serbia",
          abbreviation: "RS",
          phonecode: "381",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "197",
          name: "Seychelles",
          abbreviation: "SC",
          phonecode: "248",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "198",
          name: "Sierra Leone",
          abbreviation: "SL",
          phonecode: "232",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "199",
          name: "Singapore",
          abbreviation: "SG",
          phonecode: "65",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "250",
          name: "Sint Maarten (Dutch part)",
          abbreviation: "SX",
          phonecode: "1721",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "200",
          name: "Slovakia",
          abbreviation: "SK",
          phonecode: "421",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "201",
          name: "Slovenia",
          abbreviation: "SI",
          phonecode: "386",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "202",
          name: "Solomon Islands",
          abbreviation: "SB",
          phonecode: "677",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "203",
          name: "Somalia",
          abbreviation: "SO",
          phonecode: "252",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "204",
          name: "South Africa",
          abbreviation: "ZA",
          phonecode: "27",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "205",
          name: "South Georgia",
          abbreviation: "GS",
          phonecode: "500",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "116",
          name: "South Korea",
          abbreviation: "KR",
          phonecode: "82",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "206",
          name: "South Sudan",
          abbreviation: "SS",
          phonecode: "211",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "207",
          name: "Spain",
          abbreviation: "ES",
          phonecode: "34",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "208",
          name: "Sri Lanka",
          abbreviation: "LK",
          phonecode: "94",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "209",
          name: "Sudan",
          abbreviation: "SD",
          phonecode: "249",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "210",
          name: "Suriname",
          abbreviation: "SR",
          phonecode: "597",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "211",
          name: "Svalbard And Jan Mayen Islands",
          abbreviation: "SJ",
          phonecode: "47",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "212",
          name: "Swaziland",
          abbreviation: "SZ",
          phonecode: "268",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "213",
          name: "Sweden",
          abbreviation: "SE",
          phonecode: "46",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "214",
          name: "Switzerland",
          abbreviation: "CH",
          phonecode: "41",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "215",
          name: "Syria",
          abbreviation: "SY",
          phonecode: "963",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "216",
          name: "Taiwan",
          abbreviation: "TW",
          phonecode: "886",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "217",
          name: "Tajikistan",
          abbreviation: "TJ",
          phonecode: "992",
          active: "0",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "218",
          name: "Tanzania",
          abbreviation: "TZ",
          phonecode: "255",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "219",
          name: "Thailand",
          abbreviation: "TH",
          phonecode: "66",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "220",
          name: "Togo",
          abbreviation: "TG",
          phonecode: "228",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "221",
          name: "Tokelau",
          abbreviation: "TK",
          phonecode: "690",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "222",
          name: "Tonga",
          abbreviation: "TO",
          phonecode: "676",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "223",
          name: "Trinidad And Tobago",
          abbreviation: "TT",
          phonecode: "+1-868",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "224",
          name: "Tunisia",
          abbreviation: "TN",
          phonecode: "216",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "225",
          name: "Turkey",
          abbreviation: "TR",
          phonecode: "90",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "226",
          name: "Turkmenistan",
          abbreviation: "TM",
          phonecode: "993",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "227",
          name: "Turks And Caicos Islands",
          abbreviation: "TC",
          phonecode: "+1-649",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "228",
          name: "Tuvalu",
          abbreviation: "TV",
          phonecode: "688",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "229",
          name: "Uganda",
          abbreviation: "UG",
          phonecode: "256",
          active: "0",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "230",
          name: "Ukraine",
          abbreviation: "UA",
          phonecode: "380",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "231",
          name: "United Arab Emirates",
          abbreviation: "AE",
          phonecode: "971",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "232",
          name: "United Kingdom",
          abbreviation: "GB",
          phonecode: "44",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "233",
          name: "United States",
          abbreviation: "US",
          phonecode: "1",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "234",
          name: "United States Minor Outlying Islands",
          abbreviation: "UM",
          phonecode: "1",
          active: "1",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "235",
          name: "Uruguay",
          abbreviation: "UY",
          phonecode: "598",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "236",
          name: "Uzbekistan",
          abbreviation: "UZ",
          phonecode: "998",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "237",
          name: "Vanuatu",
          abbreviation: "VU",
          phonecode: "678",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "238",
          name: "Vatican City State (Holy See)",
          abbreviation: "VA",
          phonecode: "379",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "239",
          name: "Venezuela",
          abbreviation: "VE",
          phonecode: "58",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "240",
          name: "Vietnam",
          abbreviation: "VN",
          phonecode: "84",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "241",
          name: "Virgin Islands (British)",
          abbreviation: "VG",
          phonecode: "+1-284",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "242",
          name: "Virgin Islands (US)",
          abbreviation: "VI",
          phonecode: "+1-340",
          active: "1",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "243",
          name: "Wallis And Futuna Islands",
          abbreviation: "WF",
          phonecode: "681",
          active: "0",
          sorting: "1",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "244",
          name: "Western Sahara",
          abbreviation: "EH",
          phonecode: "212",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "245",
          name: "Yemen",
          abbreviation: "YE",
          phonecode: "967",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "246",
          name: "Zambia",
          abbreviation: "ZM",
          phonecode: "260",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          id: "247",
          name: "Zimbabwe",
          abbreviation: "ZW",
          phonecode: "263",
          active: "0",
          sorting: "0",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
        id: "251",
        abbreviation: "XA",
        name: "External Territories of Australia",
        phonecode: "61",
        sorting: "0",
        active: "1",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        id: "252",
        abbreviation: "KR",
        name: "Korea South",
        phonecode: "82",
        sorting: "0",
        active: "1",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        id: "253",
        abbreviation: "YU",
        name: "Yugoslavia",
        phonecode: "38",
        sorting: "0",
        active: "1",
        created_at: new Date(),
        updated_at: new Date()
    },
  ]);
  return await queryInterface.sequelize.query(`ALTER SEQUENCE "ticket_exchange".countries_id_seq RESTART WITH ${246 + 1}`);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('countries', null, {});
  }
};
