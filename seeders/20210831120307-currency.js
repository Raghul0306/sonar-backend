'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return queryInterface.bulkInsert({ tableName: 'currency', schema: 'ticket_exchange' }, [ 
      {
          name: "Afghanistan",
          currency: "Afghani",
          iso_code: "AFN",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Albania",
          currency: "Lek",
          iso_code: "ALL",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Algeria",
          currency: "Algerian Dinar",
          iso_code: "DZD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "American Samoa",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Andorra",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Angola",
          currency: "Kwanza",
          iso_code: "AOA",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Anguilla",
          currency: "East Caribbean Dollar",
          iso_code: "XCD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Antartica",
          currency: "none",
          iso_code: "none",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Antigua And Barbuda",
          currency: "East Caribbean Dollar",
          iso_code: "XCD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Argentina",
          currency: "Argentine Peso",
          iso_code: "ARS",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Armenia",
          currency: "Armenian Dram",
          iso_code: "AMD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Aruba",
          currency: "Aruban Florin",
          iso_code: "AWG",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Australia",
          currency: "Australian Dollar",
          iso_code: "AUD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Austria",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Azerbaijan",
          currency: "Azerbaijan Manat",
          iso_code: "AZN",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Bahamas (the)",
          currency: "Bahamian Dollar",
          iso_code: "BSD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Bahrain",
          currency: "Bahraini Dinar",
          iso_code: "BHD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Bangladesh",
          currency: "Taka",
          iso_code: "BDT",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Barbados",
          currency: "Barbados Dollar",
          iso_code: "BBD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Belarus",
          currency: "Belarusian Ruble",
          iso_code: "BYN",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Belgium",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Belize",
          currency: "Belize Dollar",
          iso_code: "BZD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Benin",
          currency: "CFA Franc BCEAO",
          iso_code: "XOF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Bermuda",
          currency: "Bermudian Dollar",
          iso_code: "BMD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Bhutan",
          currency: "Indian Rupee",
          iso_code: "INR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Bolivia (plurinational State Of)",
          currency: "Boliviano",
          iso_code: "BOB",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Bosnia And Herzegovina",
          currency: "Convertible Mark",
          iso_code: "BAM",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Botswana",
          currency: "Pula",
          iso_code: "BWP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Bouvet Island",
          currency: "Norwegian Krone",
          iso_code: "NOK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Brazil",
          currency: "Brazilian Real",
          iso_code: "BRL",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "British Indian Ocean Territory (the)",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Brunei Darussalam",
          currency: "Brunei Dollar",
          iso_code: "BND",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Bulgaria",
          currency: "Bulgarian Lev",
          iso_code: "BGN",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Burkina Faso",
          currency: "CFA Franc BCEAO",
          iso_code: "XOF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Burundi",
          currency: "Burundi Franc",
          iso_code: "BIF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Cambodia",
          currency: "Riel",
          iso_code: "KHR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Cameroon",
          currency: "CFA Franc BEAC",
          iso_code: "XAF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Canada",
          currency: "Canadian Dollar",
          iso_code: "CAD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Cape Verde",
          currency: "na",
          iso_code: "na",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Cayman Islands (the)",
          currency: "Cayman Islands Dollar",
          iso_code: "KYD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Central African Republic (the)",
          currency: "CFA Franc BEAC",
          iso_code: "XAF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Chad",
          currency: "CFA Franc BEAC",
          iso_code: "XAF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Chile",
          currency: "Chilean Peso",
          iso_code: "CLP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "China",
          currency: "Yuan Renminbi",
          iso_code: "CNY",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Christmas Island",
          currency: "Australian Dollar",
          iso_code: "AUD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Cocos (keeling) Islands (the)",
          currency: "Australian Dollar",
          iso_code: "AUD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Colombia",
          currency: "Colombian Peso",
          iso_code: "COP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Comoros (the)",
          currency: "Comorian Franc",
          iso_code: "KMF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Congo (the Democratic Republic Of The)",
          currency: "Congolese Franc",
          iso_code: "CDF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Congo (the)",
          currency: "CFA Franc BEAC",
          iso_code: "XAF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Cook Islands (the)",
          currency: "New Zealand Dollar",
          iso_code: "NZD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Costa Rica",
          currency: "Costa Rican Colon",
          iso_code: "CRC",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Côte D'ivoire",
          currency: "CFA Franc BCEAO",
          iso_code: "XOF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Croatia",
          currency: "Kuna",
          iso_code: "HRK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Cuba",
          currency: "Cuban Peso",
          iso_code: "CUP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Cyprus",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Czechia",
          currency: "Czech Koruna",
          iso_code: "CZK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Denmark",
          currency: "Danish Krone",
          iso_code: "DKK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Djibouti",
          currency: "Djibouti Franc",
          iso_code: "DJF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Dominica",
          currency: "East Caribbean Dollar",
          iso_code: "XCD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Dominican Republic (the)",
          currency: "Dominican Peso",
          iso_code: "DOP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "East Timor",
          currency: "na",
          iso_code: "na",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Ecuador",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Egypt",
          currency: "Egyptian Pound",
          iso_code: "EGP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "El Salvador",
          currency: "El Salvador Colon",
          iso_code: "SVC",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Equatorial Guinea",
          currency: "CFA Franc BEAC",
          iso_code: "XAF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Eritrea",
          currency: "Nakfa",
          iso_code: "ERN",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Estonia",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Ethiopia",
          currency: "Ethiopian Birr",
          iso_code: "ETB",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "External Territories of Australia",
          currency: "Australian Dollar",
          iso_code: "AUS",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Falkland Islands (the) [malvinas]",
          currency: "Falkland Islands Pound",
          iso_code: "FKP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Faroe Islands (the)",
          currency: "Danish Krone",
          iso_code: "DKK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Fiji",
          currency: "Fiji Dollar",
          iso_code: "FJD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Finland",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "France",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "French Guiana",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "French Polynesia",
          currency: "CFP Franc",
          iso_code: "XPF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "French Southern Territories (the)",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Gabon",
          currency: "CFA Franc BEAC",
          iso_code: "XAF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Gambia (the)",
          currency: "Dalasi",
          iso_code: "GMD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Georgia",
          currency: "Lari",
          iso_code: "GEL",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Germany",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Ghana",
          currency: "Ghana Cedi",
          iso_code: "GHS",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Gibraltar",
          currency: "Gibraltar Pound",
          iso_code: "GIP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Greece",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Greenland",
          currency: "Danish Krone",
          iso_code: "DKK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Grenada",
          currency: "East Caribbean Dollar",
          iso_code: "XCD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Guadeloupe",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Guam",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Guatemala",
          currency: "Quetzal",
          iso_code: "GTQ",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Guernsey",
          currency: "Pound Sterling",
          iso_code: "GBP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Guinea",
          currency: "Guinean Franc",
          iso_code: "GNF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Guinea-bissau",
          currency: "CFA Franc BCEAO",
          iso_code: "XOF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Guyana",
          currency: "Guyana Dollar",
          iso_code: "GYD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Haiti",
          currency: "Gourde",
          iso_code: "HTG",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Heard Island And Mcdonald Islands",
          currency: "Australian Dollar",
          iso_code: "AUD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Honduras",
          currency: "Lempira",
          iso_code: "HNL",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Hong Kong",
          currency: "Hong Kong Dollar",
          iso_code: "HKD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Hungary",
          currency: "Forint",
          iso_code: "HUF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Iceland",
          currency: "Iceland Krona",
          iso_code: "ISK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "India",
          currency: "Indian Rupee",
          iso_code: "INR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Indonesia",
          currency: "Rupiah",
          iso_code: "IDR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Iran (islamic Republic Of)",
          currency: "Iranian Rial",
          iso_code: "IRR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Iraq",
          currency: "Iraqi Dinar",
          iso_code: "IQD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Ireland",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Israel",
          currency: "New Israeli Sheqel",
          iso_code: "ILS",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Italy",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Jamaica",
          currency: "Jamaican Dollar",
          iso_code: "JMD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Japan",
          currency: "Yen",
          iso_code: "JPY",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Jersey",
          currency: "Pound Sterling",
          iso_code: "GBP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Jordan",
          currency: "Jordanian Dinar",
          iso_code: "JOD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Kazakhstan",
          currency: "Tenge",
          iso_code: "KZT",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Kenya",
          currency: "Kenyan Shilling",
          iso_code: "KES",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Kiribati",
          currency: "Australian Dollar",
          iso_code: "AUD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Korea (the Democratic People’s Republic Of)",
          currency: "North Korean Won",
          iso_code: "KPW",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Korea (the Republic Of)",
          currency: "Won",
          iso_code: "KRW",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Kuwait",
          currency: "Kuwaiti Dinar",
          iso_code: "KWD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Kyrgyzstan",
          currency: "Som",
          iso_code: "KGS",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Lao People’s Democratic Republic (the)",
          currency: "Lao Kip",
          iso_code: "LAK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Latvia",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Lebanon",
          currency: "Lebanese Pound",
          iso_code: "LBP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Lesotho",
          currency: "Loti",
          iso_code: "LSL",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Liberia",
          currency: "Liberian Dollar",
          iso_code: "LRD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Libya",
          currency: "Libyan Dinar",
          iso_code: "LYD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Liechtenstein",
          currency: "Swiss Franc",
          iso_code: "CHF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Lithuania",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Luxembourg",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Macao",
          currency: "Pataca",
          iso_code: "MOP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "North Macedonia",
          currency: "Denar",
          iso_code: "MKD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Madagascar",
          currency: "Malagasy Ariary",
          iso_code: "MGA",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Malawi",
          currency: "Malawi Kwacha",
          iso_code: "MWK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Malaysia",
          currency: "Malaysian Ringgit",
          iso_code: "MYR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Maldives",
          currency: "Rufiyaa",
          iso_code: "MVR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Mali",
          currency: "CFA Franc BCEAO",
          iso_code: "XOF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Malta",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Isle of Man",
          currency: "Pound Sterling",
          iso_code: "GBP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Marshall Islands (the)",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Martinique",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Mauritania",
          currency: "Ouguiya",
          iso_code: "MRU",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Mauritius",
          currency: "Mauritius Rupee",
          iso_code: "MUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Mayotte",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Mexico",
          currency: "Mexican Peso",
          iso_code: "MXN",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Micronesia (federated States Of)",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Moldova (the Republic Of)",
          currency: "Moldovan Leu",
          iso_code: "MDL",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Monaco",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Mongolia",
          currency: "Tugrik",
          iso_code: "MNT",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Montserrat",
          currency: "East Caribbean Dollar",
          iso_code: "XCD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Morocco",
          currency: "Moroccan Dirham",
          iso_code: "MAD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Mozambique",
          currency: "Mozambique Metical",
          iso_code: "MZN",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Myanmar",
          currency: "Kyat",
          iso_code: "MMK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Namibia",
          currency: "Namibia Dollar",
          iso_code: "NAD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Nauru",
          currency: "Australian Dollar",
          iso_code: "AUD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Nepal",
          currency: "Nepalese Rupee",
          iso_code: "NPR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Netherlands Antilles",
          currency: "na",
          iso_code: "na",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Netherlands (the)",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "New Caledonia",
          currency: "CFP Franc",
          iso_code: "XPF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "New Zealand",
          currency: "New Zealand Dollar",
          iso_code: "NZD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Nicaragua",
          currency: "Cordoba Oro",
          iso_code: "NIO",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Niger (the)",
          currency: "CFA Franc BCEAO",
          iso_code: "XOF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Nigeria",
          currency: "Naira",
          iso_code: "NGN",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Niue",
          currency: "New Zealand Dollar",
          iso_code: "NZD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Norfolk Island",
          currency: "Australian Dollar",
          iso_code: "AUD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Northern Mariana Islands (the)",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Norway",
          currency: "Norwegian Krone",
          iso_code: "NOK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Oman",
          currency: "Rial Omani",
          iso_code: "OMR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Pakistan",
          currency: "Pakistan Rupee",
          iso_code: "PKR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Palau",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Palestine, State Of",
          currency: "No universal currency",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Panama",
          currency: "Balboa",
          iso_code: "PAB",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Papua New Guinea",
          currency: "Kina",
          iso_code: "PGK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Paraguay",
          currency: "Guarani",
          iso_code: "PYG",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Peru",
          currency: "Sol",
          iso_code: "PEN",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Philippines (the)",
          currency: "Philippine Peso",
          iso_code: "PHP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Pitcairn",
          currency: "New Zealand Dollar",
          iso_code: "NZD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Poland",
          currency: "Zloty",
          iso_code: "PLN",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Portugal",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Puerto Rico",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Qatar",
          currency: "Qatari Rial",
          iso_code: "QAR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Réunion",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Romania",
          currency: "Romanian Leu",
          iso_code: "RON",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Russian Federation (the)",
          currency: "Russian Ruble",
          iso_code: "RUB",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Rwanda",
          currency: "Rwanda Franc",
          iso_code: "RWF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Saint Helena, Ascension And Tristan Da Cunha",
          currency: "Saint Helena Pound",
          iso_code: "SHP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Saint Kitts And Nevis",
          currency: "East Caribbean Dollar",
          iso_code: "XCD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Saint Lucia",
          currency: "East Caribbean Dollar",
          iso_code: "XCD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Saint Pierre And Miquelon",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Saint Vincent And The Grenadines",
          currency: "East Caribbean Dollar",
          iso_code: "XCD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Samoa",
          currency: "Tala",
          iso_code: "WST",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "San Marino",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Sao Tome And Principe",
          currency: "Dobra",
          iso_code: "STN",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Saudi Arabia",
          currency: "Saudi Riyal",
          iso_code: "SAR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Senegal",
          currency: "CFA Franc BCEAO",
          iso_code: "XOF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Serbia",
          currency: "Serbian Dinar",
          iso_code: "RSD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Seychelles",
          currency: "Seychelles Rupee",
          iso_code: "SCR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Sierra Leone",
          currency: "Leone",
          iso_code: "SLL",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Singapore",
          currency: "Singapore Dollar",
          iso_code: "SGD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Slovakia",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Slovenia",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "small uk territories",
          currency: "Pound Sterling",
          iso_code: "GBP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Solomon Islands",
          currency: "Solomon Islands Dollar",
          iso_code: "SBD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Somalia",
          currency: "Somali Shilling",
          iso_code: "SOS",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "South Africa",
          currency: "Rand",
          iso_code: "ZAR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "South Georgia And The South Sandwich Islands",
          currency: "No universal currency",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "South Sudan",
          currency: "South Sudanese Pound",
          iso_code: "SSP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Spain",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Sri Lanka",
          currency: "Sri Lanka Rupee",
          iso_code: "LKR",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Sudan (the)",
          currency: "Sudanese Pound",
          iso_code: "SDG",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Suriname",
          currency: "Surinam Dollar",
          iso_code: "SRD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Svalbard And Jan Mayen",
          currency: "Norwegian Krone",
          iso_code: "NOK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Sawziland",
          currency: "na",
          iso_code: "na",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Sweden",
          currency: "Swedish Krona",
          iso_code: "SEK",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Switzerland",
          currency: "Swiss Franc",
          iso_code: "CHF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Syrian Arab Republic",
          currency: "Syrian Pound",
          iso_code: "SYP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Taiwan (province Of China)",
          currency: "New Taiwan Dollar",
          iso_code: "TWD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Tajikistan",
          currency: "Somoni",
          iso_code: "TJS",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Tanzania, United Republic Of",
          currency: "Tanzanian Shilling",
          iso_code: "TZS",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Thailand",
          currency: "Baht",
          iso_code: "THB",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Togo",
          currency: "CFA Franc BCEAO",
          iso_code: "XOF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Tokelau",
          currency: "New Zealand Dollar",
          iso_code: "NZD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Tonga",
          currency: "Pa’anga",
          iso_code: "TOP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Trinidad And Tobago",
          currency: "Trinidad and Tobago Dollar",
          iso_code: "TTD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Tunisia",
          currency: "Tunisian Dinar",
          iso_code: "TND",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Turkey",
          currency: "Turkish Lira",
          iso_code: "TRY",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Turkmenistan",
          currency: "Turkmenistan New Manat",
          iso_code: "TMT",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Turks And Caicos Islands (the)",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Tuvalu",
          currency: "Australian Dollar",
          iso_code: "AUD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Uganda",
          currency: "Uganda Shilling",
          iso_code: "UGX",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Ukraine",
          currency: "Hryvnia",
          iso_code: "UAH",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "United Arab Emirates (the)",
          currency: "UAE Dirham",
          iso_code: "AED",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "United Kingdom Of Great Britain And Northern Ireland (the)",
          currency: "Pound Sterling",
          iso_code: "GBP",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "United States Of America (the)",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "United States Minor Outlying Islands (the)",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Uruguay",
          currency: "Peso Uruguayo",
          iso_code: "UYU",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Uzbekistan",
          currency: "Uzbekistan Sum",
          iso_code: "UZS",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Vanuatu",
          currency: "Vatu",
          iso_code: "VUV",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Vatican City",
          currency: "na",
          iso_code: "na",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Venezuela (bolivarian Republic Of)",
          currency: "Bolívar Soberano",
          iso_code: "VES",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Viet Nam",
          currency: "Dong",
          iso_code: "VND",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Virgin Islands (british)",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Virgin Islands (u.s.)",
          currency: "US Dollar",
          iso_code: "USD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Wallis And Futuna",
          currency: "CFP Franc",
          iso_code: "XPF",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Western Sahara",
          currency: "Moroccan Dirham",
          iso_code: "MAD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Yemen",
          currency: "Yemeni Rial",
          iso_code: "YER",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Yugoslavia",
          currency: "na",
          iso_code: "na",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Zambia",
          currency: "Zambian Kwacha",
          iso_code: "ZMW",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Zimbabwe",
          currency: "Zimbabwe Dollar",
          iso_code: "ZWL",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Zz08_gold",
          currency: "Gold",
          iso_code: "XAU",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Zz09_palladium",
          currency: "Palladium",
          iso_code: "XPD",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Zz10_platinum",
          currency: "Platinum",
          iso_code: "XPT",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "Zz11_silver",
          currency: "Silver",
          iso_code: "XAG",
          created_at: new Date(),
          updated_at: new Date()
      },
      {
          name: "European Union",
          currency: "Euro",
          iso_code: "EUR",
          created_at: new Date(),
          updated_at: new Date()
      }
])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     return queryInterface.bulkDelete('currency', null, {});
  }
};
