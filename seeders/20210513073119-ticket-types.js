'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'ticket_types', schema: 'ticket_exchange' }, [{
      name: 'eTicket',
      delivery_type_description:`eTickets are tickets that are sent to you via email often delivered as a PDF or another downloadable format.`,
      alternative_name:`PDF Tickets, Print at Home`,
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'eTransfer',
      delivery_type_description:`eTransfers tickets are tickets that are delivered outside of Yadara via a third-party secure ticketing platform. Information on how to accept your tickets will be provided at Order Confirmation.`,
      alternative_name:`Mobile Transfer, Ticketmaster Transfer, Flash Seats`,
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'Hard Stock',
      delivery_type_description:`Hard stock tickets are unique paper tickets often printed by the venue.`,
      alternative_name: `Paper tickets`,
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'Will Call',
      delivery_type_description:`Will Call ticket is a ticket that you collect from the venue''s box office.`,
      alternative_name: `Box Office Pickup`,
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'Local Pick Up',
      delivery_type_description:'Hard Stock tickets that are collected near or at the venue. ',
      alternative_name: `LPU, Meet and Greet, Paperless tickets`,
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'QR Code',
      delivery_type_description:'This is a QR Code that is delivered to you as an attachment via email. You MUST present this attachment via a smart phone. The venue will not accept or scan a printed copy of this mobile QR code.',
      alternative_name: `QR, QR Code, Screenshot`,
      created_at: new Date(),
      updated_at: new Date()
    }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ticket_types', null, {});
  }
};
