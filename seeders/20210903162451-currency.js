'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return await queryInterface.bulkInsert({ tableName: 'currency', schema: 'ticket_exchange' }, [{
      name: 'USD',
      iso_code: '840',
      created_at: new Date(),
      created_by:1
    }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('currency', null, {});
  }
};
