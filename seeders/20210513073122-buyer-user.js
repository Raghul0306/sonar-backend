'use strict';

const bcrypt = require("bcrypt");
const awsHelper = require('../helpers/awskms');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'buyerUsers', schema: 'ticket_exchange' }, [
    {
      first_name:'Admin',
      last_name:'Yadara',
      email:'admin@yadara.com',
      phone_number:67989796533,
      user_role_id:6,
      stripeID:104,
      address1:'',
      address2:'',
      city:'',
      state:'',
      country:'',
      zip_code:987654,
      notes:'',
      is_email_login: true,
      password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      first_name:'Buyer',
      last_name:'Yadara',
      email:'buyer@yadara.com',
      phone_number:67989796533,
      user_role_id:6,
      stripeID:105,
      address1:'',
      address2:'',
      city:'',
      state:'',
      country:'',
      zip_code:987654,
      notes:'',
      is_email_login: true,
      password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
      created_at: new Date(),
      updated_at: new Date()
    },
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('buyerUsers', null, {});
  }
};
