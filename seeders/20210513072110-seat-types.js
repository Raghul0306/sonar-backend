'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'seat_type', schema: 'ticket_exchange' }, [{
      name: 'Consecutive',
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'General Admission',
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'Odd/Even',
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'Piggyback',
      created_at: new Date(),
      updated_at: new Date()
    }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('seat_type', null, {});
  }
};
