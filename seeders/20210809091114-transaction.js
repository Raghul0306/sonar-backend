'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'transaction', schema: 'ticket_exchange' }, [
    {
      user_id:1,
      event_id: 1, 
      listing_group_id: 1, 
      buyer_user_id:1, 
      payment_id: 546,
      order_status_id: 1, 
      tevo_id:1954620,
      mercury_id:58964,    
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      user_id:2,
      event_id: 1, 
      listing_group_id: 1, 
      buyer_user_id:2, 
      payment_id: 874,
      order_status_id: 1, 
      tevo_id:1954450,
      mercury_id:87964,    
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      user_id:3,
      event_id: 1, 
      listing_group_id: 1, 
      buyer_user_id:1, 
      payment_id: 746,
      order_status_id: 1, 
      tevo_id:1544620,
      mercury_id:58104,    
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      user_id:4,
      event_id: 1, 
      listing_group_id: 2, 
      buyer_user_id:1, 
      payment_id: 145,
      order_status_id: 1, 
      tevo_id:1951590,
      mercury_id:10964,    
      created_at: new Date(),
      updated_at: new Date()
    },
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('transaction', null, {});
  }
};
