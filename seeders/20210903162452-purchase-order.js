'use strict';

let purchaseOrders = [
	{
		"user_id": 1,
		"vendor_id": 1,
		"purchase_date": "09/02/2021 08:45",
		"purchase_amount": 176.44,
		"currency_id": 1,
		"listing_group_id": 5,
		"payment_method_id": 1,
		"payment_type_id": 1,
		"ticketing_system_id": 1,
		"account_id": 1,
		"external_reference": "26-21351/UK3",
		"active": true,
		"sales_tax": 0,
		"shipping_and_handling_fee": 0,
		"fees": 0,
		"created_at": new Date(),
		"created_by": 1
	},
	{
		"user_id": 1,
		"vendor_id": 1,
		"purchase_date": "09/02/2021 08:44",
		"purchase_amount": 156.36,
		"currency_id": 1,
		"listing_group_id": 6,
		"payment_method_id": 1,
		"payment_type_id": 1,
		"ticketing_system_id": 1,
		"account_id": 1,
		"external_reference": "44-35872x1/UK1",
		"active": true,
		"sales_tax": 0,
		"shipping_and_handling_fee": 0,
		"fees": 0,
		"created_at": new Date(),
		"created_by": 1
	},
	{
		"user_id": 1,
		"vendor_id": 1,
		"purchase_date": "09/02/2021 08:39",
		"purchase_amount": 176.44,
		"currency_id": 1,
		"listing_group_id": 7,
		"payment_method_id": 1,
		"payment_type_id": 1,
		"ticketing_system_id": 1,
		"account_id": 1,
		"external_reference": "26-21313/UK3",
		"active": true,
		"sales_tax": 0,
		"shipping_and_handling_fee": 0,
		"fees": 0,
		"created_at": new Date(),
		"created_by": 1
	},
	{
		"user_id": 1,
		"vendor_id": 1,
		"purchase_date": "09/02/2021 08:32",
		"purchase_amount": 176.44,
		"currency_id": 1,
		"listing_group_id": 8,
		"payment_method_id": 1,
		"payment_type_id": 1,
		"ticketing_system_id": 1,
		"account_id": 1,
		"external_reference": "26-21210/UK3",
		"active": true,
		"sales_tax": 0,
		"shipping_and_handling_fee": 0,
		"fees": 0,
		"created_at": new Date(),
		"created_by": 1
	},
	{
		"user_id": 1,
		"vendor_id": 1,
		"purchase_date": "09/02/2021 08:32",
		"purchase_amount": 269.1,
		"currency_id": 1,
		"listing_group_id": 9,
		"payment_method_id": 1,
		"payment_type_id": 1,
		"ticketing_system_id": 1,
		"account_id": 1,
		"external_reference": "5-40493/UK3",
		"active": true,
		"sales_tax": 0,
		"shipping_and_handling_fee": 0,
		"fees": 0,
		"created_at": new Date(),
		"created_by": 1
	}
]

module.exports = {
  up: async (queryInterface, Sequelize) => {

    purchaseOrders = purchaseOrders.map(purchaseOrder=>{
      purchaseOrder.purchase_date = new Date(purchaseOrder.purchase_date);
      purchaseOrder.active = true;
      purchaseOrder.payment_last4_digits = Math. floor(1000 + Math. random() * 9000);
      return purchaseOrder;
    });

    return await queryInterface.bulkInsert({ tableName: 'purchase_order', schema: 'ticket_exchange' },  purchaseOrders);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('purchase_order', null, {});
  }
};
