'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'cancelled_order_type', schema: 'ticket_exchange' }, [{
      name:"refund",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      name:"refund pending",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      name:"no refund",
      created_at: new Date(),
      updated_at: new Date()
    }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    
  }
};
