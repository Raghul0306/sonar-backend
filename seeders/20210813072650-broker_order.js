'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert({ tableName: 'broker_order', schema: 'ticket_exchange' },
        [{
          user_id: 1, 
          event_id:1,
          listing_group_id:1,
          channel_id: 1,
          channel_reference_id: 1,
          transaction_id: 1,
          buyer_user_id: 1,
          order_status_id: 1,
          delivery_status_id: 1,
          delivery_at:new Date(),
          cancelled_order_type_id: 3,
          created_at : new Date(),
          created_by : 1,
          updated_at : new Date(),
          updated_by : 1
        },
        {
          user_id: 1, 
          event_id:2,
          listing_group_id:2,
          channel_id: 1,
          channel_reference_id: 1,
          transaction_id: 1,
          buyer_user_id: 2,
          order_status_id: 2,
          delivery_status_id: 3,
          delivery_at:new Date(),
          cancelled_order_type_id: 3,
          created_at : new Date(),
          created_by : 1,
          updated_at : new Date(),
          updated_by : 1
        },
      ]);
  },

  down: async (queryInterface, Sequelize) => {

  }
};
