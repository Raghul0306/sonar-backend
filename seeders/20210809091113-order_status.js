'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'order_status', schema: 'ticket_exchange' }, [{
      name:"completed",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      name:"active",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      name:"cancelled refund",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      name:"cancelled no refund",
      created_at: new Date(),
      updated_at: new Date()
    }, 

    {
      name:"hold",
      created_at: new Date(),
      updated_at: new Date()
    }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
  }
};
