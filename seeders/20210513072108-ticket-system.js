'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'ticketing_systems', schema: 'ticket_exchange' }, [
      {
        "ticketing_system_name": "Ticketmaster",
        "country_id": 231,
        "url": "www.ticketmaster.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketek",
        "country_id": 13,
        "url": "www.ticketmaster.com.au",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "AXS",
        "country_id": 231,
        "url": "www.axs.com ",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "AXS",
        "country_id": 230,
        "url": "www.axs.com.au",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Tickets.com 231",
        "country_id": 231,
        "url": "www.tickets.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Tickets.co.230",
        "country_id": 230,
        "url": "Tickets.co.230",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Accelevents",
        "country_id": 231,
        "url": "https://www.accelevents.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eventzilla",
        "country_id": 231,
        "url": "https://www.eventzilla.net/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eventbrite",
        "country_id": 231,
        "url": "http://www.eventbrite.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "FareHarbor",
        "country_id": 155,
        "url": "https://fareharbor.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eventix",
        "country_id": 155,
        "url": "https://eventix.io/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Xola",
        "country_id": 231,
        "url": "https://www.xola.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Silent Auction Pro",
        "country_id": 231,
        "url": "https://www.silentauctionpro.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticket Tailor",
        "country_id": 230,
        "url": "https://www.tickettailor.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketSource",
        "country_id": 230,
        "url": "https://www.ticketsource.co.230/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "EventCreate",
        "country_id": 231,
        "url": "https://www.eventcreate.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "RSVPify",
        "country_id": 231,
        "url": "https://rsvpify.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "GiveSmart",
        "country_id": 231,
        "url": "https://www.givesmart.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ThunderTix",
        "country_id": 231,
        "url": "https://www.thundertix.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Help Scout",
        "country_id": 231,
        "url": "https://www.helpscout.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Purplepass Ticketing",
        "country_id": 231,
        "url": "https://www.purplepass.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Bookwhen",
        "country_id": 230,
        "url": "https://bookwhen.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Tix",
        "country_id": 231,
        "url": "https://www.tix.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Picatic",
        "country_id": 38,
        "url": "https://www.picatic.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Showpass",
        "country_id": 38,
        "url": "https://www.showpass.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Brushfire",
        "country_id": 231,
        "url": "https://www.brushfire.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "RepairShopr",
        "country_id": 231,
        "url": "http://www.repairshopr.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Socio",
        "country_id": 231,
        "url": "https://socio.events/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "VBO Tickets",
        "country_id": 231,
        "url": "https://www.vbotickets.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "AudienceView Select",
        "country_id": 38,
        "url": "https://www.audienceview.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Bizzabo",
        "country_id": 231,
        "url": "http://www.bizzabo.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Sparxo",
        "country_id": 231,
        "url": "https://www.sparxo.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Event Essentials",
        "country_id": 231,
        "url": "https://event-essentials.net/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Localist",
        "country_id": 231,
        "url": "http://localist.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "AttendStar",
        "country_id": 231,
        "url": "https://www.attendstar.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "SimpleTix",
        "country_id": 231,
        "url": "https://www.simpletix.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketSpice",
        "country_id": 231,
        "url": "https://www.ticketspice.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Townscript",
        "country_id": 101,
        "url": "http://www.townscript.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "osConcert",
        "country_id": 230,
        "url": "https://osconcert.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "SaffireTix",
        "country_id": 231,
        "url": "https://www.saffire.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Weemss",
        "country_id": "33",
        "url": "https://weemss.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "accesso ShoWare",
        "country_id": 231,
        "url": "https://accesso.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "SeatAdvisor Box Office (SABO)",
        "country_id": 231,
        "url": "http://www.seatadvisor.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Fienta",
        "country_id": "68",
        "url": "https://fienta.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Giveffect",
        "country_id": 231,
        "url": "https://www.giveffect.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Zaui",
        "country_id": 38,
        "url": "https://www.zaui.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Raklet",
        "country_id": 231,
        "url": "http://hello.raklet.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ROLLER",
        "country_id": 231,
        "url": "https://www.roller.software/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "KORONA",
        "country_id": 231,
        "url": "https://koronapos.com/pos-system/ticketing-pos-system/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "EventsFrame",
        "country_id": "57",
        "url": "https://www.eventsframe.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketPeak",
        "country_id": 231,
        "url": "https://ticketpeak.site/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Grenadine Event Planner",
        "country_id": 38,
        "url": "https://events.grenadine.co/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Blackbaud Altru",
        "country_id": 231,
        "url": "https://www.blackbaud.com/products/blackbaud-altru",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "HelloAsso",
        "country_id": 75,
        "url": "http://www.helloasso.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Oveit",
        "country_id": 231,
        "url": "https://oveit.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Events.com",
        "country_id": 231,
        "url": "https://events.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eventdex",
        "country_id": 231,
        "url": "https://www.eventdex.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "PatronManager CRM",
        "country_id": 231,
        "url": "https://patronmanager.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketbud",
        "country_id": 231,
        "url": "http://ticketbud.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "InEvent",
        "country_id": 231,
        "url": "https://inevent.com/en/home.php",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "CrowdChange",
        "country_id": 38,
        "url": "https://www.crowdchange.co/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Glitnir Ticketing System",
        "country_id": 231,
        "url": "http://www.glitnir.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Tessitura",
        "country_id": 231,
        "url": "https://www.tessituranetwork.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Afton Tickets",
        "country_id": 231,
        "url": "https://aftontickets.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Nutickets",
        "country_id": 230,
        "url": "https://www.nutickets.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Brown Paper Tickets",
        "country_id": 231,
        "url": "https://www.brownpapertickets.com/browse.html",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketingHub",
        "country_id": 230,
        "url": "https://www.ticketinghub.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Yapsody",
        "country_id": 231,
        "url": "https://www.yapsody.com/ticketing/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "EcholoN",
        "country_id": 82,
        "url": "https://www.echolon.de/de/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Line-Up",
        "country_id": 230,
        "url": "https://www.lineupnow.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketCo",
        "country_id": 230,
        "url": "https://ticketco.events/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketManager",
        "country_id": 231,
        "url": "https://ticketmanager.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Xorbia Tickets",
        "country_id": 231,
        "url": "https://www.bigtickets.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketsetup",
        "country_id": 231,
        "url": "https://ticketsetup.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Explara",
        "country_id": 101,
        "url": "https://www.explara.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ratality",
        "country_id": 202,
        "url": "https://ratality.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Wintix / Webtix",
        "country_id": 231,
        "url": "https://www.centerstage.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Floktu",
        "country_id": 13,
        "url": "https://www.floktu.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Arts People",
        "country_id": 231,
        "url": "https://www.arts-people.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eventcube",
        "country_id": 230,
        "url": "https://www.eventcube.io/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Agile Ticketing",
        "country_id": 231,
        "url": "https://agiletix.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "EventsCase",
        "country_id": 230,
        "url": "http://www.eventscase.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TryBooking",
        "country_id": 13,
        "url": "https://www.trybooking.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "FreshTix",
        "country_id": 231,
        "url": "https://www.freshtix.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ClearEvent",
        "country_id": 38,
        "url": "http://www.clearevent.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Allcal",
        "country_id": 231,
        "url": "https://allcal.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Herogami",
        "country_id": 231,
        "url": "https://www.herogami.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketLeap",
        "country_id": 231,
        "url": "https://www.ticketleap.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Theatre Manager",
        "country_id": 38,
        "url": "https://help.theatremanager.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Betterez",
        "country_id": 38,
        "url": "https://www.betterez.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eventcombo",
        "country_id": 231,
        "url": "http://www.eventcombo.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "NIGHTOUT",
        "country_id": 231,
        "url": "https://nightout.com/#/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Optimo VENUE & SPACE MANAGEMENT",
        "country_id": 230,
        "url": "https://optimo.software/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "onlive",
        "country_id": "196",
        "url": "https://www.onlive.io/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Easy-To-Use Ticketing Software",
        "country_id": 231,
        "url": "http://www.showclix.com/ticketing",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "MyBoxOffice.us",
        "country_id": 231,
        "url": "https://myboxoffice.us/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Active8",
        "country_id": 231,
        "url": "http://www.active8pos.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Spektrix",
        "country_id": 231,
        "url": "http://www.spektrix.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "High Trek POS",
        "country_id": 231,
        "url": "https://www.hightrekpos.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "RocketRez",
        "country_id": 38,
        "url": "https://rocketrez.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketSauce",
        "country_id": 231,
        "url": "https://ticketsauce.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Evenium.net",
        "country_id": 231,
        "url": "Evenium.net",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "EventBookings",
        "country_id": 13,
        "url": "https://www.eventbookings.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "EventTitans",
        "country_id": 231,
        "url": "https://www.eventtitans.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Etix",
        "country_id": 231,
        "url": "http://www.etix.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Bookitbee",
        "country_id": 230,
        "url": "https://www.bookitbee.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Clubspeed",
        "country_id": 231,
        "url": "https://clubspeed.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "PromoTix",
        "country_id": 231,
        "url": "https://www.promotix.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Bitpod Event",
        "country_id": 231,
        "url": "https://www.bitpod.io/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "O-tix",
        "country_id": 13,
        "url": "http://www.o-tix.com.au/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Galaxy Ticketing & Guest Experience Solution",
        "country_id": 231,
        "url": "https://www.gatewayticketing.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Roboticket",
        "country_id": "175",
        "url": "http://roboticket.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Prekindle",
        "country_id": 231,
        "url": "https://www.prekindle.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "HoldMyTicket",
        "country_id": 231,
        "url": "https://tickets.holdmyticket.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketing 4 Schools",
        "country_id": 231,
        "url": "https://ticketing4schools.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "elevent",
        "country_id": 231,
        "url": "https://getelevent.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Gatemaster",
        "country_id": 231,
        "url": "https://gatemaster.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "egocentric Systems",
        "country_id": 82,
        "url": "http://www.egocentric-systems.de/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ventrata",
        "country_id": 230,
        "url": "https://ventrata.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ezEvent",
        "country_id": 231,
        "url": "https://www.ezevent.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "WP Event Manager",
        "country_id": 82,
        "url": "https://wp-eventmanager.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Coconut Tickets",
        "country_id": 230,
        "url": "https://coconuttickets.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Diobox",
        "country_id": 231,
        "url": "https://d.io/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Rodeo Ticket",
        "country_id": 231,
        "url": "https://www.rodeoticket.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TICKETsage Custom Solutions",
        "country_id": 231,
        "url": "http://www.ticketsage.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Mitingu",
        "country_id": 230,
        "url": "https://www.mitingu.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "seats.io",
        "country_id": 21,
        "url": "https://www.seats.io/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Sensible Cinema",
        "country_id": 231,
        "url": "https://www.sensiblecinema.com//",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "SpinGo",
        "country_id": 231,
        "url": "https://www.spingo.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eventbee",
        "country_id": 231,
        "url": "https://www.eventbee.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eden Workplace",
        "country_id": 231,
        "url": "https://www.edenworkplace.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Dynamic Ticket Solutions",
        "country_id": 231,
        "url": "https://www.dynamicticketsolutions.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "XING Events",
        "country_id": 82,
        "url": "https://www.xing-events.com/en/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "HappsNow",
        "country_id": 231,
        "url": "https://www.happsnow.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ludus",
        "country_id": 21,
        "url": "https://ludus.one/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Repair Spots",
        "country_id": 231,
        "url": "http://www.repairspots.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "The Arts Ticketing Solutions",
        "country_id": 231,
        "url": "https://www.theartsticketingsolutions.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketSocket Enterprise",
        "country_id": 231,
        "url": "https://ticketsocket.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "VIA",
        "country_id": 230,
        "url": "https://www.red61.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Momice",
        "country_id": 155,
        "url": "https://www.momice.com/en/home",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "WizTix",
        "country_id": 231,
        "url": "https://www.wiztix.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ZenTix",
        "country_id": 13,
        "url": "https://zentix.com.au/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ImagiTix",
        "country_id": 231,
        "url": "http://www.imagitix.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Vista Cinema",
        "country_id": 157,
        "url": "https://www.vista.co/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ExtremeTix",
        "country_id": 231,
        "url": "https://hello.etix.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketForce",
        "country_id": 231,
        "url": "http://www.ticketforce.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketOps",
        "country_id": 38,
        "url": "https://www.ticketops.ca/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eventgroove",
        "country_id": 231,
        "url": "https://www.eventgroove.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "JACRO",
        "country_id": 231,
        "url": "https://jacro.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Popklass",
        "country_id": 231,
        "url": "https://popklass.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Seat Engine Ticketing",
        "country_id": 231,
        "url": "https://www.seatengine.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticket Alternative",
        "country_id": 231,
        "url": "http://www.ticketalternative.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketebo",
        "country_id": 13,
        "url": "https://www.ticketebo.com.au/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Tickethome",
        "country_id": "14",
        "url": "http://tickethome.at",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Tito",
        "country_id": "105",
        "url": "https://ti.to/home",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "WebTicketManager",
        "country_id": 230,
        "url": "https://www.webticketmanager.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "WhenNow",
        "country_id": 231,
        "url": "https://whennow.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Aluvii",
        "country_id": 231,
        "url": "http://aluvii.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eventtus",
        "country_id": "229",
        "url": "https://eventtus.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "InstantSeats",
        "country_id": 231,
        "url": "http://www.instantseats.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketpro",
        "country_id": 38,
        "url": "https://www.ticketpro.ca/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketReturn",
        "country_id": 231,
        "url": "http://marketing.ticketreturn.com/home",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Turnit Ride",
        "country_id": "68",
        "url": "https://turnit.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Wakeque",
        "country_id": "58",
        "url": "https://wakeque.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Guest Manager Event Management Platform",
        "country_id": 231,
        "url": "https://www.guestmanager.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Mobile Ticket",
        "country_id": "58",
        "url": "http://www.unwire.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ProVenue",
        "country_id": 231,
        "url": "https://provenue.tickets.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "BilheteiraOnline",
        "country_id": "176",
        "url": "https://www.bol.pt/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "BMI Leisure",
        "country_id": 21,
        "url": "http://www.bmileisure.com/en",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Choice Ticketing",
        "country_id": 231,
        "url": "https://choiceticketing.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Concierge Live",
        "country_id": 231,
        "url": "https://www.conciergelive.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "DataTrax Ticketing Solution",
        "country_id": 38,
        "url": "https://www.datatraxtechnologies.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "e-RegisterNow",
        "country_id": 38,
        "url": "http://e-registernow.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "eRegistration",
        "country_id": 231,
        "url": "https://convergence.net/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Future Ticketing",
        "country_id": "105",
        "url": "https://www.futureticketing.ie/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Inviton",
        "country_id": "197",
        "url": "https://www.inviton.eu/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "MyTicket",
        "country_id": "120",
        "url": "http://kenzap.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Juven Event Management",
        "country_id": "196",
        "url": "https://juven.co/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Online Booking",
        "country_id": 230,
        "url": "http://www.kbgroup230.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "PatronBase",
        "country_id": 230,
        "url": "https://www.patronbase.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Online Ticketing",
        "country_id": 231,
        "url": "http://etracktion.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ploxel",
        "country_id": 230,
        "url": "https://www.ploxel.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Projektron BCS",
        "country_id": 82,
        "url": "https://www.projektron.de/en/bcs/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Smeetz",
        "country_id": "212",
        "url": "https://business.smeetz.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "SalesPoint",
        "country_id": 231,
        "url": "http://www.intouchultima.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketMeister PRO",
        "country_id": 231,
        "url": "http://www.meistersoftware.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketBiscuit",
        "country_id": 231,
        "url": "http://ticketbiscuit/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Titan",
        "country_id": "106",
        "url": "https://titantechgroup.co.il/en/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "We Scan Tickets",
        "country_id": 230,
        "url": "https://wescantickets.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "White Label Tickets",
        "country_id": 38,
        "url": "https://whitelabeltickets.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "BookNow Software",
        "country_id": 230,
        "url": "https://booknowsoftware.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ovations",
        "country_id": 231,
        "url": "https://www.cendynovations.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketCreator",
        "country_id": 82,
        "url": "https://www.ticketcreator.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Wicket Ticketing System",
        "country_id": 231,
        "url": "http://www.washingtonres.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ticketmatic",
        "country_id": 231,
        "url": "https://www.ticketmatic.com/en/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "18Tickets",
        "country_id": 107,
        "url": "https://www.18months.it",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "4flow EMS",
        "country_id": 231,
        "url": "https://www.4flow.com/index.html",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "abfGO",
        "country_id": "223",
        "url": "https://www.abfgo.com/en/home/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Access Hoy",
        "country_id": 205,
        "url": "http://www.accesshoy.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Admit One",
        "country_id": 230,
        "url": "https://www.admit-one.eu/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ATMS+",
        "country_id": 38,
        "url": "https://vantixsystems.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ATicket",
        "country_id": 13,
        "url": "http://www.venssoft.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Attendize",
        "country_id": "105",
        "url": "https://www.attendize.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Attendly",
        "country_id": 13,
        "url": "https://www.attendly.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Auraweb",
        "country_id": 155,
        "url": "https://www.redkarma.eu",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Automated Kiosk Ticketing",
        "country_id": 231,
        "url": "https://livewiredigital.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "B.A.M Ticketing",
        "country_id": "14",
        "url": "https://www.bam.fan/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Bilet",
        "country_id": "193",
        "url": "http://www.ticketsalesoftware.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Billetweb",
        "country_id": 75,
        "url": "https://www.billetweb.fr/en/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Box Office Liaison",
        "country_id": 38,
        "url": "http://boxofficeliaison.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Box Office Software",
        "country_id": 231,
        "url": "http://www.ticketingsystems.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Box Office Xpress",
        "country_id": 38,
        "url": "http://www.boxofficexpress.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Box-Office 2000",
        "country_id": 230,
        "url": "http://www.nortechsoftware.co.230/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "CellarPass Guest Management Platform",
        "country_id": 231,
        "url": "http://www.cellarpass.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "CentreStage",
        "country_id": 230,
        "url": "http://www.deltacomputerservices.co.230",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Charta",
        "country_id": 107,
        "url": "http://www.charta.it/eng.php",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Clappit",
        "country_id": 107,
        "url": "http://www.clappit.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Connfair",
        "country_id": 82,
        "url": "https://www.connfair.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Core POS",
        "country_id": 231,
        "url": "http://ticketevolution.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Crowdcore",
        "country_id": 38,
        "url": "http://crowdcore.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Datakal Ticketing",
        "country_id": "57",
        "url": "http://www.datakal.cz",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "DesignMyNight Ticketing",
        "country_id": 230,
        "url": "http://DesignMyNight.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Details",
        "country_id": 231,
        "url": "http://www.planelements.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "DevKit",
        "country_id": 107,
        "url": "https://devkit.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Digifactory",
        "country_id": 75,
        "url": "https://www.digifactory.fr/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "DigiTickets",
        "country_id": 230,
        "url": "http://www.digitickets.co.230",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "E-Ticketing",
        "country_id": 101,
        "url": "http://crb.co.in",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "e7gezly",
        "country_id": "64",
        "url": "http://www.e7gezly.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eclipse Ticketing",
        "country_id": 230,
        "url": "http://www.eclipseticketing.co.230/en/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "EduTrak eClassTrak",
        "country_id": 231,
        "url": "http://www.edutrak.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Elevate",
        "country_id": 231,
        "url": "http://gingerbreadshed.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Employee Request Manager",
        "country_id": 231,
        "url": "https://www.crowcanyon.com/sharepoint-contact/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Event Ticketing Software",
        "country_id": 231,
        "url": "http://www.myeventhub.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eventfrog",
        "country_id": 82,
        "url": "http://eventfrog.de",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eventix Ticketing Software",
        "country_id": 13,
        "url": "http://www.eventix.com.au",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "EventRay",
        "country_id": 231,
        "url": "https://eventray.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Eventsity",
        "country_id": 13,
        "url": "http://www.eventsity.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "EventSprout",
        "country_id": 231,
        "url": "http://eventsprout.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Expresso Ticketing",
        "country_id": 231,
        "url": "http://www.expressoticketing.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "FanFueled",
        "country_id": 231,
        "url": "http://www.fanfueled.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "FasTrace Ticketing Software",
        "country_id": 21,
        "url": "http://www.zetes.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Flame Booking & Ticketing System",
        "country_id": 230,
        "url": "http://www.flameconcepts.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "FocusPoint360",
        "country_id": 231,
        "url": "http://www.kmitsolutions.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Folio Box Office Software",
        "country_id": 231,
        "url": "http://www.martechsys.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Getix",
        "country_id": 230,
        "url": "https://getix.co",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "glistrr",
        "country_id": 230,
        "url": "https://www.glistrr.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Global Ticket",
        "country_id": 155,
        "url": "https://www.globalticket.nl",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Go-Mus",
        "country_id": 82,
        "url": "https://www.giantmonkey.de",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "GrowTix",
        "country_id": 231,
        "url": "http://www.growtix.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "GS GET",
        "country_id": "132",
        "url": "http://www.globalsoft.com.my",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Guestsnow",
        "country_id": 231,
        "url": "https://guestsnowapp.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "GUTS",
        "country_id": 155,
        "url": "https://guts.tickets",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "HandyTix",
        "country_id": 13,
        "url": "http://www.handytix.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Helm Tickets",
        "country_id": 230,
        "url": "https://helmtickets.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "HelpCenter",
        "country_id": "126",
        "url": "https://www.helpcenterapp.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "IdeasRegistration",
        "country_id": 38,
        "url": "http://www.ideasregistration.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Intix Ticketing",
        "country_id": 13,
        "url": "https://www.in-tix.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Itinio Tour & Ticketing",
        "country_id": 231,
        "url": "http://www.itinio.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Itix",
        "country_id": 155,
        "url": "https://www.itix.nl",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Konnectclub",
        "country_id": 231,
        "url": "https://home.konnectclub.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Mapado Pro",
        "country_id": 75,
        "url": "https://pro.mapado.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Maxim",
        "country_id": 230,
        "url": "http://www.torsystems.co.230",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Meeti",
        "country_id": "228",
        "url": "https://meeti.us",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Mix Tix",
        "country_id": 202,
        "url": "https://mixtix.co.za",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Mobile Ticket App",
        "country_id": 231,
        "url": "http://www.mobileticketapp.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "MobilityTicket",
        "country_id": 107,
        "url": "https://www.movesion.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Monad Ticketing",
        "country_id": 230,
        "url": "http://www.monadticketing.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "New Era Tickets",
        "country_id": 231,
        "url": "https://www.paciolan.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Nliven",
        "country_id": 231,
        "url": "https://www.tixtrack.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "O-CITY",
        "country_id": "212",
        "url": "https://www.o-city.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "OOOH.Events",
        "country_id": 107,
        "url": "https://oooh.events",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Open ABT",
        "country_id": 231,
        "url": "http://openabt.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Oscar",
        "country_id": 230,
        "url": "http://www.savoysystems.co.230",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Outbound Online Solutions",
        "country_id": 231,
        "url": "http://www.outboundsoftware.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Palco4",
        "country_id": 205,
        "url": "https://palco4.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Parafait",
        "country_id": 101,
        "url": "https://www.semnox.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Patron Assist",
        "country_id": 231,
        "url": "https://patronassist.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Paydro",
        "country_id": 155,
        "url": "https://paydro.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Paylogic",
        "country_id": 155,
        "url": "https://www.paylogic.com/en/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Peatix",
        "country_id": 231,
        "url": "https://peatix.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Pleasant Tickets",
        "country_id": 38,
        "url": "http://www.pleasant-tickets.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "POSitive Cinema",
        "country_id": "175",
        "url": "https://positivecinema.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Pretix",
        "country_id": 82,
        "url": "https://pretix.eu/about/en/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Printix Pro",
        "country_id": 38,
        "url": "http://www.vtix.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ProTicket",
        "country_id": 82,
        "url": "https://www.proticket.info/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Qcue",
        "country_id": 231,
        "url": "http://www.qcue.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Quantix",
        "country_id": 231,
        "url": "http://www.quantixpos.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Radario Marketing Platform",
        "country_id": 231,
        "url": "http://radario.cc",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ready Theatre Systems",
        "country_id": 231,
        "url": "https://www.rts-solutions.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ReplyBuy",
        "country_id": 231,
        "url": "http://www.replybuy.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Rodrigue",
        "country_id": 75,
        "url": "http://rodrigue-solution.com/en/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Roftr",
        "country_id": 101,
        "url": "http://www.roftr.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "SAP Event Ticketing",
        "country_id": 231,
        "url": "https://www.sap.com/products/crm.html",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Seatedly",
        "country_id": 230,
        "url": "http://seatedly.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Seatlion",
        "country_id": "212",
        "url": "https://www.seatlion.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Selling Ticket Box Office",
        "country_id": 231,
        "url": "http://www.sellingticket.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Softix",
        "country_id": 13,
        "url": "http://www.softix.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Soiree Sympa",
        "country_id": 75,
        "url": "https://www.soireesympa.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Spectatix Sales",
        "country_id": 230,
        "url": "http://www.axlr8.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Sportals",
        "country_id": 230,
        "url": "http://www.sportals.online",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Stride Events",
        "country_id": 231,
        "url": "http://www.strideevents.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Symfonee Ticketing System",
        "country_id": 231,
        "url": "http://www.symfonee.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "SysRod",
        "country_id": "30",
        "url": "https://www.rodosoft.com.br",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TapReports",
        "country_id": 231,
        "url": "http://ticketmac.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Syx",
        "country_id": "14",
        "url": "http://www.syxautomations.com/en",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Telemaco",
        "country_id": 107,
        "url": "https://www.pluservice.net",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Tessitura Bridge",
        "country_id": 230,
        "url": "https://afterdigital.co.230",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "The Electronic Box Office",
        "country_id": "106",
        "url": "http://www.7bear.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "The Little Box Office",
        "country_id": 230,
        "url": "http://www.thelittleboxoffice.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "The Virtual Ticketer",
        "country_id": 231,
        "url": "https://www.vts-no.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Theatre Point-of-Sale",
        "country_id": 231,
        "url": "http://www.retrieversoftwareinc.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TI360",
        "country_id": "198",
        "url": "http://wearecreativio.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticket ABC",
        "country_id": "105",
        "url": "http://www.ticketabc.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticket Booking System",
        "country_id": 230,
        "url": "http://www.e-availability.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticket Forest",
        "country_id": 231,
        "url": "https://ticketforest.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticket Innovation Hub",
        "country_id": 155,
        "url": "http://ticketinnovationhub.nl",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticket Rocket",
        "country_id": 38,
        "url": "http://ticketrocket.co/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketbooth",
        "country_id": 13,
        "url": "http://www.ticketbooth.com.au",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketDesq",
        "country_id": 13,
        "url": "https://www.ticketdesq.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketEase",
        "country_id": 230,
        "url": "https://ticketease.co.230",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketing Done Right",
        "country_id": 231,
        "url": "http://www.flavorus.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketing Software",
        "country_id": 155,
        "url": "http://www.esbtsb.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketing Solutions by OMNI",
        "country_id": 231,
        "url": "http://www.omniticket.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketing System",
        "country_id": 231,
        "url": "http://www.gdeasia.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TICKETINO",
        "country_id": "212",
        "url": "https://organizer.ticketino.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketIQ",
        "country_id": 231,
        "url": "https://aisera.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketlight",
        "country_id": 230,
        "url": "http://www.ticketlinesolutions.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ticketscript",
        "country_id": 155,
        "url": "http://www.ticketscript.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketsolve",
        "country_id": "105",
        "url": "http://www.ticketsolve.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketTool",
        "country_id": "228",
        "url": "https://tickettool.net",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketure",
        "country_id": 231,
        "url": "https://www.ticketure.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TicketVoodoo",
        "country_id": 230,
        "url": "http://www.ticketvoodoo.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TiCKiTAP",
        "country_id": 107,
        "url": "http://www.petalconsulting.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Tickster",
        "country_id": "211",
        "url": "https://about.tickster.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Tikkl",
        "country_id": 231,
        "url": "http://www.tikkl.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Timely Event Ticketing",
        "country_id": 38,
        "url": "https://time.ly/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "tix123",
        "country_id": 38,
        "url": "http://www.tix123.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TixHub",
        "country_id": 38,
        "url": "http://www.tixhub.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Tixily",
        "country_id": 231,
        "url": "https://tixily.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TM15",
        "country_id": 231,
        "url": "http://www.printbox.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "TSM/Ticketing",
        "country_id": 230,
        "url": "http://www.medoc.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Vanco Events",
        "country_id": 231,
        "url": "https://www.vancopayments.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Veezi",
        "country_id": 157,
        "url": "http://www.veezi.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "VIBUS",
        "country_id": 82,
        "url": "http://www.swh-software.de",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Vivaticket",
        "country_id": 107,
        "url": "https://corporate.vivaticket.com/it",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Vix Pulse",
        "country_id": 13,
        "url": "https://vixtechnology.com/",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Weezevent",
        "country_id": 75,
        "url": "https://weezevent.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Xceed Nightgraph",
        "country_id": 205,
        "url": "http://www.xceed.me",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Yodel",
        "country_id": 231,
        "url": "http://www.yodelpass.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Yurplan",
        "country_id": 75,
        "url": "https://yurplan.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Stubhub",
        "country_id": 231,
        "url": "https://www.stubhub.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Viagogo",
        "country_id": 231,
        "url": "https://www.viagogo.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Seatgeek",
        "country_id": 231,
        "url": "https://www.seatgeek.com",
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Vivid Seats",
        "country_id": 231,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Gametime",
        "country_id": 231,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticket Network",
        "country_id": 231,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticket Evolution",
        "country_id": 231,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Yadara",
        "country_id": 231,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Tixstock",
        "country_id": 230,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticketmaster Resale",
        "country_id": 231,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "ViaSeat",
        "country_id": 230,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Ticombo",
        "country_id": 82,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Live Football Tickets",
        "country_id": 82,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Gigsberg",
        "country_id": 82,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Vibetickets",
        "country_id": 230,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Stubhub International",
        "country_id": 205,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      },
      {
        "ticketing_system_name": "Fanpass",
        "country_id": 230,
        "active": "1",
        "created_at": new Date(),
        "updated_at": new Date()
      }

    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ticketing_systems', null, {});
  }
};
