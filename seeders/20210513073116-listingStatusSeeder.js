'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'listing_status', schema: 'ticket_exchange' }, [
    {
      status:"qc_hold",
      name:"QC Hold",
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      status:"active",
      name:"Active",
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      status:"inactive",
      name:"Inactive",
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      status:"sold",
      name:"Sold",
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      status:"viagogo_hold",
      name:"Viagogo Hold",
      created_at: new Date(),
      updated_at: new Date()
    }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('listing_status', null, {});
  }
};
