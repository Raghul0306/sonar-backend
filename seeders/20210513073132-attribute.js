'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'attributes', schema: 'ticket_exchange' }, [{
      attribute_name:"attribute value one",      
      active:1,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      attribute_name:"attribute value two",      
      active:1,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      attribute_name:"attribute value three",      
      active:1,
      created_at: new Date(),
      updated_at: new Date()
    }
    
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('attributes', null, {});
  }
};
