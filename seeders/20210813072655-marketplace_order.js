'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert({ tableName: 'marketplace_order', schema: 'ticket_exchange' },
        [{
          broker_order_id: 1, 
          event_id:1,
          listing_group_id:1,
          buyer_user_id: 1, 
          user_id: 1, 
          order_status_id: 2,
          transaction_id:1,
          delivery_status_id: 3,
          delivery_at: new Date(), 
          created_at : new Date(),
          created_by : 1,
          updated_at : new Date(),
          updated_by : 1
        }
      ]);
  },

  down: async (queryInterface, Sequelize) => {

  }
};
