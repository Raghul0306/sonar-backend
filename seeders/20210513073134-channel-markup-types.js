'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'channel_markup_types', schema: 'ticket_exchange' }, [{
      channel_markup_name_type_name:"%",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      channel_markup_name_type_name:"$",
      created_at: new Date(),
      updated_at: new Date()
    }
    
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('channel_markup_types', null, {});
  }
};
