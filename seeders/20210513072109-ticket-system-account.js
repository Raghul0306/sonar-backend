'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'ticketing_system_accounts', schema: 'ticket_exchange' }, [{
      ticketing_system_id:1,
      user_id:4,
      parent_id:5,
      account_name:"brokerusermytickets@gmail.com",
      active:1,
      created_at: new Date(),
      updated_at: new Date()
    }
    
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ticketing_system_accounts', null, {});
  }
};
