'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'user_roles', schema: 'ticket_exchange' }, [
    {
      user_role_name: 'super_super_admin',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'super_admin',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'admin',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'broker',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'broker_user',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'accounting_admin',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'accounting_user',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'processing_admin',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'processing_user',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'marketplace_admin',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'marketplace_user',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'buyer',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'support',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'support_admin',
      created_at: new Date(),
      updated_at: new Date()
    },{
      user_role_name: 'support_user',
      created_at: new Date(),
      updated_at: new Date()
    }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_roles', null, {});
  }
};
