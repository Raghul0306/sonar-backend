'use strict';
let events = [
  {
    "name": "Keane",
    "category_id": 1,
    "performer_id": 1,
    "opponent_id": 2,
    "venue_id": 2,
    "tickettype_id": 0,
    "created_by": 1,
    "date": "2021-12-25 14:26:57.47",
    "genre_id": 1,
    "eventdate_day": "monday",
    "event_status_id": 2,
    "created_at": new Date(),
    "updated_at": new Date(),
    "mercury_id": 4398587
  },
  {
    "name": "Ricky Gervais - Super Nature",
    "category_id": 1,
    "performer_id": 1,
    "opponent_id": 2,
    "venue_id": 2,
    "tickettype_id": 0,
    "created_by": 1,
    "date": "2021-12-25 14:26:57.47",
    "genre_id": 1,
    "eventdate_day": "monday",
    "event_status_id": 2,
    "created_at": new Date(),
    "updated_at": new Date(),
    "mercury_id": 4398587
  },
  {
    "name": "An Evening with Michael Buble",
    "category_id": 1,
    "performer_id": 1,
    "opponent_id": 2,
    "venue_id": 2,
    "tickettype_id": 0,
    "created_by": 1,
    "date": "2021-12-25 14:26:57.47",
    "genre_id": 1,
    "eventdate_day": "monday",
    "event_status_id": 2,
    "created_at": new Date(),
    "updated_at": new Date(),
    "mercury_id": 4398587
  },
  {
    "name": "Live At The Royal Crescent - Olly Murs",
    "category_id": 1,
    "performer_id": 1,
    "opponent_id": 2,
    "venue_id": 2,
    "tickettype_id": 0,
    "created_by": 1,
    "date": "2021-12-25 14:26:57.47",
    "genre_id": 1,
    "eventdate_day": "monday",
    "event_status_id": 2,
    "created_at": new Date(),
    "updated_at": new Date(),
    "mercury_id": 4398587
  },
  {
    "name": "You Me At Six",
    "category_id": 1,
    "performer_id": 1,
    "opponent_id": 2,
    "venue_id": 2,
    "tickettype_id": 0,
    "created_by": 1,
    "date": "2021-12-25 14:26:57.47",
    "genre_id": 1,
    "eventdate_day": "monday",
    "event_status_id": 2,
    "created_at": new Date(),
    "updated_at": new Date(),
    "mercury_id": 4398587
  },
  {
    "name": "The Specials",
    "category_id": 1,
    "performer_id": 1,
    "opponent_id": 2,
    "venue_id": 2,
    "tickettype_id": 0,
    "created_by": 1,
    "date": "2021-12-25 14:26:57.47",
    "genre_id": 1,
    "eventdate_day": "monday",
    "event_status_id": 2,
    "created_at": new Date(),
    "updated_at": new Date(),
    "mercury_id": 4398587
  },
  {
    "name": "The Dualers",
    "category_id": 1,
    "performer_id": 1,
    "opponent_id": 2,
    "venue_id": 2,
    "tickettype_id": 0,
    "created_by": 1,
    "date": "2021-12-25 14:26:57.47",
    "genre_id": 1,
    "eventdate_day": "monday",
    "event_status_id": 2,
    "created_at": new Date(),
    "updated_at": new Date(),
    "mercury_id": 4398587
  },
];

let vendors = [
  {
    name: "TicketMaster",
    address: "Viale Monza 347",
    zip: "20126",
    city: "Milan (MI)",
    state: "Italy",
    country: "Italy",
    country_id: "107",
    email: "Tickets@email.ticketmaster.co.uk",
    phone: "39 02 45074055",
    url: "https://www.18months.it",
    created_at: new Date()
  },

];

let ticketingSystemAccounts = [
  {
    ticketing_system_id:1,
    user_id:4,
    parent_id:5,
    account_name:"mfodor@decoreglimmer.com",
    active:1,
    created_at: new Date(),
    updated_at: new Date()
  },
  {
    ticketing_system_id:1,
    user_id:4,
    parent_id:5,
    account_name:"clawrence@burtsbricks.com",
    active:1,
    created_at: new Date(),
    updated_at: new Date()
  },
  {
    ticketing_system_id:1,
    user_id:4,
    parent_id:5,
    account_name:"petrafar@billykins.com",
    active:1,
    created_at: new Date(),
    updated_at: new Date()
  },
  {
    ticketing_system_id:1,
    user_id:4,
    parent_id:5,
    account_name:"barcinh@rocketsstartup.com",
    active:1,
    created_at: new Date(),
    updated_at: new Date()
  },
  {
    ticketing_system_id:1,
    user_id:4,
    parent_id:5,
    account_name:"marckneller@unitedhealthenterprises.com",
    active:1,
    created_at: new Date(),
    updated_at: new Date()
  },
  {
    ticketing_system_id:1,
    user_id:4,
    parent_id:5,
    account_name:"erzsebetal@billykins.com",
    active:1,
    created_at: new Date(),
    updated_at: new Date()
  }
];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let eventsIsDone = await queryInterface.bulkInsert({ tableName: 'event', schema: 'ticket_exchange' }, events,{returning: true});
    let vendorsIsDone = await queryInterface.bulkInsert({ tableName: 'vendor', schema: 'ticket_exchange' }, vendors,{returning: true});
    let ticketingSystemAccountsIsDone = await queryInterface.bulkInsert({ tableName: 'ticketing_system_accounts', schema: 'ticket_exchange' }, ticketingSystemAccounts,{returning: true});
    return (eventsIsDone && vendorsIsDone && ticketingSystemAccountsIsDone)
  },

  down: async (queryInterface, Sequelize) => {
    let eventsIsDone = await queryInterface.bulkDelete('event', null, {});
    let vendorsIsDone = await queryInterface.bulkDelete('vendor', null, {});
    let ticketingSystemAccountsIsDone = await queryInterface.bulkDelete('ticketing_system_accounts', null, {});
    return (eventsIsDone && vendorsIsDone && ticketingSystemAccountsIsDone)
  }
};
