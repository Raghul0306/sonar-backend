'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'splits', schema: 'ticket_exchange' }, [{
      name: 'Custom',
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'Any',
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'Never Leave One',
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'Default',
      created_at: new Date(),
      updated_at: new Date()
    },{
        name: 'No-Splits',
        created_at: new Date(),
        updated_at: new Date()
      }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('splits', null, {});
  }
};
