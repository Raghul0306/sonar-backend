'use strict';

let purchaseOrderListings = [
    {
        "purchase_order_id": "5",
        "listing_id": "22"
    },
    {
        "purchase_order_id": "5",
        "listing_id": "21"
    },
    {
        "purchase_order_id": "5",
        "listing_id": "23"
    },
    {
        "purchase_order_id": "2",
        "listing_id": "16"
    },
    {
        "purchase_order_id": "2",
        "listing_id": "15"
    },
    {
        "purchase_order_id": "3",
        "listing_id": "17"
    },
    {
        "purchase_order_id": "4",
        "listing_id": "19"
    },
    {
        "purchase_order_id": "4",
        "listing_id": "20"
    },
    {
        "purchase_order_id": "1",
        "listing_id": "13"
    },
    {
        "purchase_order_id": "3",
        "listing_id": "18"
    },
    {
        "purchase_order_id": "5",
        "listing_id": "24"
    },
    {
        "purchase_order_id": "1",
        "listing_id": "14"
    }
]

  
  
module.exports = {
  up: async (queryInterface, Sequelize) => {

    purchaseOrderListings = purchaseOrderListings.map(purchaseOrderListing=>{
     return {
      purchase_order_id: Number(purchaseOrderListing.purchase_order_id),
      listing_id: Number(purchaseOrderListing.listing_id)
     }
    });

    return await queryInterface.bulkInsert({ tableName: 'purchase_order_listing', schema: 'ticket_exchange' },  purchaseOrderListings);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('purchase_order_listing', null, {});
  }
};
