'use strict';
const bcrypt = require("bcrypt");
const awsHelper = require('../helpers/awskms')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    var userPassword = await awsHelper.awsEncrypt(await bcrypt.hash('Mytickets@123', 10));
    return queryInterface.bulkInsert({ tableName: 'user_account', schema: 'ticket_exchange_private' }, [
    
      {
        user_id: 1,
        email: 'admin@phunnel.co',
        password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 2,
        email: 'superadmin@phunnel.com',
        password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 3,
        email: 'repsadmin@gmail.com',
        password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 4,
        email: 'marketplaceadmn.mytickets@gmail.com',
        password_hash: userPassword,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 5,
        email: 'marketplaceusermytickets@gmail.com',
        password_hash: userPassword,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 6,
        email: 'brokeradm.mytickets@gmail.com',
        password_hash: userPassword,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 7,
        email: 'brokerusermytickets@gmail.com',
        password_hash: userPassword,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 8,
        email: 'accountingadmin@gmail.com',
        password_hash: userPassword,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 9,
        email: 'accountinguser@gmail.com',
        password_hash: userPassword,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 10,
        email: 'processingadmin@gmail.com',
        password_hash: userPassword,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 11,
        email: 'processinguser@gmail.com',
        password_hash: userPassword,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 12,
        email: 'nick@reps2.com',
        password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 13,
        email: 'brian@reps2.com',
        password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 14,
        email: 'ryan@reps2.com',
        password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 15,
        email: 'erik@reps2.com',
        password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 16,
        email: 'tim@phunnel.co',
        password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 17,
        email: 'supportadmin@gmail.com',
        password_hash: userPassword,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 18,
        email: 'supportuser@gmail.com',
        password_hash: userPassword,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 19,
        email: 'rubsadmin@gmail.com',
        password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 20,
        email: 'qualiaadmin@gmail.com',
        password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 21,
        email: 'atheleteadmin@gmail.com',
        password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_id: 22,
        email: 'ticketresaleadmin@gmail.com',
        password_hash: await awsHelper.awsEncrypt(await bcrypt.hash('Admin@123', 10)),
        created_at: new Date(),
        updated_at: new Date()
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_account', null, {});
  }
};
