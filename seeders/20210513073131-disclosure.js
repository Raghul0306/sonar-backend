'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'disclosures', schema: 'ticket_exchange' }, [
    {
        disclosure_name: "18+ Section",
        description: "Ticket holder must be at least 18 years old to enter.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "21+ Section",
        description: "Ticket holder must be at least 21 years old to enter.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "ADA wheelchair accessible",
        description: "Seating reserved for wheelchair attendees and their companions.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "ADA wheelchair accessible only",
        description: "Area is reserved for a wheelchair, there is no physical seat.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Alcohol free section",
        description: "Alcohol is not permitted in this section.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Bar Stool Seating",
        description: "Seat is a bar stool.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Behind protective netting",
        description: "Seat is located behind a protective net, view will have a net at all times in front of the action.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Behind the stage",
        description: "Seat is located behind the stage, the view may be impacted.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Child ticket 16 and under",
        description: "Ticket holder must be a child aged 16 years or younger to enter.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Extreme side printed on ticket",
        description: "Seats are on the side of the stage. The view is likely to be obstructed due to the location of the seat.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Folding chair printed on ticket",
        description: "The seat is in a temporary folding chair.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Fully vaccinated",
        description: "Ticket holder must be fully vaccinated and provide proof of vaccination to gain entry.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Limited or Obstructed view printed on ticket",
        description: "View from seat is obstructed by an object between the seat and the performer. ",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Limited side view printed on ticket",
        description: "Seat is located on the side of the stage and as a result part of the view of the stage will be obstructed.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Limited view of Jumbotron/video screen printed on ticket",
        description: "Seat is located in a position where the view of the video screen is limited.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Negative COVID-19 test required for entry",
        description: "Proof of a negative COVID-19 test required for entry. See venue policy for more information. ",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "No video printed on ticket",
        description: "Patrons may not record video while attending the event.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "No view printed on ticket",
        description: "There is no view of the event from the seat.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Partial Suite shared - Non-reserved Seating",
        description: "The ticket is for a suite that is shared amongst several patrons. There are no reserved seats, and there may not be enough seats to accomodate every person with a suite ticket.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Partial suite standing room",
        description: "The ticket is for a suite that is shared amongst several patrons. There are no reserved seats, and there may not be enough seats to accomodate every person with a suite ticket.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Partial suite with reserved seating",
        description: "The ticket is for a suite that is shared amongst several patrons. Each patron has a reserved seat.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Partial view",
        description: "There is only a partial view of the action from the seat.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Piggy back seats",
        description: "One seat is behind the other seat.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Possible obstruction",
        description: "View from seat is obstructed by an object between the seat and the performer. ",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Print at home",
        description: "Print at Home tickets must be printed and displayed on paper. The venue will not scan tickets shown on a smart phone.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Printing Required",
        description: "Printing Required tickets must be printed and displayed on paper. The venue will not scan tickets shown on a smart phone.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Railing view",
        description: "View from seat is obstructed by a railing.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Rear view",
        description: "Seat is located behind the stage, the view may be impacted.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Restricted leg room",
        description: "The amount of leg room in this seat is limited and less than typical seating.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Restricted view printed on ticket",
        description: "View from seat is restricted.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Seat behind pipe printed on ticket",
        description: "View from seat is obstructed by a pipe.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Seats are Socially Distanced",
        description: "The seats will be spaced out according to social distancing guidelines. This can either be physically distant seats or blocked off seats to allow for social distancing amongst patrons.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Semi-ambulatory seating",
        description: "Seats for patrons with disabilities who require additional leg room.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Senior Ticket",
        description: "Ticket holder must be at least 65 years of age to use this ticket.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Side stage printed on ticket",
        description: "Seat is located at the side of the stage, the view may be impacted.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Side view printed on ticket",
        description: "Seat is located at the side of the stage, the view may be impacted.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Speaker seats",
        description: "Seat is located in front of a speaker.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Standing room only",
        description: "Standing Room Only tickets provide access to the venue, often including a specific area to watch the event. Standing Room Only tickets do not offer a seat.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Student ticket ID required",
        description: "These tickets require a student ID in order to gain entry into the event. Ticket holders without a student ID will not be admitted.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Under overhang",
        description: "These seats are located underneath an overhang. ",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Vaccination required",
        description: "Ticket holder must be fully vaccinated and provide proof of vaccination to gain entry.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Wheelchair accessible",
        description: "Seating reserved for wheelchair attendees and their companions.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Wheelchair accessible only",
        description: "Area is reserved for a wheelchair, there is no physical seat.",
        created_at: new Date(),
        updated_at: new Date()
    },
    {
        disclosure_name: "Wheelchair companion seats",
        description: "Seats that are next to wheelchair accessible seating. They are inteneded to be used in conjunction with a wheelchair only ticket.",
        created_at: new Date(),
        updated_at: new Date()
    }
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('disclosures', null, {});
  }
};
