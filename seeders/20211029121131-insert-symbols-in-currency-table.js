'use strict';

let currencySymbols = [
  {
      code: "AED",
      symbol: "د.إ"
  },
  {
      code: "AFN",
      symbol: "Af"
  },
  {
      code: "ALL",
      symbol: "L"
  },
  {
      code: "AMD",
      symbol: "Դ"
  },
  {
      code: "AOA",
      symbol: "Kz"
  },
  {
      code: "ARS",
      symbol: "$"
  },
  {
      code: "AUD",
      symbol: "$"
  },
  {
      code: "AWG",
      symbol: "ƒ"
  },
  {
      code: "AZN",
      symbol: "ман"
  },
  {
      code: "BAM",
      symbol: "КМ"
  },
  {
      code: "BBD",
      symbol: "$"
  },
  {
      code: "BDT",
      symbol: "৳"
  },
  {
      code: "BGN",
      symbol: "лв"
  },
  {
      code: "BHD",
      symbol: "ب.د"
  },
  {
      code: "BIF",
      symbol: "₣"
  },
  {
      code: "BMD",
      symbol: "$"
  },
  {
      code: "BND",
      symbol: "$"
  },
  {
      code: "BOB",
      symbol: "Bs."
  },
  {
      code: "BRL",
      symbol: "R$"
  },
  {
      code: "BSD",
      symbol: "$"
  },
  {
      code: "BTN",
      symbol: ""
  },
  {
      code: "BWP",
      symbol: "P"
  },
  {
      code: "BYN",
      symbol: "Br"
  },
  {
      code: "BZD",
      symbol: "$"
  },
  {
      code: "CAD",
      symbol: "$"
  },
  {
      code: "CDF",
      symbol: "₣"
  },
  {
      code: "CHF",
      symbol: "₣"
  },
  {
      code: "CLP",
      symbol: "$"
  },
  {
      code: "CNY",
      symbol: "¥"
  },
  {
      code: "COP",
      symbol: "$"
  },
  {
      code: "CRC",
      symbol: "₡"
  },
  {
      code: "CUP",
      symbol: "$"
  },
  {
      code: "CVE",
      symbol: "$"
  },
  {
      code: "CZK",
      symbol: "Kč"
  },
  {
      code: "DJF",
      symbol: "₣"
  },
  {
      code: "DKK",
      symbol: "kr"
  },
  {
      code: "DOP",
      symbol: "$"
  },
  {
      code: "DZD",
      symbol: "د.ج"
  },
  {
      code: "EGP",
      symbol: "£"
  },
  {
      code: "ERN",
      symbol: "Nfk"
  },
  {
      code: "ETB",
      symbol: ""
  },
  {
      code: "EUR",
      symbol: "€"
  },
  {
      code: "FJD",
      symbol: "$"
  },
  {
      code: "FKP",
      symbol: "£"
  },
  {
      code: "GBP",
      symbol: "£"
  },
  {
      code: "GEL",
      symbol: "ლ"
  },
  {
      code: "GHS",
      symbol: "₵"
  },
  {
      code: "GIP",
      symbol: "£"
  },
  {
      code: "GMD",
      symbol: "D"
  },
  {
      code: "GNF",
      symbol: "₣"
  },
  {
      code: "GTQ",
      symbol: "Q"
  },
  {
      code: "GYD",
      symbol: "$"
  },
  {
      code: "HKD",
      symbol: "$"
  },
  {
      code: "HNL",
      symbol: "L"
  },
  {
      code: "HRK",
      symbol: "Kn"
  },
  {
      code: "HTG",
      symbol: "G"
  },
  {
      code: "HUF",
      symbol: "Ft"
  },
  {
      code: "IDR",
      symbol: "Rp"
  },
  {
      code: "ILS",
      symbol: "₪"
  },
  {
      code: "INR",
      symbol: "₹"
  },
  {
      code: "IQD",
      symbol: "ع.د"
  },
  {
      code: "IRR",
      symbol: "﷼"
  },
  {
      code: "ISK",
      symbol: "Kr"
  },
  {
      code: "JMD",
      symbol: "$"
  },
  {
      code: "JOD",
      symbol: "د.ا"
  },
  {
      code: "JPY",
      symbol: "¥"
  },
  {
      code: "KES",
      symbol: "Sh"
  },
  {
      code: "KGS",
      symbol: ""
  },
  {
      code: "KHR",
      symbol: "៛"
  },
  {
      code: "KPW",
      symbol: "₩"
  },
  {
      code: "KRW",
      symbol: "₩"
  },
  {
      code: "KWD",
      symbol: "د.ك"
  },
  {
      code: "KYD",
      symbol: "$"
  },
  {
      code: "KZT",
      symbol: "〒"
  },
  {
      code: "LAK",
      symbol: "₭"
  },
  {
      code: "LBP",
      symbol: "ل.ل"
  },
  {
      code: "LKR",
      symbol: "Rs"
  },
  {
      code: "LRD",
      symbol: "$"
  },
  {
      code: "LSL",
      symbol: "L"
  },
  {
      code: "LYD",
      symbol: "ل.د"
  },
  {
      code: "MAD",
      symbol: "د.م."
  },
  {
      code: "MDL",
      symbol: "L"
  },
  {
      code: "MGA",
      symbol: ""
  },
  {
      code: "MKD",
      symbol: "ден"
  },
  {
      code: "MMK",
      symbol: "K"
  },
  {
      code: "MNT",
      symbol: "₮"
  },
  {
      code: "MOP",
      symbol: "P"
  },
  {
      code: "MRU",
      symbol: "UM"
  },
  {
      code: "MUR",
      symbol: "₨"
  },
  {
      code: "MVR",
      symbol: "ރ."
  },
  {
      code: "MWK",
      symbol: "MK"
  },
  {
      code: "MXN",
      symbol: "$"
  },
  {
      code: "MYR",
      symbol: "RM"
  },
  {
      code: "MZN",
      symbol: "MTn"
  },
  {
      code: "NAD",
      symbol: "$"
  },
  {
      code: "NGN",
      symbol: "₦"
  },
  {
      code: "NIO",
      symbol: "C$"
  },
  {
      code: "NOK",
      symbol: "kr"
  },
  {
      code: "NPR",
      symbol: "₨"
  },
  {
      code: "NZD",
      symbol: "$"
  },
  {
      code: "OMR",
      symbol: "ر.ع."
  },
  {
      code: "PAB",
      symbol: "B/."
  },
  {
      code: "PEN",
      symbol: "S/."
  },
  {
      code: "PGK",
      symbol: "K"
  },
  {
      code: "PHP",
      symbol: "₱"
  },
  {
      code: "PKR",
      symbol: "₨"
  },
  {
      code: "PLN",
      symbol: "zł"
  },
  {
      code: "PYG",
      symbol: "₲"
  },
  {
      code: "QAR",
      symbol: "ر.ق"
  },
  {
      code: "RON",
      symbol: "L"
  },
  {
      code: "RSD",
      symbol: "din"
  },
  {
      code: "RUB",
      symbol: "р."
  },
  {
      code: "RWF",
      symbol: "₣"
  },
  {
      code: "SAR",
      symbol: "ر.س"
  },
  {
      code: "SBD",
      symbol: "$"
  },
  {
      code: "SCR",
      symbol: "₨"
  },
  {
      code: "SDG",
      symbol: "£"
  },
  {
      code: "SEK",
      symbol: "kr"
  },
  {
      code: "SGD",
      symbol: "$"
  },
  {
      code: "SHP",
      symbol: "£"
  },
  {
      code: "SLL",
      symbol: "Le"
  },
  {
      code: "SOS",
      symbol: "Sh"
  },
  {
      code: "SRD",
      symbol: "$"
  },
  {
      code: "STN",
      symbol: "Db"
  },
  {
      code: "SYP",
      symbol: "ل.س"
  },
  {
      code: "SZL",
      symbol: "L"
  },
  {
      code: "THB",
      symbol: "฿"
  },
  {
      code: "TJS",
      symbol: "ЅМ"
  },
  {
      code: "TMT",
      symbol: "m"
  },
  {
      code: "TND",
      symbol: "د.ت"
  },
  {
      code: "TOP",
      symbol: "T$"
  },
  {
      code: "TRY",
      symbol: "₤"
  },
  {
      code: "TTD",
      symbol: "$"
  },
  {
      code: "TWD",
      symbol: "$"
  },
  {
      code: "TZS",
      symbol: "Sh"
  },
  {
      code: "UAH",
      symbol: "₴"
  },
  {
      code: "UGX",
      symbol: "Sh"
  },
  {
      code: "USD",
      symbol: "$"
  },
  {
      code: "UYU",
      symbol: "$"
  },
  {
      code: "UZS",
      symbol: ""
  },
  {
      code: "VEF",
      symbol: "Bs F"
  },
  {
      code: "VND",
      symbol: "₫"
  },
  {
      code: "VUV",
      symbol: "Vt"
  },
  {
      code: "WST",
      symbol: "T"
  },
  {
      code: "XAF",
      symbol: "₣"
  },
  {
      code: "XCD",
      symbol: "$"
  },
  {
      code: "XPF",
      symbol: "₣"
  },
  {
      code: "YER",
      symbol: "﷼"
  },
  {
      code: "ZAR",
      symbol: "R"
  },
  {
      code: "ZMW",
      symbol: "ZK"
  },
  {
      code: "ZWL",
      symbol: "$"
  }
]

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all(currencySymbols.map(data=>queryInterface.sequelize.query(`UPDATE ticket_exchange.currency SET symbol = '${data.symbol}' WHERE iso_code = '${data.code}'`)))
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all(currencySymbols.map(data=>queryInterface.sequelize.query(`UPDATE ticket_exchange.currency SET symbol = '${data.symbol}' WHERE iso_code = '${data.code}'`)));
  }
};
