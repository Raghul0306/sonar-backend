'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'companies', schema: 'ticket_exchange' }, [
    
    {
      company_name: 'Phunnel',
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date()
    },
    {
      company_name: 'Athlete Management Australia',
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date()
    },
    {
      company_name: 'The Ticket Resale Company',
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date()
    },
    {
      company_name: 'Reps & Company',
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date()
    },
    {
      company_name: 'Reps',
      parent_company_id: 4, 
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date()
    },
    {
      company_name: 'Rubs',
      parent_company_id: 4, 
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date()
    },
    {
      company_name: 'Qualia',
      parent_company_id: 4, 
      created_at: new Date(),
      created_by: 1,
      updated_at: new Date()
    },
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('companies', null, {});
  }
};
