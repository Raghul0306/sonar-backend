'use strict';

let datas = [
  {
    id: 1,
    company_id:1
  },
  {
    id: 2,
    company_id:4
  },
  {
    id: 3,
    company_id:5
  },
  {
    id: 4,
    company_id:1
  },
  {
    id: 5,
    company_id:1
  },
  {
    id: 6,
    company_id:5
  },
  {
    id: 7,
    company_id:5
  },
  {
    id: 8,
    company_id:6
  },
  {
    id: 9,
    company_id:6
  },
  {
    id: 10,
    company_id:6
  },
  {
    id: 11,
    company_id:6
  },
  {
    id: 12,
    company_id:2
  },
  {
    id: 13,
    company_id:2
  },
  {
    id: 14,
    company_id:2
  },
  {
    id: 15,
    company_id:2
  },
  {
    id: 16,
    company_id:2
  },
  {
    id: 17,
    company_id:2
  },
  {
    id: 18,
    company_id:2
  }, 
  {
    id: 19,
    company_id:6
  }, 
  {
    id: 20,
    company_id:7
  }, 
  {
    id: 21,
    company_id:2
  }, 
  {
    id: 22,
    company_id:3
  }
];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all(datas.map(data=>queryInterface.sequelize.query(`UPDATE ticket_exchange.user SET company_id = '${data.company_id}' WHERE id = ${data.id}`)))
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all(datas.map(data=>queryInterface.sequelize.query(`UPDATE ticket_exchange.user SET company_id = '${data.company_id}' WHERE id = ${data.id}`)));
  }
};
