'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'event_status', schema: 'ticket_exchange' }, [{
      event_status: 'Completed',
      created_at: new Date(),
      updated_at: new Date()
    },{
      event_status: 'Active',
      created_at: new Date(),
      updated_at: new Date()
    },{
      event_status: 'Inactive',
      created_at: new Date(),
      updated_at: new Date()
    },{
      event_status: 'Postponed',
      created_at: new Date(),
      updated_at: new Date()
    },{
      event_status: 'Cancelled',
      created_at: new Date(),
      updated_at: new Date()
    },{
      event_status: 'Hold',
        created_at: new Date(),
        updated_at: new Date()
      }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('event_status', null, {});
  }
};
