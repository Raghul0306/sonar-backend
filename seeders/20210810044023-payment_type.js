'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'payment_type', schema: 'ticket_exchange' }, [{
      name:"Visa",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      name:"Mastercard",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      name:"American Express",
      created_at: new Date(),
      updated_at: new Date()
    }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    
  }
};
