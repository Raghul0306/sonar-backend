'use strict';
let excelEventGenre = [{
    name: 'Rock',
    created_at: new Date(),
    updated_at: new Date()
}, {
    name: 'Football',
    created_at: new Date(),
    updated_at: new Date()
}, {
    name: 'Cricket',
    created_at: new Date(),
    updated_at: new Date()
}, {
    name: 'West End',
    created_at: new Date(),
    updated_at: new Date()
}, {
    name: 'Country',
    created_at: new Date(),
    updated_at: new Date()
}, {
    name: 'Rugby',
    created_at: new Date(),
    updated_at: new Date()
}, {
    name: 'Dance',
    created_at: new Date(),
    updated_at: new Date()
}, {
    name: 'Football',
    created_at: new Date(),
    updated_at: new Date()
}, {
    name: 'Tennis',
    created_at: new Date(),
    updated_at: new Date()
}, {
    name: 'Rap',
    created_at: new Date(),
    updated_at: new Date()
}, {
    name: 'Dance',
    created_at: new Date(),
    updated_at: new Date()
}, {
    name: 'EDM',
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Pop",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Hard Rock",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Country and Folk",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Dance/Electronica",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Rap/Hip Hop",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Opera",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Adult Contemporary",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Other Concerts",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Alternative",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "R&B",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Public Speaking",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Musical",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Other Sports",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Other Theater",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Classical",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Comedy",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Ballet and Dance",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "New Age",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Rugby",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "World Music",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Tennis",
    created_at: new Date(),
    updated_at: new Date()
},
{
    name: "Family",
    created_at: new Date(),
    updated_at: new Date()
}
]
module.exports = {
    up: async (queryInterface, Sequelize) => {
        return await queryInterface.bulkInsert({ tableName: 'genre', schema: 'ticket_exchange' }, [
            ...excelEventGenre,

            {
                name: "AFL",
                parent_category: 1,
                full_name: "Australian Football League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Alternative & Indie",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Alternative Rock",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Attractions",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Auto Racing",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Ballet & Dance",
                parent_category: 3,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Baseball",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Baseball",
                parent_category: 1,
                child_category: "ILB",
                full_name: "Independent League Baseball",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Baseball",
                parent_category: 1,
                child_category: "MiLB",
                full_name: "Minor League Baseball",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Baseball",
                parent_category: 1,
                child_category: "MLB",
                full_name: "Major League Baseball",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Baseball",
                parent_category: 1,
                child_category: "World Baseball Classic",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Basketball",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Basketball",
                parent_category: 1,
                child_category: "Euroleague Basketball",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Basketball",
                parent_category: 1,
                child_category: "High School Basketball",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Basketball",
                parent_category: 1,
                child_category: "International Friendlies Basketball",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Basketball",
                parent_category: 1,
                child_category: "NBA",
                full_name: "National Basketball League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Basketball",
                parent_category: 1,
                child_category: "NBA G-League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Basketball",
                parent_category: 1,
                child_category: "NCAA Men's Basketball",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Basketball",
                parent_category: 1,
                child_category: "WNBA",
                full_name: "Womens National Basketball Association",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Boxing",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Cabaret",
                parent_category: 3,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Classical",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Comedy",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Conventions",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Country & Folk",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Cricket",
                parent_category: 1,
                child_category: "CPL",
                full_name: "Caribbean Premier League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Cricket",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Dance/Electronic",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Entertainment Shows",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Extreme Sports",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Family",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Festivals",
                parent_category: 4,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Fighting",
                parent_category: 1,
                child_category: "Mixed Martial Arts (MMA)",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Footall",
                parent_category: 1,
                child_category: "XFL",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Football",
                parent_category: 1,
                child_category: "AAF",
                full_name: "Alliance of American Football",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Football",
                parent_category: 1,
                child_category: "AAL",
                full_name: "American Arena League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Football",
                parent_category: 1,
                child_category: "CFL",
                full_name: "Canadian Football League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Football",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Football",
                parent_category: 1,
                child_category: "Legends Football League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Football",
                parent_category: 1,
                child_category: "NAL",
                full_name: "National Arena League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Football",
                parent_category: 1,
                child_category: "NCAA Football",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Football",
                parent_category: 1,
                child_category: "NFL",
                full_name: "National Football League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Golf",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Golf",
                parent_category: 1,
                child_category: "LPGA",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Golf",
                parent_category: 1,
                child_category: "PGA",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Hard Rock and Metal",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Horse Racing",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Ice Hocket",
                parent_category: 1,
                child_category: "International Hockey",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Ice Hockey",
                parent_category: 1,
                child_category: "AHL",
                full_name: "American Hockey League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Ice Hockey",
                parent_category: 1,
                child_category: "ECHL",
                full_name: "East Coast Hockey League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Ice Hockey",
                parent_category: 1,
                child_category: "NCAA Men's Hockey",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Ice Hockey",
                parent_category: 1,
                child_category: "NHL",
                full_name: "National Hockey League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Ice Hockey",
                parent_category: 1,
                child_category: "QMJHL",
                full_name: "Quebec Major Junior Hockey League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Jazz & Blues",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Lacrosse",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Lacrosse",
                parent_category: 1,
                child_category: "MLL",
                full_name: "Major League Lacrosse",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Lacrosse",
                parent_category: 1,
                child_category: "NCAA Men's Lacrosse",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Latin",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Miscellaneous",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Miscellaneous Special Events",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Miscellaneous Sports",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Motorsports",
                parent_category: 1,
                child_category: "Formula 1",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Motorsports",
                parent_category: 1,
                child_category: "IndyCar Series",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Motorsports",
                parent_category: 1,
                child_category: "Monster Trucks",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Motorsports",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Motorsports",
                parent_category: 1,
                child_category: "NASCAR",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Motorsports",
                parent_category: 1,
                child_category: "NHRA",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Motorsports",
                parent_category: 2,
                child_category: "Supercross",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Movies",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Museums",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Musicals",
                parent_category: 3,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "N",
                parent_category: 1,
                child_category: "IFL",
                full_name: "Indoor Football League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "New Age & Spiritual",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Opera",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Plays",
                parent_category: 3,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Pop",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "R&B/Urban Soul",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Rap & Hip-Hop",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Rock",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Rodeo",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Rugby",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                child_category: "Bundesliga",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                child_category: "Confederação Brasileira de Futebol",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                child_category: "Eredivisie",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                child_category: "Federacion Mexicana de Futbol",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                child_category: "Italian Serie A",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                child_category: "Ligue 1",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                child_category: "MLS",
                full_name: "Major League Soccer",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                child_category: "NWSL",
                full_name: "National Woman's Soccer League",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                child_category: "Primeira Liga",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                child_category: "Spanish Primera División",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                child_category: "USL Championship",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                child_category: "USL League One",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Soccer",
                parent_category: 1,
                child_category: "World Cup",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Special Events",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Sports",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Summer Games",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Tennis",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Theatre",
                parent_category: 3,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "TV Shows",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Virtual Experience",
                parent_category: 5,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "World Music",
                parent_category: 2,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Wrestling",
                parent_category: 1,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Wrestling",
                parent_category: 1,
                child_category: "WWE",
                full_name: "World Wrestling Federation",
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: "Wrestling",
                parent_category: 1,
                child_category: "AEW",
                full_name: "All Elite Wrestling",
                created_at: new Date(),
                updated_at: new Date()
            }
        ]);
    },

    down: async (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('genre', null, {});
    }
};
