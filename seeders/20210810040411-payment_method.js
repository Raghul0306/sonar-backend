'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'payment_method', schema: 'ticket_exchange' }, [{
      name:"Credit Card",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      name:"ACH",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      name:"Check",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      name:"Cash",
      created_at: new Date(),
      updated_at: new Date()
    }, 

    {
      name:"Bitcoin",
      created_at: new Date(),
      updated_at: new Date()
    }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    
  }
};
