'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return await queryInterface.bulkInsert({ tableName: 'performer', schema: 'ticket_exchange' }, [
        {
            "name": "MLB Home Run Derby",
            "genre_id": 1,
            "category_id": 1,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "New York Yankees",
            "genre_id": 1,
            "category_id": 1,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Chicago White Sox",
            "genre_id": 1,
            "category_id": 1,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Kansas City Royals",
            "genre_id": 1,
            "category_id": 1,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Pro Baseball Futures and Legends Game",
            "genre_id": 1,
            "category_id": 1,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Paloma Faith",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Crowded House",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "The Script",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Little Mix",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Sleep Token",
            "genre_id": 8,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Steps",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Procol Harum",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Nathan Carter",
            "genre_id": 9,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Erasure",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Van Der Graaf Generator",
            "genre_id": 10,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Texas",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Alanis Morissette",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Gary Barlow",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "The Weeknd",
            "genre_id": 11,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Blondie",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Andrea Bocelli",
            "genre_id": 12,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Snow Patrol",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Celine Dion",
            "genre_id": 13,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Bugzy Malone",
            "genre_id": 14,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Pet Shop Boys",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Deep Purple",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Bryan Adams",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Del Amitri",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Bon Iver",
            "genre_id": 15,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Genesis",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Evanescence",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Pete Tong",
            "genre_id": 10,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Jamie Webster",
            "genre_id": 15,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Amy Macdonald",
            "genre_id": 15,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Rick Astley",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Deacon Blue",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Royal Blood",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Craig David",
            "genre_id": 16,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "David Gray",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Professor Brian Cox",
            "genre_id": 17,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Beth Hart",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Nothing But Thieves",
            "genre_id": 15,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Fatboy Slim",
            "genre_id": 11,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Mrs Brown DLive Show",
            "genre_id": 18,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Premier League Darts",
            "genre_id": 19,
            "category_id": 1,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Elton John",
            "genre_id": 13,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Snoop Dogg",
            "genre_id": 11,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Dita Von Teese",
            "genre_id": 20,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "UB40",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Ozzy Osbourne",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "JLS",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Human League",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "5 Seconds of Summer",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Andre Rieu",
            "genre_id": 21,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "The Lumineers",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Jeff Dunham",
            "genre_id": 22,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "The Offspring",
            "genre_id": 15,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Michael Ball",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "IL Divo",
            "genre_id": 13,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Simply Red",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Richard Thompson",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Diversity",
            "genre_id": 23,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Hans Zimmer",
            "genre_id": 21,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Australian Pink Floyd Show",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "David Essex",
            "genre_id": 12,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Simple Minds",
            "genre_id": 15,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Chris Ramsey",
            "genre_id": 22,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Haim",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Billie Eilish",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Westlife",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Belinda Carlisle",
            "genre_id": 15,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Jason Manford",
            "genre_id": 22,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Midland",
            "genre_id": 9,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Idles",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Nile Rodgers",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Olly Murs",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "The Vamps",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "D Block Europe",
            "genre_id": 11,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "SHAGGED MARRIED ANNOYED",
            "genre_id": 20,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Thunder",
            "genre_id": 24,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Rupauls Drag Race",
            "genre_id": 20,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "HRVY",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Giants Live World Tour Finals",
            "genre_id": 19,
            "category_id": 1,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Guns N Roses",
            "genre_id": 8,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Lewis Capaldi",
            "genre_id": 13,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Gorillaz",
            "genre_id": 15,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Steve Hackett",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Duran Duran",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Bring Me the Horizon",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Riverdance",
            "genre_id": 23,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Aerosmith",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Blossoms",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "McFly",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "James",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "The Chats",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "The Hunna",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Hybrid Minds",
            "genre_id": 10,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Mo Gilligan",
            "genre_id": 22,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Lionel Richie",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Hoodoo Gurus",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Melbourne Storm",
            "genre_id": 25,
            "category_id": 1,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Chillinit",
            "genre_id": 11,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Hamilton",
            "genre_id": 18,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Maluma",
            "genre_id": 26,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Marlon Williams",
            "genre_id": 9,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Harry Styles",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "John Bishop",
            "genre_id": 22,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "The Eagles",
            "genre_id": 13,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "KSI Live",
            "genre_id": 14,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Pentatonix",
            "genre_id": 6,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Erste Bank Open",
            "genre_id": 27,
            "category_id": 1,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Paul Weller",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Six60",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Keith Urban",
            "genre_id": 9,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Biffy Clyro",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "The Wiggles",
            "genre_id": 28,
            "category_id": 3,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Eskimo Callboy",
            "genre_id": 14,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Sam Fender",
            "genre_id": 15,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Chris Stapleton",
            "genre_id": 9,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "DMAs",
            "genre_id": 15,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Mallrat",
            "genre_id": 10,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        },
        {
            "name": "Midnight Oil",
            "genre_id": 1,
            "category_id": 2,
            "created_at": new Date(),
            "updated_at": new Date()
        }
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('performer', null, {});
  }
};
