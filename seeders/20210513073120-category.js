'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'category', schema: 'ticket_exchange' }, [{
      name: 'Sports',
      parent_category_id: 0, 
      child_category_id: 0, 
      grand_child_category_id: 0, 
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'Concerts',
      parent_category_id: 0, 
      child_category_id: 0, 
      grand_child_category_id: 0, 
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'Theater',
      parent_category_id: 0, 
      child_category_id: 0, 
      grand_child_category_id: 0, 
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'Festivals',
      parent_category_id: 0, 
      child_category_id: 0, 
      grand_child_category_id: 0, 
      created_at: new Date(),
      updated_at: new Date()
    },{
      name: 'Special Events',
      parent_category_id: 0, 
      child_category_id: 0, 
      grand_child_category_id: 0, 
      created_at: new Date(),
      updated_at: new Date()
    }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('category', null, {});
  }
};
