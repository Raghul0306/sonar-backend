'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
      return await queryInterface.bulkInsert({ tableName: 'vendor', schema: 'ticket_exchange' }, [{
        name: 'Ticket Master',
        address: '55 A',
        address2:'LKD Street',
        city: 'Chicago',
        zip: '100001',
        state: 'Illinois',
        country: 'United Kingdom',
        email: 'tickets@email.ticketmaster.co.uk',
        phone: '',
        notes: '',
        created_at: new Date(),
        created_by:1,
        updated_at: new Date()
      }]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('vendor', null, {});
  }
};
