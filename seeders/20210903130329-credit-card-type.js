'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return queryInterface.bulkInsert({ tableName: 'credit_card_type', schema: 'ticket_exchange' }, [
      {
        type_name: "Chase Sapphire",
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        type_name: "AMEX Gold",
        created_at: new Date(),
        updated_at: new Date(),
      }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     return queryInterface.bulkDelete('credit_card_type', null, {});
  }
};
