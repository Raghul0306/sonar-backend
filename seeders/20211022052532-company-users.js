'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return await queryInterface.bulkInsert({ tableName: 'company_user', schema: 'ticket_exchange' }, [{
      company_id: 1,
      user_id: 1,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 4,
      user_id: 2,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 5,
      user_id: 3,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 1,
      user_id: 4,
      created_at: new Date(),
      created_by:3,
      updated_at: new Date()
    },
    {
      company_id: 1,
      user_id: 5,
      created_at: new Date(),
      created_by:4,
      updated_at: new Date()
    },
    {
      company_id: 5,
      user_id: 6,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 5,
      user_id: 7,
      created_at: new Date(),
      created_by:6,
      updated_at: new Date()
    },
    {
      company_id: 6,
      user_id: 8,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 6,
      user_id: 9,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 6,
      user_id: 10,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 6,
      user_id: 11,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 2,
      user_id: 12,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 2,
      user_id: 13,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 2,
      user_id: 14,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 2,
      user_id: 15,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 2,
      user_id: 16,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 2,
      user_id: 17,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 2,
      user_id: 18,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 6,
      user_id: 19,
      created_at: new Date(),
      created_by:1,
      updated_at: new Date()
    },
    {
      company_id: 7,
      user_id: 20,
      created_at: new Date(),
      created_by:12,
      updated_at: new Date()
    },
    {
      company_id: 2,
      user_id: 21,
      created_at: new Date(),
      created_by:12,
      updated_at: new Date()
    },
    {
      company_id: 3,
      user_id: 22,
      created_at: new Date(),
      created_by:12,
      updated_at: new Date()
    },
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('company_user', null, {});
  }
};
