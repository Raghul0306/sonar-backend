'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert({ tableName: 'broker_order_listing', schema: 'ticket_exchange' },
        [{
          broker_order_id: 1, 
          listing_id:1
        }, {
          broker_order_id: 1, 
          listing_id:2
        }, 
        {
          broker_order_id: 1, 
          listing_id:3
        }, 
        {
          broker_order_id: 2, 
          listing_id:4
        },
        {
          broker_order_id: 2, 
          listing_id:5
        },
        {
          broker_order_id: 2, 
          listing_id:6
        }
      ]);
  },

  down: async (queryInterface, Sequelize) => {

  }
};
