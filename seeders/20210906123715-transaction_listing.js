'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert({ tableName: 'transaction_listing', schema: 'ticket_exchange' },
        [{
          transaction_id: 1, 
          listing_id:1
        }, {
          transaction_id: 1, 
          listing_id:2
        }, 
        {
          transaction_id: 1, 
          listing_id:3
        }, 
        {
          transaction_id: 2, 
          listing_id:4
        },
        {
          transaction_id: 2, 
          listing_id:5
        },
        {
          transaction_id: 2, 
          listing_id:6
        }, 
        {
          transaction_id: 3, 
          listing_id:7
        },
        {
          transaction_id: 3, 
          listing_id:8
        },
        {
          transaction_id: 3, 
          listing_id:9
        }, 
        {
          transaction_id: 4, 
          listing_id:10
        },
        {
          transaction_id: 4, 
          listing_id:11
        },
        {
          transaction_id: 4, 
          listing_id:12
        }
      ]);
  },

  down: async (queryInterface, Sequelize) => {

  }
};
