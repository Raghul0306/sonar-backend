'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'delivery_status', schema: 'ticket_exchange' }, [{
      name:"completed",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      name:"cancelled",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      name:"pending",
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      name:"incompleted",
      created_at: new Date(),
      updated_at: new Date()
    }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
  }
};
