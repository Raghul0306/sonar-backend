'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert({ tableName: 'tags', schema: 'ticket_exchange' }, [{
      tag_name:"ownership",      
      active:1,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      tag_name:"pricing",     
      active:1,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      tag_name:"buying",      
      active:1,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      tag_name:"Fulfillment",      
      active:1,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      tag_name:"Ticketing System",      
      active:1,
      created_at: new Date(),
      updated_at: new Date()
    },

    {
      tag_name:"Account ID",       
      active:1,
      created_at: new Date(),
      updated_at: new Date()
    },
    
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('tags', null, {});
  }
};
