#!/bin/bash

# Next, configure the app based on the environment file and config file
if [[ "$DEPLOYMENT_GROUP_NAME" == "dev-mytickets-api" ]]; then
    cp /home/ec2-user/mytickets-api/.env.development /home/ec2-user/mytickets-api/.env
	cp /home/ec2-user/mytickets-api/config/config.json-development /home/ec2-user/mytickets-api/config/config.json
elif [[ "$DEPLOYMENT_GROUP_NAME" == "prod-mytickets-api" ]]; then
    cp /home/ec2-user/mytickets-api/.env.production /home/ec2-user/mytickets-api/.env
	cp /home/ec2-user/mytickets-api/config/config.json-production /home/ec2-user/mytickets-api/config/config.json
elif [[ "$DEPLOYMENT_GROUP_NAME" == "staging-mytickets-api" ]]; then
    cp /home/ec2-user/mytickets-api/.env.staging /home/ec2-user/mytickets-api/.env
	cp /home/ec2-user/mytickets-api/config/config.json-staging /home/ec2-user/mytickets-api/config/config.json	
fi

# restart the pm2 processor from ec2-user

sudo -iu ec2-user pm2 restart all

# Change the Owner permission for environment file and config file

sudo chown -R ec2-user:ec2-user /home/ec2-user/mytickets-api

# Server Restart

sudo service nginx restart 
