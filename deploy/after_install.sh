#!/bin/bash	

# Install node module in backend application 
cd /home/ec2-user/

# Change the owner permission of the Backend application

sudo chown -R ec2-user:ec2-user mytickets-api

# Change the create attachment folder & make  perission for S3 bucket image upload

sudo mkdir mytickets-api/attachmentfiles

sudo chmod -R 777 mytickets-api/attachmentfiles