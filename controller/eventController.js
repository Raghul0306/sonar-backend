const db = require('../models');
const fs = require('fs')
const path = require('path');
const archiver = require('archiver');
var request = require('request');
const multiparty = require('multiparty');
const sequelize = require('sequelize');
const ListingAttachments = db.listingattachments;
import { uploadURLs, s3Path ,listingStatusList} from "../constants/constants";
const AWS = require('aws-sdk');
const Op = db.Sequelize.Op;

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});
const { QueryTypes, where } = require('sequelize');
const Genre  = db.genre;
const Category=db.category;
const Listings =db.listings;
const Performer =db.performer;
const Venue = db.venue;
const Event = db.events;
const _ = require('lodash');
const { getListingStatusbyName} = require('../helpers/queryResponse');
const ListingStatus =db.listingStatus;

module.exports = {
    /**
    * Purpose: Event Details
    * 
    * return: {object} Last upload file detail
    **/
    async event_list(request, response){

        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            if (error) {
              return response.status(500).send(error);
            }
            try {

                var activeListingStatus = await getListingStatusbyName(listingStatusList.active);
                var inactiveListingStatus = await getListingStatusbyName(listingStatusList.inactive);

                var eventId = fields.eventId[0];
                var whereStr = "event.id = " + eventId;
                
             const eventDtls = await db.sequelize.query(`select event.category_id as category_id,event.genre_id as genre_id,event.id as event_id,event.mercury_id as mercury_id, event.name as event_name, TO_CHAR(event.date :: TIMESTAMP,'Day - Mon dd, yyyy HH24:MM') as event_date, array_agg(distinct venue.name) as venue_name, array_agg(distinct venue.city) as venue_city, array_agg(distinct venue.address) as venue_address, array_agg(distinct venue.state) as venue_state, array_agg(distinct venue.country) as venue_country, DATE_PART('day',event.date :: TIMESTAMP - NOW() :: TIMESTAMP) as days_to_event, avg(listcnt.totalseats) :: numeric(20) as total_seats, avg(listcnt.soldtkts) :: numeric(20) as total_sold_tickets, avg(listcnt.totalavailabletkts) :: numeric(20) as totalavailabletkts, avg(listcnt.last7days) :: numeric(20) as total_sold_tickets_in_last_7days, avg(listcnt.awaitingdelivery) :: numeric(20) as waiting_delivery, array_agg(distinct performer.name) as performer_name, array_remove( array_agg(distinct CASE WHEN listing_group.listing_status_id=` + inactiveListingStatus.id + ` THEN true WHEN listing_group.listing_status_id=` + activeListingStatus.id + ` THEN false ELSE false END), null ) as inactive, array_remove(array_agg(distinct listing_group.listing_status_id), null) as listing_status from ticket_exchange.event left outer join ticket_exchange.listing_group on event.id = listing_group.event_id left outer join ticket_exchange.listing_tags on listing_group.id = listing_tags.listing_group_id left outer join ticket_exchange.event_tags on event.id = event_tags.event_id left outer join ticket_exchange.tags eventtag on event_tags.tag_id = eventtag.id left outer join ticket_exchange.tags listingtag on listing_tags.tag_id = listingtag.id left outer join ticket_exchange.venue on event.venue_id = venue.id left outer join ticket_exchange.performer on event.performer_id = performer.id left outer join (select listing_group.event_id, sum(listing_group.seat_end :: integer - listing_group.seat_start :: integer + 1) as totalseats, sum(st.soldtkts) as soldtkts, sum(st.last7days) as last7days, sum(st.awaitingdelivery) as awaitingdelivery, sum(listing_group.seat_end :: integer - listing_group.seat_start :: integer + 1) - sum(st.soldtkts) as totalavailabletkts from ticket_exchange.listing_group left outer join (select listing_group_id, sum(case when sold then 1 else 0 end) as soldtkts, sum(case when sold and delivered is false then 1 else 0 end) as awaitingdelivery, sum(case when sold and updated_at between current_date - interval '7 days' and current_date then 1 else 0 end) as last7days from ticket_exchange.listing where 1 = 1 group by listing_group_id) st on listing_group.id = st.listing_group_id where 1 = 1 group by listing_group.event_id) as listcnt on event.id = listcnt.event_id where  `+ whereStr +`  group by event.id order by event.id;`, { type: QueryTypes.SELECT });
            
            if (eventDtls.length > 0) {
                
                /*

                Old query
                
                const listing_details = await db.sequelize.query(`select listing_group.seat_type_id,listing_group.splits_id,listing_group.quantity as ticket_quantity,listing_group.id as listinggroupid, array_agg(distinct listing.id) as listingid, listing_group.event_id, listing_group.section, listing_group.row, listing_group.seat_start || '-' || listing_group.seat_end as seatdtl, listing_group.unit_cost as cost, listing_group.group_price as price, listing_group.last_price, listing_group.last_price_change, avg(st.price) as soldprice, avg(st.soldtkts) :: numeric(20) as totalsold, avg(listing_group.seat_end :: integer - listing_group.seat_start :: integer + 1) :: numeric(20) - avg(st.soldtkts) :: numeric(20) as totalavailable, listing_group.face_value,  array_remove(array_agg(distinct listing.inactive),null) as inactive, listing_group.ticket_type_id, array_agg(distinct ticket_types.name) as tickettypename, avg(st.last7days) :: numeric(20) as last7days, avg(st.awaitingdelivery) :: numeric(20) as awaitingdelivery, avg(st.margin) :: numeric(20, 2) as margin, avg(st.margintype) :: numeric(20) as margintype, array_remove(array_agg(distinct listingtag.tag_name), null) as tag_name,array_agg(DISTINCT disclosures.disclosure_name) AS disclosureName from ticket_exchange.listing_group left outer join (select listing_group_id, sum(case when sold then 1 else 0 end) as soldtkts, sum(case when sold then price else 0 end) as price, sum(case when sold and delivered is false then 1 else 0 end) as awaitingdelivery, sum(case when sold and price > 0 then trunc((price / unit_cost) :: numeric, 2) else 0 end) as margin, sum(case when sold then price else 0 end) - sum(case when sold then unit_cost else 0 end) as margintype, sum(case when sold and updated_at between current_date - interval '7 days' and current_date then 1 else 0 end) as last7days from ticket_exchange.listing where 1 = 1 group by listing_group_id) st on listing_group.id = st.listing_group_id left outer join ticket_exchange.listing on listing_group.id = listing.listing_group_id left outer join ticket_exchange.ticket_types on listing_group.ticket_type_id = ticket_types.id  left outer join ticket_exchange.listing_disclosures ON listing_group.id = listing_disclosures.listing_group_id LEFT OUTER JOIN ticket_exchange.disclosures ON listing_disclosures.disclosure_id = disclosures.id left outer join (select listing_tags.listing_group_id, tags.tag_name from ticket_exchange.listing_tags inner join ticket_exchange.tags on listing_tags.tag_id = tags.id where 1 = 1) listingtag on listingtag.listing_group_id = listing_group.id ` + listingWhrStr + ` group by listing_group.id;`, { type: QueryTypes.SELECT }); */

                var listingWhrStr = "where 1 = 1 and listing_group.event_id = " + eventId
                let queryString = `select
                    listing_group.id as listinggroupid,
                    array_agg(distinct listing.id) as listingid,
                    listing_group.event_id,
                    listing_group.section,
                    listing_group.quantity as ticket_quantity,
                    listing_group.row,
                    listing_group.seat_start || '-' || listing_group.seat_end as seatdtl,
                    listing_group.unit_cost as cost,
                    listing_group.face_value,
                    listing_group.ticket_type_id,
                    array_agg(distinct ticket_types.name) as tickettypename,
                    array_agg(distinct ticket_types.delivery_type_description) as delivery_type_description,
                    array_agg(
                        distinct (
                            case
                                when listing.sold then 'yes'
                                else 'no'
                            end
                        )
                    ) as sold,
                    array_agg(DISTINCT disclosures.disclosure_name) AS disclosureName,
                    array_agg(distinct array[disclosures.disclosure_name, disclosures.description]) as disclosure_data,
                    array_remove(array_agg(distinct listing.active), null) as active,
                    array_remove(array_agg(distinct listing.inactive), null) as inactive,
                    array_remove(array_agg(distinct listing_status.status), null) as status,
                    array_remove(array_agg(distinct listingtag.tag_name), null) as tag_name
                from
                    ticket_exchange.listing_group
                    left outer join ticket_exchange.listing on listing_group.id = listing.listing_group_id
                    left outer join ticket_exchange.ticket_types on listing_group.ticket_type_id = ticket_types.id
                    left outer join ticket_exchange.listing_disclosures ON listing_group.id = listing_disclosures.listing_group_id 
                    LEFT OUTER JOIN ticket_exchange.disclosures ON listing_disclosures.disclosure_id = disclosures.id
                    left join ticket_exchange.listing_status ON listing_status.id = listing.listing_status_id 
                    left outer join (
                        select
                            listing_tags.listing_group_id,
                            tags.tag_name
                        from
                            ticket_exchange.listing_tags
                            inner join ticket_exchange.tags on listing_tags.tag_id = tags.id
                        where
                            1 = 1
                    ) listingtag on listingtag.listing_group_id = listing_group.id  ${listingWhrStr} 
                group by
                    listing_group.id;`;
                let listing_details = await db.sequelize.query(queryString);

                await eventDtls.forEach(async (listingGroup,index)=>{
                    
                    let listingsRes = [];

                    await listing_details[0].forEach((listing,listingIndex)=>{
                        
                        if (listing.event_id == listingGroup.event_id) { 
                            listingsRes.push({
                                listing_group_id: listing.listinggroupid,
                                listing_id:listing.listingid,
                                event_id:listing.event_id,
                                section:listing.section,
                                row:listing.row,
                                seat_details:listing.seatdtl,
                                cost:listing.cost,
                                tags:listing.tag_name,
                                ticket_type_name:listing.tickettypename,
                                delivery_type_description:listing.delivery_type_description,
                                ticket_quantity:listing.ticket_quantity,
                                disclosureName:listing.disclosurename,
                                splitsId: listing.splits_id,
                                status:listing.status,
                                seat_type_id:listing.seat_type_id,
                                disclosure_data: listing.disclosure_data
                            });
                        }
                    });

                    eventDtls[index].listing_details = listingsRes;
                
                })
                    
            }
            if (eventDtls) {
                return response.send(eventDtls);
            } else {
                var data = {'message': "No events available"}
                return response.send(data);
            }

            }
            catch (err) {
            return response.status(500).send(err);
            }
          });
    },

    async downloadAttachmentsZipfile(req,res){

        const eventId = req.body.eventId;
        const listingId = req.body.listingGroupId;
        const selectedAttachment = req.body.selectedAttachment;

        var attachment_ids = [];
        selectedAttachment.map((item) => {
            attachment_ids.push(item.value);
        })
        
        let listing_attachments = await ListingAttachments.findAll({
            where: {
                id: {
                    [Op.in]: attachment_ids
                  }
            },raw:true
        })

        var appDir = path.dirname(require.main.filename);

        const bucketName =  process.env.AWS_BUCKETNAME + uploadURLs.listingsAttachmentURL + '/'+ eventId + '/'+ listingId; 

        const output = fs.createWriteStream(appDir + '/attachmentfiles/tickets.zip');
        const archive = archiver('zip', {
            zlib: { level: 9 } // Sets the compression level.
        });      

            // Handle various useful events if you need them.
            archive.on('end', function() {
                console.log( "end" );
            });
            
            archive.on('error', function(err) {
                throw err;
            });

            // Set up the archive we want to present as a download.
            archive.pipe(output);

            // Set filenames for each object to go into the zip.
            listing_attachments.map((object, index) => {
                const { Key } = object.attachment_url;

                var attachmentURL = object.attachment_url;
                const keyname = path.basename(attachmentURL);
                const ext2 = path.extname(attachmentURL);
                const nameWithoutExt2 = path.basename(keyname, ext2);
                
                try {
                    const objectSrc = s3
                    .getObject({ Bucket: bucketName, Key: keyname })
                    .createReadStream();
                
                    if(objectSrc)  {
                        archive.append(objectSrc, {
                            name: ++index + nameWithoutExt2 + ext2
                        });
                    }
                } catch(err) {
                    console.error(err)
                }

            });

            // Start streaming everything.
            archive.finalize();

            output.on('close', function() {
                var zipPaths = appDir + '/attachmentfiles/tickets.zip'
                
                res.download(zipPaths, function(err) {
                    try {
                        fs.unlink(zipPaths, function (err) {
                            if (err) console.error(err);
                            // if no error, file has been deleted successfully
                            console.log('File deleted!');
                        });                        
                    } catch(err) {
                        console.error(err)
                    } 
                      
                    if (err) {
                      console.log("form builder package is not exist"); 
                    } 
                });
            });
    },

    async viewTicketFile(req, res) {
        
        const attachmentId = req.body.attachmentId;
        const eventId = req.body.eventId;
        const listingId = req.body.listingId;

        let listing_attachments = await ListingAttachments.findOne({
            attributes: [
                [sequelize.col('attachment_name'), 'attachmentname'], 
                [sequelize.col('attachment_url'), 'attachmenturl'], 
            ], 
            where: {id: attachmentId},
            raw:true
        })
        
        const bucketName =  process.env.AWS_BUCKETNAME + uploadURLs.listingsAttachmentURL + '/'+ eventId + '/'+ listingId; 
        var attachmenturl = listing_attachments.attachmenturl;

        try {
            const params = {
                Bucket: bucketName,
                Key: path.basename(attachmenturl)
            };

            const s3 = new AWS.S3({
                accessKeyId: process.env.AWS_ACCESS_KEY,
                secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
            });

            s3.getObject(params).promise()
            .then(file => {
                res.setHeader('content-type', file.ContentType);
                res.send(file.Body.toString('base64'));
            })
            .catch(err => {        
                res.send(false);
            });

        } catch (error) {
            res.send(false);
        }
    },
    
    async getCategory(request,response){
        try{                
            const categoryList = await Category.findAll({
                where: { name: { [Op.in]: request.query.name } },
                order:[
                    ["id", "ASC"]
                ],
                attributes:['id','name']
            });
            var message="Category Fetch successfully"
            return response.json({message: message, result: categoryList});
        }
        catch(error){
          return response.status(500).send(error);
        }
    },

    async getPerformers(request, response) {
        try {
            let names = request.query.names;
            let namesIn = "";
            if(names && names.length){
                namesIn = names.join("','");
            }
            let performers = await db.sequelize.query(`SELECT
                "performer"."id",
                "performer"."name",
                "performer"."category_id",
                "performer"."genre_id",
                row_to_json( "genre".*) AS genre,
                row_to_json("category".*) AS category,
                (count("event->listing_groups->purchase_orders")+count("event->broker_orders")) as "ordersCount"
            FROM
                "ticket_exchange"."performer" AS "performer"
                LEFT OUTER JOIN "ticket_exchange"."genre" AS "genre" ON "performer"."genre_id" = "genre"."id"
                INNER JOIN "ticket_exchange"."category" AS "category" ON "performer"."category_id" = "category"."id"
                AND "category"."name" IN ('${namesIn}')
                LEFT OUTER JOIN "ticket_exchange"."event" AS "event" ON "performer"."id" = "event"."performer_id"
                LEFT OUTER JOIN "ticket_exchange"."broker_order" AS "event->broker_orders" ON "event"."id" = "event->broker_orders"."event_id"
                LEFT OUTER JOIN "ticket_exchange"."listing_group" AS "event->listing_groups" ON "event"."id" = "event->listing_groups"."event_id"
                LEFT OUTER JOIN "ticket_exchange"."purchase_order" AS "event->listing_groups->purchase_orders" ON "event->listing_groups"."id" = "event->listing_groups->purchase_orders"."listing_group_id"
                
                GROUP BY "performer"."id",
                "performer"."name",
                "performer"."category_id",
                "performer"."genre_id","genre".*,"category".*;
           `)
            return response.json({ 
                message: "Performer List Fetch successfully", 
                result: performers[0] 
            });

        } catch (error) {
            return response.status(500).send(error);
        }
    },

    async getGenre(request,response){
        try{                
            // const genreList = await Genre.findAll({
            //     order:[
            //         ["id", "ASC"]
            //     ],
            //     attributes:['id','name','child_category','parent_category']
            // });
            // Don't remove
            const genreList = await db.sequelize.query(`select *,(select name from "ticket_exchange"."category" where "genre"."parent_category" = "id"::text) as category_name from "ticket_exchange"."genre" as genre`)
            var message="Genre Fetch successfully";
            return response.json({message: message, result: genreList[0]});
        }
        catch(error){
            return response.status(500).send(error);
        }
    },
    async getListings(request,response) {
        try{
            Listings.belongsTo(db.listingStatus, { foreignKey: 'listing_status_id' });
            const listings = await Listings.findAll({
                attributes:['id','hold','seat_number',[sequelize.col('listing_status.status'), 'status'], [sequelize.literal(`CASE WHEN status = '${listingStatusList.active}' THEN true ELSE false END`), 'active']],
                where:{
                    id:{
                        [Op.in]:request.query.listingIds
                    }
                },
                include: {
                    model: ListingStatus,
                    attributes: [],
                }
            });
            return response.json({result: 'success', data: listings});
        }
        catch(error){
            return response.status(500).send(error);
        }
    },
    async updateListings(request,response) {
        try{       
            const form = new multiparty.Form();
            form.parse(request, async (error, fields, files) => {
                if (error) {
                    return response.status(500).send(error);
                }
                let listingIds = _.get(fields,'listingIds');
                await Listings.update({hold:true},{
                    where:{
                        id:{
                            [Op.in]:listingIds
                        }
                    }
                });
                
                return response.json({result: 'success', message: "Listing status successfully updated"});
            });
        }
        catch(error){
            return response.status(500).send(error);
        }
    },
    async editTicketAttachment(request, response) {
        
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            if (error) {
                return response.status(500).send(error);
            };
            try {
                const path = files.attachment_file_data[0].path;
                const buffer = fs.readFileSync(path);
                const attachment_file = fields.attachment_file[0];
                const eventId = fields.eventId[0];
                const ListingId = fields.ListingId[0];
                const attachmentId = fields.attachmentId[0];
                
                const params = {
                    Body: buffer,
                    Bucket: process.env.AWS_BUCKETNAME + uploadURLs.listingsAttachmentURL + '/' + eventId + '/' + ListingId,
                    Key: attachment_file,
                };
                const upload = await s3.upload(params).promise();
                if (upload) {
                    
                    var attachmentdata = {
                         attachment_url: s3Path.basePath+upload.Bucket+ '/' +upload.key,
                    };

                    await ListingAttachments.update(
                        attachmentdata,
                        {
                            where: { id: attachmentId }
                        }
                    );
                    
                    response.send('File uploaded successfully')  
                }
            } catch (err) {
                return response.status(500).send(err);
            }
        });

        
    }, 
    async getVenueList(request,response){
        try{               
            let {searchCityText = ""} = request.query;
            const venues =  await db.sequelize.query(`SELECT 
                "venue"."id", 
                "venue"."name", 
                "venue"."city", 
                "venue"."latitude", 
                "venue"."longitude", 
                json_agg(row_to_json("event".*)) as events
            FROM 
                "ticket_exchange"."venue" AS "venue" 
                LEFT OUTER JOIN "ticket_exchange"."event" AS "event" ON "venue"."id" = "event"."venue_id" 
                where "venue"."city" ILIKE '%${searchCityText}%'
                group by "venue"."id", "venue"."name", "venue"."city", "venue"."latitude", "venue"."longitude";
            `)
            console.log("venues => ",venues)
            return response.status(200).json({result: 'success',data:venues[0]});
        }
        catch(error){
            return response.status(500).send(error);
        }
    }

}
    
