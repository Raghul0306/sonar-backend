const fileuploads = require('./fileuploadController');
const events = require('./eventController');
const buyerUser = require('./buyerUserController');
const order = require('./orderController.js')
const tickets = require('./ticketController');
const favorities = require('./favoritiesController.js');
const webhook = require('./webhookController');
module.exports = {
    fileuploads,
    events,
    buyerUser,
    order,
    tickets,
    favorities,
    webhook
}