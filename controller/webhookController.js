const webHookChecks = require('../helpers/emailParserHelper');
const addRecords = require('../helpers/addParserRecords');
const constants = require('../constants/constants');

module.exports = {

    async saveTicketMasterListing(request, response)  {

        let parsedData = request.body;
        let responseOutput = {};

        let range = Object.keys(parsedData).filter((ele)=>ele.endsWith("_range"))
        parsedData.ticket_range = [];
        parsedData.ticket_range = range.length>0 ? parsedData[range[0]] : []

        parsedData.seat_type = await webHookChecks.findSeatType(parsedData)
        parsedData.seat_type_id = constants.mailParser.seatTypesID[parsedData.seat_type]; //populate seat_type_id
        
        //store parsed data in emailparser table
        let parserStatus = await webHookChecks.addParserData(parsedData);

        console.log("=====parser details=====");
        console.log(parserStatus);

        if(!parserStatus.status){
            responseOutput.result = constants.mailParser.response.failed.result;
            responseOutput.status = constants.mailParser.response.failed.status;
            responseOutput.message = parserStatus.message;

            await webHookChecks.updateEmailParser(parserStatus, responseOutput);
            return response.json(responseOutput);
        }

        //check event exists in system
        let eventDetails = await webHookChecks.checkEventExists(parsedData);

        console.log("=====event details=====");
        console.log(eventDetails);

        if(eventDetails == null){
            responseOutput.result = constants.mailParser.response.failed.result;
            responseOutput.status = constants.mailParser.response.failed.status;
            responseOutput.message = constants.mailParser.response.failed.eventNotMatch;

            await webHookChecks.updateEmailParser(parserStatus, responseOutput);
            return response.json(responseOutput);
        }

        //check ticket system exists in system
        let ticketSystemAccount = await webHookChecks.getTicketSystemAccount(parsedData);

        console.log("=====ticketsystemaccount details=====");
        console.log(ticketSystemAccount);

        if(ticketSystemAccount == null){
            responseOutput.result = constants.mailParser.response.failed.result;
            responseOutput.status = constants.mailParser.response.failed.status;
            responseOutput.message = constants.mailParser.response.failed.ticketSystemNotMatch;

            await webHookChecks.updateEmailParser(parserStatus, responseOutput);
            return response.json(responseOutput);
        }

        //check vendor exists in system
        let vendorDetails = await webHookChecks.getVendorDetails(parsedData);

        console.log("=====vendor details=====");
        console.log(vendorDetails);

        if(vendorDetails == null){
            responseOutput.result = constants.mailParser.response.failed.result;
            responseOutput.status = constants.mailParser.response.failed.status;
            responseOutput.message = constants.mailParser.response.failed.vendorNotMach;

            await webHookChecks.updateEmailParser(parserStatus, responseOutput);
            return response.json(responseOutput);
        }

        //create listingGroup
        let listingGroupData = await addRecords.ListingGroupInputFormat(parsedData, ticketSystemAccount, eventDetails);

        console.log("=====listing group details=====");
        console.log(listingGroupData);

        if(!listingGroupData.status){

            responseOutput.result = constants.mailParser.response.failed.result;
            responseOutput.status = constants.mailParser.response.failed.status;
            responseOutput.message = listingGroupData.message;

            await webHookChecks.updateEmailParser(parserStatus, responseOutput);
            return response.json(responseOutput);
        }

        //create listings
        let listingsIds = [];

        for(let seatCount = 0; seatCount < parsedData.ticket_range.length; seatCount++){

            let listingData = await addRecords.ListingInputFormat(parsedData, listingGroupData.data, seatCount);

            if(!listingData.status){
                responseOutput.result = constants.mailParser.response.failed.result;
                responseOutput.status = constants.mailParser.response.failed.status;
                responseOutput.message = listingData.message;

                await webHookChecks.updateEmailParser(parserStatus, responseOutput);
                return response.json(responseOutput);
            }

            listingsIds.push(listingData.data.dataValues.id);
        }

        let purchaseOrderData = await addRecords.purchaseOrderInputFormat(parsedData, vendorDetails, listingGroupData.data);

        console.log("=====purchase order details=====");
        console.log(purchaseOrderData);

        if(!purchaseOrderData.status){

            responseOutput.result = constants.mailParser.response.failed.result;
            responseOutput.status = constants.mailParser.response.failed.status;
            responseOutput.message = purchaseOrderData.message;

            await webHookChecks.updateEmailParser(parserStatus, responseOutput);
            return response.json(responseOutput);

        }

        for(let listingCounts = 0; listingCounts < listingsIds.length; listingCounts++)
        {
            let purchaseOrderListingsData = await addRecords.purchaseOrderListingsInputs(purchaseOrderData.data, listingsIds[listingCounts]);

            if(!purchaseOrderListingsData.status){
                responseOutput.result = constants.mailParser.response.failed.result;
                responseOutput.status = constants.mailParser.response.failed.status;
                responseOutput.message = purchaseOrderListingsData.message;

                await webHookChecks.updateEmailParser(parserStatus, responseOutput);
                return response.json(responseOutput);
            }
        }

        responseOutput.result = constants.mailParser.response.success.result;
        responseOutput.status = constants.mailParser.response.success.status;
        responseOutput.message =  constants.mailParser.response.success.parsingCompleted;

        await webHookChecks.updateEmailParser(parserStatus, responseOutput);
        return response.json(responseOutput);
    }

}