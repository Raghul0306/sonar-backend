const db = require('../models');
const Brokerorder=db.brokerorder;
const Event=db.events;
const BrokerOrderListing=db.brokerorderlisting;
const ListingGroup= db.listinggroup;
const ListingDisclosure=db.listingdisclosures;
module.exports = {
    async getMyTickets(request, response)  { 
        try {
            const userId = request.params.userId;               
            Brokerorder.belongsTo (db.events, {foreignKey: 'event_id'}); 
            Event.belongsTo (db.venue, {foreignKey: 'venue_id'});
            Brokerorder.belongsTo (db.listinggroup, {foreignKey:'listing_group_id'});         
            Brokerorder.belongsTo(db.deliverystatus,{foreignKey:'delivery_status_id'})          
            Brokerorder.belongsTo(db.buyerUsers,{foreignKey:'buyer_user_id'});
            Brokerorder.belongsTo(db.orderstatus,{foreignKey:'order_status_id'});
            Brokerorder.hasMany(db.brokerorderlisting, {foreignKey: 'broker_order_id'});           
            BrokerOrderListing.belongsTo(db.listings,{foreignKey:'listing_id'});
                 
            ListingDisclosure.belongsTo(db.disclosures,{foreignKey:'disclosure_id'});
            ListingGroup.belongsTo(db.tickettype,{foreignKey:'ticket_type_id'})
            ListingGroup.hasMany(db.listingdisclosures,{foreignKey:'listing_group_id'});           
            let brokerOrders = await Brokerorder.findAll({
                where:{
                    buyer_user_id:userId
                },
                attributes:['id','delivery_at','created_at'],                                                                       
                include: [
                    {
                        model: db.events,
                        attributes: ['name','date'],
                        include: [
                            {
                                model: db.venue,
                                attributes: ['name','address','address2','city','country','zip_code','state'],                             
                            }                            
                        ]                        
                    },                                    
                    {model:db.listinggroup,           
                    attributes:['delivery_type','unit_cost','section','row','seat_start','seat_end'],                            
                    include:[
                        {
                        model:db.listingdisclosures,                      
                        attributes:['id'],                                              
                        include:[
                            {
                                model:db.disclosures,
                                where:{
                                    active:'1'
                                },
                                attributes:['disclosure_name'],                
                            }
                        ]
                        }                        
                    ]  
                                   
                    } ,
                    {model:db.brokerorderlisting,
                    attributes:['broker_order_id'],
                    include:[
                        {
                        model:db.listings,attributes:['id','delivered','seat_number']
                        }
                    ]
                    },                  
                    {model:db.buyerUsers,attributes:['first_name','last_name','email']},
                    {model:db.orderstatus,attributes:['name']},
                    {model:db.deliverystatus,attributes:['name']},  
                    {model:db.listinggroup,attributes:[],include:[{model:db.tickettype,attributes:['name','delivery_type_description']}]}                     
            
                ]           
            })               
            return response.json({result: 'success', data:brokerOrders});
        }
        catch (error) {
            return response.status(500).send(error);
        } 
    }
}