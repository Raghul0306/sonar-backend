const db = require('../models');
const BuyerUsers = db.buyerUsers;
const TicketingSystemUserLogin = db.ticketingsystemuserlogin;
const bcrypt = require("bcrypt");
const awsHelper = require('../helpers/awskms');
const multiparty = require('multiparty');
const sendMail = require('../helpers/sendMail');
const mailTemplate = require('../helpers/mailTemplate');
const authEmailTemplate = require('../helpers/authEmailTemplate');

const jwt = require('jsonwebtoken');
const _ = require('lodash');
const buyerUserHelper = require('../helpers/buyerUser.js');
const channel = require('../helpers/channel.js');
const googleHelper = require('../helpers/googleHelper.js');
const facebookHelper = require('../helpers/facebookHelper.js');
const constants = require('../constants/constants.js');
const {GoogleOAuthToken,FacebookAuthToken} = db;

module.exports = {
     /**
          * Input : User Details
          * Function For : Adding Buyer User account details to buyerUsers table
          */
    async registerUser(request,response) {
        try {
            const form = new multiparty.Form();
            form.parse(request, async (error, fields, files) => {
                if (error) {
                    return response.status(500).send(error);
                }
                    
                let userRole = await buyerUserHelper.getBuyerUserRole();
                let userRoleId = _.get(userRole,'dataValues.id');
                if(!userRoleId) return  response.status(400).send({ result: 'failed', message:'Invalid Role!' });
                
                let bueryUserData = await buyerUserHelper.formDataToBuyerUserData(fields);

                let channelData = await channel.getCurrentUserChannel(request);
                let channelId = _.get(channelData,'dataValues.id');
                
                if(channelId) bueryUserData.channel_id = channelId;

                const accountExists = await buyerUserHelper.getBuyerUsers({ 
                    attributes:['id','first_name','last_name','email','phone_number','user_role_id','stripeID','promo_code_used','address1','address2','city','state','country','zip_code','notes','password_hash','token'],
                    where: { email: bueryUserData.email,user_role_id: userRoleId} });
                let token = await buyerUserHelper.getCryptoRandonToken();
                    
                if(_.get(accountExists,'0.dataValues.token')){
                    return response.status(400).send({ result: 'failed', message:'Password set link already sent to your email!' })
                } else if(!_.get(accountExists,'0.dataValues.token') && _.get(accountExists,'0.dataValues.password_hash')) {
                    return response.status(400).send({ result: 'failed', message:'Credential already exists' })
                } else if(_.get(accountExists,'0.dataValues.id') && !_.get(accountExists,'0.dataValues.token') && !_.get(accountExists,'0.dataValues.password_hash')) {
                    await buyerUserHelper.updateBuyerUsers(Object.assign({},bueryUserData,{ token, is_user_registered:true }),{
                        where: {
                            id: _.get(accountExists,'0.dataValues.id'),
                        }
                    });
                } else {
                    await buyerUserHelper.createBuyerUser(Object.assign({}, bueryUserData, {
                        user_role_id: userRoleId,
                        token: token,
                        created_at: Date.now(),
                        updated_at: Date.now(),
                        is_user_registered:true,
                        is_email_login:true
                    }));
                }

                let resetUrl = process.env.APP_YADARA_URL + '/set-password/' + token;
                var mailContents = {};
                mailContents.from = process.env.YADARA_EMAIL_FROM_ADDRESS;
                mailContents.to = bueryUserData.email;
                mailContents.subject = "Mail to set password";
                mailContents.message = await authEmailTemplate.yadaraPasswordSetMailTemplate(resetUrl,mailContents.to);
                sendMail.outgoingMails(mailContents);

                return response.status(200).send({ result: 'success', message:'Credential Successfully Added!' });
            });
        } catch(e) {
            return response.status(500).send({ result: 'failed', error:e });
        } 
        
    },

    async updateResetPassword(request,response){
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            
            if (error) {
                return response.status(500).send(error);
            }

            let token = fields.token && fields.token[0];
            let confirmPassword = fields.confirmPassword && fields.confirmPassword[0];
            let password = fields.password && fields.password[0];

            if(!token) return response.status(400).send({result:'failed',message:'Token required!'});

            if (!password) return response.status(400).send({result:'failed',message:'Password required!'});

            if (!confirmPassword) return response.status(400).send({result:'failed',message:'Confirmation password required!'});             
            
            if (confirmPassword !== password) return response.status(400).send({result:'failed',message:'Password and Confirm Password should be same!'});

            let userDetails = (await  buyerUserHelper.getBuyerUsers({
                attributes:['id','first_name','last_name','email','phone_number','user_role_id','stripeID','promo_code_used','address1','address2','city','state','country','zip_code','notes','password_hash','token'],
                where:{
                    token:token
                }
            }))[0];

            let email = _.get(userDetails,'dataValues.email');
            if(!userDetails) return response.status(400).send({result:'failed',message:'Invalid request!'});            

            let aws_crypted = await awsHelper.awsEncrypt(await bcrypt.hash(password, 10));
            
            let updatePassword = await buyerUserHelper.updateBuyerUsers({ password_hash: aws_crypted, token:null },{
                where: {
                    id: userDetails.dataValues.id,
                }
            });

            if((updatePassword && updatePassword[0] == 1) || (updatePassword.dataValues.id)){
                return response.status(200).send({result:'success',message:'Password updated successfully!',data:{email}});
            } else {
                return response.status(400).send({result:'failed',message:'Something went wrong while update the password!'});
            }
        })
        
    },
    async login(request,response) {
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            if (error) {
              return response.status(500).send(error);
            }
            let email = fields.email && fields.email[0];
            let password = fields.password && fields.password[0];

            if(!email)  return response.status(400).send({result:'failed',message:'Email is required!'});

            if(!password)  return response.status(400).send({result:'failed',message:'Password is required!'});

            const buyerUser = (await  buyerUserHelper.getBuyerUsers({
                attributes:['id','first_name','last_name','email','phone_number','user_role_id','stripeID','promo_code_used','address1','address2','city','state','country','zip_code','notes','password_hash','token'],
                where:{
                    email:email
                }
            }))[0];
            
            if (buyerUser) {
                if(!_.get(buyerUser,'dataValues.password_hash')){
                    return response.status(400).send({result:'failed',message:"Invalid credentials!"});
                }
                let aws_decrypted = await awsHelper.awsDecrypt(_.get(buyerUser,'dataValues.password_hash'));
                if (!bcrypt.compareSync(password, aws_decrypted)) {
                    return response.status(400).send({result:'failed',message:"Invalid credentials!"});
                } 
                let userDetails = {
                    firstName: buyerUser.dataValues.first_name,
                    lastName: buyerUser.dataValues.last_name,
                    userId: buyerUser.dataValues.id,
                    userRole: buyerUser.dataValues.user_role_id,          
                }

                const accessToken = jwt.sign( 
                    userDetails,
                    process.env.APP_JWT_KEY,{
                        expiresIn: 3600,
                    }
                );

                const refreshToken = jwt.sign( userDetails,  process.env.APP_JWT_KEY);

                return response.status(200).send({result:'success', data:{id:buyerUser.dataValues.id,accessToken:accessToken,refreshToken:refreshToken}});
                
            } else {
                return response.status(400).send({result:'failed', message:"Invalid credentials!"});
            }

        })
    },
    async googleSignUp(request,response) {
        let {type,code} = request.body;
        try{
            
            let {data:googleTokenResponse} = await googleHelper.getTokens({code,
                client_id: constants.googleOAuthKeys.clientId,
                client_secret: constants.googleOAuthKeys.clientSecret,
                redirect_uri:  constants.googleOAuthKeys.yadaraRedirectURI,
                grant_type: constants.googleOAuthKeys.grantType
            });
            
            let userProfile = await googleHelper.getUser({token:googleTokenResponse.access_token}); 
            
            const {email,name,id:googleId} = _.get(userProfile,'data');

            let buyerUser = (await  buyerUserHelper.getBuyerUsers({
                attributes:['id'],
                where:{email}
            }))[0];
            
            if(type == constants.authType.signUp) {

                if(buyerUser) return response.status(400).send({result:'failed',message:'User already exists!'});   

                let userRole = await buyerUserHelper.getBuyerUserRole();
                
                let userRoleId = _.get(userRole,'dataValues.id');

                if(!userRoleId) return  response.status(400).send({ result: 'failed', message:'Invalid Role!' });
                
                let buyerUserData = {
                    first_name: name ? name.split(" ")[0] :'',
                    last_name: name ? name.split(" ")[1] : '',
                    email:email,
                    password_hash: await awsHelper.awsEncrypt(await bcrypt.hash(`yadara@${new Date().getFullYear()}`, 10)),
                    google_id: googleId,
                    is_google_login: true,
                    is_user_registered:true
                }
                
                buyerUser = await buyerUserHelper.createBuyerUser(
                    Object.assign({},
                        buyerUserData,
                        {
                            user_role_id: userRoleId,
                            created_at: Date.now(),
                            updated_at: Date.now(),
                            address1:'',
                            city:'',
                            state:'',
                            country:''
                        }
                    )
                );

            } else if(type == constants.authType.signIn){
                if(!buyerUser) return response.status(400).send({result:'failed',message:'User does not exists!'});   
            }         
            
            await GoogleOAuthToken.create({
                buyer_user_id: buyerUser.dataValues.id,
                google_id: googleId,
                token_type: googleTokenResponse.token_type,
                access_token: googleTokenResponse.access_token,
                refresh_token: googleTokenResponse.refresh_token,
                id_token: googleTokenResponse.id_token,
                revoked: false,
                scope: googleTokenResponse.scope,
                expires_in: googleTokenResponse.expires_in
            });

            let userDetails = {
                firstName: buyerUser.dataValues.first_name,
                lastName: buyerUser.dataValues.last_name,
                userId: buyerUser.dataValues.id,
                userRole: buyerUser.dataValues.user_role_id,          
            }

            const appAccessToken = jwt.sign( 
                userDetails,
                process.env.APP_JWT_KEY,{
                    expiresIn: 3600,
                }
            );

            const refreshToken = jwt.sign( userDetails,  process.env.APP_JWT_KEY);

            return response.status(200).send({result:'success', data:{id:buyerUser.dataValues.id,accessToken:appAccessToken,refreshToken:refreshToken}});
        
        } catch(e){
            return response.status(400).send({result:'failed',message:_.get(e,'response.data.error_description'),errors:e})            
        } 
        
    },
    async facebookSignUp(request,response){
        let {type,accessToken:token,scope,expiresIn} = request.body;
        try{
            const { data: {first_name,last_name,id:facebookId} } = await facebookHelper.getProfile({token,fields:'first_name,last_name'});
            
            let buyerUser = (await  buyerUserHelper.getBuyerUsers({
                attributes:['id'],
                where:{facebook_id:facebookId,is_facebook_login:true}
            }))[0];
            
            if(type == constants.authType.signUp) {

                if(buyerUser) return response.status(400).send({result:'failed',message:'User already exists!'});   

                let userRole = await buyerUserHelper.getBuyerUserRole();
                
                let userRoleId = _.get(userRole,'dataValues.id');

                if(!userRoleId) return  response.status(400).send({ result: 'failed', message:'Invalid Role!' });
                
                let buyerUserData = {
                    first_name: first_name,
                    last_name: last_name,
                    password_hash: await awsHelper.awsEncrypt(await bcrypt.hash(`yadara@${new Date().getFullYear()}`, 10)),
                    facebook_id: facebookId,
                    is_facebook_login: true,
                    email:'',
                    is_user_registered:true
                }
                
                buyerUser = await buyerUserHelper.createBuyerUser(
                    Object.assign({},
                        buyerUserData,
                        {
                            user_role_id: userRoleId,
                            created_at: Date.now(),
                            updated_at: Date.now(),
                            address1:'',
                            city:'',
                            state:'',
                            country:''
                        }
                    )
                );

            } else if(type == constants.authType.signIn){
                if(!buyerUser) return response.status(400).send({result:'failed',message:'User does not exists!'});   
            }         
            
            await FacebookAuthToken.create({
                buyer_user_id: buyerUser.dataValues.id,
                facebook_id:facebookId,
                access_token: token,
                revoked: false,
                scope: scope,
                expires_in: expiresIn
            });

            let userDetails = {
                firstName: buyerUser.dataValues.first_name,
                lastName: buyerUser.dataValues.last_name,
                userId: buyerUser.dataValues.id,
                userRole: buyerUser.dataValues.user_role_id,          
            }

            const appAccessToken = jwt.sign( 
                userDetails,
                process.env.APP_JWT_KEY,{
                    expiresIn: 3600,
                }
            );

            const refreshToken = jwt.sign( userDetails,  process.env.APP_JWT_KEY);

            return response.status(200).send({result:'success', data:{id:buyerUser.dataValues.id,accessToken:appAccessToken,refreshToken:refreshToken}});
        
        } catch(e){
            console.log("errors => ",e)
            return response.status(400).send({result:'failed',message:_.get(e,'response.data.error_description'),errors:e})            
        } 
    },
    async getUser(request,response){
        const {buyerUserId} = request.params;

        if(!buyerUserId) return response.status(400).send({result:'failed', message:"Invalid buyer user id!"});

        const buyerUser = await BuyerUsers.findOne({ 
            attributes:['id','first_name','last_name','email','phone_number','user_role_id','address1','address2','city','state','country','zip_code','notes','is_google_login','is_facebook_login','is_email_login'],
            where: { id: buyerUserId } 
        });

        if(!buyerUser) return response.status(400).send({result:'failed', message:"Invalid buyer user id!"});
        
        return response.status(200).send({result:'success', data:buyerUser});

    },
    async sendResetMail(request,response) {
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            let emailType = fields.emailType[0];
            if (error) {
              return response.status(500).send(error);
            }
            let email = fields.email[0];

            let buyerUserDetails = (await  buyerUserHelper.getBuyerUsers({
                attributes:['id','first_name','last_name','email','phone_number','user_role_id','stripeID','promo_code_used','address1','address2','city','state','country','zip_code','notes','password_hash','token','is_google_login','is_facebook_login','is_email_login'],
                where:{
                    email:email
                }
            }))[0];

            let googleLogin = _.get(buyerUserDetails,'dataValues.is_google_login');
            let facebookLogin = _.get(buyerUserDetails,'dataValues.is_facebook_login');
            if(facebookLogin||googleLogin){
                return response.status(400).send({result:'failed', message:'You are unable to reset password!'});
            }
            
            if (buyerUserDetails) {
        
                let token = _.get(buyerUserDetails,'dataValues.token');
                let buyerUserId = _.get(buyerUserDetails,'dataValues.id');
                let resetUrl;
                if (token) {
                    resetUrl = process.env.APP_YADARA_URL + '/change-password/' + buyerUserDetails.dataValues.token;
                } else {
                    let newToken = await buyerUserHelper.getCryptoRandonToken();
                    await buyerUserHelper.updateBuyerUsers({
                        token : newToken
                    } , {
                        where:{
                            id:buyerUserId
                        }
                    });
                    resetUrl = process.env.APP_YADARA_URL + '/change-password/' + newToken;
                }
        
                var mailContents = {};
                mailContents.from = process.env.YADARA_EMAIL_FROM_ADDRESS;
                mailContents.to = email;
                mailContents.subject = "Mail to reset password";
                mailContents.message = await authEmailTemplate.yadaraForgetPasswordMailTemplate(resetUrl,emailType);
                const mail_status = await sendMail.outgoingMails(mailContents);
                if(mail_status){
                    return response.status(200).send({result:'success', message:'We have sent information on how to reset your password to your email address'});
                }else{
                    return response.status(400).send({result:'failed', message:'Invalid credential! Please check the entered email!'});
                }
            }
            else {
                return response.status(400).send({result:'failed', message:'Invalid credential! Please check the entered email!'});
            }
        });
    },
    async ticketDeliverTo(request,response) {

        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {

            let buyerUserData = await buyerUserHelper.formDataToBuyerUserData(fields);

            let buyerUserDetails = (await  buyerUserHelper.getBuyerUsers({
                attributes:['id','first_name','last_name','email','phone_number','user_role_id','address1','address2','city','state','country','zip_code','notes'],
                where:{
                    email:buyerUserData.email
                }
            }))[0];

            if(!buyerUserDetails) {
                let userRole = await buyerUserHelper.getBuyerUserRole();
    
                let userRoleId = _.get(userRole,'dataValues.id');
                if(!userRoleId) return  response.status(400).send({ result: 'failed', message:'Invalid Role!' });

                buyerUserDetails = await buyerUserHelper.createBuyerUser(
                    Object.assign({},
                        buyerUserData,
                        {
                            user_role_id: userRoleId,
                            created_at: Date.now(),
                            updated_at: Date.now(),
                            is_user_registered: false
                        }
                    )
                );
            }

            let userData = await buyerUserHelper.formatUserDetailsForToken(buyerUserDetails)
            let accessToken = await buyerUserHelper.getToken(userData,'accessToken',process.env.APP_OPEN_JWT_KEY);
            return response.status(200).send({result:'success', data:{ticketDeliverUser:buyerUserDetails,accessToken}});

        });

    },
    async refreshToken(request,response) {

        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            if (error) {
              return response.status(500).send(error);
            }
            var {userId} = jwt.decode(fields.refreshToken[0]);
            let buyerUser = (await  buyerUserHelper.getBuyerUsers({
                attributes:['id','first_name','last_name','email','phone_number','user_role_id','stripeID','promo_code_used','address1','address2','city','state','country','zip_code','notes','password_hash','token'],
                where:{id:userId}
            }))[0];
            if(!buyerUser)
                return response.status(400).json({result:'failed',message:'User Not found!'})
            const accessToken = jwt.sign({
                firstName: buyerUser.dataValues.first_name,
                lastName: buyerUser.dataValues.last_name,
                userId: buyerUser.dataValues.id,
                userRole: buyerUser.dataValues.user_role_id,          
            },
            process.env.APP_JWT_KEY,{
                expiresIn: 3600,
            });
            return response.status(200).send({result:'success', data:{id:buyerUser.dataValues.id,accessToken:accessToken}});
        
        });
        
    },
    async saveProfile(request,response) {
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            if (error) {
              return response.status(500).send(error);
            }
            try{
                let userId = _.get(fields,'buyer_user_id.0');
                if(!userId) return response.status(400).json({result:'failed',message:'Invalid user id!'});
                let user = await BuyerUsers.findOne({
                    attributes:['id','first_name','last_name','email','phone_number','user_role_id','stripeID','promo_code_used','address1','address2','city','state','country','zip_code','notes','password_hash','token'],
                    where:{id:userId}
                })
                if(!user) return response.status(400).json({result:'failed',message:'Invalid user id!'});

                let datas = {
                    first_name:'',
                    last_name:'',
                    email: '',
                    phone_number: '',
                    address1: '',
                    address2: '',
                    city: '',
                    state: '',
                    zip_code: '',
                    country: ''
                }

                for(let key in datas){
                    if(_.get(fields,key+'.0') || _.get(fields,key+'.0')=='')
                        datas[key] = _.get(fields,key+'.0');
                    else delete datas[key];
                }
                await BuyerUsers.update(datas,{where:{id:userId}})

                return response.status(200).json({status:'success',message:'Contact information successfully updated!'})
            } catch(e) {
                return response.status(500).json({status:'error',error:e})
            }
            
            
        })
    },
    async saveTicketingSystemUserLogin(request,response) {
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            if (error) {
              return response.status(500).send(error);
            }
            
            try{
                let datas = {};
                for(let key in fields){
                    if(_.get(fields,key+'.0') || _.get(fields,key+'.0')=='')
                        datas[key] = _.get(fields,key+'.0');
                }
                let ticketingSystem = await TicketingSystemUserLogin.findOne({
                    where:{
                        buyer_user_id : datas.buyer_user_id,
                        user_name : datas.user_name
                    }
                });
                if(ticketingSystem){
                    await TicketingSystemUserLogin.update({
                        ticketing_system_id : datas.ticketing_system_id
                    },{
                        where:{
                            buyer_user_id : datas.buyer_user_id,
                            user_name : datas.user_name
                        }
                    });
                    return response.status(200).json({status:'success',message:'Ticketing system user login successfully updated!'})
                } else {
                    await TicketingSystemUserLogin.create(datas);
                    return response.status(200).json({status:'success',message:'Ticketing system user login successfully added!'})
                }
                
            } catch(e) {
                return response.status(500).json({status:'error',error:e})
            }
            
            
        })
    }
}
    
