const db = require('../models');
const multiparty = require('multiparty');
const { Op } = require('sequelize');
const sequelize = require('sequelize');
const path = require('path');
const AWS = require('aws-sdk');
const Transaction = db.transaction;
const Brokerorder = db.brokerorder;
const Event = db.events;
const Listing = db.listings;
const Listinggroup = db.listinggroup;
const Buyerusers = db.buyerUsers;
const Deliverystatus = db.deliverystatus;
const Orderstatus = db.orderstatus;
const Channel = db.channels;
const Brokerorderlisting = db.brokerorderlisting;
const Marketplaceorder = db.marketplaceorder;
const Marketplaceorderlisting = db.marketplaceorderlisting;
const PurchaseOrderInvoiceAttachment = db.purchaseorderinvoiceattachment;

const Transactionlisting = db.transactionlisting;
const Tickettype = db.tickettype;
const TicketSystem= db.ticketingsystems;

const _ = require('lodash');
const { folderNames,listingStatusList } = require('../constants/constants');
const { getListingStatusbyName } = require('../helpers/queryResponse');
module.exports = {
    async orderTickets(request, response) {
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {

            if (error) {
                return response.status(500).send(error);
            };

            try {

                let userId = _.get(fields, 'userId.0');
                let ticketQuantity = _.get(fields, 'ticketQuantity.0');

                if (!ticketQuantity) return response.status(400).send({ result: 'failed', message: 'Ticket Quantity mandatory field' });

                if (isNaN(ticketQuantity)) return response.status(400).send({ result: 'failed', message: 'Invalid Ticket Quantity' });

                let buyerUserId = _.get(fields, 'buyerUserId.0');

                if (!buyerUserId) return response.status(400).send({ result: 'failed', message: 'Buyer User Id mandatory field' });

                let buyerUser = await Buyerusers.findOne({ attributes: ['id'], where: { id: buyerUserId } });

                if (!buyerUser) return response.status(400).send({ result: 'failed', message: 'Invalid Buyer User Id' });

                let listingIds = _.get(fields, 'listingIds', []);

                let listings = await Listing.findAll({
                    attributes: ['id', 'listing_group_id'],
                    where: { id: { [Op.in]: listingIds } }
                });

                let uinqueListingGroupId = new Set();
                listings.map((listing) => {
                    uinqueListingGroupId.add(_.get(listing, 'dataValues.listing_group_id'))
                })

                if ([...uinqueListingGroupId].length > 1 || listingIds.length != listings.length) return response.status(400).send({ result: 'failed', message: 'Invalid Listing Id' });

                let listingGroupId = [...uinqueListingGroupId][0];
                let listingGroup = await Listinggroup.findOne({
                    attributes:['eventId','in_hand'],
                    where: {
                        id: listingGroupId
                    }
                });

                let inHandDate = _.get(listingGroup, 'dataValues.in_hand');
                if (!inHandDate)
                    inHandDate = new Date();
                else
                    inHandDate = new Date(inHandDate);

                

                let eventId = _.get(listingGroup, 'dataValues.eventId');
                let event = await Event.findOne({ attributes:['created_by'], where: { id: eventId } });

                let sellerId = _.get(event, 'dataValues.created_by');

                let orderStatus = await Orderstatus.findOne({ attributes:['id'], where: { name: 'active' } });
                let orderStatusId = _.get(orderStatus, 'dataValues.id');

                let deliveryStatus = await Deliverystatus.findOne({ attributes:['id'], where: { name: 'pending' } });
                let deliveryStatusId = _.get(deliveryStatus, 'dataValues.id');

                let transactionData = {
                    user_id: sellerId,
                    order_status_id: orderStatusId,
                    buyer_user_id: buyerUserId,
                    payment_id: 328794,
                    event_id: eventId,
                    listing_group_id: listingGroupId,
                    payout_id: 1,
                    created_at: new Date(),
                    created_by: userId
                }

                let transaction = await Transaction.create(transactionData, { returning: ['id'] });
                let transactionId = _.get(transaction, 'dataValues.id');

                let channel = await Channel.findOne({
                    attributes: ['id'],
                    where: { channel_name: 'Yadara' }
                });

                let channelId = _.get(channel, 'dataValues.id');
                let channel_reference_id = 1;

                let brokerOrderData = {
                    event_id: eventId,
                    listing_group_id: listingGroupId,
                    channel_id: channelId,
                    channel_reference_id: channel_reference_id,
                    transaction_id: transactionId,
                    buyer_user_id: buyerUserId,
                    user_id: sellerId,
                    order_status_id: orderStatusId,
                    delivery_status_id: deliveryStatusId,
                    delivery_at: inHandDate,
                    cancelled_order_type_id: null,
                    createdAt: new Date(),
                    created_by: userId
                }

                let brokerOrder = await Brokerorder.create(brokerOrderData);
                let brokerOrderId = _.get(brokerOrder, 'dataValues.id');

                let marketplaceOrderData = {
                    broker_order_id: brokerOrderId,
                    user_id: sellerId,
                    event_id: eventId,
                    listing_group_id: Number(listingGroupId),
                    transaction_id: transactionId,
                    buyer_user_id: Number(buyerUserId),
                    order_status_id: orderStatusId,
                    delivery_status_id: deliveryStatusId,
                    createdAt: new Date(),
                    created_by: userId,
                }

                let marketPlaceOrder = await Marketplaceorder.create(marketplaceOrderData);

                let marketPlaceOrderId = _.get(marketPlaceOrder, 'dataValues.id');

                let transactionListingsData = [];
                let brokerOrderListingData = [];
                let marketplaceOrderListingData = [];

                for (let listingId of listingIds) {
                    transactionListingsData.push({
                        transaction_id: transactionId,
                        listing_id: listingId,
                    });
                    brokerOrderListingData.push({
                        broker_order_id: brokerOrderId,
                        listing_id: listingId,
                    });
                    marketplaceOrderListingData.push({
                        marketplace_order_id: marketPlaceOrderId,
                        listing_id: listingId,
                    });
                }

                await Transactionlisting.bulkCreate(transactionListingsData);

                await Brokerorderlisting.bulkCreate(brokerOrderListingData);

                await Marketplaceorderlisting.bulkCreate(marketplaceOrderListingData);

                const soldListingStatus = await getListingStatusbyName(listingStatusList.sold);

                await Listing.update({ hold: false, listing_status_id:soldListingStatus.id }, {
                    where: {
                        id: {
                            [Op.in]: listingIds
                        }
                    }
                });

                return response.status(200).json({ status: 'success', message: 'Order successfully processed', orderId: brokerOrderId });

            } catch (e) {
                return response.status(500).send({ result: 'failed', error: e });
            }

        })
    },

    async viewPurchaseOrderFile(req, res) {

        const attachmentId = req.body.purchase_order_attachment_id;

        let purchase_order_attachments = await PurchaseOrderInvoiceAttachment.findOne({
            attributes: [
                [sequelize.col('attachment_name'), 'attachment_name'],
                [sequelize.col('attachment_url'), 'attachment_url'],
                [sequelize.col('purchase_order_id'), 'purchase_order_id'],
            ],
            where: { id: attachmentId },
            raw: true
        });


        const bucketName = process.env.AWS_BUCKETNAME + '/'+ folderNames.purchaseOrderFolder +'/' + purchase_order_attachments.purchase_order_id;
        var attachmenturl = purchase_order_attachments.attachment_url;

        try {
            const params = {
                Bucket: bucketName,
                Key: path.basename(attachmenturl)
            };

            const s3 = new AWS.S3({
                accessKeyId: process.env.AWS_ACCESS_KEY,
                secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
            });

            s3.getObject(params).promise()
                .then(file => {
                    res.setHeader('content-type', file.ContentType);
                    res.send(file.Body.toString('base64'));
                })
                .catch(err => {
                    res.send(false);
                });

        } catch (error) {
            res.send(false);
        }
    },
    async getOrderDetails(request, response) {
        try {
            let { orderId } = request.params;

            if (!orderId) return response.status(400).send({ result: 'failed', message: 'Order id required' });

            Brokerorder.belongsTo(db.listinggroup, { foreignKey: 'listing_group_id' });
            Listing.belongsTo(db.listinggroup, { foreignKey: 'listing_group_id' });
            Listinggroup.belongsTo(Tickettype,{foreignKey:'ticket_type_id'})
            Listinggroup.belongsTo(TicketSystem,{foreignKey:'ticket_system_id'})

            let brokerOrder = await Brokerorder.findOne({
                attributes: ['id', 'delivery_at', [sequelize.col('listing_group.in_hand'), 'in_hand']],
                where: {
                    id: orderId
                },
                include: {
                    model: Listinggroup,
                    attributes: ['delivery_type'],
                    include: [{
                            model: Tickettype,
                            attributes: ['name'],
                        },{
                            model: TicketSystem,
                            attributes: ['ticketing_system_name'],
                    }]
                }
            });

            if (!brokerOrder) return response.status(400).send({ result: 'failed', message: 'Invalid order id' });

            return response.status(200).send({ result: 'success', data: brokerOrder });
        } catch (e) {
            return response.status(500).send({ result: 'failed', error: e });
        }
    },
    async getStates(request, response) {
        const Venue = db.venue;
        try {
            let stateList = await Venue.findAll({
                attributes: [[sequelize.fn('DISTINCT', sequelize.col('state')), 'state']]
            })
            return response.json({ result: 'success', data: stateList });
        } catch (e) {
            return response.status(500).send({ result: 'failed', error: e });
        }
    }
}