//S3 bucket
const fs = require('fs');
const AWS = require('aws-sdk');
const multiparty = require('multiparty');
const path = require('path');
const db = require('../models');
const sequelize = require('sequelize');
const Events = db.events;
const Channels = db.channels;
const Venue = db.venue;
const Category = db.category;
const Genre = db.genre;
import { uploadURLs, folderNames, s3Path } from "../constants/constants";

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});
//S3 bucket

module.exports = {

    /**
    * Purpose: Upload File
    * 
    * return: {object} Last upload file detail
    **/
    async upload(request, response) {
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            if (error) {
                return response.status(500).send(error);
            };
            try {
                const path1 = files.attachment_file_data[0].path;
                const buffer = fs.readFileSync(path1);
                const attachment_file = fields.attachment_file[0];
                const eventId = fields.eventId[0];
                const ListingId = fields.ListingId[0];
                const params = {
                    Body: buffer,
                    Bucket: process.env.AWS_BUCKETNAME + uploadURLs.listingsAttachmentURL + '/' + eventId + '/' + ListingId,
                    Key: attachment_file,
                };
                const upload = s3.upload(params).promise();
                if (upload) {
                    response.send('File uploaded successfully')
                }
            } catch (err) {
                return response.status(500).send(err);
            }
        });

        response.send('File uploaded successfully')
    },
    /**
     * Download the ticket file from aws s3 bucket
     * @param {*} request  event id, listing id, filename
     * @param {*} res file converted into base64 string and response
     */
    async downloadTicketFile(request, res) {
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            if (error) {
                res.send(false);
            };
            const eventId = fields.eventId[0];
            const ListingId = fields.ListingId[0];
            const attachmentname = fields.attachmentname[0];
            try {
                const params = {
                    Bucket: process.env.AWS_BUCKETNAME + uploadURLs.listingsAttachmentURL + '/' + eventId + '/' + ListingId,
                    Key: attachmentname
                };
                const s3 = new AWS.S3({
                    accessKeyId: process.env.AWS_ACCESS_KEY,
                    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
                });
                s3.getObject(params).promise()
                    .then(file => {
                        res.send(file.Body.toString('base64'));
                    })
                    .catch(err => {
                        res.send(false);
                    });
            } catch (error) {
                res.send(false);
            }
        });
    },

    /**
     * Page : View Event Attachment
     * Function For : To View Single Image Common Function
     * Ticket No : TIC 97 Sprint 7
     */
        async viewEventAttachment(req, res) {
        
            const eventId = req.body.eventId;

            let event_attachments = await Events.findOne({
                attributes: [
                    [sequelize.col('image'), 'attachmenturl']
                ], 
                where: {id: eventId},
                raw:true
            })
            var attachmenturl = event_attachments.attachmenturl;

            const bucketName =  process.env.AWS_BUCKETNAME + uploadURLs.uploadListingsURL + '/'+ folderNames.eventsFolder +'/'+ eventId;
            try {
                const params = {
                    Bucket: bucketName,
                    Key: path.basename(attachmenturl)
                };
                const s3 = new AWS.S3({
                    accessKeyId: process.env.AWS_ACCESS_KEY,
                    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
                });

                s3.getObject(params).promise()
                .then(file => {
                    res.setHeader('content-type', file.ContentType);
                    res.send(file.Body.toString('base64'));
                    
                })
                .catch(err => {     
                    res.send(false);
                });
    
            } catch (error) {
                res.send(false);
            }
        },

    /**
     * Page : Purchase Order
     * Function For : To Upload Single Image Common Function
     * Ticket No : TIC 529 Sprint 7
     */
    async invoiceUpload(request, response) {
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            if (error) {
                return response.status(500).send(error);
            }
            try {
                const path = files.attachment_file_data[0].path;
                const buffer = fs.readFileSync(path);
                const attachment_file = fields.attachment_file_name[0];
                const purchaseOrderId = fields.attachment_po_id[0]
                const Bucketpath = process.env.AWS_BUCKETNAME + '/'+ folderNames.purchaseOrderFolder +'/' + purchaseOrderId;
                const params = {
                    Body: buffer,
                    Bucket: Bucketpath,
                    Key: attachment_file,
                };
                const upload = s3.upload(params).promise();
                if (upload) {
                    return response.send('File uploaded successfully')
                }

            } catch (err) {
                return response.status(500).send(err);
            }
        });
    },

    /**
     * Page : -
     * Function For : To Upload Single Image Common Function
     * Ticket No : TIC 586 Sprint 7
     */
    async imageupload(request, response) {
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            if (error) {
                return response.status(500).send(error);
            };
            try {
                const path = files.attachment_file_data[0].path;
                const buffer = fs.readFileSync(path);
                const attachment_file = fields.attachment_file[0];
                const folderName1 = fields.folderName1[0];
                const folderName2 = (fields.folderName2 && fields.folderName2.length > 0) ? fields.folderName2[0] : "";//Primary ID"
                var bucketPath = process.env.AWS_BUCKETNAME + uploadURLs.uploadListingsURL + '/' + folderName1;
                if (folderName2 != "") {
                    bucketPath += '/' + folderName2;
                }
                const params = {
                    Body: buffer,
                    Bucket: bucketPath,
                    Key: attachment_file,
                };

                const upload = await s3.upload(params).promise();
                if (upload) {

                    var attachmentdata = {
                        image: s3Path.basePath + upload.Bucket + '/' + upload.key,
                    };

                    if (folderName1 == folderNames.eventsFolder) {
                        await Events.update(
                            attachmentdata,
                            {
                                where: { id: folderName2 }
                            }
                        );
                    }
                    else if (folderName1 == folderNames.channelsFolder) {
                        var attachmentdata1 = {
                            logo_url: s3Path.basePath + upload.Bucket + '/' + upload.key,
                        };

                        await Channels.update(
                            attachmentdata1,
                            {
                                where: { id: folderName2 }
                            }
                        );
                    }
                    else if (folderName1 == folderNames.venuesFolder) {
                        await Venue.update(
                            attachmentdata,
                            {
                                where: { id: folderName2 }
                            }
                        );
                    }
                    else if (folderName1 == folderNames.caegoryFolder) {
                        await Category.update(
                            attachmentdata,
                            {
                                where: { id: folderName2 }
                            }
                        );
                    }
                    else if (folderName1 == folderNames.genreFolder) {
                        await Genre.update(
                            attachmentdata,
                            {
                                where: { id: folderName2 }
                            }
                        );
                    }

                    response.send('File uploaded successfully')
                }
            } catch (err) {
                return response.status(500).send(err);
            }
        });
    },

    async uploadlistingfile(request, response) {
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            if (error) {
                return response.status(500).send(error);
            };
            try {
                var fileName = files.file[0].originalFilename; // original file name
                var file = files.file[0].path; // real file path with temporary name

                fs.readFile(file, function (err, data) {
                    if (!fileName) {
                        res.redirect("/");
                        res.end();
                    }
                    else {
                        var currentdate = new Date();
                        var myCurDate = currentdate.getDate() + ""
                            + (currentdate.getMonth() + 1) + ""
                            + currentdate.getFullYear() + ""
                            + currentdate.getHours() + ""
                            + currentdate.getMinutes() + ""
                            + currentdate.getSeconds();

                        var appDir = path.dirname(require.main.filename);
                        var filePath = "uploads/" + "" + myCurDate + "" + fileName;
                        var newPath = appDir + "/" + filePath;
                        fs.writeFile(newPath, data, function (err) {
                            return response.status(200).send(filePath);
                        });
                    }
                });

            } catch (err) {
                return response.status(500).send(err);
            }
        });
    },
}

