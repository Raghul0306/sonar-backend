const db = require('../models');
const _ = require('lodash');
const multiparty = require('multiparty');
const Favorites = db.favorities;
module.exports = {
    saveFavorities: (request,response) => {
        const form = new multiparty.Form();
        form.parse(request, async (error, fields, files) => {
            if (error) {
              return response.status(500).send(error);
            }
            try{
                let data = {};
                for(let key in fields){
                    data[key] = fields[key][0];
                }
                let buyerUserId = data.buyerUserId;
                let eventId = data.eventId;
                let favoriteType = data.favoriteType;
                let favorite = await Favorites.findOne({
                    where:{
                        buyer_user_id: buyerUserId,
                        event_id: eventId,
                    }
                });

                let FavoritesData = {
                    favorite_type: favoriteType == 'true',
                    buyer_user_id: buyerUserId,
                    event_id: eventId,
                    createdAt:  new Date(),
                    created_by: buyerUserId
                }

                if(!favorite){
                    FavoritesData.createdAt = new Date();
                    FavoritesData.created_by = buyerUserId;
                    await Favorites.create(FavoritesData);
                } else {
                    FavoritesData.updatedAt = new Date();
                    FavoritesData.updated_by = buyerUserId;
                    await Favorites.update(FavoritesData,{
                        where: {
                            id: _.get(favorite,'dataValues.id')
                        }
                    });
                }

                return response.status(200).send({ result: 'success', message:'Favorites Successfully Updated!' });
            } catch(e) {
                return response.status(500).send({ result: 'failed', error:e });
            }

        })
        
    },
    getFavorities: async (request,response) => {
        let {buyerUserId} = request.params;
        try{
            let favorites = await Favorites.findAll({
                attributes:['favorite_type','event_id'],
                where:{
                    buyer_user_id: buyerUserId,
                    favorite_type: true
                }
            });
            return response.status(200).json({status:'success', data:favorites});

        } catch(e) {
            return response.status(500).send({ result: 'failed', error:e });
        }
    }
}