module.exports = function (sequelize, Sequelize, DataTypes) {

    const Companies = sequelize.define("companies", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
    
          company_name: {
            type: Sequelize.STRING,
            allowNull: false
          },

          parent_company_id: {
            type: Sequelize.INTEGER,
            after: "company_name",
            allowNull: true,
            onDelete: 'cascade',
            references: {
            model: {
            tableName: 'companies',
            schema: 'ticket_exchange'
            },
            key: 'id'
            },
            comment: "user phone number country id are stored in this field"
          },
          address1: {
            type: Sequelize.TEXT,
            after: "parent_company_id",
            allowNull: true,
            comment: "company name is stored in this field"
          },
          address2: {
            type: Sequelize.TEXT,
            after: "address1",
            allowNull: true,
            comment: "company name is stored in this field"
          },
          city: {
            type: Sequelize.INTEGER,
            after: "address2",
            allowNull: true,
            onDelete: 'cascade',
            references: {
            model: {
            tableName: 'cities',
            schema: 'ticket_exchange'
            },
            key: 'id'
            },
            comment: "company city id are stored in this field"
          },
          state: {
            type: Sequelize.INTEGER,
            after: "city",
            allowNull: true,
            onDelete: 'cascade',
            references: {
            model: {
            tableName: 'states',
            schema: 'ticket_exchange'
            },
            key: 'id'
            },
            comment: "company state id are stored in this field"
          },
          zip_code: {
            allowNull: true, 
            after: "state",
            type: Sequelize.STRING,
            comment: "company zip code are stored in this field"
          },
          country: {
            type: Sequelize.INTEGER,
            after: "zip_code",
            allowNull: true,
            onDelete: 'cascade',
            references: {
            model: {
            tableName: 'countries',
            schema: 'ticket_exchange'
            },
            key: 'id'
            },
            comment: "company country id are stored in this field"
          },
          
          createdAt: {
              field: 'created_at', 
            allowNull: false,
            type: Sequelize.DATE,
            comment: "When the genre field is created the timestamp is stored in this field"
          },

          created_by: {
            type: Sequelize.INTEGER,
            allowNull: false,
            comment: "The user who made the create"
          },

          updatedAt: {
            field: 'updated_at', 
            allowNull: true,
            type: Sequelize.DATE,
            comment: "When the gentre field is updated the timestamp is stored in this field"
          },

          updated_by: {
            type: Sequelize.INTEGER,
            allowNull: true,
            comment: "The user who made the update"
          }
    },
        {
            schema: 'ticket_exchange',
            tableName: 'companies'
        },
    );

    return Companies;

}
