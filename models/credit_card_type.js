module.exports = function (sequelize, Sequelize, DataTypes) {

    const CreditCardType = sequelize.define("credit_card_type", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,       
          },
          type_name: {
            allowNull: true,
            type: Sequelize.STRING
          },
          createdAt: {
            field: 'created_at', 
            allowNull: false,
            type: Sequelize.DATE,
            comment: "the date and time the order as created"
          },
          created_by: {
            allowNull: true,
            type: Sequelize.INTEGER,
            comment: "who created the order"
          },
          updatedAt: {
            field: 'updated_at', 
            allowNull: true,
            type: Sequelize.DATE,
            comment: "the date and time the order was last updated"
          },
          updated_by: {
            allowNull: true,
            type: Sequelize.INTEGER,
            comment: "the user who made the update"
          }
    },
        {
            schema: 'ticket_exchange',
            tableName: 'credit_card_type'
        },
    );

    return CreditCardType;

}
