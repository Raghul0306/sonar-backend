module.exports = function(sequelize, Sequelize, DataTypes) {

  const Useraccount = sequelize.define("user_account", {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    user_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'user',
          schema: 'ticket_exchange'
        },
        key: 'id'
      }
    },

    email: {
      type: Sequelize.STRING
    },
    password_hash: {
      type: Sequelize.TEXT
    },
    createdAt: {
      field: 'created_at',
      allowNull: false,
      type: Sequelize.DATE,
    },
    updatedAt: {
      field: 'updated_at',
      allowNull: false,
      type: Sequelize.DATE,
    }
    
  }, 
  {
      schema: 'ticket_exchange_private',
      tableName: 'user_account'
  });

  return Useraccount;

}
