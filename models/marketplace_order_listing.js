module.exports = function (sequelize, Sequelize, DataTypes) {
  const Marketplaceorderlisting = sequelize.define("marketplace_order_listing",{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    marketplace_order_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'marketplace_order',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "marketplace order id stored in this field"
    },
    listing_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'listing',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "listing id stored in this field"
    }
  },
  {
      schema: 'ticket_exchange',
      tableName: 'marketplace_order_listing',
      timestamps: false,
  });
  return Marketplaceorderlisting;
};