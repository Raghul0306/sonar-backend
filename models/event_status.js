module.exports = function (sequelize, Sequelize, DataTypes) {

    const EventStatus = sequelize.define("event_status", {

        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          event_status: {
            type: Sequelize.STRING,
            allowNull: false
          },
          createdAt: {
            field: 'created_at', 
            allowNull: false,
            type: Sequelize.DATE
          },
          created_by: {
            allowNull: false, 
            type: Sequelize.INTEGER, 
            defaultValue: 0
          }, 
          updatedAt: {
            field: 'updated_at', 
            allowNull: false,
            type: Sequelize.DATE
          }, 
          updated_by: {
            allowNull: false, 
            type: Sequelize.INTEGER, 
            defaultValue: 0
          }
    },
        {
            schema: 'ticket_exchange',
            tableName: 'event_status'
        },
        );

    return EventStatus;

}
