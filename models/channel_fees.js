module.exports = function (sequelize, Sequelize, DataTypes) {

    const ChannelFees = sequelize.define("channel_fees", {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        user_id: {
          allowNull: false, 
          type: Sequelize.INTEGER,
          onDelete: 'cascade',
          references: {
            model: {
              tableName: 'user',
              schema: 'ticket_exchange'
            },
            key: 'id'
          }
        }, 
        channel_id: {
          allowNull: false, 
          type: Sequelize.INTEGER,
          onDelete: 'cascade',
          references: {
            model: {
              tableName: 'channels',
              schema: 'ticket_exchange'
            },
            key: 'id'
          }
        },
        fee: {
          allowNull: false, 
          type: Sequelize.FLOAT
        },
        createdAt: {
            field: 'created_at',
            allowNull: false,
            type: Sequelize.DATE,
            comment: "When the channel fees field is created the timestamp is stored in this field"
          },
        created_by: {
          allowNull: false, 
          type: Sequelize.INTEGER,
          onDelete: 'cascade',
          references: {
            model: {
              tableName: 'user',
              schema: 'ticket_exchange'
            },
            key: 'id'
          }
        }, 
        updatedAt: {
            field: 'updated_at',
            allowNull: false,
            type: Sequelize.DATE,
            comment: "When the channel fees field is updated the timestamp is stored in this field"
          }, 
        updated_by: {
          allowNull: true, 
          type: Sequelize.INTEGER,
          onDelete: 'cascade',
          references: {
            model: {
              tableName: 'user',
              schema: 'ticket_exchange'
            },
            key: 'id'
          }
        }
    },
    {
        schema: 'ticket_exchange',
        tableName: 'channel_fees'
    },
  );

  return ChannelFees;
  }