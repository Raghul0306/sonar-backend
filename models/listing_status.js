module.exports = function (sequelize, Sequelize, DataTypes) {

    const listingStatus = sequelize.define("listing_status", {

        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        status: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        name: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        createdAt: {
            field:'created_at',
            type: Sequelize.DATE
        },
        created_by: {
            allowNull: false, 
            type: Sequelize.INTEGER, 
            defaultValue: 0
          }, 
        updatedAt: {
            field:'updated_at',
            type: Sequelize.DATE
        }, 
        updated_by: {
            allowNull: true, 
            type: Sequelize.INTEGER, 
            defaultValue: 0
        }
    },
        {
            schema: 'ticket_exchange',
            tableName: 'listing_status'
        },
        );

    return listingStatus;

}
