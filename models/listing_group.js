module.exports = function (sequelize, Sequelize, DataTypes) {

  const ListingGroup = sequelize.define("listing_group", {

    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    seller_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'user',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    },

    eventId: {
      allowNull: false,
      field: 'event_id',
      type: Sequelize.INTEGER,
      onDelete: 'cascade',
      references: {
          model: 'event',
          key: 'id'
      }
    },

    in_hand: {
      type: Sequelize.DATE
    },

    ticket_type_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'ticket_types',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    },

    splits_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'splits',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    },

    seat_type_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'seat_type',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    },

    ticket_system_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'ticketing_systems',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    },

    ticket_system_account: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'user',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    },

    hide_seat_numbers: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },

    section: {
      type: Sequelize.STRING,
    },

    row: {
      type: Sequelize.STRING,
    },

    quantity: {
      type: Sequelize.INTEGER,
      defaultValue: 0,
    },

    seat_start: {
      type: Sequelize.STRING,
    },

    seat_end: {
      type: Sequelize.STRING,
    },

    face_value: {
      type: Sequelize.FLOAT
    },

    unit_cost: {
      type: Sequelize.FLOAT,
    },

    group_cost: {
      type: Sequelize.FLOAT,
    },

    group_price: {
      type: Sequelize.FLOAT,
    },

    last_price: {
      type: Sequelize.FLOAT
    },

    last_price_change: {
      type: Sequelize.DATE
    },

    PO_ID: {
      type: Sequelize.STRING,
    },

    internal_notes: {
      type: Sequelize.TEXT,
    },

    external_notes: {
      type: Sequelize.TEXT,
    },

    zone_seating: {
      type: Sequelize.STRING,
    },

    is_attachments: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },

    tevoid: {
      type: Sequelize.INTEGER,
    },

    mercuryid: {
      type: Sequelize.INTEGER,
    },

    delivery_type: {
      type: Sequelize.STRING,
      comment: "delivery type are stored in this field"
    },

    tags: {
      type: Sequelize.STRING,
    },

    inactive: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      comment: "inactive are stored in this field"
    },
    
    is_removed: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      comment: "is_removed are stored in this field"
    },

    qc_hold: {
      allowNull: false, 
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },

    listing_status_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'ticketing_systems',
          schema: 'listing_status'
        },        
        key: 'id'
      },
      defaultValue: 1
    },
    
    createdAt: {
      field: 'created_at',
      allowNull: false,
      type: Sequelize.DATE,
      comment: "When the listing field is created the timestamp is stored in this field"
    },
    created_by: {
      allowNull: false, 
      type: Sequelize.INTEGER, 
      defaultValue: 0
    }, 
    updatedAt: {
      field: 'updated_at',
      allowNull: false,
      type: Sequelize.DATE,
      comment: "When the listing field is updated the timestamp is stored in this field"
    }, 
    updated_by: {
      allowNull: true, 
      type: Sequelize.INTEGER, 
      defaultValue: 0
    }
  },
      {
          schema: 'ticket_exchange',
          tableName: 'listing_group'
      },
  );

  return ListingGroup;

}
