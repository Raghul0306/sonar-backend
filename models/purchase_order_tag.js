module.exports = function (sequelize, Sequelize, DataTypes) {

  const PurchaseOrderTag = sequelize.define("purchase_order_tag", {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    purchase_order_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'purchase_order',
          schema: 'ticket_exchange'
        },
        key: 'id'
      }
    },
    tag_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'tags',
          schema: 'ticket_exchange'
        },
        key: 'id'
      }
    },
    createdAt: {
      field: 'created_at', 
      allowNull: false,
      type: Sequelize.DATE
    },
    created_by: {
      allowNull: false, 
      type: Sequelize.INTEGER,
      defaultValue: 0
    }, 
    updatedAt: {
      field: 'updated_at', 
      allowNull: false,
      type: Sequelize.DATE
    }, 
    updated_by: {
      allowNull: false, 
      type: Sequelize.INTEGER,
      defaultValue: 0
    }
  },
  {
      schema: 'ticket_exchange',
      tableName: 'purchase_order_tag'
  },
);

return PurchaseOrderTag;

}