module.exports = function (sequelize, Sequelize, DataTypes) {

  const Channels = sequelize.define("channels", {

      id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        channel_name: {
          type: Sequelize.TEXT,
          allowNull: false,
          comment: "channel name is stored in this field"
        },
        logo_url: {
          type: Sequelize.TEXT, 
          allowNull: true, 
          comment: "logo path will be stored in this field"
        }, 
        active: {
          type: Sequelize.BOOLEAN,
          defaultValue: true,
          comment: "channels will show based on active status"
        },
        createdAt: {
            field: 'created_at', 
          allowNull: false,
          type: Sequelize.DATE,
          comment: "When the user channel markup field is created the timestamp is stored in this field"
        },
        updatedAt: {
            field: 'updated_at', 
          allowNull: true,
          type: Sequelize.DATE,
          comment: "When the user channel markup field is updated the timestamp is stored in this field"
        }
  },
      {
          schema: 'ticket_exchange',
          tableName: 'channels'
      },
      );

  return Channels;

}
