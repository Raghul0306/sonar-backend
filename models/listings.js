module.exports = function (sequelize, Sequelize, DataTypes) {

  const Listing = sequelize.define("listing", {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    listingGroupId: {
      field: 'listing_group_id',
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'listing_group',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    },

    buyer_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'user',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    },

    in_hand: {
      type: Sequelize.DATE,
    },
    
    seat_number: {
      type: Sequelize.STRING,
    },

    price: {
      type: Sequelize.FLOAT,
      comment: "price are stored in this field"
    },

    unit_cost: {
      type: Sequelize.FLOAT,
      comment: "cost are stored in this field"
    },

    sold: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },

    face_value: {
      type: Sequelize.FLOAT
    },
    
    delivered: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },

    inactive: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },

    hold: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },

    pdf: {
      type: Sequelize.STRING,
    },

    last_price: {
      type: Sequelize.FLOAT,
    },

    last_price_change: {
      type: Sequelize.DATE,
    },

    data_type: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'ticket_types',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    },

    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },

    listing_status_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'ticketing_systems',
          schema: 'listing_status'
        },        
        key: 'id'
      },
      defaultValue: 1
    },
    
    createdAt: {
      field:'created_at',
      type: Sequelize.DATE
  },
  updatedAt: {
      field:'updated_at',
      type: Sequelize.DATE
  }, 

  },
      {
          schema: 'ticket_exchange',
          tableName: 'listing'
      },

  );

  return Listing;

}
