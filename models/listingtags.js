module.exports = function (sequelize, Sequelize, DataTypes) {

    const ListingTags = sequelize.define("listing_tags", {

        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        listing_group_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'listing_group',
                schema: 'ticket_exchange'
              },
              key: 'id'
            },
          },
        tag_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'tags',
                schema: 'ticket_exchange'
              },
              key: 'id'
            },
          },
        createdAt: {
            field:'created_at',
            type: Sequelize.DATE
        },
        updatedAt: {
            field:'updated_at',
            type: Sequelize.DATE
        }, 
    },
        {
            schema: 'ticket_exchange',
            tableName: 'listing_tags'
        },
        );

    return ListingTags;

}
