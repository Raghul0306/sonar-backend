module.exports = function (sequelize, Sequelize, DataTypes) {

    const Events = sequelize.define("event", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          name: {
            type: Sequelize.STRING,
            allowNull: false,
          },
    
          categoryId: {
            field: 'category_id',
            type: Sequelize.INTEGER,
            allowNull: true,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'category',
                schema: 'ticket_exchange'
              },
              key: 'id'
            },
          },
    
          performerId: {
            field: 'performer_id',
            type: Sequelize.INTEGER,
            allowNull: true,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'performer',
                schema: 'ticket_exchange'
              },
              key: 'id'
            },
          },
    
          opponent: {
            field: 'opponent_id',
            type: Sequelize.INTEGER,
            allowNull: true,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'performer',
                schema: 'ticket_exchange'
              },
              key: 'id'
            },
          }, 
    
          venueId: {
            field: "venue_id",
            type: Sequelize.INTEGER,
            allowNull: true,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'venue',
                schema: 'ticket_exchange'
              },
              key: 'id'
            },
          },

          eventStatusId: {
            field: "event_status_id",
            type: Sequelize.INTEGER,
            allowNull: true,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'event_status',
                schema: 'ticket_exchange'
              },
              key: 'id'
            },
          },
    
          ticketSystemId: {
            field: "ticket_system_id",
            type: Sequelize.INTEGER,
            allowNull: true,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'ticketing_systems',
                schema: 'ticket_exchange'
              },
              key: 'id'
            },
          },  

          tickettype_id: {
            allowNull: false, 
            type: Sequelize.INTEGER
          },
    
          date: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
          },
    
          tevoid: {
            field: "tevo_id",
            type: Sequelize.INTEGER,
          },
    
          mercuryid: {
            field: "mercury_id",
            type: Sequelize.INTEGER,
          },
    
          popularity_score: {
            type: Sequelize.FLOAT,
          },
    
          eventdate_day: {
            type: Sequelize.STRING,
          },          
    
          event_link:{
            type: Sequelize.TEXT,
            allowNull: true
          },
          
          image: {
            type: Sequelize.TEXT, 
            allowNull: true
          },

          created_by: {
            type: Sequelize.INTEGER,
            allowNull: true,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'user',
                schema: 'ticket_exchange'
              },
              key: 'id'
            },
          },
    
          genre_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'genre',
                schema: 'ticket_exchange'
              },
              key: 'id'
            },
          },
    
          disclosures: {
            type: Sequelize.STRING,
          },
    
          notes: {
            type: Sequelize.TEXT,
          },
          createdAt: {
            field: 'created_at',
            type: Sequelize.DATE
        },
        updatedAt: {
          field: 'updated_at',
          type: Sequelize.DATE
      },
    },
        {
            schema: 'ticket_exchange',
            tableName: 'event'
        },
    );

    return Events;

}
