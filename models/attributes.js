module.exports = function (sequelize, Sequelize, DataTypes) {

    const Attributes = sequelize.define("attributes", {

        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        attribute_name: {
            type: Sequelize.STRING,
          },
        user_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
            model: {
            tableName: 'user',
            schema: 'ticket_exchange'
            },
            key: 'id'
        }
        },
        active: {
            type: Sequelize.INTEGER,
            defaultValue: 1,
          },
        createdAt: {
            field:'created_at',
            type: Sequelize.DATE
        },
        updatedAt: {
            field:'updated_at',
            type: Sequelize.DATE
        }, 
    },
        {
            schema: 'ticket_exchange',
            tableName: 'attributes'
        },
        );

    return Attributes;

}
