module.exports = function (sequelize, Sequelize, DataTypes) {

  const Seattypes = sequelize.define("seat_type", {
      id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
      },
      name: {
          allowNull: false,
          type: Sequelize.STRING
      },
      createdAt: {
          field: 'created_at',
          type: Sequelize.DATE
      },
      updatedAt: {
          field: 'updated_at',
          type: Sequelize.DATE
      }, 
  },
      {
          schema: 'ticket_exchange',
          tableName: 'seat_type'
      },
      );

  return Seattypes;

}
