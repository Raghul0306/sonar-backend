module.exports = function (sequelize, Sequelize, DataTypes) {

  const ListingAttributes = sequelize.define("listing_attributes", {

      id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
      },
      listingGroupId: {
          field: 'listing_group_id',
          type: Sequelize.INTEGER,
          allowNull: false,
          onDelete: 'cascade',
          references: {
            model: {
              tableName: 'listing_group',
              schema: 'ticket_exchange'
            },
            key: 'id'
          },
        },
        attribute_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          onDelete: 'cascade',
          references: {
            model: {
              tableName: 'attributes',
              schema: 'ticket_exchange'
            },
            key: 'id'
          },
        },
      createdAt: {
          field:'created_at',
          type: Sequelize.DATE
      },
      updatedAt: {
          field:'updated_at',
          type: Sequelize.DATE
      }, 
  },
      {
          schema: 'ticket_exchange',
          tableName: 'listing_attributes'
      },
      );

  return ListingAttributes;

}
