module.exports = function(sequelize, Sequelize, DataTypes) {

    const Userroles = sequelize.define("user_roles", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
            },
            user_role_name: {
            type: Sequelize.STRING
            },
            createdAt: {
            field: 'created_at',
            allowNull: false,
            type: Sequelize.DATE,
            },
            updatedAt: {
            field: 'updated_at',
            allowNull: false,
            type: Sequelize.DATE,
            }
    }, 
    {
            schema: 'ticket_exchange',
            tableName: 'user_roles'
    });
  
    return Userroles;
  
  }
  