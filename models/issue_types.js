module.exports = function (sequelize, Sequelize, DataTypes) {

    const ListingAttributes = sequelize.define("issue_types", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            allowNull: false,
            type: Sequelize.STRING
        },
        created_at: {
            allowNull: false,
            type: Sequelize.DATE
        },
        created_by: {
            allowNull: false,
            type: Sequelize.INTEGER,
            onDelete: 'cascade',
            references: {
                model: {
                    tableName: 'user',
                    schema: 'ticket_exchange'
                },
                key: 'id'
            }

        },
        updated_at: {
            allowNull: true,
            type: Sequelize.DATE,

        },
        updated_by: {
            allowNull: true,
            type: Sequelize.INTEGER,
            onDelete: 'cascade',
            references: {
                model: {
                    tableName: 'user',
                    schema: 'ticket_exchange'
                },
                key: 'id'
            }

        }
    },
        {
            schema: 'ticket_exchange',
            tableName: 'issue_types'
        },
    );

    return ListingAttributes;

}
