module.exports = function (sequelize, Sequelize, DataTypes) {
  const Purchaseorderinvoiceattachment = sequelize.define("purchase_order_invoice_attachment", {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    }, 
    purchase_order_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'purchase_order',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    },
    attachment_name: {
      type: Sequelize.STRING,
      allowNull: true
    },
    attachment_url: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE, 
      defaultValue: Sequelize.NOW
    },
    created_by: {
      allowNull: false, 
      type: Sequelize.INTEGER, 
      defaultValue: 0
    }, 
    updated_at: {
      allowNull: true,
      type: Sequelize.DATE, 
      defaultValue: Sequelize.NOW
    }, 
    updated_by: {
      allowNull: true, 
      type: Sequelize.INTEGER, 
      defaultValue: 0
    }

  },
      {
          schema: 'ticket_exchange',
          tableName: 'purchase_order_invoice_attachment',
          timestamps: false
      },
      );
  return Purchaseorderinvoiceattachment;
}