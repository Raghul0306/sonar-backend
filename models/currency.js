module.exports = function (sequelize, Sequelize, DataTypes) {

  const Currency = sequelize.define("currency", {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    name: {
      allowNull: true, 
      type: Sequelize.STRING
    },
    iso_code: {
      allowNull: true, 
      type: Sequelize.STRING
    }, 
    symbol: {
      allowNull: true, 
      type: Sequelize.STRING
    },
    createdAt: {
      field: 'created_at', 
      allowNull: false,
      type: Sequelize.DATE
    },
    created_by: {
      allowNull: false, 
      type: Sequelize.INTEGER, 
      defaultValue: 0
    }, 
    updatedAt: {
      field: 'updated_at', 
      allowNull: false,
      type: Sequelize.DATE
    }, 
    updated_by: {
      allowNull: false, 
      type: Sequelize.INTEGER, 
      defaultValue: 0
    }
  },
  {
      schema: 'ticket_exchange',
      tableName: 'currency'
  },
);

return Currency;

}
