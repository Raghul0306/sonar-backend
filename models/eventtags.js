module.exports = function (sequelize, Sequelize, DataTypes) {

    const EventTags = sequelize.define("event_tags", {

        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        eventId: {
            type: Sequelize.INTEGER,
            field: 'event_id',
            allowNull: false,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'event',
                schema: 'ticket_exchange'
              },
              key: 'id'
            },
          },
          tagId: {
            type: Sequelize.INTEGER,
            field: 'tag_id',
            allowNull: false,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'tags',
                schema: 'ticket_exchange'
              },
              key: 'id'
            },
          },
        createdAt: {
            field:'created_at',
            type: Sequelize.DATE
        },
        updatedAt: {
            field:'updated_at',
            type: Sequelize.DATE
        }, 
    },
        {
            schema: 'ticket_exchange',
            tableName: 'event_tags'
        },
        );

    return EventTags;

}
