module.exports = function(sequelize, Sequelize, DataTypes) {

  const Companyuser = sequelize.define("company_user", {
    
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    company_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'companies',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "company id are stored in this field"
    },
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'user',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "user id are stored in this field"
    },
    createdAt: {
      field: 'created_at',
      allowNull: false,
      type: Sequelize.DATE,
    },
    created_by: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    updatedAt: {
      field: 'updated_at',
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_by: {
      type: Sequelize.INTEGER,
      allowNull: true
    }

  }, 
  {
    schema: 'ticket_exchange',
    tableName: 'company_user'
  });

  return Companyuser;

}
