module.exports = function (sequelize, Sequelize, DataTypes) {

    const Favorites = sequelize.define("favorites", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
        },
        favorite_type: {
            allowNull: false,
            type: Sequelize.BOOLEAN
        },
        buyer_user_id: {
            allowNull: false,
            type: Sequelize.INTEGER,
            references: {
                model: {
                tableName: 'buyerUsers',
                schema: 'ticket_exchange'
                },
                key: 'id'
            }
        },
        event_id: {
            allowNull: false,
            type: Sequelize.INTEGER,
            references: {
                model: {
                tableName: 'event',
                schema: 'ticket_exchange'
                },
                key: 'id'
            }
        },
        listing_group_id: {
            allowNull: true,
            type: Sequelize.INTEGER,
            references: {
                model: {
                tableName: 'listing_group',
                schema: 'ticket_exchange'
                },
                key: 'id'
            }
        },
        createdAt: {
            field:'created_at',
            type: Sequelize.DATE
        },
        created_by: {
            allowNull: true,
            type: Sequelize.INTEGER
        },
        updatedAt: {
            field:'updated_at',
            type: Sequelize.DATE
        },
        updated_by: {
            allowNull: true,
            type: Sequelize.INTEGER
        }
    },
    {
        schema: 'ticket_exchange',
        tableName: 'favorites'
    });

    return Favorites;

}
