module.exports = function (sequelize, Sequelize, DataTypes) {

    const Ticketingsystemaccounts = sequelize.define("ticketing_system_accounts", {

      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ticketing_system_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'ticketing_systems',
            schema: 'ticket_exchange'
          },
          key: 'id'
        }
      },

      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        }
      },

      parent_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'user',
            schema: 'ticket_exchange'
          },
          key: 'id'
        }
      },

      credit_card_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'credit_card',
            schema: 'ticket_exchange'
          },
          key: 'id'
        }
      },

      account_name: {
        type: Sequelize.STRING,
        allowNull: false
      },

      active: {
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      createdAt: {
        field:'created_at',
        allowNull: false,
        type: Sequelize.DATE
    },
    updatedAt: {
        field:'updated_at',
        allowNull: false,
        type: Sequelize.DATE
    }, 

    },
        {
            schema: 'ticket_exchange',
            tableName: 'ticketing_system_accounts'
        },
        );

    return Ticketingsystemaccounts;

}
