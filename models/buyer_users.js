module.exports = function(sequelize, Sequelize, DataTypes) {

  return sequelize.define("buyerUsers", {
    
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    first_name: {
      type: Sequelize.STRING
    },
    last_name: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
    },
    phone_number: {
      type: Sequelize.STRING
    },
    user_role_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'user_roles',
          schema: 'ticket_exchange'
        },
        key: 'id'
      }
    },
    channel_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'channels',
          schema: 'ticket_exchange'
        },
        key: 'id'
      }
    },
    stripeID: {
      type: Sequelize.STRING
    },
    promo_code_used: {
      type: Sequelize.DataTypes.BOOLEAN,
    },
    address1: {
      type: Sequelize.TEXT,
    },
    address2: {
      type: Sequelize.TEXT,
    },
    city: {
      type: Sequelize.STRING
    },
    state: {
      type: Sequelize.STRING
    },
    zip_code: {
      type: Sequelize.STRING
    },
    country: {
      type: Sequelize.STRING
    },
    notes: {
      type: Sequelize.TEXT,
    },
    password_hash: {
      type: Sequelize.TEXT,
    },
    token: {
      type: Sequelize.STRING,
    },
    is_google_login: {
      allowNull: true, 
      type: Sequelize.DataTypes.BOOLEAN,
      defaultValue: false
    },
    is_facebook_login: {
      allowNull: true, 
      type: Sequelize.DataTypes.BOOLEAN,
      defaultValue: false
    },
    is_email_login: {
      allowNull: true, 
      type: Sequelize.DataTypes.BOOLEAN,
      defaultValue: false
    },
    facebook_id: {
      allowNull: true, 
      type: Sequelize.STRING
    },
    is_user_registered: {
      allowNull: true, 
      type: Sequelize.DataTypes.BOOLEAN,
      defaultValue: true
    },
    google_id: {
      allowNull: true, 
      type: Sequelize.STRING,
      comment: "User GoogleId"
    },
    createdAt: {
      field: 'created_at',
      allowNull: false,
      type: Sequelize.DATE,
    },
    updatedAt: {
      field: 'updated_at',
      allowNull: false,
      type: Sequelize.DATE,
    }

  }, 
  {
      schema: 'ticket_exchange',
      tableName: 'buyerUsers'
  });

}
