module.exports = function (sequelize, Sequelize, DataTypes) {

  const PaymentType = sequelize.define("payment_type", {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    name: {
      allowNull: false, 
      type: Sequelize.STRING
    },
    createdAt: {
      field: 'created_at', 
      allowNull: false,
      type: Sequelize.DATE
    },
    created_by: {
      allowNull: false, 
      type: Sequelize.INTEGER,
      defaultValue: 0
    }, 
    updatedAt: {
      field: 'updated_at', 
      allowNull: false,
      type: Sequelize.DATE
    }, 
    updated_by: {
      allowNull: false, 
      type: Sequelize.INTEGER,
      defaultValue: 0
    }
  },
  {
      schema: 'ticket_exchange',
      tableName: 'payment_type'
  },
);

return PaymentType;

}