module.exports = function (sequelize, Sequelize, DataTypes) {

  const Vendor = sequelize.define("vendor", {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    name: {
      allowNull: false, 
      type: Sequelize.STRING
    },
    address: {
      allowNull: true, 
      type: Sequelize.TEXT
    }, 
    address2: {
      allowNull: true, 
      type: Sequelize.TEXT
    }, 
    zip: {
      allowNull: true, 
      type: Sequelize.STRING
    }, 
    city: {
      allowNull: true, 
      type: Sequelize.STRING
    }, 
    state: {
      allowNull: true, 
      type: Sequelize.STRING
    }, 
    country: {
      allowNull: true, 
      type: Sequelize.STRING
    }, 
    email: {
      allowNull: true, 
      type: Sequelize.STRING
    }, 
    phone: {
      allowNull: true, 
      type: Sequelize.STRING
    }, 
    notes: {
      allowNull: true, 
      type: Sequelize.TEXT
    }, 
    createdAt: {
      field: 'created_at', 
      allowNull: false,
      type: Sequelize.DATE
    },
    created_by: {
      allowNull: false, 
      type: Sequelize.INTEGER,
      defaultValue: 0
    }, 
    updatedAt: {
      field: 'updated_at', 
      allowNull: false,
      type: Sequelize.DATE
    }, 
    updated_by: {
      allowNull: false, 
      type: Sequelize.INTEGER,
      defaultValue: 0
    }
  },
  {
      schema: 'ticket_exchange',
      tableName: 'vendor'
  },
);

return Vendor;

}