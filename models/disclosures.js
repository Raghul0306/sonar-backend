module.exports = function (sequelize, Sequelize, DataTypes) {

    const Disclosures = sequelize.define("disclosures", {

        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        disclosure_name: {
            type: Sequelize.STRING,
          },
        user_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
            model: {
            tableName: 'user',
            schema: 'ticket_exchange'
            },
            key: 'id'
        }
        },
        active: {
            type: Sequelize.INTEGER,
            defaultValue: 1,
          },
        createdAt: {
            field:'created_at',
            type: Sequelize.DATE
        },
        updatedAt: {
            field:'updated_at',
            type: Sequelize.DATE
        }, 
    },
        {
            schema: 'ticket_exchange',
            tableName: 'disclosures'
        },
        );

    return Disclosures;

}
