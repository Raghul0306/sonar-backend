module.exports = function (sequelize, Sequelize, DataTypes) {

    return sequelize.define("google_oauth_tokens", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        buyer_user_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            onDelete: 'cascade',
            references: {
                model: {
                tableName: 'buyerUsers',
                schema: 'ticket_exchange'
                },
                key: 'id'
            }
        },
        google_id: {
            type: Sequelize.STRING,
            allowNull: false
        },
        token_type: {
            type: Sequelize.STRING,
            allowNull: false
        },
        access_token: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        refresh_token: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        id_token: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        revoked: {
            type: Sequelize.BOOLEAN,
            allowNull: true
        },
        scope: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        expires_in:{
            type: Sequelize.INTEGER,
            allowNull: false
        },
        createdAt: {
            field:'created_at',
            allowNull: false,
            type: Sequelize.DATE
        },
        updatedAt: {
            field:'updated_at',
            allowNull: true,
            type: Sequelize.DATE
        },
    },
    {
            schema: 'ticket_exchange',
            tableName: 'google_oauth_tokens'
    });

}
