module.exports = function(sequelize, Sequelize, DataTypes) {

  const Useraccount = sequelize.define("password_reset_links", {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'user',
          schema: 'ticket_exchange'
        },
        key: 'id'
      }
    },
    token: {
      type: Sequelize.STRING,
      allowNull: false
    },
    createdAt: {
      field: 'created_at',
      type: Sequelize.DATE,
    },
    updatedAt: {
      field: 'updated_at',
      type: Sequelize.DATE,
    }
  }, 
  {
      schema: 'ticket_exchange',
      tableName: 'password_reset_links'
  });

  return Useraccount;

}