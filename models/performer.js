module.exports = function (sequelize, Sequelize, DataTypes) {

    const Performer = sequelize.define("performer", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING
        },
        category_id: {
            type: Sequelize.INTEGER
        },
        tevoid: {
            type: Sequelize.INTEGER
        },
        mercuryid: {
            type: Sequelize.INTEGER
        },
        popularity_score: {
            type: Sequelize.FLOAT
        },
        genre_id: {
            type: Sequelize.INTEGER
        },
        createdAt: {
            field:'created_at',
            type: Sequelize.DATE
        },
        updatedAt: {
            field:'updated_at',
            type: Sequelize.DATE
        }, 
    },
        {
            schema: 'ticket_exchange',
            tableName: 'performer'
        },
        );

    return Performer;

}
