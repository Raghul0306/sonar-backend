module.exports = function (sequelize, Sequelize, DataTypes) {
  const transaction_listing = sequelize.define("transaction_listing",{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    transaction_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'transaction',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "transaction id stored in this field"
    },
    listing_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'listing',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "listing id stored in this field"
    }
  }, {
    schema: 'ticket_exchange',
    tableName: 'transaction_listing',
    timestamps: false
  });
  return transaction_listing;
};
