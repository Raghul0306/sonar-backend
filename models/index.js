const dbConfig = require("../db/db.js");
const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.user = require("./user.js")(sequelize, Sequelize);
db.useraccount = require("./useraccount.js")(sequelize, Sequelize);
db.userroles = require("./userroles.js")(sequelize, Sequelize);
db.countries = require("./countries.js")(sequelize, Sequelize);
db.states = require("./states.js")(sequelize, Sequelize);
db.cities = require("./cities.js")(sequelize, Sequelize);
db.listings = require("./listings.js")(sequelize, Sequelize);
db.events = require("./events.js")(sequelize, Sequelize);
db.performer = require("./performer.js")(sequelize, Sequelize);
db.venue = require("./venue.js")(sequelize, Sequelize);
db.category = require("./category.js")(sequelize, Sequelize);
db.tickettype = require("./tickettype.js")(sequelize, Sequelize);
db.genre = require("./genre.js")(sequelize, Sequelize);
db.tags = require("./tags.js")(sequelize, Sequelize);
db.ticketingsystems = require("./ticketingsystems.js")(sequelize, Sequelize);
db.disclosures = require("./disclosures.js")(sequelize, Sequelize);
db.attributes = require("./attributes.js")(sequelize, Sequelize);
db.eventtags = require("./eventtags")(sequelize, Sequelize);
db.listingtags = require("./listingtags")(sequelize, Sequelize);
db.listingdisclosures = require("./listing_disclosures")(sequelize, Sequelize);
db.listingattributes = require("./listing_attributes")(sequelize, Sequelize);
db.channels = require("./channels")(sequelize, Sequelize);
db.channelfees = require("./channel_fees")(sequelize, Sequelize);
db.listingattachments = require("./listingattachments")(sequelize, Sequelize);
db.listingchannelmarkups = require("./listing_channel_markups")(sequelize, Sequelize);
db.seattypes = require("./seattypes.js")(sequelize, Sequelize);
db.listinggroup = require("./listing_group")(sequelize, Sequelize);
db.splits = require("./splits")(sequelize, Sequelize);
db.buyerUsers = require("./buyer_users")(sequelize, Sequelize);
db.transaction = require("./transaction")(sequelize, Sequelize);
db.ticketingsystemaccounts = require("./ticketingsystemaccounts")(sequelize, Sequelize);
db.pricelog = require("./price_log")(sequelize, Sequelize);
db.resetpassword = require("./password_reset_links")(sequelize, Sequelize)
db.eventstatus = require("./event_status")(sequelize, Sequelize)
db.brokerorder = require("./broker_order")(sequelize, Sequelize)
db.channelreference = require("./channel_reference")(sequelize, Sequelize)
db.deliverystatus = require("./delivery_status")(sequelize, Sequelize);
db.marketplaceorder = require("./marketplace_order")(sequelize, Sequelize);
db.orderstatus = require("./order_status")(sequelize, Sequelize);
db.creditcard = require("./credit_card.js")(sequelize, Sequelize);
db.creditcardtype = require("./credit_card_type.js")(sequelize, Sequelize);

db.ticketingsystemuserlogin = require("./ticketing_system_user_login")(sequelize, Sequelize);
db.purchaseorder = require('./purchase_order')(sequelize, Sequelize)
db.paymenttype = require('./payment_type')(sequelize, Sequelize)
db.paymentmethod = require('./payment_method')(sequelize, Sequelize)
db.purchaseorderlisting = require('./purchase_order_listing')(sequelize, Sequelize)
db.vendor = require('./vendor')(sequelize, Sequelize)
db.brokerorderlisting = require('./broker_order_listing')(sequelize, Sequelize);
db.purchaseordertag = require('./purchase_order_tag')(sequelize, Sequelize);
db.marketplaceorderlisting = require('./marketplace_order_listing')(sequelize, Sequelize)
db.purchaseorderinvoiceattachment = require('./purchase_order_invoice_attachment')(sequelize, Sequelize)

db.transactionlisting = require("./transaction_listing")(sequelize, Sequelize);
db.favorities = require("./favorites")(sequelize, Sequelize);
db.companies = require("./companies")(sequelize, Sequelize);
db.companyuser = require("./company_user.js")(sequelize, Sequelize);
db.listingStatus = require("./listing_status")(sequelize, Sequelize);
db.GoogleOAuthToken = require("./google_oauth_tokens")(sequelize, Sequelize);
db.FacebookAuthToken = require("./facebook_auth_tokens")(sequelize, Sequelize);
db.emailparser = require("./email_parser")(sequelize, Sequelize);

db.issueTypes = require("./issue_types")(sequelize, Sequelize);
db.issues = require("./issues")(sequelize, Sequelize);

db.currency = require("./currency")(sequelize, Sequelize);

//Relations 
db.performer.belongsTo(db.category, { foreignKey: 'category_id' });
db.performer.belongsTo(db.genre, { foreignKey: 'genre_id' });

module.exports = db;
