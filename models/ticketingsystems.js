module.exports = function (sequelize, Sequelize, DataTypes) {

    const Ticketingsystems = sequelize.define("ticketing_systems", {

        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        ticketing_system_name: {
            type: Sequelize.STRING,
          },
          country_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'countries',
                schema: 'ticket_exchange'
              },
              key: 'id'
            },
          },
        active: {
            type: Sequelize.INTEGER,
            defaultValue: 1,
          },
        createdAt: {
            field:'created_at',
            type: Sequelize.DATE
        },
        updatedAt: {
            field:'updated_at',
            type: Sequelize.DATE
        }, 
    },
        {
            schema: 'ticket_exchange',
            tableName: 'ticketing_systems'
        },
        );

    return Ticketingsystems;

}
