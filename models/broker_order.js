module.exports = function (sequelize, Sequelize, DataTypes) {

  const BrokerOrder = sequelize.define("broker_order", {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'user',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "user id stored in this field"
    }, 
    event_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'event',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "event id stored in this field"
    },
    listing_group_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'listing_group',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "listing group id stored in this field"
    },
    channel_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'channels',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "channel id stored in this field"
    },
    channel_reference_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'channel_reference',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "channel reference id stored in this field"
    },
    transaction_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'transaction',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "transaction id stored in this field"
    },
    buyer_user_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'buyerUsers',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "buyer user id stored in this field"
    },
    order_status_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'order_status',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "order status id stored in this field"
    },
    delivery_status_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'delivery_status',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "delivery status id stored in this field"
    },
    delivery_at: {
      type: Sequelize.DATE, 
      allowNull: true, 
      comment: 'Delivery date is stored in this field'
    }, 
    cancelled_order_type_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'cancelled_order_type',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "cancelled order type id stored in this field"
    },
    createdAt: {
      field: 'created_at', 
      allowNull: false,
      type: Sequelize.DATE
    },
    created_by: {
      allowNull: false, 
      type: Sequelize.INTEGER, 
      defaultValue: 0
    }, 
    updatedAt: {
      field: 'updated_at', 
      allowNull: true,
      type: Sequelize.DATE
    }, 
    updated_by: {
      allowNull: true, 
      type: Sequelize.INTEGER, 
      defaultValue: 0
    }
  },
      {
          schema: 'ticket_exchange',
          tableName: 'broker_order'
      },
  );

  return BrokerOrder;

}
