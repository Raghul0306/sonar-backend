module.exports = function(sequelize, Sequelize, DataTypes) {

    const Countries = sequelize.define("countries", {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
            },
    
            abbreviation: {
            type: Sequelize.STRING,
            allowNull: false
          },
    
          name: {
            type: Sequelize.STRING,
            allowNull: false
          },
    
          phonecode: {
            type: Sequelize.STRING,
            allowNull: false
          },
    
          sorting: {
            type: Sequelize.INTEGER,
            defaultValue: 0
          },
    
          active: {
            type: Sequelize.INTEGER,
            defaultValue: 1
          },

          createdAt: {
            field: 'created_at',
            allowNull: false,
            type: Sequelize.DATE,
          },
          updatedAt: {
            field: 'updated_at',
            allowNull: false,
            type: Sequelize.DATE,
          }
    }, 
    {
            schema: 'ticket_exchange',
            tableName: 'countries'
    });
  
    return Countries;
  
  }
  