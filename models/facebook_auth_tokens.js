module.exports = function (sequelize, Sequelize, DataTypes) {

    return sequelize.define("facebook_auth_tokens", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        buyer_user_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            onDelete: 'cascade',
            references: {
                model: {
                tableName: 'buyerUsers',
                schema: 'ticket_exchange'
                },
                key: 'id'
            }
        },
        facebook_id: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        access_token: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        revoked: {
            type: Sequelize.BOOLEAN,
            allowNull: true
        },
        scope: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        expires_in:{
            type: Sequelize.INTEGER,
            allowNull: false
        },
        createdAt: {
            field:'created_at',
            allowNull: false,
            type: Sequelize.DATE
        },
        updatedAt: {
            field:'updated_at',
            allowNull: true,
            type: Sequelize.DATE
        },
    },
    {
            schema: 'ticket_exchange',
            tableName: 'facebook_auth_tokens'
    });

}
