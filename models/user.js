module.exports = function(sequelize, Sequelize, DataTypes) {

  const User = sequelize.define("user", {
    
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    first_name: {
      type: Sequelize.STRING
    },
    last_name: {
      type: Sequelize.STRING
    },
    phone: {
      type: Sequelize.STRING
    },
    phonecode: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'countries',
          schema: 'ticket_exchange'
        },
        key: 'id'
      }
    },
    email: {
      type: Sequelize.STRING
    },

    user_role: {
      type: Sequelize.INTEGER,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'user_roles',
          schema: 'ticket_exchange'
        },
        key: 'id'
      }
    },
    verification_token:{
      type: Sequelize.STRING
    },
    mercuryid: {
      type: Sequelize.STRING,
      unique: true
    },
    stripe_id: {
      type: Sequelize.STRING,
      unique: true
    },
    last_login: {
      type: Sequelize.DATE
    },

    
    company_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'companies',
          schema: 'ticket_exchange'
        },
        key: 'id'
      }
    },
    parent_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      comment: "User parent id are stored in this field"
    },
    street_address_1: {
      type: Sequelize.TEXT
    },
    street_address_2: {
      type: Sequelize.TEXT
    },
    city: {
      type: Sequelize.STRING
    },
    state: {
      type: Sequelize.STRING
    },
    zip_code: {
      type: Sequelize.STRING
    },
    country: {
      type: Sequelize.STRING
    },

    needsToChangePassword: {
      type: Sequelize.DataTypes.BOOLEAN,
      defaultValue: true
    },
    needsToAcceptTerms: {
      type: Sequelize.DataTypes.BOOLEAN,
      defaultValue: true
    },
    needsToAcceptPrivacy: {
      type: Sequelize.DataTypes.BOOLEAN,
      defaultValue: true
    },
    needsToAcceptSeller: {
      type: Sequelize.DataTypes.BOOLEAN,
      defaultValue: true
    },
    needsToSyncBank: {
      type: Sequelize.DataTypes.BOOLEAN,
      defaultValue: true
    },

    notes: {
      type: Sequelize.TEXT
    },

    active: {
      type: Sequelize.INTEGER,
      defaultValue: 1
    },


    createdAt: {
      field: 'created_at',
      allowNull: false,
      type: Sequelize.DATE,
    },
    created_by: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    updatedAt: {
      field: 'updated_at',
      allowNull: false,
      type: Sequelize.DATE,
    },
    updated_by: {
      type: Sequelize.INTEGER,
      allowNull: true
    }

  }, 
  {
    schema: 'ticket_exchange',
    tableName: 'user'
  });

  return User;

}
