module.exports = function (sequelize, Sequelize, DataTypes) {

  const PriceLog = sequelize.define("price_log", {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    event_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'event',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    }, 
    listing_group_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'listing_group',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    }, 
    listing_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'listing',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    }, 
    price: {
      type: Sequelize.FLOAT,
      comment: 'Price is stored in this field'
    }, 
    new_price: {
      type: Sequelize.FLOAT,
      comment: 'Updated Price is stored in this field'
    }, 
    confirmed_price_change: {
      allowNull: false, 
      type: Sequelize.BOOLEAN
    }, 
    createdAt: {
      field: 'created_at', 
      allowNull: false,
      type: Sequelize.DATE
    },
    created_by: {
      allowNull: false, 
      type: Sequelize.INTEGER
    },
    updatedAt: {
      field: 'updated_at', 
      allowNull: false,
      type: Sequelize.DATE
    }
  },
      {
          schema: 'ticket_exchange',
          tableName: 'price_log'
      },
      );

  return PriceLog;

}
