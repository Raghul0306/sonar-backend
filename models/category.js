module.exports = function (sequelize, Sequelize, DataTypes) {

    const Category = sequelize.define("category", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            allowNull: false,
            type: Sequelize.STRING
        },
        tevoid: {
            type: Sequelize.STRING
        },
        mercuryid: {
            type: Sequelize.STRING
        },
        parent_category_id: {
            type: Sequelize.INTEGER
        },
        child_category_id: {
            type: Sequelize.INTEGER
        },
        grand_child_category_id: {
            type: Sequelize.INTEGER
        },
        image: {
        type: Sequelize.TEXT, 
        allowNull: true, 
        comment: "image path will be stored in this field"
        },
        createdAt: {
            field: 'created_at',
            type: Sequelize.DATE
        },
        updatedAt: {
            field: 'updated_at',
            type: Sequelize.DATE
        }, 
    },
        {
            schema: 'ticket_exchange',
            tableName: 'category'
        },
        );

    return Category;

}
