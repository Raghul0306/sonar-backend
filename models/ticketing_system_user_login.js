module.exports = function (sequelize, Sequelize, DataTypes) {
  const Ticketingsystemuserlogin = sequelize.define(
    "ticketing_system_user_login",
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      buyer_user_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: "cascade",
        references: {
          model: {
            tableName: "buyerUsers",
            schema: "ticket_exchange",
          },
          key: "id",
        },
      },
      ticketing_system_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: "cascade",
        references: {
          model: {
            tableName: "ticketing_systems",
            schema: "ticket_exchange",
          },
          key: "id",
        },
      },
      user_name: {
        type: Sequelize.STRING,
      },
      createdAt: {
        field: "created_at",
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        field: "updated_at",
        allowNull: false,
        type: Sequelize.DATE,
      },
    },
    {
      schema: "ticket_exchange",
      tableName: "ticketing_system_user_login",
    }
  );

  return Ticketingsystemuserlogin;
};
