module.exports = function (sequelize, Sequelize, DataTypes) {

  const DeliveryStatus = sequelize.define("delivery_status", {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    name: {
      allowNull: false, 
      type: Sequelize.STRING
    },
    delivery_at: {
      allowNull: true, 
      type: Sequelize.DATE
    }, 
    createdAt: {
      field: 'created_at', 
      allowNull: false,
      type: Sequelize.DATE
    },
    created_by: {
      allowNull: false, 
      type: Sequelize.INTEGER, 
      defaultValue: 0
    }, 
    updatedAt: {
      field: 'updated_at', 
      allowNull: false,
      type: Sequelize.DATE
    }, 
    updated_by: {
      allowNull: false, 
      type: Sequelize.INTEGER, 
      defaultValue: 0
    }
  },
  {
      schema: 'ticket_exchange',
      tableName: 'delivery_status'
  },
);

return DeliveryStatus;

}
