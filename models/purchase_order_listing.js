module.exports = function (sequelize, Sequelize, DataTypes) {
  const Purchaseorderlisting = sequelize.define("purchase_order_listing", {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    purchase_order_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'purchase_order',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    },
    listing_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'listing',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
    }

  },
      {
          schema: 'ticket_exchange',
          tableName: 'purchase_order_listing',
          timestamps: false
      },
      );
  return Purchaseorderlisting;
}