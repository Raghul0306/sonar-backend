module.exports = function (sequelize, Sequelize, DataTypes) {

  const ListingDisclosures = sequelize.define("listing_disclosures", {

      id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
      },
      listingGroupId: {
          field: 'listing_group_id',
          type: Sequelize.INTEGER,
          allowNull: false,
          onDelete: 'cascade',
          references: {
            model: {
              tableName: 'listing_group',
              schema: 'ticket_exchange'
            },
            key: 'id'
          },
        },
        disclosure_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          onDelete: 'cascade',
          references: {
            model: {
              tableName: 'disclosures',
              schema: 'ticket_exchange'
            },
            key: 'id'
          },
        },
      createdAt: {
          field:'created_at',
          type: Sequelize.DATE
      },
      updatedAt: {
          field:'updated_at',
          type: Sequelize.DATE
      }, 
  },
      {
          schema: 'ticket_exchange',
          tableName: 'listing_disclosures'
      },
      );

  return ListingDisclosures;

}
