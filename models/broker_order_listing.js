module.exports = function (sequelize, Sequelize, DataTypes) {
  const BrokerOrderList = sequelize.define("broker_order_listing",{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    broker_order_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'broker_order',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "broker order id stored in this field"
    },
    listing_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      onDelete: 'cascade',
      references: {
        model: {
          tableName: 'listing',
          schema: 'ticket_exchange'
        },
        key: 'id'
      },
      comment: "listing id stored in this field"
    }
  },
  {
      schema: 'ticket_exchange',
      tableName: 'broker_order_listing',
      timestamps: false,
  });
  return BrokerOrderList;
};