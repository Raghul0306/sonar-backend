module.exports = function (sequelize, Sequelize, DataTypes) {

    const ListingAttributes = sequelize.define("issues", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          issue_type_id: {
            allowNull: false, 
            type: Sequelize.INTEGER,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'issue_types',
                schema: 'ticket_exchange'
              },
              key: 'id'
            }
          }, 
          assigner: {
            allowNull: false, 
            type: Sequelize.INTEGER,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'user',
                schema: 'ticket_exchange'
              },
              key: 'id'
            }
          },
          listing_group_id: {
            allowNull: true, 
            type: Sequelize.INTEGER,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'listing_group',
                schema: 'ticket_exchange'
              },
              key: 'id'
            }
          },
          event_id: {
            allowNull: true, 
            type: Sequelize.INTEGER,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'event',
                schema: 'ticket_exchange'
              },
              key: 'id'
            }
          },
          assignee: {
            allowNull: true, 
            type: Sequelize.INTEGER,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'user',
                schema: 'ticket_exchange'
              },
              key: 'id'
            }
          },
          createdAt: {
            field:'created_at', 
            allowNull: false,
            type: Sequelize.DATE
          },
          created_by: {
            allowNull: false, 
            type: Sequelize.INTEGER,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'user',
                schema: 'ticket_exchange'
              },
              key: 'id'
            }
          }, 
          updatedAt: {
            field:'updated_at', 
            allowNull: true,
            type: Sequelize.DATE
          }, 
          updated_by: {
            allowNull: true, 
            type: Sequelize.INTEGER,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'user',
                schema: 'ticket_exchange'
              },
              key: 'id'
            }
          }
    },
        {
            schema: 'ticket_exchange',
            tableName: 'issues'
        },
    );

    return ListingAttributes;

}
