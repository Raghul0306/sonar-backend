module.exports = function (sequelize, Sequelize, DataTypes) {

    const Listingattachments = sequelize.define("listing_attachments", {

        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },     
    
          listing_group_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            onDelete: 'cascade',
            references: {
              model: {
                tableName: 'listing_group',
                schema: 'ticket_exchange'
              },
              key: 'id'
            }
          },
    
          attachment_name: {
            type: Sequelize.STRING,
            allowNull: true
          },
    
          attachment_url: {
            type: Sequelize.TEXT,
            allowNull: false
          },
          
          active: {
            type: Sequelize.INTEGER,
            defaultValue: 1
          },
    
          createdAt: {
            field:'created_at',
            allowNull: false,
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW
          },
          updatedAt: {
            field:'updated_at',
            allowNull: false,
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW
          },
    },
        {
            schema: 'ticket_exchange',
            tableName: 'listing_attachments'
        },
        );

    return Listingattachments;

}
