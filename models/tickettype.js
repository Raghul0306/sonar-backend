module.exports = function (sequelize, Sequelize, DataTypes) {

  const Tickettype = sequelize.define("ticket_types", {
      id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
      },
      name: {
          allowNull: false,
          type: Sequelize.STRING
      },
      createdAt: {
          field: 'created_at',
          type: Sequelize.DATE
      },
      updatedAt: {
          field: 'updated_at',
          type: Sequelize.DATE
      }, 
  },
      {
          schema: 'ticket_exchange',
          tableName: 'ticket_types'
      },
      );

  return Tickettype;

}
