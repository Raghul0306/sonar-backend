module.exports = function (sequelize, Sequelize, DataTypes) {

    const Venue = sequelize.define("venue", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING
        },
        address:{
            type: Sequelize.TEXT
        },
        address2:{
            type: Sequelize.TEXT
        },
        city:{
            type: Sequelize.STRING
        },
        zip_code:{
            type: Sequelize.STRING
        },
        state:{
            type: Sequelize.STRING
        },
        country:{
            type: Sequelize.STRING
        },
        default_category: {
            type: Sequelize.INTEGER
        },
        tevoid: {
            type: Sequelize.INTEGER
        },
        mercuryid: {
            type: Sequelize.INTEGER
        },
        popularity_score: {
            type: Sequelize.FLOAT
        },
        latitude: {
            allowNull: true,
            type: Sequelize.FLOAT
        },
        longitude: {
            allowNull: true,
            type: Sequelize.FLOAT
        },
        timezone: {
            allowNull: true,
            type: Sequelize.STRING
        },
        image: {
            type: Sequelize.TEXT, 
            allowNull: true, 
            comment: "image path will be stored in this field"
        },

        createdAt: {
            field:'created_at',
            type: Sequelize.DATE
        },
        updatedAt: {
            field:'updated_at',
            type: Sequelize.DATE
        }, 
    },
        {
            schema: 'ticket_exchange',
            tableName: 'venue'
        },
        );

    return Venue;

}
