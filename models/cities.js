module.exports = function(sequelize, Sequelize, DataTypes) {

    const Cities = sequelize.define("cities", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      name: {
        type: Sequelize.STRING,
        allowNull: false
      },

      country_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'countries',
            schema: 'ticket_exchange'
          },
          key: 'id'
        }
      },

      state_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: {
            tableName: 'states',
            schema: 'ticket_exchange'
          },
          key: 'id'
        }
      },

      active: {
        type: Sequelize.INTEGER,
        allowNull: false, 
        defaultValue: 1
      },

      createdAt: {
        field: 'created_at',
        allowNull: false,
        type: Sequelize.DATE,
      },

      created_by: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      
      updatedAt: {
        field: 'updated_at',
        allowNull: false,
        type: Sequelize.DATE,
      },

      updated_by: {
        type: Sequelize.INTEGER,
        allowNull: true
      }
    }, 
    {
            schema: 'ticket_exchange',
            tableName: 'cities'
    });
  
    return Cities;
  
  }
  