const app = require('express').Router();
const fileuploadController = require('../controller').fileuploads;
const eventController = require('../controller').events;
 
const { verifyAuthToken,verifyOpenAPIAuthToken, verifyWebhookToken } = require('../middleware/auth.js');
const ticketController = require('../controller/ticketController.js')
const {buyerUser,favorities,order,tickets, webhook} = require('../controller/index');
 

module.exports = (app) => {

  app.post('/upload', fileuploadController.upload);
  app.post('/imageupload', fileuploadController.imageupload);
  app.post('/downloads', fileuploadController.downloadTicketFile);
  app.post('/invoiceUpload', fileuploadController.invoiceUpload);
  app.post('/viewEventAttachment', fileuploadController.viewEventAttachment);
  app.post('/event-details', eventController.event_list);
  app.post('/downloadAttachmentsZipfile', eventController.downloadAttachmentsZipfile);
  app.post('/viewTicketFile', eventController.viewTicketFile);
  app.post('/editTicketAttachment', eventController.editTicketAttachment);
  
  app.post('/uploadlistingfile', fileuploadController.uploadlistingfile);//for upload listings CSV/xlx file

  app.get('/event/listings', eventController.getListings);
  app.post('/event/listings', eventController.updateListings);

  app.get('/category/list', eventController.getCategory);
  app.get('/genre/list', eventController.getGenre);
  app.get('/performer', eventController.getPerformers);

  app.post('/buyer-user/register', buyerUser.registerUser);
  app.post('/buyer-user/login', buyerUser.login);
  app.post('/buyer-user/reset-password-update', buyerUser.updateResetPassword);
  app.post('/buyer-user/forget-password', buyerUser.sendResetMail);
  app.post('/buyer-user/ticket-deliver-user', buyerUser.ticketDeliverTo);
  app.post('/buyer-user/refresh-token', buyerUser.refreshToken);
  app.post('/buyer-user/save-profile', verifyAuthToken ,buyerUser.saveProfile);
  app.get('/buyer-user/:buyerUserId', verifyAuthToken, buyerUser.getUser);
  app.post('/buyer-user/ticketing-system/user-login/save', buyerUser.saveTicketingSystemUserLogin);
  app.post('/buyer-user/google-signup',buyerUser.googleSignUp);
  app.post('/buyer-user/facebook-signup',buyerUser.facebookSignUp);
  app.post('/order/order-tickets', verifyOpenAPIAuthToken ,  order.orderTickets);

  app.get('/ticket/mytickets/:userId', verifyAuthToken, ticketController.getMyTickets);
 
  app.post('/purchase-order-invoice-view', order.viewPurchaseOrderFile);
 
  app.get('/order/get-states',order.getStates);
  
  app.get('/ticket/mytickets/:userId',verifyAuthToken,tickets.getMyTickets);

  app.get('/event/venue/list',eventController.getVenueList)
  app.get('/order/order-tickets/:orderId', verifyOpenAPIAuthToken ,order.getOrderDetails);
  app.post('/favorities/save', verifyAuthToken ,favorities.saveFavorities);
  app.get('/favorities/:buyerUserId', verifyAuthToken, favorities.getFavorities);
  app.post('/geteventwebhook',verifyWebhookToken, webhook.saveTicketMasterListing);
}
