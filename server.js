const express = require("express");
var bodyParser = require('body-parser');
const { postgraphile } = require("postgraphile");
require("babel-core/register");

const { ApolloServer } = require("apollo-server-express");
const typeDefs = require("./graphqlfunctions/schema");
const query = require('./graphqlfunctions/queries');
const mutation = require('./graphqlfunctions/mutations');
var routes = require('./routes/index');
const app = express();

const resolvers = {
  Mutation: mutation,
  Query: query,
};



app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
import "babel-polyfill";
const schedule = require("node-schedule");
import {
  CreateTransaction,
  ChargePayment,
  GetStripeUser,
  GetStripeUserByEmail,
  SaveCardForFutureUse,
  GetStripeToken,
  CreateStripeAuthCharge,
  FinalizeStripeAuthCharge
} from './Payments/functions'
import {
  TevoPerformersByCategory,
  TevoEventsByPerformer,
  TevoListingsByEvent,
  TevoCategories,
  TevoListingByID,
  TevoCreateClient,
  TevoClients,
  TevoCreateOrder,
  TevoClientById,
  TevoOrders,
  TevoOrdersByClient,
  TevoClientsByName, TevoGetEtickets, TevoDeliverEtickets
} from "./Tevo/tevoQueries";
import {
  MercuryCategories,
  MercuryPerformersByCategoryPath,
  MercuryEventsByPerformer,
  MercuryListingsByEvent,
  MercuryListingByID,
  MercuryCreateOrder
} from "./Mercury/mercuryQueries";

const pluginArray = [
  // tevo API functions
  TevoPerformersByCategory, TevoEventsByPerformer, TevoListingsByEvent, TevoCategories, TevoListingByID, TevoCreateClient, TevoClients, TevoCreateOrder, TevoClientById, TevoOrders, TevoOrdersByClient, TevoGetEtickets, TevoDeliverEtickets, TevoClientsByName,
  // stripe API functions
  CreateTransaction, ChargePayment, GetStripeUser, GetStripeUserByEmail, SaveCardForFutureUse, GetStripeToken, CreateStripeAuthCharge, FinalizeStripeAuthCharge,
  // mercury API functions
  MercuryCategories, MercuryPerformersByCategoryPath, MercuryEventsByPerformer, MercuryListingsByEvent, MercuryListingByID, MercuryCreateOrder
];

//Read the ENV file to start the server
const dotenv = require('dotenv');
dotenv.config();

const log = require("simple-node-logger").createSimpleLogger("project.log");

const server = new ApolloServer({
    typeDefs: typeDefs,
    resolvers: resolvers,
    playground: {
        endpoint: "/graphql",
    },
    context: ({ req }) => {
        const user = req.headers.user
            ? JSON.parse(req.headers.user)
            : req.user
            ? req.user
            : null;
        return { user };
    },
});
server.applyMiddleware({ app });

//Route Start
require('./routes')(app);
//Route End

app.use(
  postgraphile(
    process.env.DATABASE_URL,
    "ticket_exchange",
    {
      watchPg: true,
      graphiql: true,
      enableCors: true,
      appendPlugins: pluginArray
    }
  )
);

app.listen(process.env.PORT, async () => {
  console.log("Server started!");
});