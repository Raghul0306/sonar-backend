const AWS = require('aws-sdk')
/**
|--------------------------------------------------
| TODO: Add a security token to these methods to make sure they are getting authorized
|--------------------------------------------------
*/

/**
|--------------------------------------------------
| Functions to:
 Fetch User from AWS
 Write stripe ID to AWS account
 Write tevo Customer Id to AWS Account
|--------------------------------------------------
*/

// const poolData = {
//   UserPoolId: "us-east-1_7z56wRCgv", //Should be environment variable
//   ClientId: "7iq5jl7mp7im10u3mfs9ifaqll"  //Should be environment variable
// };

// let userPool = new CognitoUserPool(poolData);

// export async function listUserPool(){
//   userPool.listUsers({}, (err, data) => {
//         if (!err) {
//             console.log('Successfull...');
//             console.log(JSON.stringify(data));
//         } else {
//             console.log('Error...');
//             console.log(JSON.stringify(err));
//         }
//         callback(null, 'Done');
//     })
// }

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: "eu-central-1",
    credentials: Auth.essentialCredentials(credentials)
  });
  var params = {
    UserPoolId: environment.amplify.Auth.userPoolId, /* required */
    AttributesToGet: [
      "email",
    ],
    Limit: 0
  };
  cognitoidentityserviceprovider.listUsers(params, function (err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else console.log(data);           // successful response
  });