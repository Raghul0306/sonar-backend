const { makeExtendSchemaPlugin, gql } = require("graphile-utils");
import dotenv from 'dotenv/config';
import axios from "axios";
// needed to access any MercuryAPI functions, filled in with "generateMercuryAccessToken()" below
let mercuryAccessToken = {};
let catalogAPIHeaders = {};
let mercuryAPIHeaders = {};

// =========================
// #region Utility Functions
// =========================
/**
 * Will generate a random GUID to pass to a
 * mercury order. Works by offsetting the
 * first 13 hex numbers by a hex portion of
 * the timestamp, and once depleted offsets
 * by a hex portion of the microseconds
 * since pageload.
 * Adapted from: https://stackoverflow.com/a/8809472
 */
function generateGUID() {
    var d = new Date().getTime();//Timestamp
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        r = (d + r)%16 | 0;
        d = Math.floor(d/16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}
// ============================
// #endregion Utility Functions
// ============================

// ===============================
// #region Mercury Token Functions
// ===============================
(async function generateMercuryAccessToken(){
    const tokenUrl = `${process.env.MERCURY_SANDBOX_TOKEN}?grant_type=client_credentials`;
    /**
    |--------------------------------------------------
    | Express to post 
    then get the bearer token via base64
    then make the request to TN with the broker id param and site id Param.
    |--------------------------------------------------
    */
    const base64String = Buffer.from(`${process.env.MERCURY_SANDBOX_CONSUMER_KEY}:${process.env.MERCURY_SANDBOX_CONSUMER_SECRET}`).toString('base64');
    const headers = {
        "Authorization": "Basic " + base64String,
        "Content-Type": "application/x-www-form-urlencoded",
        "Connection": "keep-alive",
    }
    try {
        let mercuryTokenResponse = await axios({method: "POST", url: tokenUrl, headers: headers});
        //console.info("Generated Mercury Token succesfully");
        return mercuryTokenResponse;
    }
    catch(err) {
        console.error("Couldn't generate Mercury Token.");
        console.error(err);
        return err
    }
})().then(function(tokenResponse){
    mercuryAccessToken = tokenResponse;
    catalogAPIHeaders = {
        "Authorization": `Bearer ${mercuryAccessToken}`,
        "Accept": "application/json",
        "Connection": "keep-alive"
    };
    mercuryAPIHeaders = {
        "Accept": "application/json",
        "X-Identity-Context": `"broker-id=${process.env.MERCURY_SANDBOX_BROKER_ID};website-config-id=${process.env.MERCURY_SANDBOX_MERCURY_ID}"`,
        "Authorization": `Bearer ${mercuryAccessToken}`,
        "Connection": "keep-alive"
    };
});
// ==================================
// #endregion Mercury Token Functions
// ==================================

// ==================================
// #region Mercury Category Functions
// ==================================
export const MercuryCategories = makeExtendSchemaPlugin(build => {
    return {
        typeDefs: gql`
            type MercuryCategory {
                mercury_path: String
                name: String
            }
            extend type Query {
                MercuryCategories: [MercuryCategory]
            }
        `,
        resolvers: {
            Query: {
                MercuryCategories: async (obj, args, context, info) => {
                    try {
                        let response = await axios.get(
                            `${process.env.MERCURY_SANDBOX_CATALOG_API}categories?filter=depth%20eq%201&websiteConfigId=${process.env.MERCURY_SANDBOX_CATALOG_ID}&brokerId=${process.env.MERCURY_SANDBOX_BROKER_ID}`,
                            { params: {}, headers: catalogAPIHeaders }
                        );
                        let mercuryCategories = [];
                        for (let responseCount = 0; responseCount < response.data.results.length; responseCount++) {
                            mercuryCategories.push(response.data.results[responseCount]);
                            mercuryCategories[responseCount].mercury_path = response.data.results[responseCount].path;
                            mercuryCategories[responseCount].name = response.data.results[responseCount].text.name;
                        }
                        return mercuryCategories;
                    } catch (err) {
                        return err;
                    }
                }
            }
        }
    };
});
// =====================================
// #endregion Mercury Category Functions
// =====================================

// ===================================
// #region Mercury Performer Functions
// ===================================
export const MercuryPerformersByCategoryPath = makeExtendSchemaPlugin(build => {
    return {
        typeDefs: gql`
            type MercuryPerformer {
                name: String
                mercuryid: Int
                popularity_score: Float
                primary: Boolean
            }
            extend type Query {
                MercuryPerformers(categoryPath: String): [MercuryPerformer]
            }
        `,
        resolvers: {
            Query: {
                MercuryPerformers: async (obj, args, context, info) => {
                    try {
                        let response = await axios.get(
                            `${process.env.MERCURY_SANDBOX_CATALOG_API}performers?filter=defaultCategory%2Fpath%20eq%20'${args.categoryPath}'&websiteConfigId=${process.env.MERCURY_SANDBOX_CATALOG_ID}&brokerId=${process.env.MERCURY_SANDBOX_BROKER_ID}`,
                            { params: {}, headers: catalogAPIHeaders }
                        );
                        let mercuryPerformers = [];
                        for (let responseCount = 0; responseCount < response.data.results.length; responseCount++) {
                            mercuryPerformers.push(response.data.results[responseCount]);
                            mercuryPerformers[responseCount].name = response.data.results[responseCount].text.name;
                            mercuryPerformers[responseCount].mercuryid = response.data.results[responseCount].id;
                        }
                        return mercuryPerformers;
                    } catch (err) {
                        return err;
                    }
                }
            }
        }
    }
});
// ======================================
// #endregion Mercury Performer Functions
// ======================================

// ===============================
// #region Mercury Event Functions
// ===============================
export const MercuryEventsByPerformer = makeExtendSchemaPlugin(build => {
    return {
      typeDefs: gql`
        type MercuryEvent {
            name: String
            mercuryid: Int
            occurs_at: Date
            available_count: Int
            primary_performer: Int
        }
        extend type Query {
            MercuryEvents(performerID: Int): [MercuryEvent]
        }
      `,
      resolvers: {
        Query: {
            MercuryEvents: async (obj, args, context, info) => {
                try {
                    let response = await axios.get(
                        `${process.env.MERCURY_SANDBOX_CATALOG_API}events?performerFilter=id%20eq%20${args.performerID}&websiteConfigId=${process.env.MERCURY_SANDBOX_CATALOG_ID}&brokerId=${process.env.MERCURY_SANDBOX_BROKER_ID}`,
                        { params: {}, headers: catalogAPIHeaders }
                    );
                    let mercuryEvents = [];
                    for (let responseCount = 0; responseCount < response.data.results.length; responseCount++) {
                        mercuryEvents.push(response.data.results[responseCount]);
                        mercuryEvents[responseCount].name = response.data.results[responseCount].text.name;
                        mercuryEvents[responseCount].mercuryid = response.data.results[responseCount].id;
                        mercuryEvents[responseCount].occurs_at = response.data.results[responseCount].date.datetime;
                        mercuryEvents[responseCount].available_count = response.data.results[responseCount]._metadata.ticketCount;
                        mercuryEvents[responseCount].primary_performer = args.performerID;
                    }
                    return mercuryEvents;
                } catch (err) {
                    return err;
                }
            }
        }
      }
    };
});
// ==================================
// #endregion Mercury Event Functions
// ==================================

// =================================
// #region Mercury Listing Functions
// =================================
export const MercuryListingsByEvent = makeExtendSchemaPlugin(build => {
    return {
      typeDefs: gql`
        type MercuryListing {
            eventid: Int
            exchange_ticket_group_id: Int
            wholesale_price: Float
            row: String
            section: String
            available_quantity: Int
            instant_delivery: Boolean
            in_hand: String
            in_hand_on: Date
            retail_price: Float
            splits: [Int]
            format: [String]
            seats: [Int]
        }
        extend type Query {
            MercuryListings(eventID: Int): [MercuryListing]
        }
      `,
      resolvers: {
        Query: {
            MercuryListings: async (obj, args, context, info) => {
                try {
                    let response = await axios.get(
                        `${process.env.MERCURY_SANDBOX_MERCURY_API}ticketgroups?eventId=${args.eventID}`,
                        { params: {}, headers: mercuryAPIHeaders }
                    );
                    let mercuryListings = [];
                    for (let responseCount = 0; responseCount < response.data.totalCount; responseCount++) {
                        mercuryListings[responseCount] = {};
                        mercuryListings[responseCount].eventid = response.data.ticketGroups[responseCount].id;
                        mercuryListings[responseCount].exchange_ticket_group_id = response.data.ticketGroups[responseCount].exchangeTicketGroupId;
                        mercuryListings[responseCount].wholesale_price = response.data.ticketGroups[responseCount].unitPrice.wholesalePrice.value;
                        mercuryListings[responseCount].row = response.data.ticketGroups[responseCount].seats.row;
                        mercuryListings[responseCount].section = response.data.ticketGroups[responseCount].seats.section;
                        mercuryListings[responseCount].available_quantity = response.data.ticketGroups[responseCount].availableQuantity;
                        mercuryListings[responseCount].in_hand = response.data.ticketGroups[responseCount].onHandDate;
                        mercuryListings[responseCount].retail_price = response.data.ticketGroups[responseCount].unitPrice.retailPrice.value;
                        mercuryListings[responseCount].format = response.data.ticketGroups[responseCount].deliveryMethods;
                        mercuryListings[responseCount].splits = response.data.ticketGroups[responseCount].purchasableQuantities;
                        // combine the "seats" information to give a list of seat numbers
                        let lowSeat = response.data.ticketGroups[responseCount].seats.lowSeat;
                        let highSeat = response.data.ticketGroups[responseCount].seats.highSeat;
                        mercuryListings[responseCount].seats = [];
                        for (let seatCount = lowSeat; seatCount <= highSeat; seatCount++) {
                            mercuryListings[responseCount].seats.push(seatCount);
                        }
                        // not sure what to do for these.
                        //mercuryListings[i].instant_delivery = true;
                        //mercuryListings[i].in_hand_on = ;
                    }
                    return mercuryListings;
                } catch (err) {
                    return err;
                }
            }
        }
      }
    };
});

export const MercuryListingByID = makeExtendSchemaPlugin(build => {
    return {
        typeDefs: gql`
            extend type Query {
                MercuryListingByID(exchangeTicketGroupId: Int): MercuryListing
            }
        `,
        resolvers: {
            Query: {
                MercuryListingByID: async (obj, args, context, info) => {
                    try {
                        let response = await axios.get(
                            `${process.env.MERCURY_SANDBOX_MERCURY_API}ticketgroups?exchangeTicketGroupId=${args.exchangeTicketGroupId}`,
                            { params: {}, headers: mercuryAPIHeaders }
                        );
                        let mercuryListing = {};
                        let mercuryMatchedListing = response.data.ticketGroups[0];
                        mercuryListing.eventid = mercuryMatchedListing.eventId;
                        mercuryListing.exchange_ticket_group_id = mercuryMatchedListing.exchangeTicketGroupId;
                        mercuryListing.wholesale_price = mercuryMatchedListing.unitPrice.wholesalePrice.value;
                        mercuryListing.row = mercuryMatchedListing.seats.row;
                        mercuryListing.section = mercuryMatchedListing.seats.section;
                        mercuryListing.available_quantity = mercuryMatchedListing.availableQuantity;
                        mercuryListing.in_hand = mercuryMatchedListing.onHandDate;
                        mercuryListing.retail_price = mercuryMatchedListing.unitPrice.retailPrice.value;
                        mercuryListing.format = mercuryMatchedListing.deliveryMethods;
                        mercuryListing.splits = mercuryMatchedListing.purchasableQuantities;
                        // combine the "seats" information to give a list of seat numbers
                        let lowSeat = mercuryMatchedListing.seats.lowSeat;
                        let highSeat = mercuryMatchedListing.seats.highSeat;
                        mercuryListing.seats = [];
                        for (let seatCount = lowSeat; seatCount <= highSeat; seatCount++) {
                            mercuryListing.seats.push(seatCount);
                        }
                        // not sure what to do for these.
                        //mercuryListing.instant_delivery = true;
                        //mercuryListing.in_hand_on = ;
                        return mercuryListing;
                    } catch (err) {
                        return err;
                    }
                }
            }
        }
    };
});
// ====================================
// #endregion Mercury Listing Functions
// ====================================

// ===============================
// #region Mercury Order Functions
// ===============================
/**
 * Will create an order object that mercury expects
 * to order tickets, using a previously created.
 * You must create a lock, then create an order
 * before any Mercury order succeeds. This function
 * assumes there was a successful Stripe transaction
 * between the end user and Phunnel before it runs.
 * When this returns true, a transaction is written
 * to the phunnel transaction DB.
 * @param {JSONObject} lockData 
 */
async function createMercuryOrderFromMercuryLock(lockData, recipientInfo, deliveryInfo, stripeID) {
    let url = `${process.env.MERCURY_SANDBOX_MERCURY_API}orders/freeformaddress`;
    let orderData = {
        "buyRequestId": generateGUID(),
        "lockRequestId": lockData.data.lockRequestId,
        "recipient": {
            "name": recipientInfo.name,
            "email": recipientInfo.email,
            "address": {
                "address1": recipientInfo.address.address1,
                "address2": recipientInfo.address.address2,
                "city": recipientInfo.address.city,
                "state": recipientInfo.address.state,
                "postalCode": recipientInfo.address.postalCode,
                "countryCode": recipientInfo.address.countryCode,
                "phone": recipientInfo.address.phone
            }
        },
        "notes": stripeID, // check if this is internal- if so we can store StripeID here
        "delivery": {
            "method": deliveryInfo.method,
            "deliveryCost": deliveryInfo.deliveryCost,
            "notes": deliveryInfo.notes
        }
    };
    try {
        let mercuryOrderResponse = await axios.post(url, orderData, {headers: mercuryAPIHeaders});
        if(!mercuryOrderResponse.hasError) {
            console.info(mercuryOrderResponse);
            console.info(`Mercury order created for ticket group #${mercuryOrderResponse.config.data.exchangeTicketGroupId}.`);
            return mercuryOrderResponse;
        }
    }
    catch(e) {
        // handle error below success case because this will never hit if we have an order response.
        console.error("Axios failed to get the mercury order via ticket group ID.");
        console.error(e.response.data.validationErrors);
        return false;
    }
}

/**
 * Will create a mercury lock, which is required for
 * running any Mercury order. Meaningfully uses sandbox
 * data right now.
 * @param {JSONObject} mercuryTicketGroup 
 * @param {int} quantity 
 */
async function createMercuryLockFromTicketGroup(mercuryTicketGroupId, quantity, wholesalePrice) {
    // first a lock is created using the MercuryAPI
    let url = `${process.env.MERCURY_SANDBOX_MERCURY_API}lock`;
    let lockData = {
        "lockRequestId": generateGUID(),
        "ticketGroupId": mercuryTicketGroupId, //mercuryTicketGroup.exchangeTicketGroupId,
        "quantity": quantity, //quantity,
        "wholesalePrice": wholesalePrice, //mercuryTicketGroup.unitPrice.wholesalePrice.value,
        "overridePrice": false // can change this to "TRUE" if you want to add markups at this stage
    };
    console.info("About to hit axios with mercury lock request....");
    try{
        console.info(lockData);
        let mercuryLock = await axios.post(url, lockData, {headers: mercuryAPIHeaders});
        if(!mercuryLock.hasError) {
            console.info("Axios succeeded in getting a mercury lock.");
            return mercuryLock;
        }
    }
    catch(err) {
        // handle error below success case because this will never hit if we have an order.
        console.error("Axios failed to get a mercury lock.");
        console.error(err);
        return false;
    }
}

/**
 * Will retrieve an order that has been created with
 * the MercuryAPI. Needs the mercury transaction ID
 * for the lookup of the order, and will return the
 * order itself.
 * @param {int} mercuryTransactionId 
 */
async function getMercuryOrderByMercuryTransactionId(mercuryTransactionId) {
    let url = `${process.env.MERCURY_SANDBOX_MERCURY_API}orders/${mercuryTransactionId}`;
    let response = await axios.get(url, {headers: mercuryAPIHeaders});
    if(!response.hasError) {
        console.info("Axios was able to get the Mercury order by Mercury transaction ID.");
        return response;
    }
    // handle error below success case because this will never hit if we have an order.
    console.error("Axios was unable to get the Mercury order by Mercury transaction ID.");
    return err;
}

/**
 * The main entry point for the Phunnel API to
 * create a Mercury order. Leverages functions
 * createMercuryLockFromTicketGroup() and
 * createMercuryOrderFromMercuryLock() in
 * this file to do the work.
 */
export const MercuryCreateOrder = makeExtendSchemaPlugin(build => {
    return {
        typeDefs: gql`
            input MercuryAddressInput {
                address1: String!
                address2: String
                city: String!
                state: String!
                postalCode: Int!
                countryCode: String!
                phone: String!
            }
            type MercuryAddress {
                address1: String!
                address2: String
                city: String!
                state: String!
                postalCode: Int!
                countryCode: String!
                phone: String!
            }
            input MercuryRecipientInput {
                name: String!
                email: String!
                address: MercuryAddressInput!
            }
            type MercuryRecipient {
                name: String!
                email: String!
                address: MercuryAddress
            }
            type MercuryTicketGroup {
                id: Int!
                in_hand: Boolean!
                in_hand_date: String!
                #event_name: String!
                #event_date: String!
            }
            input MercuryDeliveryInput {
                method: String!
                deliveryCost: Float!
                notes: String
            }
            type MercuryOrder {
                mercuryOrderId: Int!
                buyer: MercuryRecipient!
                ticketGroup: MercuryTicketGroup!
            }
            extend type Query {
                MercuryCreateOrder(
                    stripeID: String!,
                    ticketGroupId: Int!,
                    quantity: Int!,
                    wholesalePrice: Float!,
                    recipientInfo: MercuryRecipientInput!,
                    deliveryInfo: MercuryDeliveryInput!
                ): MercuryOrder
            }
        `,
        resolvers: {
            Query: {
                MercuryCreateOrder: async (obj, args, context, info) => {
                    // Step 1: ensure stripe card is auth'd (happens in payments-processor)
                    // Step 2: lock mercury order
                    let mercuryLock = await createMercuryLockFromTicketGroup(args.ticketGroupId, args.quantity, args.wholesalePrice);
                    // Step 3: order mercury using lock if stripe charge works
                    let mercuryOrder = await createMercuryOrderFromMercuryLock(mercuryLock, args.recipientInfo, args.deliveryInfo, args.stripeID);
                    // fill up our mercury order object with expected values (defined by GraphQL type)
                    let returnMercuryOrder = {};
                    returnMercuryOrder.mercuryOrderId = mercuryOrder.data.mercuryTransactionId;
                    returnMercuryOrder.buyer = args.recipientInfo;
                    returnMercuryOrder.ticketGroup = {
                        id: args.ticketGroupId,
                        in_hand: mercuryOrder.data.delivery.transferComplete,
                        in_hand_date: mercuryOrder.data.delivery.shipByDate,
                    }
                    return returnMercuryOrder;
                    // Step 4: charge stripe card (happens in payments-processor)
                }
            }
        }
    };
});
// ==================================
// #endregion Mercury Order Functions
// ==================================