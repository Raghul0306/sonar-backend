const bcrypt = require("bcrypt");
const db = require('../../models');
const User = db.user;
const Useraccount = db.useraccount;
const awsHelper = require('../../helpers/awskms');
const attributes = require("../../models/attributes");

/* Login Section */
/**
     * Input : User Credentials
     * Output : User Table ID
     * Function For : Authenticate User with AWS KMS
     * Ticket No : TIC-209
*/
async function login(_, { input: { login, password } }) {
    const user = await User.findOne({
        attributes: ["id", "first_name", "last_name", "phone", "phonecode", "email", "user_role", "mercuryid", "stripe_id", "last_login", "company_id", "street_address_1", "street_address_2", "city", "state", "zip_code", "country", "needsToChangePassword", "needsToAcceptTerms",
        "needsToAcceptPrivacy", "needsToAcceptSeller", "needsToSyncBank", "notes", "active", ["created_at", "createdAt"], ["updated_at", "updatedAt"]],
         where: { email: login } })
    if (user) {
        const useraccount = await Useraccount.findOne({ where: { email: login } })
        if (useraccount) {
            let aws_decrypted = await awsHelper.awsDecrypt(useraccount.password_hash);
            if (!bcrypt.compareSync(password, aws_decrypted)) {
                return "Password is wrong";
            } else {
                var data = { id: user.id };

                await User.update(
                    { last_login: Date.now() },
                    { where: { id: useraccount.user_id } }
                )

                return data;
            }
        } else {
            return "Password is wrong";
        }
    } else {
        return "This email doesn't exist.";
    }
}



module.exports = {
    login
};


