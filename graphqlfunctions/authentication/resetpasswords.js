const bcrypt = require("bcrypt");
const db = require('../../models');
const User = db.user;
const Useraccount = db.useraccount;
const ResetPassword = db.resetpassword;
const awsHelper = require('../../helpers/awskms');
var nodeMailer = require('nodemailer');
const sendMail = require('../../helpers/sendMail');
const mailTemplate = require('../../helpers/mailTemplate');
const crypto = require("crypto");
import { userRolesLower, responseStatus } from "../../constants/constants";

/**
    * Page : home
    * Function For : To send the support mail to the support@myticketspos.com from dashboard
    * Ticket No : TIC-40
    * TO DO : Need to fetch those data dynamically
*/

async function sendSupportMail(_, { input: { subject, message, user_role } }) {
    var mailTo = '';

    if (user_role == userRolesLower.broker_user || user_role == userRolesLower.broker) {
        mailTo = process.env.EMAIL_BROKER_SUPPORT_TO;
    } else {
        mailTo = process.env.EMAIL_MARKETPLACE_SUPPORT_TO
    }

    let mailOptions = {
        from: '"MY TICKET" <support@myticketspos.com>',
        to: mailTo,
        subject: subject,
        message: message,
    };
    const mail_status = await sendMail.outgoingMails(mailOptions);

    if (mail_status) {
        return { status: responseStatus.success };
    } else {
        return { status: responseStatus.failed };
    }

}

async function brokerSupportMail(_, { input: { subject, message, from } }) {
    var mailTo = '';
    mailTo = process.env.EMAIL_BROKER_SUPPORT_TO;

    var mailContents = {};
    mailContents.from = from;
    mailContents.to = process.env.EMAIL_BROKER_SUPPORT_TO;
    mailContents.subject = subject;
    mailContents.message = await mailTemplate.brokerSupportMailTemplate(message);
    const mail_status = await sendMail.outgoingMails(mailContents);
    if (mail_status) {
        return { status: responseStatus.success };
    } else {
        return { status: responseStatus.failed };
    }

}


async function sendChangePassword(_, { input: { passwordDetails } }) {
    if (passwordDetails.confirmpassword !== passwordDetails.password) {
        return { change_password_status: false }
    }

    const userDetails = await User.findOne({
        where: {
            email: passwordDetails.email
        }
    });
    if (userDetails) {
        const userTokenDetails = await ResetPassword.findOne({
            where: {
                user_id: userDetails.dataValues.id,
                token: passwordDetails.token
            }
        });

        if (userTokenDetails) {

            let b_password = await bcrypt.hash(passwordDetails.password, 10);
            let aws_crypted = await awsHelper.awsEncrypt(b_password);
            const updatePassword = await Useraccount.update({ password_hash: aws_crypted },
                {
                    where: {
                        email: passwordDetails.email,
                        user_id: userDetails.dataValues.id
                    }
                });
            if (updatePassword[0] == 1) {
                const deletedStatus = await ResetPassword.destroy({
                    where: {
                        user_id: userDetails.dataValues.id
                    }
                });
                if (deletedStatus == 1) {
                    return { change_password_status: true }
                }
            }
        }
    }
    return { change_password_status: false }
}


async function sendResetMail(_, { input: { email } }) {
    const userDetails = await User.findOne({
        where: {
            email: email
        }
    });

    if (userDetails) {

        const userTokenDetails = await ResetPassword.findOne({
            where: {
                user_id: userDetails.dataValues.id
            }
        });

        let resetUrl;
        if (userTokenDetails) {
            resetUrl = process.env.APP_URL + 'change-password/' + userTokenDetails.dataValues.token;
        } else {
            var tokenDetails = {};
            tokenDetails.user_id = userDetails.dataValues.id;
            tokenDetails.token = crypto.randomBytes(32).toString("hex");
            var tokenResponse = await ResetPassword.create(tokenDetails);
            resetUrl = process.env.APP_URL + 'change-password/' + tokenResponse.token;
        }

        var mailContents = {};
        mailContents.from = process.env.EMAIL_FROM_ADDRESS;
        mailContents.to = email;
        mailContents.subject = "Mail to reset password";
        mailContents.message = await mailTemplate.passwordResetMailTemplate(resetUrl);
        const mail_status = await sendMail.outgoingMails(mailContents);
        if (mail_status) {
            return { reset_status: true }
        } else {
            return { reset_status: false }
        }
    }
    else {
        return { reset_status: false }
    }
}

async function checkResetToken(_, { input: { token } }) {
    try {
        if (token) {
            var tokenResponse = await ResetPassword.findOne({
                where: {
                    token: token
                }
            });
            let tokenResults = JSON.parse(JSON.stringify(tokenResponse));
            console.log(tokenResults.id);

            return { id: tokenResults.id }
        }
    } catch (error) {
        return { id: 0 }
    }
}
module.exports = {
    sendSupportMail,
    sendChangePassword,
    sendResetMail,
    brokerSupportMail,
    checkResetToken
};


