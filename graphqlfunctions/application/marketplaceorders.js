const db = require('../../models');
const Op = db.Sequelize.Op;

const Marketplaceorder = db.marketplaceorder;
const BrokerOrder = db.brokerorder;
const BuyerUsers = db.buyerUsers;
const Deliverystatus = db.deliverystatus;
const Channels = db.channels;
const OrderStatus = db.orderstatus;
const Events = db.events;
const Performer = db.performer;
const Venue = db.venue;
const EventStatus = db.eventstatus;
const ListingGroup = db.listinggroup;
const Listing = db.listings;
const ListingTags = db.listingtags;
const TicketingSystem = db.ticketingsystems;
const TicketingSystemAccount = db.ticketingsystemaccounts;
const Tags = db.tags;
const ListingAttachments = db.listingattachments;
const User = db.user;

const sequelize = require('sequelize');
const { QueryTypes, where } = require('sequelize');

/**
 * Page : MyTickets 
 * Function For : To Get All Market Place Orders
 * Ticket No : TIC 458
 */
async function allMarketPlaceOrders(_, { input: { searches, orderStartDate, orderEndDate, deliveryStatusId } }) {

    let finalWhereStr = '',
        whereStr = '';

    if (searches && searches != "" && searches != null && searches != 'null') {
        whereStr += '( ';
        if (Number.isInteger(parseInt(searches))) {
            whereStr += ' marketplace_order.id = ' + parseInt(searches) + ' or ';
            whereStr += ' marketplace_order.listing_group_id = ' + parseInt(searches) + ' or ';
            whereStr += ' users.phone::varchar iLike \'%' + parseInt(searches) + '%\''+ ' or ';
            whereStr += ' buyerusers.phone_number::varchar iLike \'%' + parseInt(searches) + '%\''+ ' or ';
        }
        whereStr += ' channels.channel_name iLike \'%' + searches + '%\''+ ' or ';
        whereStr += ' users.email iLike \'%' + searches + '%\''+ ' or ';
        whereStr += ' ("users"."first_name" || \'-\' || "users"."last_name") iLike \'%' + searches + '%\''+ ' or ';
        whereStr += '("buyerusers"."first_name" || \'-\' || "buyerusers"."last_name") iLike \'%' + searches + '%\''+ ' or ';
        whereStr += '"buyerusers"."email" iLike \'%' + searches + '%\'';
        whereStr += ' )';
    }

    if ((orderStartDate && orderStartDate != "" && orderStartDate != null && orderStartDate != 'null')) {
        orderStartDate = orderStartDate + ' 00:00:00';
        if (orderEndDate && orderEndDate != "" && orderEndDate != null && orderEndDate != 'null') {
            orderEndDate = orderEndDate + ' 23:59:59';
        }

        if (whereStr != "") {
            whereStr += ' and ';
        }
        whereStr += ' (marketplace_order.created_at >= \'' + orderStartDate + '\'';
        if (orderEndDate && orderEndDate != "" && orderEndDate != null && orderEndDate != 'null') {
            whereStr += ' and marketplace_order.created_at <= \'' + orderEndDate + '\'';
        }
        whereStr += ' )';
    }

    if (deliveryStatusId && deliveryStatusId != "" && deliveryStatusId != null && deliveryStatusId != 'null' && deliveryStatusId != 'all') {
        if (whereStr != "") {
            whereStr += ' and ';
        }
        whereStr += 'marketplace_order.delivery_status_id in ( ' + deliveryStatusId.join(",") + '  )';
    }

    if (whereStr != "") {
        finalWhereStr = ' and (' + whereStr + ')';
    }

    const marketPlaceOrderDetails = await db.sequelize.query(`select
    "marketplace_order"."id" as "marketplaceorderid",
    to_char(
      "marketplace_order"."created_at" :: timestamp,
      'dd/mm/yyyy'
    ) as "orderdate",
    "marketplace_order"."listing_group_id",
    "marketplace_order"."delivery_status_id",
    "delivery_status"."name" as "deliverystatusname",
    to_char(
      "marketplace_order"."delivery_at" :: timestamp,
      'mm/dd/yyyy'
    ) as "deliverydate",
    "marketplace_order"."user_id",
    "users"."first_name" || '-' || "users"."last_name" as "sellerusername",
    "users"."email" as "selleremail",
    "users"."phone" as "sellerphonenumber",
	"buyerusers"."first_name" || '-' || "buyerusers"."last_name" as "buyerusername", 
	"buyerusers"."email" as "buyeremail", 
	"buyerusers"."phone_number" as "buyerphonenumber",
    "channel_reference"."external_channel_reference_id"
  from
    "ticket_exchange"."marketplace_order"
    left outer join "ticket_exchange"."broker_order" on "marketplace_order"."broker_order_id" = "broker_order"."id"
    left outer join "ticket_exchange"."channels" on "broker_order"."channel_id" = "channels"."id"
    left outer join "ticket_exchange"."channel_reference" on "broker_order"."channel_reference_id" = "channel_reference"."id"
    left outer join "ticket_exchange"."delivery_status" on "delivery_status"."id" = "marketplace_order"."delivery_status_id"
    left outer join "ticket_exchange"."user" as "users" on "marketplace_order"."user_id" = "users"."id"
	left outer join "ticket_exchange"."buyerUsers" as "buyerusers" on "marketplace_order"."buyer_user_id" = "buyerusers"."id"
  where 1=1 ` + finalWhereStr, { type: QueryTypes.SELECT }); 

    let finalResJSON = {};
    if (marketPlaceOrderDetails) {
        let jsonData = JSON.parse(JSON.stringify(marketPlaceOrderDetails));
        finalResJSON['isError'] = false;
        finalResJSON['msg'] = 'Data Returned Successfully.';
        finalResJSON['data'] = jsonData;
    }
    else {
        finalResJSON['isError'] = true;
        finalResJSON['msg'] = 'Some Error in Retrieving Data.';
    }
    return finalResJSON;
}

/**
 * Page : Market Place Orders Detail Page 
 * Function For : To Get Single Market Place Order Details 
 * Ticket No : TIC 498
 */
async function getSingleMarketPlaceOrderDetails(_, { input: { marketPlaceOrderId } }) {
    /* Marketplaceorder.belongsTo(BrokerOrder);
    BrokerOrder.belongsTo(Marketplaceorder);

    BrokerOrder.belongsTo(Channels);
    Channels.belongsTo(BrokerOrder);

    BrokerOrder.belongsTo(BuyerUsers);
    BuyerUsers.belongsTo(BrokerOrder);

    Marketplaceorder.belongsTo(User);
    User.belongsTo(Marketplaceorder);

    Marketplaceorder.belongsTo(OrderStatus);
    OrderStatus.belongsTo(Marketplaceorder);

    Marketplaceorder.belongsTo(Events);
    Events.belongsTo(Marketplaceorder);

    Events.belongsTo(Performer);
    Performer.belongsTo(Events);

    Events.belongsTo(Venue);
    Venue.belongsTo(Events);

    Events.belongsTo(EventStatus);
    EventStatus.belongsTo(Events);

    Marketplaceorder.belongsTo(ListingGroup);
    ListingGroup.belongsTo(Marketplaceorder);

    ListingGroup.hasMany(Listing);
    Listing.belongsTo(ListingGroup);

    ListingGroup.hasMany(ListingTags);
    ListingTags.belongsTo(ListingGroup);

    ListingTags.belongsTo(Tags);
    Tags.belongsTo(ListingTags);
    
    Marketplaceorder.belongsTo(Deliverystatus);
    Deliverystatus.belongsTo(Marketplaceorder);

    ListingGroup.belongsTo(TicketingSystem);
    TicketingSystem.belongsTo(ListingGroup);

    ListingGroup.belongsTo(TicketingSystemAccount);
    TicketingSystemAccount.belongsTo(ListingGroup);

    ListingGroup.hasMany(ListingAttachments);
    ListingAttachments.belongsTo(ListingGroup);

    const marketPlaceOrderDetails = await Marketplaceorder.findAll({
        attributes: [
            [sequelize.col('marketplace_order.id'), 'marketplaceorderid'], 
            [sequelize.literal("TO_CHAR(marketplace_order.created_at :: timestamp, 'dd-mm-yyyy')"), 'orderdate'],
            [sequelize.col('marketplace_order.order_status_id'), 'order_status_id'], 
            [sequelize.col('marketplace_order.event_id'), 'event_id'], 
            [sequelize.col('marketplace_order.listing_group_id'), 'listing_group_id'], 
            [sequelize.col('marketplace_order.delivery_status_id'), 'delivery_status_id'], 
            [sequelize.literal("TO_CHAR(marketplace_order.delivery_at :: timestamp, 'dd/mm/yyyy')"), 'deliverydate'],
        ],
        include: [
            {
                model: BrokerOrder,
                required: false,
                attributes: [
                    [sequelize.col('channel_id'), 'channel_id'], 
                    [sequelize.col('buyer_user_id'), 'buyer_user_id'], 
                ],
                on: {
                    col1: sequelize.where(sequelize.col('marketplace_order.broker_order_id'), '=', sequelize.col('broker_order.id')), 
                }, 
                where: {}, 
                include: [
                    {
                        model: Channels,
                        required: false,
                        attributes: [
                            [sequelize.col('channel_name'), 'channelname'], 
                        ],
                        on: {
                            col1: sequelize.where(sequelize.col('broker_order.channel_id'), '=', sequelize.col('broker_order->channel.id')), 
                        }, 
                        where: {},
                    }, 
                    {
                        model: BuyerUsers, 
                        required: false, 
                        attributes: [
                            [sequelize.fn('concat', sequelize.col('broker_order->buyerUser.first_name'), ' ', sequelize.col('broker_order->buyerUser.last_name')), 'buyerusername'], 
                            [sequelize.col('email'), 'buyeremail'], 
                            [sequelize.col('phone_number'), 'buyerphonenumber']
                        ], 
                        on: {
                            col1: sequelize.where(sequelize.col('marketplace_order.buyer_user_id'), '=', sequelize.col('broker_order->buyerUser.id')), 
                        }, 
                        where: {}
                    }
                ]
            }, 
            {
                model: User, 
                required: false, 
                attributes: [
                    [sequelize.fn('concat', sequelize.col('user.first_name'), ' ', sequelize.col('user.last_name')), 'sellerusername'], 
                    [sequelize.col('email'), 'selleremail'], 
                    [sequelize.col('phone'), 'sellerphonenumber'], 
                    [sequelize.col('company_name'), 'sellercompanyname']
                ], 
                on: {
                    col1: sequelize.where(sequelize.col('marketplace_order.seller_id'), '=', sequelize.col('user.id')), 
                }, 
                where: {}
            },
            {
                model: OrderStatus, 
                required: false, 
                attributes: [
                    [sequelize.col('name'), 'orderstatusname'], 
                ], 
                on: {
                    col1: sequelize.where(sequelize.col('marketplace_order.order_status_id'), '=', sequelize.col('order_status.id')), 
                }, 
                where: {}
            }, 
            {
                model: Events, 
                required: false, 
                attributes: [
                    [sequelize.col('name'), 'eventname'], 
                    [sequelize.literal("TO_CHAR(event.date :: timestamp, 'Day MM DD, YYYY @ HH:MI')"), 'eventdate'], 
                ], 
                on: {
                    col1: sequelize.where(sequelize.col('marketplace_order.event_id'), '=', sequelize.col('event.id')), 
                }, 
                where: {}, 
                include: [
                    {
                        model: Performer,
                        required: false,
                        attributes: [
                            [sequelize.col('name'), 'performername'], 
                        ],
                        on: {
                            col1: sequelize.where(sequelize.col('event.performer_id'), '=', sequelize.col('event->performer.id')), 
                        }, 
                        where: {},
                    },
                    {
                        model: Venue,
                        required: false,
                        attributes: [
                            [sequelize.col('name'), 'venuename'], 
                            [sequelize.col('city'), 'venuecity'], 
                            [sequelize.col('state'), 'venuestate'], 
                            [sequelize.col('country'), 'venuecountry'], 
                        ],
                        on: {
                            col1: sequelize.where(sequelize.col('event.venue_id'), '=', sequelize.col('event->venue.id')), 
                        }, 
                        where: {},
                    },
                    {
                        model: EventStatus, 
                        required: false, 
                        attributes: [
                            [sequelize.col('event_status'), 'event_status'], 
                        ], 
                        on: {
                            col1: sequelize.where(sequelize.col('event.event_status_id'), '=', sequelize.col('event->event_status.id')), 
                        }, 
                        where: {}
                    }
                ]
            }, 
            {
                model: ListingGroup, 
                required: false, 
                attributes: [
                    [sequelize.col('section'), 'section'], 
                    [sequelize.col('row'), 'row'], 
                    [sequelize.fn('concat', sequelize.col('seat_start'), ' - ', sequelize.col('seat_end')), 'seats'], 
                    [sequelize.literal("TO_CHAR(listing_group.in_hand :: timestamp, 'dd-mm-yyyy')"), 'inhand'], 
                    [sequelize.col('ticket_system_id'), 'ticketing_system_id'], 
                    [sequelize.col('ticket_system_account'), 'ticketing_system_account_id'], 
                    [sequelize.literal('SUM(price)'), 'price'], 
                    [sequelize.literal('SUM(CASE WHEN "listing_group->listings"."sold" and "listing_group->listings"."price" > 0 THEN TRUNC(("listing_group->listings"."price" / "listing_group->listings"."unit_cost") :: numeric, 2) else 0 end)'), 'margin'], 
                    [sequelize.literal('SUM(CASE WHEN "listing_group->listings"."sold" is true THEN 1 ELSE 0 END)'), 'soldcount']
                ], 
                on: {
                    col1: sequelize.where(sequelize.col('marketplace_order.listing_group_id'), '=', sequelize.col('listing_group.id')), 
                }, 
                where: {}, 
                include: [
                    {
                        model: Listing, 
                        required: false, 
                        attributes: [], 
                        on: {
                            col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listing_group->listings.listing_group_id')), 
                        }, 
                        where: {}
                    }, 
                    {
                        model: ListingTags, 
                        required: false, 
                        attributes: [
                            [sequelize.col('id'), 'listingtagid'], 
                        ], 
                        on: {
                            col1: sequelize.where(sequelize.col('listing_group->listings.listing_group_id'), '=', sequelize.col('listing_group->listing_tags.listing_group_id')), 
                        }, 
                        where: {}, 
                        include: [
                            {
                                model: Tags,
                                required: false, 
                                attributes: [
                                    [sequelize.col('id'), 'tagid'], 
                                    [sequelize.col('tag_name'), 'tagname'], 
                                ],
                                on: {
                                    col1: sequelize.where(sequelize.col('listing_group->listing_tags.tag_id'), '=', sequelize.col('listing_group->listing_tags->tag.id')), 
                                }, 
                                where: {}
                            }
                        ]
                    }, 
                    {
                        model: TicketingSystem, 
                        required: false, 
                        attributes: [
                            [sequelize.col('ticketing_system_name'), 'ticketingsystemname'], 
                        ], 
                        on: {
                            col1: sequelize.where(sequelize.col('listing_group.ticket_system_id'), '=', sequelize.col('listing_group->ticketing_system.id')), 
                        }, 
                        where: {}
                    }, 
                    {
                        model: TicketingSystemAccount, 
                        required: false, 
                        attributes: [
                            [sequelize.col('account_name'), 'ticketingsystemaccountname']
                        ], 
                        on: {
                            col1: sequelize.where(sequelize.col('listing_group.ticket_system_account'), '=', sequelize.col('listing_group->ticketing_system_account.id')), 
                        }, 
                        where: {}
                    }, 
                    {
                        model: ListingAttachments, 
                        required: false, 
                        attributes: [
                            [sequelize.col('attachment_name'), 'attachmentname'], 
                            [sequelize.col('attachment_url'), 'attachmenturl'], 
                        ], 
                        on: {
                            col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listing_group->listing_attachments.listing_group_id')), 
                        }, 
                        where: {}
                    }
                ]
            }, 
            {
                model: Deliverystatus, 
                required: false, 
                attributes: [
                    [sequelize.col('name'), 'deliverystatusname'],
                ], 
                on: {
                    col1: sequelize.where(sequelize.col('marketplace_order.delivery_status_id'), '=', sequelize.col('delivery_status.id')), 
                }, 
                where: {}
            }
        ],
        where: {
            id: marketPlaceOrderId
        },
        group: ['marketplace_order.id', 'broker_order.id', 'broker_order->channel.id', 'broker_order->buyerUser.id', 'order_status.id', 'event.id', 'event->performer.id', 'event->venue.id', 'event->event_status.id', 'listing_group.id', 'listing_group->listing_tags.id', 'listing_group->listing_tags->tag.id', 'listing_group->ticketing_system.id', 'listing_group->ticketing_system_account.id', 'listing_group->listing_attachments.id', 'delivery_status.id', 'user.id']
    }); */

    const mainData = await db.sequelize.query(`select 
    "marketplace_order"."id" as "marketplaceorderid", to_char("marketplace_order"."created_at" :: timestamp, 'dd-mm-yyyy') as orderdate, 
    "marketplace_order"."order_status_id", "marketplace_order"."event_id", "marketplace_order"."listing_group_id", 
    "marketplace_order"."delivery_status_id", to_char("marketplace_order"."delivery_at" :: timestamp, 'dd/mm/yyyy') as "deliverydate", 
    "broker_order"."channel_id", "marketplace_order"."buyer_user_id", "channels"."channel_name" as "channelname", 
    "buyerUsers"."first_name"|| ' ' || "buyerUsers"."last_name" as "buyerusername", "buyerUsers"."email" as "buyeremail", 
    "buyerUsers"."phone_number" as "buyerphonenumber", "user"."first_name" || ' ' || "user"."last_name" as "sellerusername", "user"."email" as "selleremail", 
    "user"."phone" as "sellerphonenumber", "companies"."company_name" as "sellercompanyname", "marketplace_order"."order_status_id", "order_status"."name" as "orderstatusname", 
    "event"."name" as "eventname", to_char("event"."date" :: timestamp, 'Day MM DD, YYYY @ HH:MI') as "eventdate", "performer"."name" as "performername", 
    "venue"."name" as "venuename", "venue"."city" as "venuecity","venue"."timezone" as "timezone", "venue"."state" as "venuestate", "venue"."country" as "venuecountry", 
    "event_status"."event_status" as "event_status", "listing_group"."section", "listing_group"."row", "listing_group"."seat_start" || '-' || "listing_group"."seat_end" as "seats", 
    to_char("listing_group"."in_hand" :: timestamp, 'dd-mm-yyyy') as "inhand", "listing_group"."ticket_system_id" as "ticketing_system_id", 
    "listing_group"."ticket_system_account" as "ticketing_system_account_id", "ticketing_systems"."ticketing_system_name" as "ticketingsystemname", 
    "ticketing_system_accounts"."account_name" as "ticketingsystemaccountname", "delivery_status"."name" as "deliverystatusname", "listing_group"."ticket_type_id","ticket_types"."name" as "tickettypename"
    from 
    "ticket_exchange"."marketplace_order"
    left outer join "ticket_exchange"."broker_order" on "marketplace_order"."broker_order_id" = "broker_order"."id"
    left outer join "ticket_exchange"."channels" on "broker_order"."channel_id" = "channels"."id"
    left outer join "ticket_exchange"."buyerUsers" on "marketplace_order"."buyer_user_id" = "buyerUsers"."id"
    left outer join "ticket_exchange"."user" on "marketplace_order"."user_id" = "user"."id"
    left outer join "ticket_exchange"."companies" on "user"."company_id" = "companies"."id"
    left outer join "ticket_exchange"."order_status" on "marketplace_order"."order_status_id" = "order_status"."id"
    left outer join "ticket_exchange"."event" on "marketplace_order"."event_id" = "event"."id"
    left outer join "ticket_exchange"."performer" on "event"."performer_id" = "performer"."id"
    left outer join "ticket_exchange"."venue" on "event"."venue_id" = "venue"."id"
    left outer join "ticket_exchange"."event_status" on "event"."event_status_id" = "event_status"."id"
    left outer join "ticket_exchange"."listing_group" on "marketplace_order"."listing_group_id" = "listing_group"."id"
    left outer join "ticket_exchange"."ticketing_systems" on "listing_group"."ticket_system_id" = "ticketing_systems"."id"
    left outer join "ticket_exchange"."ticketing_system_accounts" on "listing_group"."ticket_system_account" = "ticketing_system_accounts"."id"
    left outer join "ticket_exchange"."delivery_status" on "marketplace_order"."delivery_status_id" = "delivery_status"."id"
    left outer join "ticket_exchange"."ticket_types" on "listing_group"."ticket_type_id" = "ticket_types"."id"
    where "marketplace_order"."id" = ` + marketPlaceOrderId, { type: QueryTypes.SELECT });

    let myResJSON = {};
    if (mainData.length > 0) {
        const listingData = await db.sequelize.query(`select "listing"."listing_group_id", 
        sum("listing"."price") as "price",
        sum(case when "listing"."sold" and "listing"."price" > 0 then trunc(("listing"."price"/"listing"."unit_cost") :: numeric, 2) else 0 end) as margin,
        sum(case when "listing"."sold" is true then 1 else 0 end) as "soldcount"
        from
        "ticket_exchange"."marketplace_order_listing"
        inner join "ticket_exchange"."listing" on "listing"."id" = "marketplace_order_listing"."listing_id"
        where "listing"."listing_group_id" = ` + mainData[0].listing_group_id + `
        group by "listing"."listing_group_id";`, { type: QueryTypes.SELECT });

        const listingTagsData = await db.sequelize.query(`select "listing_tags"."id" as "listing_tag_id", "tags"."tag_name"
        from 
        "ticket_exchange"."listing_group"
        inner join "ticket_exchange"."listing_tags" on "listing_group"."id" = "listing_tags"."listing_group_id"
        inner join "ticket_exchange"."tags" on "listing_tags"."tag_id" = "tags"."id"
        where "listing_group"."id" = ` + mainData[0].listing_group_id, { type: QueryTypes.SELECT });

        const listingAttachmentsData = await db.sequelize.query(`select "listing_attachments"."id", "listing_attachments"."attachment_name", "listing_attachments"."attachment_url"
        from
        "ticket_exchange"."listing_group"
        inner join "ticket_exchange"."listing_attachments" on "listing_group"."id" = "listing_attachments"."listing_group_id"
        where "listing_group"."id" =  ` + mainData[0].listing_group_id + ` ORDER BY "listing_attachments"."id" ASC `, { type: QueryTypes.SELECT });

        let brokerOrderJSON = {};
        let channelJSON = {};
        let buyerUserJSON = {};
        let userJSON = {};
        let orderStatusJSON = {};
        let eventJSON = {};
        let performerJSON = {};
        let venueJSON = {};
        let listingGroupJSON = {};
        let deliveryStatusJSON = {};
        let ticketingSystemJSON = {};
        let ticketingSystemAccountJSON = {};
        let ticketTypesJSON = {};

        channelJSON["channelname"] = mainData[0].channelname;

        buyerUserJSON["buyerusername"] = mainData[0].buyerusername;
        buyerUserJSON["buyeremail"] = mainData[0].buyeremail;
        buyerUserJSON["buyerphonenumber"] = mainData[0].buyerphonenumber;

        brokerOrderJSON["channel_id"] = mainData[0].channel_id;
        brokerOrderJSON["buyer_user_id"] = mainData[0].buyer_user_id;
        brokerOrderJSON["channel"] = channelJSON;
        brokerOrderJSON["buyerUser"] = buyerUserJSON;

        userJSON["sellerusername"] = mainData[0].sellerusername;
        userJSON["selleremail"] = mainData[0].selleremail;
        userJSON["sellerphonenumber"] = mainData[0].sellerphonenumber;
        userJSON["sellercompanyname"] = mainData[0].sellercompanyname;

        orderStatusJSON["orderstatusname"] = mainData[0].orderstatusname;

        performerJSON["performername"] = mainData[0].performername;

        venueJSON["venuename"] = mainData[0].venuename;
        venueJSON["venuecity"] = mainData[0].venuecity;
        venueJSON["venuestate"] = mainData[0].venuestate;
        venueJSON["venuecountry"] = mainData[0].venuecountry;
        venueJSON["timezone"] = mainData[0].timezone;
        eventJSON["eventname"] = mainData[0].eventname;
        eventJSON["eventdate"] = mainData[0].eventdate;
        eventJSON["performer"] = performerJSON;
        eventJSON["venue"] = venueJSON;
        eventJSON["event_status"] = mainData[0].event_status;

        ticketingSystemJSON["ticketingsystemname"] = mainData[0].ticketingsystemname;

        ticketingSystemAccountJSON["ticketing_system_account"] = mainData[0].ticketingsystemaccountname;

        listingGroupJSON["section"] = mainData[0].section;
        listingGroupJSON["row"] = mainData[0].row;
        listingGroupJSON["seats"] = mainData[0].seats;
        listingGroupJSON["inhand"] = mainData[0].inhand;
        listingGroupJSON["ticketing_system_id"] = mainData[0].ticketing_system_id;
        listingGroupJSON["ticketing_system_account_id"] = mainData[0].ticketing_system_account_id;

        if (listingData.length > 0) {
            listingGroupJSON["price"] = listingData[0].price;
            listingGroupJSON["margin"] = listingData[0].margin;
            listingGroupJSON["soldcount"] = listingData[0].soldcount;
            listingGroupJSON["listing_tags"] = listingTagsData;
        }

        listingGroupJSON["ticketing_system"] = ticketingSystemJSON;
        listingGroupJSON["ticketing_system_account"] = ticketingSystemAccountJSON;
        listingGroupJSON["listing_attachments"] = listingAttachmentsData;

        deliveryStatusJSON["deliverystatusname"] = mainData[0].deliverystatusname;

        ticketTypesJSON["ticket_type_id"] = mainData[0].ticket_type_id;
        ticketTypesJSON["ticket_type_name"] = mainData[0].tickettypename;

        myResJSON["marketplaceorderid"] = mainData[0].marketplaceorderid;
        myResJSON["orderdate"] = mainData[0].orderdate;
        myResJSON["order_status_id"] = mainData[0].order_status_id;
        myResJSON["event_id"] = mainData[0].event_id;
        myResJSON["listing_group_id"] = mainData[0].listing_group_id;
        myResJSON["delivery_status_id"] = mainData[0].delivery_status_id;
        myResJSON["deliverydate"] = mainData[0].deliverydate;
        myResJSON["broker_order"] = brokerOrderJSON;
        myResJSON["user"] = userJSON;
        myResJSON["order_status"] = orderStatusJSON;
        myResJSON["event"] = eventJSON;
        myResJSON["listing_group"] = listingGroupJSON;
        myResJSON["delivery_status"] = deliveryStatusJSON;
        myResJSON["ticket_type"] = ticketTypesJSON;
    }

    let finalResJSON = {};
    if (myResJSON) {
        let jsonData = JSON.parse(JSON.stringify(myResJSON));
        finalResJSON['isError'] = false;
        let newArray = [];
        newArray.push(myResJSON);
        finalResJSON['msg'] = 'Data Returned Successfully.';
        finalResJSON['data'] = myResJSON;

    }
    else {
        finalResJSON['isError'] = true;
        finalResJSON['msg'] = 'Some Error in Retrieving Data.';
    }
    return finalResJSON;
}


module.exports = {
    allMarketPlaceOrders,
    getSingleMarketPlaceOrderDetails
};
