const db = require('../../models');
const User = db.user;
const Listings = db.listings;
const Events = db.events;
const Performer = db.performer;
const Venue = db.venue;
const Op = db.Sequelize.Op;
const TicketType = db.tickettype;
const Genre = db.genre;
const Category=db.category;
const Tags = db.tags;
const Ticketingsystems = db.ticketingsystems;
const Disclosures = db.disclosures;
const Attributes = db.attributes;
const EventTags = db.eventtags;
const ListingTags = db.listingtags;
const ListingDisclosures = db.listingdisclosures;
const ListingAttributes = db.listingattributes;
const Channels = db.channels;
const CreditCards = db.creditcard;
const CreditCardTypes = db.creditcardtype;
const ListingChannelMarkups = db.listingchannelmarkups;
const Listingattachments = db.listingattachments;
const Seattype = db.seattypes;
const ListingGroup = db.listinggroup;
const Splits = db.splits;
const Ticketingsystemaccounts = db.ticketingsystemaccounts;
const PriceLog = db.pricelog;
const Purchaseorder = db.purchaseorder;
const Purchaseorderlisting = db.purchaseorderlisting;
const Purchaseordertag = db.purchaseordertag;
const Purchaseorderinvoiceattachment = db.purchaseorderinvoiceattachment;
const ListingStatus = db.listingStatus;
const Companies = db.companies;
const Issues = db.issues;
const IssueTypes = db.issueTypes;
import axios from "axios";
import { s3Path, folderNames, tableNames, uploadListingsColumnNames, splitTypes, shareTypes, seat_type, seatTypes, ticketTypes, commonVariables, levelNames, listingVariables, uploadURLs, userRolesLower, inputType, listingStatusList, sampleChannelFees, mailParser } from "../../constants/constants";
const csv = require('csv-parser');
const fs = require('fs')
var request = require('request');
var FormData = require('form-data');
const reader = require('xlsx');
const { getUserIdsForListingGroupAccess, getUserRole } = require('../../helpers/userAccess.js');
const { getEventDetails, getListingDetails, getListingStatusbyName, getListingStatusbyId, getRecentPricelogChanger } = require('../../helpers/queryResponse');

const AWS = require('aws-sdk');
const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});
const sequelize = require('sequelize');
const { QueryTypes, where } = require('sequelize');
const { table } = require('console');
const { isValidInput } = require('../../helpers/inputValidate.js');
/**
 * Page : MyTickets 
 * Function For : To Get All Ticket Types
 * Ticket No : TIC 17, 104, 105 & 106
 */
async function allTicketTypes() {
    const ticketTypeDtls = await TicketType.findAll({
        attributes: ['id', 'name', 'created_at', 'updated_at'],
        where: {}
    });
    if (ticketTypeDtls) {
        return JSON.parse(JSON.stringify(ticketTypeDtls));
    } else {
        return "No data";
    }
}

/**
 * Page : MyTickets 
 * Function For : To Get All EventID, Performer, Venue & Listings Details For Auto Suggest
 * Ticket No : TIC 17, 104, 105 & 106
 */
async function getAllEventIdPerformerVenueCityCountryTags(_, { input: { searchStr, isOperationFilter } }) {
    let eventIdDtls = await Events.findAll({
        attributes: ['id'],
        where: {
            [Op.or]: [
                sequelize.where(
                    sequelize.cast(sequelize.col('id'), 'varchar'),
                    { [Op.iLike]: '%' + parseInt(searchStr) + '%' }
                ),
            ]
        },
        order: [
            ['id', 'ASC'],
        ]
    });
    let eventNameDtls = await Events.findAll({
        attributes: ['name'],
        where: { name: { [Op.iLike]: '%' + searchStr + '%' } }
    });
    let performerDtls = await Performer.findAll({
        attributes: [[sequelize.fn('DISTINCT', sequelize.col('name')), 'name']],
        where: { name: { [Op.iLike]: '%' + searchStr + '%' } }
    });
    let venueNameDtls = await Venue.findAll({
        attributes: [[sequelize.fn('DISTINCT', sequelize.col('name')), 'name']],
        where: { name: { [Op.iLike]: '%' + searchStr + '%' } }
    });
    let venueCityDtls = await db.sequelize.query(`select concat(city,', ',country) as city from ticket_exchange.venue where concat(city,', ',country) iLike \'%` + searchStr + `%\';`, { type: QueryTypes.SELECT });

    let venueCountryDtls = await Venue.findAll({
        attributes: [[sequelize.fn('DISTINCT', sequelize.col('country')), 'country']],
        where: { country: { [Op.iLike]: '%' + searchStr + '%' } }
    });
    let tagDtls = await Tags.findAll({
        attributes: [[sequelize.fn('DISTINCT', sequelize.col('tag_name')), 'tag_name']],
        where: { tag_name: { [Op.iLike]: '%' + searchStr + '%' } }
    });

    let listingArray = [];

    if (eventIdDtls || eventNameDtls || performerDtls || venueNameDtls || venueCityDtls || venueCountryDtls || tagDtls) {
        let eventIdDtlsJSON = JSON.parse(JSON.stringify(eventIdDtls));
        let eventNameDtlsJSON = JSON.parse(JSON.stringify(eventNameDtls));
        let performerDtlsJSON = JSON.parse(JSON.stringify(performerDtls));
        let venueNameDtlsJSON = JSON.parse(JSON.stringify(venueNameDtls));
        let venueCountryDtlsJSON = JSON.parse(JSON.stringify(venueCountryDtls));
        let tagDtlsJSON = JSON.parse(JSON.stringify(tagDtls));


        eventIdDtlsJSON.forEach(row => {
            if (row.id != null && row.id != 'null' && row.id != "")
                listingArray.push(row.id);
        });
        eventNameDtlsJSON.forEach(row => {
            if (row.name != null && row.name != 'null' && row.name != "")
                listingArray.push(row.name);
        });
        performerDtlsJSON.forEach(row => {
            if (row.name != null && row.name != 'null' && row.name != "")
                listingArray.push(row.name);
        });
        venueNameDtlsJSON.forEach(row => {
            if (row.name != null && row.name != 'null' && row.name != "")
                listingArray.push(row.name);
        });
        venueCityDtls.forEach(row => {
            if (row.city != null && row.city != 'null' && row.city != "")
                listingArray.push(row.city);
        });
        venueCountryDtlsJSON.forEach(row => {
            if (row.country != null && row.country != 'null' && row.country != "")
                listingArray.push(row.country);
        });
        tagDtlsJSON.forEach(row => {
            if (row.tag_name != null && row.tag_name != 'null' && row.tag_name != "")
                listingArray.push(row.tag_name);
        });

    }
    if (isOperationFilter) {
        let companyDetails = await Companies.findAll({
            attributes: [[sequelize.fn('DISTINCT', sequelize.col('company_name')), 'company_name']],
            where: { company_name: { [Op.iLike]: '%' + searchStr + '%' } }
        });
        var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);

        let purchaseOrderDetails = await db.sequelize.query(`SELECT "purchase_order"."id" as po_id,"purchase_order"."external_reference"FROM"ticket_exchange"."purchase_order"AS"purchase_order" INNER JOIN "ticket_exchange"."listing_group" AS"listing_group"ON"purchase_order"."listing_group_id"="listing_group"."id"AND"listing_group"."listing_status_id"= ` + qcListingStatus.id + ` AND"listing_group"."is_removed"= false WHERE ( CAST("purchase_order"."id"AS VARCHAR) ILIKE'%` + searchStr + `%' or "purchase_order"."external_reference" ILIKE '%` + searchStr + `%') ORDER BY"purchase_order"."id"ASC;`, { type: QueryTypes.SELECT });

        if (companyDetails || purchaseOrderDetails) {
            let companyDetailsJSON = JSON.parse(JSON.stringify(companyDetails));
            let purchaseOrderDetailsJSON = JSON.parse(JSON.stringify(purchaseOrderDetails));

            companyDetailsJSON.forEach(row => {
                if (row.company_name != null && row.company_name != 'null' && row.company_name != "")
                    listingArray.push(row.company_name);
            });
            purchaseOrderDetailsJSON.forEach(row => {
                if (row.po_id != null && row.po_id != 'null' && row.po_id != "")
                    listingArray.push(row.po_id);
            });
            purchaseOrderDetailsJSON.forEach(row => {
                if (row.external_reference != null && row.external_reference != 'null' && row.external_reference != "")
                    listingArray.push(row.external_reference);
            });
        }
    }
    let jsonData = JSON.parse(JSON.stringify(listingArray));
    if (jsonData.length) { jsonData = [...new Set(jsonData)]; }
    return jsonData;
}

/**
 * MyTickets Page - To Get All Sub Category Details - TIC 17, 104, 105 & 106
 */
async function allGenreDetails() {
    const genreDetails = await Genre.findAll({
        attributes: ['id', 'name', 'parent_category', 'child_category', 'tevo_id', 'mercury_id', 'created_at', 'updated_at'],
        where: {}
    });
    if (genreDetails) {
        return JSON.parse(JSON.stringify(genreDetails));
    } else {
        return "No data";
    }
}

/**
 * MyTickets Page - To Get All Category Details
 */
 async function allCategoryDetails() {
    const categoryDetails = await Category.findAll({
        attributes: ['id', 'name'],
        where: {}
    });
    if (categoryDetails) {
        return JSON.parse(JSON.stringify(categoryDetails));
    } else {
        return "No data";
    }
}

/**
 * Page : Add Listings Filter
 * Function For : To Get Default & Filtered Listings
 * Ticket No : TIC 109, 110, 112
 */
async function addListingsFilter(_, { input: { searches, from_date, to_date, day_of_week, ticket_type, genre, tags, in_hand, page, limit } }) {

    let whereStr = '';
    let finalWhereStr = '';
    let day_of_week_state = false
    let paginationCondition = '';
    if (limit && page) {
        paginationCondition = ` limit ${limit} OFFSET ${(page - 1) * limit} `;
    }

    if (searches && searches != "" && searches != null && searches != 'null') {
        whereStr += '( ';
        if (Number.isInteger(parseInt(searches))) {
            whereStr += ' event.id = ' + parseInt(searches) + ' or ';
        }
        whereStr += 'event.name iLike \'%' + searches + '%\'';
        whereStr += ' or performer.name iLike \'%' + searches + '%\'';
        whereStr += ' or venue.name iLike \'%' + searches + '%\'';
        whereStr += ' or concat(venue.city, \', \', venue.country) iLike \'%' + searches + '%\'';
        whereStr += ' or venue.country iLike \'%' + searches + '%\'';
        whereStr += ' or tags.tag_name iLike \'%' + searches + '%\'';
        whereStr += ' )';
    }

    if ((from_date && from_date != "" && from_date != null && from_date != 'null')) {
        if (whereStr != "") {
            whereStr += ' and ';
        }
        whereStr += ' (event.date >= \'' + from_date + '\'';
        if (to_date && to_date != "" && to_date != null && to_date != 'null') {
            whereStr += ' and event.date <= \'' + to_date + '\'';
        }
        whereStr += ' )';
    }

    if (day_of_week && day_of_week != "" && day_of_week != null && day_of_week != 'null') {
        if (whereStr != "") {
            whereStr += ' and ';
        }
        whereStr += `"event"."eventdate_day" in ( '` + day_of_week.join("','") + `'  )`;
    }

    if (genre && genre != "" && genre != null && genre != 'null' && Number.isInteger(parseInt(genre))) {
        if (whereStr != "") {
            whereStr += ' and ';
        }
        whereStr += 'event.genre_id = ' + genre;
    }

    if (tags && tags.length) {
        whereStr += ` ${whereStr != "" ? ' and ' : ''} listing_tags.tag_id in (${tags.join()})`;
    }
    if (whereStr != "") {
        finalWhereStr = ' and (' + whereStr + ')';
    }
    if (finalWhereStr == '') {
        return;
    }
    else {
        return db.sequelize.query(`SELECT event.id as event_id, event.name as eventname, TO_CHAR(event.date,'YYYY-MM-DD HH24:MI') as eventdate,  array_agg(distinct venue.name) as venuename, array_agg(distinct venue.city) as city, array_agg(distinct venue.state) as state, array_agg(distinct venue.country) as country, array_agg(distinct venue.timezone) as timezone from ticket_exchange.event left outer join ticket_exchange.event_tags on event.id = event_tags.event_id left outer join ticket_exchange.listing_group on event.id = listing_group.event_id left outer join ticket_exchange.tags on event_tags.tag_id = tags.id left outer join ticket_exchange.performer on event.performer_id = performer.id left outer join ticket_exchange.venue on event.venue_id = venue.id left outer join ticket_exchange.genre on event.genre_id = genre.id left outer join ticket_exchange.listing_tags on listing_group.id = listing_tags.listing_group_id where 1=1 ` + finalWhereStr + ` group by event.id ${paginationCondition};`, { type: QueryTypes.SELECT });
    }
}

/**
 * AddListing Page - To Get All Tags
 */
async function allTags(_, { input: { userId } }) {
    let whereCondition = { active: 1 }
    var Userdetails;
    if (userId) {
        Userdetails = await User.findOne({ attributes: ['id', 'user_role'], where: { id: userId } });

        if (Userdetails && Userdetails.user_role == 1) {
            whereCondition = { active: 1 }
        }
        else if (Userdetails && (Userdetails.user_role == 2 || Userdetails.user_role == 4)) {
            let where_users_id = [parseInt(userId)]
            const Userlistdetailsoutput = await Ticketingsystemaccounts.findAll({
                attributes: ['user_id'],
                where: { parent_id: userId }
            });
            if (Userlistdetailsoutput) {
                for (let userListCount = 0; userListCount < Userlistdetailsoutput.length; userListCount++) {
                    where_users_id.push(Userlistdetailsoutput[userListCount].user_id);
                }
            }
            whereCondition = { active: 1, user_id: { [Op.or]: { [Op.in]: where_users_id, [Op.is]: null } } }
        }
        else { /*if (userId) */
            whereCondition = { active: 1, user_id: { [Op.or]: { [Op.in]: [userId], [Op.is]: null } } }
        }
    }
    const tagDtls = await Tags.findAll({
        attributes: ['id', 'tag_name', 'created_at', 'updated_at'],
        where: whereCondition,
        order: [
            ['tag_name', 'ASC'],
        ]
    });
    if (tagDtls) {
        return JSON.parse(JSON.stringify(tagDtls));
    } else {
        return "No data";
    }
}

/**
 * AddListing Page - To Get All Ticketing Systems
 */
async function allTicketingSystems() {
    const ticketingsystemsDtls = await Ticketingsystems.findAll({
        attributes: ['id', 'ticketing_system_name', 'created_at', 'updated_at'],
        where: { active: 1 },
        order: [['ticketing_system_name', 'ASC']]
    });
    if (ticketingsystemsDtls) {
        return JSON.parse(JSON.stringify(ticketingsystemsDtls));
    } else {
        return "No data";
    }
}

/**
 * AddListing Page - To Get All Disclosures
 */
async function allDisclosures(_, { input: { userId } }) {
    let whereCondition = { active: 1 };
    var Userdetails;
    if (userId) {
        Userdetails = await User.findOne({ attributes: ['id', 'user_role'], where: { id: userId } });

        if (Userdetails && Userdetails.user_role == 1) {
            whereCondition = { active: 1 }
        }
        else if (Userdetails && (Userdetails.user_role == 2 || Userdetails.user_role == 4)) {
            let where_users_id = [parseInt(userId)]
            const Userlistdetailsoutput = await Ticketingsystemaccounts.findAll({
                attributes: ['user_id'],
                where: { parent_id: userId }
            });
            if (Userlistdetailsoutput) {
                for (let userListCount = 0; userListCount < Userlistdetailsoutput.length; userListCount++) {
                    where_users_id.push(Userlistdetailsoutput[userListCount].user_id);
                }
            }
            whereCondition = { active: 1, user_id: { [Op.or]: { [Op.in]: where_users_id, [Op.is]: null } } }
        }
        else { /*if (userId) */
            whereCondition = { active: 1, user_id: { [Op.or]: { [Op.in]: [userId], [Op.is]: null } } }
        }
    }
    const disclosuresDtls = await Disclosures.findAll({
        attributes: ['id', 'disclosure_name', 'created_at', 'updated_at'],
        where: whereCondition,
        order: [
            ['disclosure_name', 'ASC'],
        ]
    });
    if (disclosuresDtls) {
        return JSON.parse(JSON.stringify(disclosuresDtls));
    } else {
        return "No data";
    }
}

/**
 * AddListing Page - To Get All Attributes
 */
async function allAttributes(_, { input: { userId } }) {
    let whereCondition = { active: 1 }
    var Userdetails;
    if (userId) {
        Userdetails = await User.findOne({ attributes: ['id', 'user_role'], where: { id: userId } });

        if (Userdetails && Userdetails.user_role == 1) {
            whereCondition = { active: 1 }
        }
        else if (Userdetails && (Userdetails.user_role == 2 || Userdetails.user_role == 4)) {
            let where_users_id = [parseInt(userId)]
            const Userlistdetailsoutput = await Ticketingsystemaccounts.findAll({
                attributes: ['user_id'],
                where: { parent_id: userId }
            });
            if (Userlistdetailsoutput) {
                for (let userDetailCount = 0; userDetailCount < Userlistdetailsoutput.length; userDetailCount++) {
                    where_users_id.push(Userlistdetailsoutput[userDetailCount].user_id);
                }
            }
            whereCondition = { active: 1, user_id: { [Op.or]: { [Op.in]: where_users_id, [Op.is]: null } } }
        }
        else { /*if (userId) */
            whereCondition = { active: 1, user_id: { [Op.or]: { [Op.in]: [userId], [Op.is]: null } } }
        }
    }
    const attributesDtls = await Attributes.findAll({
        attributes: ['id', 'attribute_name', 'created_at', 'updated_at'],
        where: whereCondition,
        order: [
            ['attribute_name', 'ASC'],
        ]
    });
    if (attributesDtls) {
        return JSON.parse(JSON.stringify(attributesDtls));
    } else {
        return "No data";
    }
}

/**
         * Page : MyTickets 
         * Function For : Fetching selected user name or email for view listing
         * Ticket No : TIC 245
         */
async function selectedTicketingSystemAccounts(_, { input: { id } }) {//ticket_system_account    

    var sellersData = await ListingGroup.findOne({ attributes: ['id', 'ticket_system_account', 'ticket_system_id'], where: { id: id }, raw: true });

    if (sellersData && sellersData.ticket_system_account) {

        const ticketingsystemaccountDetails = await Ticketingsystemaccounts.findAll({
            attributes: ['id', ['account_name', 'email']],
            where: { id: sellersData.ticket_system_account }//ticket_system_id
        });

        return JSON.parse(JSON.stringify(ticketingsystemaccountDetails));
    } else { return; }
}


/**
 * AddListing Page - To Get All TicketingSystemAccounts
 */
async function allTicketingSystemAccounts(_, { input: { id, userId, isOperationFilter } }) {

    let listingGroupUserIds = '';
    var whereCondition = {};
    if (!isOperationFilter) {
        whereCondition.ticketing_system_id = id;
    }
    if (userId) {
        let listingGroupUserIdsResponse = await getUserIdsForListingGroupAccess(userId);
        if (listingGroupUserIdsResponse && listingGroupUserIdsResponse.length > 0) {
            whereCondition.parent_id = listingGroupUserIdsResponse;
        }
    }


    const ticketingsystemaccountDetails = await Ticketingsystemaccounts.findAll({
        attributes: ['id', ['account_name', 'email']],
        where: whereCondition,
        order: [
            ['account_name', 'ASC'],
        ]
    });

    if (ticketingsystemaccountDetails) {
        return JSON.parse(JSON.stringify(ticketingsystemaccountDetails));
    }
    else {
        return "No data";
    }
}


/**
 * All Channels Page - To Get All Channels
 */
async function allChannels() {
    const channelDtls = await Channels.findAll({
        attributes: ['id', 'channel_name', 'created_at', 'updated_at'],
        where: { active: true }
    });
    if (channelDtls) {
        return JSON.parse(JSON.stringify(channelDtls));
    } else {
        return "No data";
    }
}

/**
 * All Seat Type Page - To Get All Seat Type
 */
async function allSeattypes() {
    const seattypesDtls = await Seattype.findAll({
        attributes: ['id', 'name', 'created_at', 'updated_at']
    });
    if (seattypesDtls) {
        return JSON.parse(JSON.stringify(seattypesDtls));
    } else {
        return "No data";
    }
}

async function getListingsDetailsbyEventId(_, { input: { event_id } }) {
    const listingDtls = await Listings.findAll({
        attributes: ['id', 'event_id', 'ticket_type_id', 'in_hand', 'splits', 'seat_type_id', 'quantity', 'ticket_system_id', 'ticket_system_account', 'hide_seat_numbers', 'section', 'row', 'seat_start', 'seat_end', 'price', 'face_value', 'cost', 'unit_cost', 'attribute_id', 'PO_ID', 'zone_seating', 'external_notes', 'internal_notes'],
        where: {
            'event_id': event_id
        },
        order: [
            ['id', 'DESC']
        ],
        limit: 1
    });

    return JSON.parse(JSON.stringify(listingDtls));
}

async function fetchChannelMarkups(_, { input: { listingGroupId } }) {
    const markup_list = await ListingChannelMarkups.findAll({
        attributes: ["id", "channel_markup_type_id", "markup_amount", "markup_active", "channel_id"],
        where: { listingGroupId: listingGroupId },
    });

    if (markup_list) {
        let listingData = { "values": JSON.parse(JSON.stringify(markup_list)) };
        return listingData;
    } else {
        return "No data";
    }
}

/**
 * Fetch the event listing details
 * @param {*} _ 
 * @param {*} param1 
 * @returns List details in JSON string
 */
async function fetchListingDetails(_, { input: { listingGroupId } }) {
    const listing = await db.sequelize.query(`SELECT
        "listing_group"."id",
        "listing_group"."event_id",
        "listing_group"."ticket_type_id",
        "listing_group"."seat_type_id",
        "listing_group"."splits_id",
        "listing_group"."ticket_system_id",
        "listing_group"."hide_seat_numbers",
        "listing_group"."ticket_system_account",
        "listing_group"."quantity",
        "listing_group"."row",
        "listing_group"."seat_start",
        "listing_group"."seat_end",
        "listing_group"."face_value",
        "listing_group"."unit_cost",
        "listing_group"."group_cost",
        "listing_group"."internal_notes",
        "listing_group"."external_notes",
        "listing_group"."section",
        "listing_group"."PO_ID",
        to_char(
            listing_group.created_at :: timestamp,
            'YYYY-MM-DD HH:MM'
        ) AS "days_old",
        to_char(listing_group.in_hand :: timestamp, 'YYYY-MM-DD') AS "in_hand",
        array_agg(DISTINCT("listing_tag"."tag_id")) AS "tagids",
        array_agg(DISTINCT("listings"."price")) AS "price",
        array_agg(DISTINCT("listings"."sold")) AS "sold",
        array_agg(DISTINCT("listing_attributes"."attribute_id")) AS "attributeids",
        array_agg(DISTINCT("listing_disclosures"."disclosure_id")) AS "disclosureids",
        array_agg(DISTINCT("listing_channel_markups"."channel_id")) AS "channel_ids",
        array_agg(DISTINCT("listing_channel_markups"."id")) AS "chm_ids",
        array_agg(DISTINCT("event"."name")) AS "event_name",
        array_agg(DISTINCT("event->venue"."name")) AS "venue_name",
        array_agg(DISTINCT("event->venue"."city")) AS "city",
        array_agg(DISTINCT("event->venue"."state")) AS "state",
        array_agg(DISTINCT("event->venue"."country")) AS "country",
        array_agg(DISTINCT("event->venue"."timezone")) AS "timezone",
        array_agg (
            DISTINCT TO_CHAR(date :: timestamp, 'YYYY-MM-DD HH24:MI')
        ) AS "eventdate"
    FROM
        "ticket_exchange"."listing_group" AS "listing_group"
        LEFT OUTER JOIN "ticket_exchange"."listing_tags" AS "listing_tag" ON "listing_group"."id" = "listing_tag"."listing_group_id"
        LEFT OUTER JOIN "ticket_exchange"."listing_attributes" AS "listing_attributes" ON "listing_group"."id" = "listing_attributes"."listing_group_id"
        LEFT OUTER JOIN "ticket_exchange"."listing_disclosures" AS "listing_disclosures" ON "listing_group"."id" = "listing_disclosures"."listing_group_id"
        LEFT OUTER JOIN "ticket_exchange"."listing_channel_markups" AS "listing_channel_markups" ON "listing_group"."id" = "listing_channel_markups"."listing_group_id"
        LEFT OUTER JOIN "ticket_exchange"."listing" AS "listings" ON "listing_group"."id" = "listings"."listing_group_id"
        LEFT OUTER JOIN "ticket_exchange"."event" AS "event" ON "listing_group"."event_id" = "event"."id"
        LEFT OUTER JOIN "ticket_exchange"."venue" AS "event->venue" ON "event"."venue_id" = "event->venue"."id"
    WHERE
        "listing_group"."id" = `+ listingGroupId + `
        AND "listing_group"."is_removed" = false
    GROUP BY
        "listing_group"."id";`, { type: QueryTypes.SELECT });

    if (listing) {
        return JSON.parse(JSON.stringify(listing));
    } else {
        return "No data";
    }
}


async function getAllSplits() {
    const splitsDtls = await Splits.findAll({
        attributes: ['id', 'name', 'created_at', 'updated_at']
    });
    if (splitsDtls) {
        return JSON.parse(JSON.stringify(splitsDtls));
    } else {
        return "No data";
    }
}

/**
 * Page : Listings
 * Function For :  Change listing group status
 * Ticket No : TIC-250
 */
async function updateListingGroupStatus(_, { input: { listing_group_id } }) {

    var activeListingStatus = await getListingStatusbyName(listingStatusList.active);
    var inactiveListingStatus = await getListingStatusbyName(listingStatusList.inactive);
    var soldListingStatus = await getListingStatusbyName(listingStatusList.sold);
    var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);

    var data = await ListingGroup.findOne({
        attributes: ['id', 'listing_status_id'],
        where: {
            id: listing_group_id
        }
    });

    var currentListingStatus = data.dataValues.listing_status_id === inactiveListingStatus.id ? activeListingStatus.id : inactiveListingStatus.id;

    var currentEventStatus = data.dataValues.listing_status_id === inactiveListingStatus.id ? false : true;

    await ListingGroup.update({ listing_status_id: currentListingStatus },
        {
            where: {
                id: { [Op.in]: listing_group_id },
                listing_status_id: { [Op.notIn]: [soldListingStatus.id, qcListingStatus.id] }
            }
        },
        { returning: ['id'] }
    );
    await Listings.update({ inactive: currentEventStatus, listing_status_id: currentListingStatus },
        {
            where: {
                listing_group_id: listing_group_id, sold: false
            }
        }, { returning: ['id'] });
    return { status_data: true }
}

/**
 * Page : MyTickets 
 * Function For : Splits & Purchase Ticket
 * Ticket No : TIC 99
 */
async function getListingGroupBasedOnSplitLogics(_, { input: { eventId, ticketsRequired } }) {
    const splitDtls = await db.sequelize.query(`select 
        listing_group.id as listinggroupid, array_remove(array_agg(case when listing.sold is false then listing.id end),null) as listingids, 
        listing_group.splits_id, array_agg(distinct splits.name) as splitname, 
        (listing_group.seat_end :: integer - listing_group.seat_start :: integer + 1) as totalseats, 
        (listing_group.seat_end :: integer - listing_group.seat_start :: integer + 1) - 
        sum(case when listing.sold then 1 else 0 end) as availabletickets
    from 
        ticket_exchange.event
        inner join ticket_exchange.listing_group on listing_group.event_id = event.id
        inner join ticket_exchange.listing on listing_group.id = listing.listing_group_id
        inner join ticket_exchange.splits on listing_group.splits_id = splits.id
    where event.id = `+ eventId + `
    group by listing_group.id;`, { type: QueryTypes.SELECT });

    let isError = false,
        msg = '',
        listingGroupIds = [],
        splitName = [];
    if (splitDtls) {
        let ticketFound = false;
        for (var splitCount = 0; splitCount < splitDtls.length; splitCount++) {
            if (splitDtls[splitCount].listinggroupid > 0) {
                if (ticketsRequired > 0 && splitDtls[splitCount].availabletickets > 0 && splitDtls[splitCount].availabletickets >= ticketsRequired) {
                    if (splitDtls[splitCount].splitname == splitTypes.neverLeaveOne) {
                        if ((splitDtls[splitCount].availabletickets - ticketsRequired) != 1) {
                            isError = false;
                            msg = 'Tickets Can Be Purchased In This Group.';
                            listingGroupIds.push(splitDtls[splitCount].listinggroupid);
                            splitName = splitDtls[splitCount].splitname;

                            ticketFound = true;
                        }
                    }
                    else if (splitDtls[splitCount].splitname == splitTypes.any) {
                        isError = false;
                        msg = 'Tickets Can Be Purchased In This Group.';
                        listingGroupIds.push(splitDtls[splitCount].listinggroupid);
                        splitName = splitDtls[splitCount].splitname;

                        ticketFound = true;
                    }
                    else if (splitDtls[splitCount].splitname == splitTypes.noSplits) {
                        if (splitDtls[splitCount].availabletickets == ticketsRequired) {
                            isError = false;
                            msg = 'Tickets Can Be Purchased In This Group.';
                            listingGroupIds.push(splitDtls[splitCount].listinggroupid);
                            splitName = splitDtls[splitCount].splitname;

                            ticketFound = true;
                        }
                    }
                }
            }
        }

        if (!ticketFound) {
            isError = true;
            msg = "No Tickets Found. Try Some other quantity.";
        }
    }
    else {
        isError = true;
        msg = "No Listing Groups Found.";
    }

    var finalResJSON = {
        "isError": isError,
        "msg": msg,
        "listingGroupIds": listingGroupIds,
        "splitName": splitName
    };
    return finalResJSON;
}

/**
 * Page : MyTickets 
 * Function For : Splits & Purchase Ticket
 * Ticket No : TIC 99
 */
async function getEventDetailsBasedOnEventId(_, { input: { eventIds } }) {
    Events.hasMany(ListingGroup);
    ListingGroup.belongsTo(Events);

    Events.belongsTo(Performer, { foreignKey: { name: 'performer_id' } });
    Performer.belongsTo(Events);

    Events.belongsTo(Venue, { foreignKey: { name: 'venueId' } });
    Venue.belongsTo(Events);

    Events.belongsTo(Genre, { foreignKey: { name: 'genre_id' } });
    Genre.belongsTo(Events);

    ListingGroup.hasOne(ListingTags, { foreignKey: { name: 'listing_group_id' } });
    ListingTags.belongsTo(Tags, { foreignKey: { name: 'tag_id' } });

    Events.hasOne(EventTags, { foreignKey: { name: 'event_id' } });
    EventTags.belongsTo(Tags, { foreignKey: { name: 'tag_id' } });

    eventIds = eventIds.split(",");

    const eventDetails = await Events.findAll({
        attributes: [
            ['id', 'event_id'],
            ['name', 'eventname'],
            ['date', 'eventdate'],
            // [sequelize.literal("TO_CHAR(date :: timestamp, 'Day - Mon dd, yyyy HH:MM')"), 'eventdate'],
            [sequelize.fn('array_remove', sequelize.fn('array_agg', sequelize.fn('DISTINCT', sequelize.col('venue.name'))), null), 'venuename'],
            [sequelize.fn('array_remove', sequelize.fn('array_agg', sequelize.fn('DISTINCT', sequelize.col('venue.timezone'))), null), 'timezone'],
            [sequelize.fn('array_remove', sequelize.fn('array_agg', sequelize.fn('DISTINCT', sequelize.col('venue.city'))), null), 'city'],
            [sequelize.fn('array_remove', sequelize.fn('array_agg', sequelize.fn('DISTINCT', sequelize.col('venue.state'))), null), 'state'],
            [sequelize.fn('array_remove', sequelize.fn('array_agg', sequelize.fn('DISTINCT', sequelize.col('venue.country'))), null), 'country'],
            [sequelize.fn('array_remove', sequelize.fn('array_agg', sequelize.fn('DISTINCT', sequelize.col('venue.timezone'))), null), 'timezone']
        ],
        include: [
            {
                model: EventTags,
                required: false,
                attributes: [],
                where: {},
                include: [
                    {
                        model: Tags,
                        required: false,
                        attributes: [],
                        where: {},
                    }
                ]
            },
            {
                model: Performer,
                required: false,
                attributes: [],
                where: {},
            },
            {
                model: Venue,
                required: false,
                attributes: [],
                where: {},
            },
            {
                model: Genre,
                required: false,
                attributes: [],
                where: {},
            }],
        where: { id: { [Op.in]: eventIds } },
        group: ['event.id']
    });

    if (eventDetails) {
        return JSON.parse(JSON.stringify(eventDetails));
    } else {
        return "No Data";
    }
}


/**
 * Page : View Listing 
 * Function For : Getting Attachment Details
 * Ticket No : TIC 24
 */
async function fetchAttachmentDetails(_, { input: { listingGroupId } }) {
    const attach_list = await Listingattachments.findAll({
        attributes: ["id", "attachment_name", "attachment_url"],
        where: { listing_group_id: listingGroupId, active: 1 },
    });

    if (attach_list) {
        return attach_list;
    } else {
        return "No data";
    }
}

/**
 * fetching splitid of list group id
 */
async function fetchListingIconsId(_, { input: { listingGroupId } }) {
    ListingGroup.hasMany(ListingDisclosures, { foreignKey: { name: 'listing_group_id' } });
    const listingGroupDtls = await ListingGroup.findAll({
        attributes: [
            "splits_id", "internal_notes", "external_notes",
            ["to_char(listing_group.in_hand :: timestamp, 'YYYY-MM-DD')", "in_hand"],
            [sequelize.fn('array_agg', sequelize.fn('DISTINCT', sequelize.col('listing_disclosures.disclosure_id'))), 'disclosure_id'],
        ],
        include: [
            {
                model: ListingDisclosures,
                required: false,
                attributes: [],
                where: {},
            }
        ],
        where: { id: listingGroupId, is_removed: false },
        group: ['listing_group.id']
    });

    if (listingGroupDtls) {
        return JSON.parse(JSON.stringify(listingGroupDtls));
    } else {
        return "No data";
    }
}

/**
 * Page : Add Listing 
 * Function For : Save Add Listing Group Data
 * Ticket No : TIC 28
 */
async function addListing(_, { input:
    {
        seller_id, buyer_id, eventIds, ticket_type_id, in_hand, splitId, seat_type_id, quantity, quantity_ga, ticket_system_id, ticket_system_account, hide_seat_number, seatInformationsJSON, channelmarkupsJSON
    }
}) {

    //Ticket Account Details Insert Start
    const TicketingsystemaccountsExit = await Ticketingsystemaccounts.findOne({
        attributes: ['id'], where: {
            ticketing_system_id: parseInt(ticket_system_id),
            user_id: parseInt(buyer_id),
            parent_id: parseInt(seller_id)
        }
    });
    if (!TicketingsystemaccountsExit) {
        const Userdetails = await User.findOne({ attributes: ['email'], where: { id: buyer_id, active: 1 } });
        if (Userdetails) {
            var TicketAccounttData = {
                ticketing_system_id: parseInt(ticket_system_id),
                user_id: parseInt(buyer_id),
                parent_id: parseInt(seller_id),
                account_name: Userdetails.email,
                created_at: Date.now(),
                updated_at: Date.now()
            }
            await Ticketingsystemaccounts.create(TicketAccounttData, { returning: ['id'] });
        }
    }
    //Ticket Account Details Insert End

    //Pdf Status Start

    let pdf;
    if (ticket_type_id) {
        const TicketTypedetails = await TicketType.findOne({ attributes: ['id', 'name'], where: { id: ticket_type_id } });
        if (TicketTypedetails.name === ticketTypes.eTicket) { pdf = "Yes"; } else { pdf = "No"; }
    } else { pdf = "No"; }

    var seatTypedetails;
    if (seat_type_id) {
        seatTypedetails = await Seattype.findOne({ attributes: ['id', 'name'], where: { id: seat_type_id }, raw: true });
    }
    //Pdf Status End

    var outputResData = [];
    var outputAttachmentData = [];
    let eventIdsNew = eventIds.split(",");
    if (eventIdsNew.length > 0) {
        for (var eventIdCount = 0; eventIdCount < eventIdsNew.length; eventIdCount++)//Multiple Events
        {
            let eventId = eventIdsNew[eventIdCount];
            for (var seatInfoCount = 0; seatInfoCount < seatInformationsJSON.length; seatInfoCount++) {
                let seatStart = seatInformationsJSON[seatInfoCount].seat_start,
                    seatEnd = seatInformationsJSON[seatInfoCount].seat_end,
                    section = seatInformationsJSON[seatInfoCount].section,
                    row = seatInformationsJSON[seatInfoCount].row,
                    cost = seatInformationsJSON[seatInfoCount].cost,
                    quantity = (seatEnd - seatStart) + 1,
                    price = seatInformationsJSON[seatInfoCount].price;

                if (seatTypedetails.name === seatTypes.generalAdmission) {
                    seatStart = 0,
                        seatEnd = 0,
                        section = "NA",
                        row = "NA",
                        quantity = parseInt(quantity_ga)
                }

                var ListingGroupInputData = {
                    //Common Data
                    seller_id: seller_id,
                    buyer_id: parseInt(buyer_id),
                    eventId: eventId,
                    ticket_type_id: ticket_type_id,
                    in_hand: in_hand,
                    splits_id: splitId,
                    seat_type_id: seat_type_id,
                    quantity: quantity,//quantity,
                    ticket_system_id: ticket_system_id,
                    ticket_system_account: ticket_system_account,
                    hide_seat_numbers: hide_seat_number,

                    //JSON Data Starts Here
                    section: section,
                    row: row,
                    seat_start: seatStart,
                    seat_end: seatEnd,
                    group_price: price, //TODO
                    face_value: seatInformationsJSON[seatInfoCount].face_value,
                    group_cost: cost,
                    unit_cost: seatInformationsJSON[seatInfoCount].unit_cost,
                    PO_ID: seatInformationsJSON[seatInfoCount].po_id,
                    zone_seating: seatInformationsJSON[seatInfoCount].zone_seating,
                    internal_notes: seatInformationsJSON[seatInfoCount].internal_notes,
                    external_notes: seatInformationsJSON[seatInfoCount].external_notes,
                    //JSON Data Ends Here

                    created_by: seller_id
                };

                const listing_data = await ListingGroup.create(ListingGroupInputData, { returning: ['id'] });
                if (listing_data) {
                    let listingGroupId = listing_data.id;
                    if (seatInformationsJSON[seatInfoCount].tags) {
                        var tagsJSON = JSON.parse(seatInformationsJSON[seatInfoCount].tags);
                        for (var tagDataCount = 0; tagDataCount < tagsJSON.length; tagDataCount++) {
                            var tagsInputData = {
                                listing_group_id: listingGroupId,
                                tag_id: tagsJSON[tagDataCount].value
                            };
                            await ListingTags.create(tagsInputData, { returning: ['id'] });
                        }
                    }
                    if (seatInformationsJSON[seatInfoCount].disclosure) {
                        var disclosuresJSON = JSON.parse(seatInformationsJSON[seatInfoCount].disclosure);
                        for (var disclosuresCount = 0; disclosuresCount < disclosuresJSON.length; disclosuresCount++) {
                            var disclosuresInputData = {
                                listingGroupId: listingGroupId,
                                disclosure_id: disclosuresJSON[disclosuresCount].value
                            };
                            await ListingDisclosures.create(disclosuresInputData, { returning: ['id'] });
                        }
                    }
                    if (seatInformationsJSON[seatInfoCount].attribute) {
                        var attributesJSON = JSON.parse(seatInformationsJSON[seatInfoCount].attribute);
                        for (var attributeDataCount = 0; attributeDataCount < attributesJSON.length; attributeDataCount++) {
                            var attributesInputData = {
                                listingGroupId: listingGroupId,
                                attribute_id: attributesJSON[attributeDataCount].value
                            };

                            await ListingAttributes.create(attributesInputData, { returning: ['id'] });
                        }
                    }
                    /**
                    * Input : Attachment Text , Attachment File
                    * Output : Attachment Insert response
                    * Function For : Attachment data insert to listing attachment table
                    * Ticket No : TIC-243
                    */
                    //Attachment
                    if (seatInformationsJSON[seatInfoCount].attachments) {
                        var attachmentsJSON = seatInformationsJSON[seatInfoCount].attachments;
                        for (var attachmentCount = 0; attachmentCount < attachmentsJSON.length; attachmentCount++) {

                            var attachment_url_data = 'nil';
                            if (attachmentsJSON[attachmentCount].attachment_file) {
                                attachment_url_data = s3Path.basePath + process.env.AWS_BUCKETNAME + '/' + folderNames.listingsFolder + '/' + eventId + '/' + listingGroupId + '/' + attachmentsJSON[attachmentCount].attachment_file;
                            }
                            var attachmentdata = {
                                listing_group_id: listingGroupId.toString(),
                                attachment_name: attachmentsJSON[attachmentCount].attachment_text,
                                attachment_url: attachment_url_data,
                            };
                            if (attachmentdata.attachment_url) {
                                const result = await Listingattachments.create(attachmentdata, { returning: ['id'] });
                                var data = { "eventId": eventId, "result": result }
                                outputAttachmentData.push(data);
                            }
                        }
                    }
                    //Attachment

                    //Channel Management Start
                    if (channelmarkupsJSON) {
                        for (let channelMarkupCount = 0; channelMarkupCount < channelmarkupsJSON.length; channelMarkupCount++) {
                            var channelmarkupsJSONArray = {
                                listingGroupId: listingGroupId,
                                user_id: seller_id,
                                channel_id: channelmarkupsJSON[channelMarkupCount].channel_id,
                                channel_markup_type_id: channelmarkupsJSON[channelMarkupCount].channel_markup_type_id,
                                markup_amount: channelmarkupsJSON[channelMarkupCount].markup_amount,
                                markup_active: channelmarkupsJSON[channelMarkupCount].switch === true ? 1 : 0
                            };
                            await ListingChannelMarkups.create(channelmarkupsJSONArray);
                        }
                    }
                    //Channel Management End

                    outputResData.push(listingGroupId);

                    //Insert Listing Details Starts Here
                    if (seatTypedetails.name === seatTypes.generalAdmission) {
                        let quantityGACount = 0;
                        for (quantityGACount = 0; quantityGACount < parseInt(quantity_ga); quantityGACount++) {
                            let listingInputData = {
                                listingGroupId: listing_data.id,
                                buyer_id: parseInt(buyer_id),
                                in_hand: in_hand,
                                section: section,
                                row: row,
                                seat_number: 0,
                                sold: false,
                                delivered: false,
                                pdf: pdf,
                                unit_cost: seatInformationsJSON[seatInfoCount].unit_cost,
                                price: seatInformationsJSON[seatInfoCount].price,
                                face_value: seatInformationsJSON[seatInfoCount].face_value,
                            };
                            await Listings.create(listingInputData);
                        }
                        quantityGACount = 0;
                    }
                    else {
                        if (seatStart != "" && seatEnd != "") {
                            let seatCount = 0;
                            for (seatCount = parseInt(seatStart); seatCount <= parseInt(seatEnd); seatCount++) {
                                let listingInputData = {
                                    listingGroupId: listing_data.id,
                                    buyer_id: parseInt(buyer_id),
                                    in_hand: in_hand,
                                    section: section,
                                    row: row,
                                    seat_number: seatCount,
                                    sold: false,
                                    delivered: false,
                                    pdf: pdf,
                                    unit_cost: seatInformationsJSON[seatInfoCount].unit_cost,
                                    price: seatInformationsJSON[seatInfoCount].price,
                                    face_value: seatInformationsJSON[seatInfoCount].face_value,
                                };
                                const listingGroupData = await Listings.create(listingInputData);
                            }
                        }
                    }

                    //Insert Listing Details Ends Here

                    //Price Log Starts - Add Listing Price

                    let listingGroupIds = [listing_data.id];

                    var listingIdDtls;
                    const listingDtls = await Listings.findAll({
                        attributes: ['id', 'listing_group_id', 'price'],
                        where: { listing_group_id: { [Op.in]: listingGroupIds } }
                    });
                    listingIdDtls = JSON.parse(JSON.stringify(listingDtls));

                    let loggedInUserId = seller_id;

                    for (var listingIdCount = 0; listingIdCount < listingIdDtls.length; listingIdCount++) {
                        var priceLogData = {
                            user_id: loggedInUserId,
                            event_id: eventId,
                            listing_group_id: listingIdDtls[listingIdCount].listing_group_id,
                            listing_id: listingIdDtls[listingIdCount].id,
                            price: 0,
                            new_price: seatInformationsJSON[seatInfoCount].price,
                            confirmed_price_change: true,
                            created_at: new Date(),
                            created_by: loggedInUserId
                        };
                        await PriceLog.create(priceLogData);
                    }

                    //Price Log Ends
                }
            }
        }
    }

    var msg = "Listing Created Successfully";
    if (outputResData.length == 1) {
        msg = "Listing Created Successfully. Listing ID is " + outputResData.toString()
    }
    else if (outputResData.length > 1 && outputResData.length != 0) {
        msg = "Listings Created Successfully. Listing IDs are " + outputResData.toString()
    }

    var finalResJSON = {
        "isError": false,
        "msg": msg,
        "listingGroupIds": outputResData,
        "attachmentData": outputAttachmentData
    };
    return finalResJSON;
}
/**
  * Input : Disclosure Value
  * Output : Data From Disclosure Table
  * Function For : Add Disclosure data insert to Disclosure table
  * Ticket No : TIC-113, TIC-124
  */
async function addDisclosure(_, { input: { values, userID } }) {
    const disclosures = await Disclosures.findOne({ where: { disclosure_name: values[0].disclosure_name.trim(), user_id: { [Op.or]: { [Op.in]: [userID], [Op.is]: null } } } })
    if (disclosures && disclosures.dataValues) {
        return { 'message': 'Disclosure Already Exists  !!!', 'data': '', status: 'error' };
    }
    else {
        var disclosureInputData = {
            disclosure_name: values[0].disclosure_name,
            user_id: userID
        };
        const disclosuresDtls = await Disclosures.create(disclosureInputData);
        var data = { 'message': 'Disclosures Created !!!', 'data': disclosuresDtls, status: 'ok' }
        return data;
    }
}


/**
  * Input : Attribute Value
  * Output : Data From Attribute Table
  * Function For : Add Attribute data insert to Attribute table
  * Ticket No : TIC-113
  */
async function addAttribute(_, { input: { values, userID } }) {
    const attributes = await Attributes.findOne({ where: { attribute_name: values[0].attribute_name.trim(), user_id: { [Op.or]: { [Op.in]: [userID], [Op.is]: null } } } })
    if (attributes && attributes.dataValues) {
        return { 'message': 'Attributes Already Exists  !!!', 'data': '', status: 'error' };
    }
    else {
        var attributeInputData = {
            attribute_name: values[0].attribute_name,
            user_id: userID
        };
        const attributesDtls = await Attributes.create(attributeInputData);
        var data = { 'message': 'Attributes Created !!!', 'data': attributesDtls, status: 'ok' }
        return data;
    }
}

/**
  * Input : Tag Value
  * Output : Data From Tag Table
  * Function For : Add Tag data insert to Tag table
  * Ticket No : TIC-113
  */
async function addTag(_, { input: { values, userID } }) {
    const tags = await Tags.findOne({ where: { tag_name: values[0].tag_name.trim(), user_id: { [Op.or]: { [Op.in]: [userID], [Op.is]: null } } } })
    if (tags && tags.dataValues) {
        return { 'message': 'Tags Already Exists  !!!', 'data': '', status: 'error' };
    }
    else {

        var tagInputData = {
            tag_name: values[0].tag_name,
            user_id: userID
        };
        const tagsDtls = await Tags.create(tagInputData);
        var data = { 'message': 'Tags Created !!!', 'data': tagsDtls, status: 'ok' }
        return data;
    }

}

/**
 * Page : View Listing 
 * Function For : Edit Listing
 * Ticket No : TIC 24
 */
async function editListing(_, { input:
    {
        listingGroupId, ticket_type_id, in_hand, splits, seat_type_id, quantity, quantity_ga, hide_seat_number, section, row, seat_start, seat_end, price, face_value, group_cost, unit_cost, po_id, internal_notes, external_notes, ticket_system_id, ticketing_system_accounts, disclosureids, attrbtids, selectedTags, channelData, seatInformationsJSON, eventId, userId, isPriceChanged
    }
}) {

    var seatTypedetails;
    if (seat_type_id) {
        seatTypedetails = await Seattype.findOne({ attributes: ['id', 'name'], where: { id: seat_type_id }, raw: true });
    }

    if (seatTypedetails.name === seatTypes.generalAdmission) {
        seat_start = 0,
            seat_end = 0,
            section = "NA",
            row = "NA",
            quantity = parseInt(quantity_ga)
    }

    var updateListData = {
        ticket_type_id: ticket_type_id,
        seat_type_id: seat_type_id,
        ticket_system_id: ticket_system_id,
        ticket_system_account: ticketing_system_accounts,
        hide_seat_numbers: hide_seat_number,
        splits_id: splits,
        section: section,
        in_hand: in_hand,
        row: row,
        quantity: quantity,
        seat_start: seat_start,
        seat_end: seat_end,
        group_price: price,
        group_cost: group_cost,
        unit_cost: unit_cost,
        po_id: po_id,
        face_value: face_value,
        internal_notes: internal_notes,
        external_notes: external_notes
    };

    let updates = await ListingGroup.update(
        updateListData,
        { where: { id: listingGroupId } }
    )

    for (var channelDataCount = 0; channelDataCount < channelData.length; channelDataCount++) {
        if (channelData[channelDataCount].rowId) {
            await ListingChannelMarkups.update(
                { markup_amount: channelData[channelDataCount].markup_amount, markup_active: channelData[channelDataCount].switch === true ? 1 : 0, channel_markup_type_id: channelData[channelDataCount].channel_markup_type_id },
                { where: { id: channelData[channelDataCount].rowId } }
            )
        }
        else {
            var channelmarkupsJSONArray = {
                listingGroupId: listingGroupId,
                user_id: userId,
                channel_id: channelData[channelDataCount].id,
                markup_amount: channelData[channelDataCount].markup_amount,
                markup_active: channelData[channelDataCount].switch === true ? 1 : 0,
                channel_markup_type_id: channelData[channelDataCount].channel_markup_type_id
            };
            await ListingChannelMarkups.create(channelmarkupsJSONArray);
        }
    }

    if (updates) {

        //Price Log Starts - Edit Listing Price
        if (isPriceChanged) {
            let listingGroupIds = [listingGroupId];

            var listingIdDtls;
            const listingDtls = await Listings.findAll({
                attributes: ['id', 'listing_group_id', 'price'],
                where: { listing_group_id: { [Op.in]: listingGroupIds } }
            });
            listingIdDtls = JSON.parse(JSON.stringify(listingDtls));

            let loggedInUserId = parseInt(userId);

            for (var listingIdCount = 0; listingIdCount < listingIdDtls.length; listingIdCount++) {
                var priceLogData = {
                    event_id: eventId,
                    listing_group_id: listingIdDtls[listingIdCount].listing_group_id,
                    listing_id: listingIdDtls[listingIdCount].id,
                    price: listingIdDtls[listingIdCount].price,
                    new_price: price,
                    confirmed_price_change: true,
                    created_at: new Date(),
                    created_by: loggedInUserId
                };
                await PriceLog.create(priceLogData);
            }
        }

        //Price Log Ends

        let listingDisclosuresData = await ListingDisclosures.findAll({ where: { listingGroupId: listingGroupId }, attributes: ['disclosure_id'], raw: true });
        let listingAttributesData = await ListingAttributes.findAll({ where: { listingGroupId: listingGroupId }, attributes: ['attribute_id'], raw: true });
        let listingTagsData = await ListingTags.findAll({ where: { listing_group_id: listingGroupId }, attributes: ['tag_id'], raw: true });
        let listingsData = await Listings.findAll({ where: { listing_group_id: listingGroupId }, attributes: ['id', 'seat_number', 'price', 'last_price'], raw: true });

        let listing_update = {
            face_value: face_value,
        };
        if (listingsData && listingsData.length > 0) {
            if (price != listingsData[0].price) {
                listing_update.price = price;
                listing_update.last_price_change = new Date();
                listing_update.last_price = listingsData[0].price ? listingsData[0].price : null;
            }
        }
        await Listings.update(
            listing_update,
            { where: { listing_group_id: listingGroupId } },
            { returning: ['id'] }
        )

        if (disclosureids.length) {
            //Disclosures Already Saved In Database
            let disclosuresDataInDb = [];
            listingDisclosuresData.map(disclosure => {
                disclosuresDataInDb.push(parseInt(disclosure.disclosure_id));
            })

            //Current Disclosures Selected
            let selectedDisclosures = [];
            disclosureids = JSON.parse(disclosureids);
            disclosureids.map(disclosure => {
                selectedDisclosures.push(parseInt(disclosure['value']));
            })

            //Disclosures To Insert New
            let insertDisclosureData = selectedDisclosures.filter(x => disclosuresDataInDb.indexOf(x) === -1);
            if (insertDisclosureData.length) {
                for (var disclosuresCount = 0; disclosuresCount < insertDisclosureData.length; disclosuresCount++) {
                    var disclosuresInputData = {
                        listingGroupId: listingGroupId,
                        disclosure_id: insertDisclosureData[disclosuresCount]
                    };
                    await ListingDisclosures.create(disclosuresInputData, { returning: ['id'] });
                }
            }

            //Disclosures to Delete
            let deleteDisclosureData = disclosuresDataInDb.filter(x => selectedDisclosures.indexOf(x) === -1);
            if (deleteDisclosureData.length) {
                await ListingDisclosures.destroy({ where: { listing_group_id: listingGroupId, disclosure_id: { [Op.in]: deleteDisclosureData } } });
            }
        }

        if (attrbtids.length) {
            //Attributes Already Saved In Database
            let attributesDataInDb = [];
            listingAttributesData.map(attribute => {
                attributesDataInDb.push(parseInt(attribute.attribute_id));
            })

            //Current Attributes Selected
            let selectedAttributes = [];
            attrbtids = JSON.parse(attrbtids);
            attrbtids.map(attribute => {
                selectedAttributes.push(parseInt(attribute['value']));
            })

            //Attributes To Insert New
            let insertAttributeData = selectedAttributes.filter(x => attributesDataInDb.indexOf(x) === -1);
            if (insertAttributeData.length) {
                for (var attributeDataCount = 0; attributeDataCount < insertAttributeData.length; attributeDataCount++) {
                    var attributesInputData = {
                        listingGroupId: listingGroupId,
                        attribute_id: insertAttributeData[attributeDataCount]
                    };
                    await ListingAttributes.create(attributesInputData, { returning: ['id'] });
                }
            }

            //Attributes to Delete
            let deleteAttributeData = attributesDataInDb.filter(x => selectedAttributes.indexOf(x) === -1);
            if (deleteAttributeData.length) {
                await ListingAttributes.destroy({ where: { listing_group_id: listingGroupId, attribute_id: { [Op.in]: deleteAttributeData } } });
            }
        }

        if (selectedTags.length) {
            //Tags Already Saved In Database
            let tagsDataInDb = [];
            listingTagsData.map(tags => {
                tagsDataInDb.push(parseInt(tags.tag_id));
            })

            //Current Tags Selected
            let tagsIds = [];
            selectedTags = JSON.parse(selectedTags);
            selectedTags.map(tags => {
                tagsIds.push(parseInt(tags['value']));
            })

            //Tags To Insert New
            let insertTagData = tagsIds.filter(x => tagsDataInDb.indexOf(x) === -1);
            if (insertTagData.length) {
                for (var tagDataCount = 0; tagDataCount < insertTagData.length; tagDataCount++) {
                    var tagsInputData = {
                        listing_group_id: listingGroupId,
                        tag_id: insertTagData[tagDataCount]
                    };
                    await ListingTags.create(tagsInputData, { returning: ['id'] });
                }
            }

            //Tags to Delete
            let deleteTagData = tagsDataInDb.filter(x => tagsIds.indexOf(x) === -1);
            if (deleteTagData.length) {
                await ListingTags.destroy({ where: { listing_group_id: listingGroupId, tag_id: { [Op.in]: deleteTagData } } });
            }
        }

        if (seat_start != "" && seat_end != "") {
            //Seat Numbers Already Saved In Database
            let seatNumbersDataInDb = [];
            listingsData.map(listings => {
                seatNumbersDataInDb.push(parseInt(listings.seat_number));
            })

            //Current Seat Numbers Selected
            let selectedSeatNumbers = [];
            for (var seatCount = seat_start; seatCount <= seat_end; seatCount++) {
                selectedSeatNumbers.push(parseInt(seatCount));
            }

            //Attributes To Insert New
            let insertListingsData = selectedSeatNumbers.filter(x => seatNumbersDataInDb.indexOf(x) === -1);
            if (insertListingsData.length) {
                for (var listingDataCount = 0; listingDataCount < insertListingsData.length; listingDataCount++) {
                    var listingsInputData = {
                        listingGroupId: listingGroupId,
                        in_hand: in_hand,
                        face_value: face_value,
                        seat_number: insertListingsData[listingDataCount],
                        unit_cost: unit_cost,
                        price: price
                    };
                    await Listings.create(listingsInputData, { returning: ['id'] });
                }
            }

            //Attributes to Delete
            let deleteListingsData = seatNumbersDataInDb.filter(x => selectedSeatNumbers.indexOf(x) === -1);
            if (deleteListingsData.length) {
                for (var deleteListingCount = 0; deleteListingCount < deleteListingsData.length; deleteListingCount++) {
                    let seatNumber = '' + deleteListingsData[deleteListingCount];
                    await Listings.destroy({ where: { listing_group_id: parseInt(listingGroupId), seat_number: seatNumber } });
                }
            }
        }

        /**
           * Input : Attachment Text , Attachment File
           * Output : Attachment Insert response
           * Function For : Attachment data insert to listing attachment table
           * Ticket No : TIC-243
           */
        //Attachment

        var outputAttachmentData = [];
        if (seatInformationsJSON[0].attachments) {
            var attachmentsJSON = seatInformationsJSON[0].attachments;

            for (var attachmentCount = 0; attachmentCount < attachmentsJSON.length; attachmentCount++) {
                var attachmentdata = {
                    listing_group_id: listingGroupId.toString(),
                    attachment_name: attachmentsJSON[attachmentCount].attachment_text
                };

                let result = {
                    listing_group_id: listingGroupId
                };
                if (attachmentsJSON[attachmentCount].attachmentId) {

                    if (attachmentsJSON[attachmentCount].attachment_file) {
                        attachmentdata.attachment_url = s3Path.basePath + process.env.AWS_BUCKETNAME + '/' + folderNames.listingsFolder + '/' + eventId + '/' + listingGroupId + '/' + attachmentsJSON[attachmentCount].attachment_file;
                    }

                    await Listingattachments.update(
                        attachmentdata,
                        {
                            where: { id: attachmentsJSON[attachmentCount].attachmentId }
                        }
                    );
                }
                else {
                    if (attachmentsJSON[attachmentCount].attachment_file) {
                        attachmentdata.attachment_url = s3Path.basePath + process.env.AWS_BUCKETNAME + '/' + folderNames.listingsFolder + '/' + eventId + '/' + listingGroupId + '/' + attachmentsJSON[attachmentCount].attachment_file;
                    }
                    else {
                        attachmentdata.attachment_url = 'nil';
                    }
                    await Listingattachments.create(attachmentdata, { returning: ['id'] });
                }

                var data = { "eventId": eventId, "result": result }
                outputAttachmentData.push(data);
            }
        }
        //Attachment

        var finalResJSON = {
            "isError": false,
            "msg": "Listing is Updated Successfully.",
            "attachmentData": outputAttachmentData
        };
        return finalResJSON;
    }
    else {
        return { response: false }
    }
}


/**
 * Page : MyTickets 
 * Function For : Update Price
 * Ticket No : TIC 99
 */
async function updateListingPriceById(_, { input: { listingGroupId, price, lastPrice, eventId, userId } }) {
    const result = await Listings.update(
        {
            price: price,
            last_price: lastPrice,
            last_price_change: new Date()
        },
        {
            where: { listing_group_id: listingGroupId, sold: false }
        }
    )

    //Price Log Starts - Listing Level Price

    let listingGroupIds = [listingGroupId];

    var listingIdDtls;
    const listingDtls = await Listings.findAll({
        attributes: ['id', 'listing_group_id', 'price'],
        where: { listing_group_id: { [Op.in]: listingGroupIds } }
    });
    listingIdDtls = JSON.parse(JSON.stringify(listingDtls));

    let loggedInUserId = userId;

    for (var listingIdCount = 0; listingIdCount < listingIdDtls.length; listingIdCount++) {
        var priceLogData = {
            user_id: loggedInUserId,
            event_id: eventId,
            listing_group_id: listingIdDtls[listingIdCount].listing_group_id,
            listing_id: listingIdDtls[listingIdCount].id,
            price: lastPrice,
            new_price: price,
            confirmed_price_change: true,
            created_at: new Date(),
            created_by: loggedInUserId
        };
        await PriceLog.create(priceLogData, { returning: ['id'] });
    }

    //Price Log Ends

    if (result) {
        return { response: true }
    }
    else return { response: false }
}


/**
 * Page : MyTickets 
 * Function For : Update Price
 * Ticket No : TIC 99
 */
async function updateMultiListingPriceById(_, { input: { listIds, eventId, price, userId, updateSymbol } }) {
    var result = '';
    var rowCount = 0;
    var listingIdDtls;
    const listingDtls = await Listings.findAll({
        attributes: ['id', 'listing_group_id', 'price'],
        where: { listing_group_id: { [Op.in]: listIds }, sold: false }
    });
    listingIdDtls = JSON.parse(JSON.stringify(listingDtls));

    let loggedInUserId = userId;

    for (var listingIdCount = 0; listingIdCount < listingIdDtls.length; listingIdCount++) {
        rowCount = listingIdCount;
        let newCalculatedPrice = parseFloat(listingIdDtls[listingIdCount].price);
        if (updateSymbol) {
            if (updateSymbol == '%') {
                newCalculatedPrice = listingIdDtls[listingIdCount].price + (listingIdDtls[listingIdCount].price * parseFloat(price) / 100);
                if (newCalculatedPrice < 0) {
                    newCalculatedPrice = 0
                }
            }
            else if (updateSymbol == '$') {
                newCalculatedPrice = parseFloat(listingIdDtls[listingIdCount].price) + parseFloat(price);
                if (newCalculatedPrice < 0) {
                    newCalculatedPrice = 0
                }
            }
        }
        else {
            newCalculatedPrice = price;
        }
        result = await Listings.update(
            {
                price: newCalculatedPrice.toFixed(2),
                last_price: listingIdDtls[listingIdCount].price.toFixed(2),
                last_price_change: new Date()
            },
            {
                where: { id: listingIdDtls[listingIdCount].id, sold: false }
            }
        );
        var priceLogData = {
            user_id: loggedInUserId,
            event_id: eventId,
            listing_group_id: listingIdDtls[listingIdCount].listing_group_id,
            listing_id: listingIdDtls[listingIdCount].id,
            price: listingIdDtls[listingIdCount].price.toFixed(2),
            new_price: newCalculatedPrice.toFixed(2),
            confirmed_price_change: true,
            created_at: new Date(),
            created_by: loggedInUserId
        };

        await PriceLog.create(priceLogData, { returning: ['id'] });
    }


    var finalResJSON = {};
    if (result) {
        finalResJSON["isError"] = false;
        finalResJSON["msg"] = "Listing Price Updated Successfully.";
    }
    else if (rowCount == 0) {
        finalResJSON["isError"] = true;
        finalResJSON["msg"] = "Selected Listings are already sold.";
    }
    else {
        finalResJSON["isError"] = true;
        finalResJSON["msg"] = "Some Error in updating the price.";
    }
    return finalResJSON;
}



/**
 * Page : MyTickets 
 * Function For : Update Share / Pause Share
 * Ticket No : TIC 99
 */

async function updateShareListings(_, { input: { listingGroupId, shareType, levelName, eventId, inActive } }) {
    var activeListingStatus = await getListingStatusbyName(listingStatusList.active);
    var inactiveListingStatus = await getListingStatusbyName(listingStatusList.inactive);
    var soldListingStatus = await getListingStatusbyName(listingStatusList.sold);
    var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);

    var listing_status_id = inActive ? inactiveListingStatus.id : activeListingStatus.id;

    var listingGroupIds = [];
    if (levelName == levelNames.listing && listingGroupId > 0) {
        listingGroupIds = [listingGroupId];
    }
    else if (levelName == levelNames.event && eventId > 0) {
        const listingGroupDtls = await ListingGroup.findAll({
            attributes: ['id'],
            where: {
                event_id: eventId,
                listing_status_id: { [Op.notIn]: [soldListingStatus.id, qcListingStatus.id] }
            }
        });

        for (var listingGroupCount = 0; listingGroupCount < listingGroupDtls.length; listingGroupCount++) {
            listingGroupIds.push(listingGroupDtls[listingGroupCount].id);
        }
    }

    var result;
    if (listingGroupIds.length) {
        result = await Listings.update(
            {
                inactive: inActive
            },
            {
                where: {
                    listing_group_id: { [Op.in]: listingGroupIds }, sold: false
                }
            }
        )

        await ListingGroup.update(
            {
                listing_status_id: listing_status_id
            },
            {
                where: {
                    id: { [Op.in]: listingGroupIds },
                    listing_status_id: { [Op.notIn]: [soldListingStatus.id, qcListingStatus.id] }
                }
            }
        )

    }

    var finalResJSON = {};
    if (result) {
        finalResJSON["isError"] = false;
        if (shareType == shareTypes.share) {
            finalResJSON["msg"] = "Listings Share Updated Successfully.";
        }
        else if (shareType == shareTypes.pauseShare) {
            finalResJSON["msg"] = "Listings Pause Share Updated Successfully.";
        }
    }
    else {
        finalResJSON["isError"] = true;
        if (shareType == shareTypes.share) {
            finalResJSON["msg"] = "Some Error in updating share listings.";
        }
        else if (shareType == shareTypes.pauseShare) {
            finalResJSON["msg"] = "Some Error in updating pause share listings.";
        }
    }
    return finalResJSON;
}

/**
 * Page : MyTickets 
 * Function For : Update Internal Notes
 * Ticket No : TIC 99
 */
async function updateInternalNotesListings(_, { input: { listingGroupId, internalNotes, levelName, eventId, selectedTags } }) {
    var listingGroupIds = [];
    if (levelName == levelNames.listing && listingGroupId > 0) {
        listingGroupIds = [listingGroupId];
    }
    else if (levelName == levelNames.event && eventId > 0) {
        var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);
        const listingGroupDtls = await ListingGroup.findAll({
            attributes: ['id'],
            where: {
                event_id: eventId,
                listing_status_id: { [Op.ne]: qcListingStatus.id }
            }
        });
        for (var listingGroupCount = 0; listingGroupCount < listingGroupDtls.length; listingGroupCount++) {
            listingGroupIds.push(listingGroupDtls[listingGroupCount].id);
        }
    }
    let listingTagsData = await ListingTags.findAll({ where: { listing_group_id: listingGroupId }, attributes: ['tag_id'], raw: true });
    if (selectedTags.length) {
        //Tags Already Saved In Database
        let tagsDataInDb = [];
        listingTagsData.map(tags => {
            tagsDataInDb.push(parseInt(tags.tag_id));
        })

        //Current Tags Selected
        let tagsIds = [];
        selectedTags = JSON.parse(selectedTags);
        selectedTags.map(tags => {
            tagsIds.push(parseInt(tags['value']));
        })

        //Tags To Insert New
        let insertTagData = tagsIds.filter(x => tagsDataInDb.indexOf(x) === -1);
        if (insertTagData.length) {
            for (var tagDataCount = 0; tagDataCount < insertTagData.length; tagDataCount++) {
                var tagsInputData = {
                    listing_group_id: listingGroupId,
                    tag_id: insertTagData[tagDataCount]
                };
                await ListingTags.create(tagsInputData, { returning: ['id'] });
            }
        }

        //Tags to Delete
        let deleteTagData = tagsDataInDb.filter(x => tagsIds.indexOf(x) === -1);
        if (deleteTagData.length) {
            await ListingTags.destroy({ where: { listing_group_id: listingGroupId, tag_id: { [Op.in]: deleteTagData } } });
        }
    }
    const result = await ListingGroup.update(
        {
            internal_notes: internalNotes
        },
        {
            where: { id: { [Op.in]: listingGroupIds } }
        }
    )
    var finalResJSON = {};
    if (result) {
        finalResJSON["isError"] = false;
        finalResJSON["msg"] = "Listing Group Internal Notes Updated Successfully.";
    }
    else {
        finalResJSON["isError"] = true;
        finalResJSON["msg"] = "Some Error in Updating Listing Group.";
    }
    return finalResJSON;
}


/**
 * Page : MyTickets 
 * Function For : Update Disclosures & External Notes
 * Ticket No : TIC 99
 */

async function updateDisclosuresInListings(_, { input: { listingGroupId, disclosures, externalNotes, levelName, eventId } }) {
    var listingGroupIds = [];
    if (levelName == levelNames.listing && listingGroupId > 0) {
        listingGroupIds = [listingGroupId];
    }
    else if (levelName == levelNames.event && eventId > 0) {
        var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);
        const listingGroupDtls = await ListingGroup.findAll({
            attributes: ['id'],
            where: { event_id: eventId, listing_status_id: { [Op.ne]: qcListingStatus.id } }
        });
        for (var listingGroupCount = 0; listingGroupCount < listingGroupDtls.length; listingGroupCount++) {
            listingGroupIds.push(listingGroupDtls[listingGroupCount].id);
        }
    }

    await ListingDisclosures.destroy({ where: { listingGroupId: { [Op.in]: listingGroupIds } } })
    let isError = false;
    for (var listingGroupIdCount = 0; listingGroupIdCount < listingGroupIds.length; listingGroupIdCount++) {
        for (var disclosuresCount = 0; disclosuresCount < disclosures.length; disclosuresCount++) {
            var inputData = {
                listingGroupId: listingGroupIds[listingGroupIdCount],
                disclosure_id: disclosures[disclosuresCount]
            };
            const result = await ListingDisclosures.create(inputData, {
                returning: ['id']
            })

            if (!result) {
                isError = true;
                break;
            }
        }
    }

    if (externalNotes != "") {
        for (var listingGroupIdCount = 0; listingGroupIdCount < listingGroupIds.length; listingGroupIdCount++) {
            await ListingGroup.update(
                {
                    external_notes: externalNotes
                },
                {
                    where: { id: listingGroupIds[listingGroupIdCount] }
                }
            )
        }
    }

    var finalResJSON = {};
    if (isError) {
        finalResJSON["isError"] = true;
        finalResJSON["msg"] = "Some Error in Updating Listing Group";
    }
    else {
        finalResJSON["isError"] = false;
        finalResJSON["msg"] = "Listing Group Disclosures Created Successfully.";
    }
    return finalResJSON;
}


/**
 * Page : MyTickets 
 * Function For : Update Splits
 * Ticket No : TIC 99
 */
async function updateSplitsInListings(_, { input: { listingGroupId, splitId, levelName, eventId } }) {
    if (splitId && splitId > 0) {
        var listingGroupIds = [];
        if (levelName == levelNames.listing && listingGroupId > 0) {
            listingGroupIds = [listingGroupId];
        }
        else if (levelName == levelNames.event && eventId > 0) {
            var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);
            const listingGroupDtls = await ListingGroup.findAll({
                attributes: ['id'],
                where: { event_id: eventId, listing_status_id: { [Op.notIn]: [qcListingStatus.id] } }
            });
            for (var listingGroupInfoCount = 0; listingGroupInfoCount < listingGroupDtls.length; listingGroupInfoCount++) {
                listingGroupIds.push(listingGroupDtls[listingGroupInfoCount].id);
            }
        }

        let isError = false;
        for (var listingGroupCount = 0; listingGroupCount < listingGroupIds.length; listingGroupCount++) {
            const result = await ListingGroup.update(
                {
                    splits_id: splitId
                },
                {
                    where: { id: listingGroupIds[listingGroupCount] }
                }
            )

            if (!result) {
                isError = true;
                break;
            }
        }

        var finalResJSON = {};
        if (isError) {
            finalResJSON["isError"] = true;
            finalResJSON["msg"] = "Some Error in Upting Listing Group";
        }
        else {
            finalResJSON["isError"] = false;
            finalResJSON["msg"] = "Listing Group Splits Updated Successfully.";
        }
        return finalResJSON;
    }
}

/**
 * Input : Ticketing System Value
 * Output : Data From Ticketing System Table
 * Function For : Add Ticketing System data insert to ticketing system table
 * Ticket No : TIC-113 , TIC-241
 */
async function addTicketingSystem(_, { input: { values } }) {
    const ticketingsystems = await Ticketingsystems.findOne({ where: { ticketing_system_name: values[0].ticketing_system_name, country_id: values[0].country_id } })
    if (ticketingsystems && ticketingsystems.dataValues) {
        return { 'message': 'Ticketing systems Already Exists  !!!', 'data': '', status: 'error' };
    }
    else {
        const ticketingSystemsDtls = await Ticketingsystems.bulkCreate(values);
        var data = { 'message': 'Ticketing Systems Created !!!', 'data': ticketingSystemsDtls, status: 'ok' }
        return data;
    }
}

/**
 * Page : MyTickets 
 * Function For : Update In-Hand Date
 * Ticket No : TIC 99
 */
async function updateInhandDateInListings(_, { input: { inHand, eventId, levelName, listingGroupId } }) {
    var soldListingStatus = await getListingStatusbyName(listingStatusList.sold);
    var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);
    try {
        var listingWhereString = "";
        if (inHand && inHand != "") {
            var listingGroupIds = [];
            var listingsParentIds = [];
            const listingGroupDtls = await ListingGroup.findAll({
                attributes: ['id'],
                where: { event_id: eventId }
            });
            for (var listingGroupCount = 0; listingGroupCount < listingGroupDtls.length; listingGroupCount++) {
                listingGroupIds.push(listingGroupDtls[listingGroupCount].id);
            }
            if (levelName == levelNames.listing && listingGroupId && listingGroupId.length) {
                listingGroupIds = listingGroupId;
            }
            const listingDtls = await Listings.findAll({
                attributes: ['listing_group_id'],
                where: { listing_group_id: { [Op.in]: listingGroupIds }, sold: false },
                group: ['listing_group_id']
            });

            let listingGroupIdsInhand;
            if (listingDtls) {
                listingGroupIdsInhand = JSON.parse(JSON.stringify(listingDtls));
            }
            for (var listingsCount = 0; listingsCount < listingGroupIdsInhand.length; listingsCount++) {
                listingsParentIds.push(listingGroupIdsInhand[listingsCount].listing_group_id);
            }
            if (levelName == levelNames.listing) {
                listingWhereString = { [Op.notIn]: [soldListingStatus.id] }
            }
            if (levelName == levelNames.event) {
                listingWhereString = { [Op.notIn]: [soldListingStatus.id, qcListingStatus.id] }
            }
            if (listingsParentIds.length > 0) {
                const result = await ListingGroup.update(
                    {
                        in_hand: inHand,
                        updated_at: new Date()
                    },
                    {
                        where: { id: { [Op.in]: listingsParentIds }, listing_status_id: listingWhereString }
                    }
                );
                const listingInhand = await Listings.update(
                    {
                        in_hand: inHand,
                        updated_at: new Date()
                    },
                    {
                        where: { listing_group_id: { [Op.in]: listingsParentIds }, sold: false, listing_status_id: listingWhereString }
                    }
                );

                var finalResJSON = {};
                if (!result) {
                    finalResJSON["isError"] = true;
                    finalResJSON["msg"] = "Some Error in Updating Listing Group";
                }
                else {
                    finalResJSON["isError"] = false;
                    finalResJSON["msg"] = "Listing Group Inhand Date updated Successfully.";
                }
            }
            else {
                var finalResJSON = {};
                finalResJSON["isError"] = true;
                finalResJSON["msg"] = "Some Error in Updating Listing Group.";
            }
            return finalResJSON;
        }
        else {
            var finalResJSON = {};
            finalResJSON["isError"] = true;
            finalResJSON["msg"] = "In hand date is not given";
            return finalResJSON;
        }
    } catch (error) {
        console.log(error)
    }

}

/**
 * Page : MyTickets 
 * Function For : Remove Listing Group
 * Ticket No : TIC 24
 */
async function removeListing(_, { input:
    { listingGroupId }
}) {

    await ListingGroup.update({ is_removed: true }, { where: { id: { [Op.in]: listingGroupId }, is_removed: false } });
    var finalResJSON = {
        "isError": false,
        "msg": "Listing is removed Successfully."
    };
    return finalResJSON;
}

async function getInHandDateListing(_, { input:
    { eventId, type }
}) {
    let listingDtls;

    if (type == listingVariables.inHand) {
        const listingGroupDtls = await ListingGroup.findOne({
            attributes: ['id'],
            where: { event_id: eventId, is_removed: false },
            raw: true
        });

        listingDtls = await Listings.findOne({
            attributes: [type],
            where: { listingGroupId: listingGroupDtls['id'] },
            raw: true
        });

    }
    else if (type == listingVariables.internalNotes) {
        listingDtls = await ListingGroup.findOne({
            attributes: [type],
            where: { event_id: eventId, is_removed: false },
            raw: true
        });
    }
    else if (type == listingVariables.externalNotes) {
        listingDtls = await ListingGroup.findOne({
            attributes: ["id", "external_notes"],
            where: { event_id: eventId, is_removed: false },
            raw: true
        });

        let listingDisclosures = await ListingDisclosures.findAll({
            attributes: [['disclosure_id', 'value']],
            where: { listingGroupId: listingDtls['id'] },
            raw: true
        });

        let listingDisclosureId = [];
        listingDisclosures.map(disclosure => {
            listingDisclosureId.push(disclosure.disclosure_id);
        })

        listingDtls["disclosures"] = listingDisclosures;
    }

    var finalResJSON = {
        "isError": false,
        "msg": "Listing data fetched Successfully.",
        "listingData": listingDtls
    };
    return finalResJSON;
}

async function getListingData(_, { input:
    { listingGroupId, level, eventId }
}) {
    let listingGroupDtls;

    Disclosures.hasMany(ListingDisclosures, { foreignKey: { name: 'disclosure_id' } });
    ListingDisclosures.belongsTo(Disclosures);

    if (level == levelNames.listing) {
        listingGroupDtls = await ListingGroup.findOne({
            attributes: ['splits_id', 'internal_notes', 'external_notes'],
            where: { id: listingGroupId },
            raw: true
        });

        let listingDisclosures = await ListingDisclosures.findAll({
            attributes: [['disclosure_id', 'value']],
            where: { listingGroupId: listingGroupId },
            raw: true
        });

        let disclosureDataInDb = [];
        listingDisclosures.map(i => {
            disclosureDataInDb.push(i.value);
        })

        listingGroupDtls["disclosures"] = disclosureDataInDb;

    } else if (level == levelNames.event) {

        listingGroupDtls = await ListingGroup.findOne({
            attributes: ['splits_id'],
            where: { eventId: eventId },
            raw: true
        });

    }

    var finalResJSON = {
        "isError": false,
        "msg": "Listing data fetched Successfully.",
        "listingData": listingGroupDtls
    };
    return finalResJSON;
}

/**
        * Page : MyTickets 
        * Function For : Splits & Purchase Ticket
        * Ticket No : TIC 99
        */
async function purchaseTicket(_, { input: { listingGroupId, ticketsRequired } }) {
    const splitDtls = await db.sequelize.query(`select a.listinggroupid, array_agg(a.id) as listingids , array_agg(distinct a.section) as section, array_agg(distinct a.row) as row, array_agg(distinct a.seat_number) as seatnumbers
        from 
            (select listing.id, listing_group.id as listinggroupid, listing_group.section, listing_group.row, listing.seat_number
            from 
            ticket_exchange.listing_group 
            inner join ticket_exchange.listing on listing_group.id = listing.listing_group_id
            where listing.sold is false and listing_group.id = `+ listingGroupId + `
            order by listing.id
            limit `+ ticketsRequired + `) a 
        where 1=1
        group by a.listinggroupid;`, { type: QueryTypes.SELECT });

    let finalResJSON = {};
    if (splitDtls && splitDtls.length > 0) {
        if (splitDtls[0].listingids.length == ticketsRequired) {
            const result = await Listings.update(
                {
                    sold: true
                },
                {
                    where: { id: { [Op.in]: splitDtls[0].listingids } }
                }
            )

            if (result) {
                finalResJSON["isError"] = false;
                finalResJSON["msg"] = "Ticket Purchased Successfully.";
                finalResJSON["listingGroupId"] = splitDtls[0].listinggroupid;
                finalResJSON["listingIds"] = splitDtls[0].listingids;
                finalResJSON["section"] = splitDtls[0].section;
                finalResJSON["row"] = splitDtls[0].row;
                finalResJSON["seatNumbers"] = splitDtls[0].seatnumbers;
            }
            else {
                finalResJSON["isError"] = true;
                finalResJSON["msg"] = "Ticket Not Purchased.";
            }
        }
        else {
            finalResJSON["isError"] = true;
            finalResJSON["msg"] = "Sufficient Tickets Not Found.";
        }
    }
    else {
        finalResJSON["isError"] = true;
        finalResJSON["msg"] = "Tickets Not Found.";
    }
    return finalResJSON;
}

/**
 * Page : MyTickets 
 * Function For : Price Update - Event Level
 * Ticket No : TIC 99
 */

async function updateEventLevelListingPrice(_, { input: { eventId, price, userId, updateSymbol, listingIds } }) {
    var listingGroupIds = [];
    var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);
    var soldListingStatus = await getListingStatusbyName(listingStatusList.sold);
    let whereConditionLisitngs =  { event_id: eventId, listing_status_id: { [Op.notIn]: [qcListingStatus.id, soldListingStatus.id] } };
    if (listingIds && listingIds.length > 0) {
        whereConditionLisitngs = { event_id: eventId, listing_status_id: { [Op.notIn]: [qcListingStatus.id, soldListingStatus.id] }, id: { [Op.in]: [listingIds] } }
    }
    const listingGroupDtls = await ListingGroup.findAll({
        attributes: ['id'],
        where: whereConditionLisitngs
    });
    for (var listingGroupCount = 0; listingGroupCount < listingGroupDtls.length; listingGroupCount++) {
        listingGroupIds.push(listingGroupDtls[listingGroupCount].id);
    }

    let result;

    //Price Log Starts - Event Level Listing Price

    const listingDtls = await Listings.findAll({
        attributes: ['id', 'listing_group_id', 'price'],
        where: { listing_group_id: { [Op.in]: listingGroupIds }, sold: false }
    });

    var listingIdDtls = JSON.parse(JSON.stringify(listingDtls));
    let loggedInUserId = parseInt(userId);


    for (var listingInfoCount = 0; listingInfoCount < listingIdDtls.length; listingInfoCount++) {

        let newCalculatedPrice = listingIdDtls[listingInfoCount].price;
        if (updateSymbol) {
            if (updateSymbol == '%') {
                newCalculatedPrice = parseFloat(listingIdDtls[listingInfoCount].price) + (parseFloat(listingIdDtls[listingInfoCount].price) * (parseFloat(price) / 100));
                if (newCalculatedPrice < 0) {
                    newCalculatedPrice = 0
                }
            }
            else if (updateSymbol == '$') {
                newCalculatedPrice = parseFloat(listingIdDtls[listingInfoCount].price) + parseFloat(price);
                if (newCalculatedPrice < 0) {
                    newCalculatedPrice = 0
                }
            }
        }
        else {
            newCalculatedPrice = price;
        }

        var priceLogData = {
            user_id: loggedInUserId,
            event_id: eventId,
            listing_group_id: listingIdDtls[listingInfoCount].listing_group_id,
            listing_id: listingIdDtls[listingInfoCount].id,
            price: listingIdDtls[listingInfoCount].price.toFixed(2),
            new_price: newCalculatedPrice.toFixed(2),
            confirmed_price_change: true,
            created_at: new Date(),
            created_by: loggedInUserId
        };


        const priceLogDtls = await PriceLog.create(priceLogData, { returning: ['id'] });

        //Price Update
        result = await Listings.update(
            {
                price: parseFloat(newCalculatedPrice).toFixed(2),
                last_price: parseFloat(listingIdDtls[listingInfoCount].price).toFixed(2),
                last_price_change: new Date()
            },
            {
                where: { id: listingIdDtls[listingInfoCount].id, sold: false }
            }
        )
    }

    //Price Log Ends

    var finalResJSON = {};
    if (result) {
        finalResJSON["isError"] = false;
        finalResJSON["msg"] = "Listing Price Updated Successfully.";
    }
    else {
        finalResJSON["isError"] = true;
        finalResJSON["msg"] = "Some Error in Updating Listing.";
    }
    return finalResJSON;
}

/**
 * Page : MyTickets
 * Function For : Mark as Sold
 * Ticket No : TIC 411
 */
async function updateListingGroupMarkAsSold(_, { input: { listingGroupId } }) {

    var soldListingStatus = await getListingStatusbyName(listingStatusList.sold);
    var inactiveListingStatus = await getListingStatusbyName(listingStatusList.inactive);
    var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);
    var result;
    if (soldListingStatus) {
        result = await Listings.update(
            {
                sold: true,
                listing_status_id: soldListingStatus.id
            },
            {
                where: { listing_group_id: { [Op.in]: listingGroupId }, sold: false, listing_status_id: { [Op.notIn]: [inactiveListingStatus.id, qcListingStatus.id] } }
            }
        )

        await ListingGroup.update(
            {
                listing_status_id: soldListingStatus.id
            },
            {
                where: { id: { [Op.in]: listingGroupId }, listing_status_id: { [Op.notIn]: [inactiveListingStatus.id, qcListingStatus.id] } }
            }
        )
    }

    var finalResJSON = {};
    if (result) {
        finalResJSON["isError"] = false;
        finalResJSON["msg"] = "Listing Group Marked as SOLD.";
    }
    else {
        finalResJSON["isError"] = true;
        finalResJSON["msg"] = "Some Error in Updating Listing Group.";
    }
    return finalResJSON;
}


async function allListings(_, { input: { seller_id } }) {

    const listing = await Listings.findAll({ where: { seller_id: seller_id } });
    if (listing) {
        return JSON.parse(JSON.stringify(listing));
    } else {
        return "No data";
    }
}


/**
 * Page : MyTickets
 * Function For : To Get Default & Filtered Listings
 * Ticket No : TIC 17, 104, 105 & 106
 */

async function filterListings(_, { input: { userId, searches, from_date, to_date, day_of_week, ticket_type, genre, tags, in_hand, section, row, quantity, inventory_attached, held, coordinates, radius, days, category, performer, isfutureevents, hold, page, limit, tagIds = [], venue, eventSearch, cities = [], completedEvents = false } }) {
    try {
        let paginationCondition = '';
        if (limit && page) {
            paginationCondition = ` limit ${limit} OFFSET ${(page - 1) * limit} `;
        }
        let listingGroupUserIds = '';

        if (userId) {
            let listingGroupUserIdsResponse = await getUserIdsForListingGroupAccess(userId);
            if (listingGroupUserIdsResponse && listingGroupUserIdsResponse.length > 0) {
                listingGroupUserIds = listingGroupUserIdsResponse;
            }
        }



        let eventIDsArr = [],
            whereStr = '',
            listingWhereStr = '',
            finalWhereStr = '',
            finalListingWhereStr = '';

        if (searches && searches != "" && searches != null && searches != 'null') {
            whereStr += '( ';
            if (Number.isInteger(parseInt(searches))) {
                whereStr += ' "event"."id" = ' + parseInt(searches) + ' or ';
            }
            whereStr += '"event"."name" iLike \'%' + searches + '%\'';
            whereStr += ' or "performer"."name" iLike \'%' + searches + '%\'';
            whereStr += ' or "venue"."name" iLike \'%' + searches + '%\'';
            whereStr += ' or concat("venue"."city", \', \', "venue"."country") iLike \'%' + searches + '%\'';
            whereStr += ' or "venue"."country" iLike \'%' + searches + '%\'';
            whereStr += ' or "eventtag"."tag_name" iLike \'%' + searches + '%\'';
            whereStr += ' or "listingtag"."tag_name" iLike \'%' + searches + '%\'';
            whereStr += ' )';
        }

        if ((from_date && from_date != "" && from_date != null && from_date != 'null')) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += ' ("event"."date" >= \'' + from_date + '\'';
            if (to_date && to_date != "" && to_date != null && to_date != 'null') {
                whereStr += ' and "event"."date" <= \'' + to_date + '\'';
            }
            whereStr += ' )';
        }
        if (isfutureevents) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += ' ("event"."date" >= CURRENT_TIMESTAMP ) ';
        }

        if (eventSearch) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"event"."name" iLike \'%' + eventSearch + '%\'';
        }

        let inConditionData = {
            tagIds: ['in', '"listing_tags"."tag_id"', tagIds, inputType.id],
            cities: ['in', '"venue"."city"', cities, inputType.string],
        }

        for (let key in inConditionData) {
            let data = inConditionData[key];
            if (data[2].length && await isValidInput(data[3], data[2])) {
                whereStr += ` ${whereStr != "" ? ' and ' : ''} ${data[1]} ${data[0]} ('${data[2].join("','")}')`;
            }
        }

    

        if (day_of_week && day_of_week != "" && day_of_week != null && day_of_week != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += `"event"."eventdate_day" in ( '` + day_of_week.join("','") + `'  )`;
        }

        if (ticket_type && ticket_type != "" && ticket_type != null && ticket_type != 'null' && Number.isInteger(parseInt(ticket_type))) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"listing_group"."ticket_type_id" = ' + ticket_type;
            listingWhereStr += '"listing_group"."ticket_type_id" = ' + ticket_type;
        }

        if (genre && genre != "" && genre != null && genre != 'null' && Number.isInteger(parseInt(genre))) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"event"."genre_id" = ' + genre;
        }

        if (category && category != "" && category != null && category != 'null' && Number.isInteger(parseInt(category))) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"event"."category_id" = ' + category;
        }

        if (performer && performer != "" && performer != null && performer != 'null' && Number.isInteger(parseInt(performer))) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += ' "event"."performer_id" = ' + performer + ' ';
        }

        if (await isValidInput(inputType.id, [venue])) {
            whereStr += ` ${whereStr != '' ? 'and' : ''} "venue"."id" = ${parseInt(venue)} `
        }

        if (tags && tags != "" && tags != null && tags != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '("eventtag"."tag_name" iLike \'%' + tags + '%\'';
            whereStr += ' or "listingtag"."tag_name" iLike \'%' + tags + '%\')';
            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += '"listingtag"."tag_name" iLike \'%' + tags + '%\'';
        }

        if (in_hand && in_hand != "" && in_hand != null && in_hand != 'null') {
            let in_hand_from = in_hand + ' 00:00:00';
            let in_hand_to = in_hand + ' 23:59:59';

            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += ' ("listing"."in_hand" >= \'' + in_hand_from + '\' and "listing"."in_hand" <= \'' + in_hand_to + '\')';
        }

        if (section && section != "" && section != null && section != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"listing_group"."section" iLike \'%' + section + '%\'';
            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += '"listing_group"."section" iLike \'%' + section + '%\'';
        }

        if (row && row != "" && row != null && row != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"listing_group"."row" iLike \'%' + row + '%\'';
            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += '"listing_group"."row" iLike \'%' + row + '%\'';
        }

        if (quantity && quantity != "" && quantity != null && quantity != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"listing_group"."quantity" = ' + quantity;
            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += '"listing_group"."quantity" = ' + quantity;
        }

        if (inventory_attached && inventory_attached != "" && inventory_attached != null && inventory_attached != 'null') {
            if (inventory_attached == commonVariables.yesValue) {
                if (listingWhereStr != "") {
                    listingWhereStr += ' and ';
                }
                listingWhereStr += '"listing"."pdf" is not null';
            }
            else if (inventory_attached == commonVariables.noValue) {
                if (listingWhereStr != "") {
                    listingWhereStr += ' and ';
                }
                listingWhereStr += '"listing"."pdf" is null';
            }
        }

        let selectQuery = '';
        let distance = '';
        if (coordinates && coordinates != '' && radius && radius != '') {
            coordinates = JSON.parse(coordinates);
            selectQuery += ", array_agg(3959 * acos( cos( radians('" + coordinates.lat + "') ) * cos( radians( venue.latitude ) ) * cos( radians( longitude ) - radians('" + coordinates.lng + "') ) + sin( radians('" + coordinates.lat + "') ) * sin( radians( latitude ) ) ) ) AS distance";
            distance = " and distance <= '{" + radius + ".0}'  ";
        }

        if (days && days != "") {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += " event.date BETWEEN CURRENT_DATE AND CURRENT_DATE + INTERVAL '" + days + " days' ";
        }

        var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);

        if (whereStr != "") {
            finalWhereStr = ' and (' + whereStr + ')';
        } else {
            finalWhereStr = ' and listing_group.event_id = event.id ';
            if (listingGroupUserIds) {
                finalWhereStr += ' and listing_group.created_by in (' + listingGroupUserIds + ') ';
            }
        }

        if (whereStr == "" && qcListingStatus && qcListingStatus.id > 0) {
            finalWhereStr += ' and NOT( listing_group.listing_status_id = ' + qcListingStatus.id + ')';
        }

        if (!completedEvents || completedEvents == false) {
            finalWhereStr += ' and event.event_status_id != 1 ';
        }

        if (listingWhereStr != "") {
            finalListingWhereStr = ' and (' + listingWhereStr + ')';
        }
        if (hold && hold != '' && hold != null) {
            let holdStatus = hold == commonVariables.yesValue ? true : false;
            finalListingWhereStr = ' and listing.hold =' + holdStatus;
        }

        finalListingWhereStr += ' and is_removed = false';

        if (listingGroupUserIds) {
            finalListingWhereStr += ' and listing_group.created_by in (' + listingGroupUserIds + ')';
        }

        if (qcListingStatus && qcListingStatus.id > 0) {
            finalListingWhereStr += ' and NOT( listing_group.listing_status_id = ' + qcListingStatus.id + ')';
        }

        var operationSelectQueryData = ', array_agg(DISTINCT purchase_order.id) as purchaseid , ';
        var operationJoinQuery = 'left JOIN ticket_exchange.purchase_order on purchase_order.listing_group_id = listing_group.id';

        const eventDetails = await getEventDetails(selectQuery, finalWhereStr, paginationCondition, distance);

        //const eventDtls = await db.sequelize.query(`SELECT * FROM ( select event.image as eventimage,event.genre_id as genreid, event.category_id as categoryid, array_agg(distinct category.name) as categoryname, event.id as event_id, array_agg(distinct genre.name) as genrename, event.name as eventname, TO_CHAR(event.date,'YYYY-MM-DD HH24:MI') as eventdate, array_agg(distinct venue.name) as venuename, array_agg(distinct venue.city) as city, array_agg(distinct venue.state) as state, array_agg(distinct countries.name) as country,  array_agg(distinct countries.abbreviation) as countrycode, DATE_PART('day', event.date :: TIMESTAMP - NOW() :: TIMESTAMP ) as daystoevent, avg(listcnt.totalseats) :: numeric(20) as totalseats, avg(listcnt.soldtkts) :: numeric(20) as totalsoldtkts, avg(listcnt.totalavailabletkts) :: numeric(20) as totalavailabletkts, avg(listcnt.last7days) :: numeric(20) as totalsoldtktsinlast7days, avg(listcnt.awaitingdelivery) :: numeric(20) as awaitingdelivery, array_agg(distinct performer.name) as performername, array_remove(array_agg(distinct listing_group.inactive), null) as inactive ` + selectQuery + ` from ticket_exchange.event  left outer join ticket_exchange.category on event.category_id = category.id left outer join ticket_exchange.genre on event.genre_id = genre.id left outer join ticket_exchange.listing_group on event.id = listing_group.event_id left outer join ticket_exchange.listing_tags on listing_group.id = listing_tags.listing_group_id left outer join ticket_exchange.event_tags on event.id = event_tags.event_id left outer join ticket_exchange.tags eventtag on event_tags.tag_id = eventtag.id left outer join ticket_exchange.tags listingtag on listing_tags.tag_id = listingtag.id left outer join ticket_exchange.venue on event.venue_id = venue.id left outer join ticket_exchange.countries on venue.country::INTEGER = countries.id left outer join ticket_exchange.performer on event.performer_id = performer.id left outer join ( select listing_group.event_id, ( case when listing_group.seat_type_id = ` + seat_type.general_admission + ` then avg(listing_group.quantity) :: numeric(20) else sum( listing_group.seat_end :: integer - listing_group.seat_start :: integer + 1 ) end ) as totalseats, sum(st.soldtkts) as soldtkts, sum(st.last7days) as last7days, sum(st.awaitingdelivery) as awaitingdelivery, ( case when listing_group.seat_type_id = ` + seat_type.general_admission + ` then avg(listing_group.quantity) :: numeric(20) else sum( listing_group.seat_end :: integer - listing_group.seat_start :: integer + 1 ) end ) - sum(st.soldtkts) as totalavailabletkts from ticket_exchange.listing_group left outer join ( select listing_group_id, sum( case when sold then 1 else 0 end ) as soldtkts, sum( case when sold and delivered is false then 1 else 0 end ) as awaitingdelivery, sum( case when sold and updated_at between current_date - interval'7 days'and current_date then 1 else 0 end ) as last7days from ticket_exchange.listing where 1 = 1 group by listing_group_id ) st on listing_group.id = st.listing_group_id where 1 = 1 AND is_removed = FALSE group by listing_group.event_id, listing_group.seat_type_id ) as listcnt on event.id = listcnt.event_id where 1 = 1` + finalWhereStr + `group by event.id order by event.id ${paginationCondition}) q WHERE 1 = 1` + distance + `;`, { type: QueryTypes.SELECT });
        var inActiveListingStatus = await getListingStatusbyName(listingStatusList.inactive);
        for (var eventDtlCount = 0; eventDtlCount < eventDetails.length; eventDtlCount++) {

            if (eventDetails[eventDtlCount].listing_status.includes(inActiveListingStatus.id)) {
                eventDetails[eventDtlCount].inactive = [true];
            } else {
                eventDetails[eventDtlCount].inactive = [false];
            }
            eventIDsArr.push(eventDetails[eventDtlCount].event_id);
        }

        if (eventIDsArr.length > 0) {

            const listingDetails = await getListingDetails(operationSelectQueryData, operationJoinQuery, finalListingWhereStr);
            //const listingDtls = await db.sequelize.query(`select listing_group.id as listinggroupid, array_agg(distinct listing.id) as listingid, listing_group.event_id, array_agg( distinct ( case when listing.sold then'yes'else'no'end ) ) as sold, listing_group.section, listing_group.row, listing_group.seat_start ||'-'|| listing_group.seat_end as seatdtl, listing_group.unit_cost as cost, ( case when sum( case when listing.sold is false then 1 else 0 end ) > 0 then sum( case when listing.sold is false then listing.price else 0 end ) / sum( case when listing.sold is false then 1 else 0 end ) else 0 end ) as price, ( case when sum( case when listing.sold is false then 1 else 0 end ) > 0 then sum( case when listing.sold is false then listing.last_price else 0 end ) / sum( case when listing.sold is false then 1 else 0 end ) else 0 end ) as last_price, array_remove( array_agg(distinct listing.last_price_change), null ) as last_price_change, avg(st.price) as soldprice, avg(st.soldtkts) :: numeric(20) as totalsold, (case when listing_group.seat_type_id = ` + seat_type.general_admission + ` then avg(listing_group.quantity) :: numeric(20) - avg(st.soldtkts) :: numeric(20) else avg( listing_group.seat_end :: integer - listing_group.seat_start :: integer + 1 ) :: numeric(20) - avg(st.soldtkts) :: numeric(20) end) as totalavailable, listing_group.face_value, listing_group.inactive as listing_inactive, array_remove(array_agg(distinct listing.active), null) as active, array_remove(array_agg(distinct listing.inactive), null) as inactive, listing_group.ticket_type_id, array_agg(distinct ticket_types.name) as tickettypename, avg(st.last7days) :: numeric(20) as last7days, avg(st.awaitingdelivery) :: numeric(20) as awaitingdelivery, avg(st.margin) :: numeric(20, 2) as margin, avg(st.margintype) :: numeric(20) as margintype, array_remove(array_agg(distinct listingtag.tag_name), null) as tag_name, array_agg( price_log.user_name) as price_by from ticket_exchange.listing_group left outer join ( select listing_group_id, sum( case when sold then 1 else 0 end ) as soldtkts, sum( case when sold then price else 0 end ) as price, sum( case when sold and delivered is false then 1 else 0 end ) as awaitingdelivery, avg( case when sold and price > 0 then trunc((price / unit_cost) :: numeric, 2) else 0 end ) as margin, avg( case when sold then price else 0 end ) - avg( case when sold then unit_cost else 0 end ) as margintype, sum( case when sold and updated_at between current_date - interval'7 days'and current_date then 1 else 0 end ) as last7days from ticket_exchange.listing where 1 = 1 group by listing_group_id ) st on listing_group.id = st.listing_group_id left outer join ticket_exchange.listing on listing_group.id = listing.listing_group_id left outer join ticket_exchange.ticket_types on listing_group.ticket_type_id = ticket_types.id left outer join ( select listing_tags.listing_group_id, tags.tag_name from ticket_exchange.listing_tags inner join ticket_exchange.tags on listing_tags.tag_id = tags.id where 1 = 1 ) listingtag on listingtag.listing_group_id = listing_group.id left outer join ( select  DISTINCT price_log.listing_group_id, price_log.id,   concat("user".first_name,' ',"user".last_name) as user_name from ticket_exchange.price_log inner join ticket_exchange."user"on"user".id = price_log.created_by where 1 = 1 ORDER BY price_log.id DESC  ) price_log on price_log.listing_group_id = listing_group.id where 1 = 1 ` + finalListingWhereStr + ` group by listing_group.id;`, { type: QueryTypes.SELECT });
            for (var eventDetailCount = 0; eventDetailCount < eventDetails.length; eventDetailCount++) {
                let listingsRes = [];
                for (var listingInfoCount = 0; listingInfoCount < listingDetails.length; listingInfoCount++) {
                    if (listingDetails[listingInfoCount].event_id == eventDetails[eventDetailCount].event_id) {
                        const recentPricelogChanger = await getRecentPricelogChanger(listingDetails[listingInfoCount].listinggroupid);
                        var price_by = "";
                        if (recentPricelogChanger) { price_by = recentPricelogChanger[0].first_name + " " + recentPricelogChanger[0].last_name; }
                        let obj = {};
                        obj['listingGroupId'] = listingDetails[listingInfoCount].listinggroupid;
                        obj['listingId'] = listingDetails[listingInfoCount].listingid;
                        obj['eventId'] = listingDetails[listingInfoCount].event_id;
                        obj['sold'] = listingDetails[listingInfoCount].sold;
                        obj['section'] = listingDetails[listingInfoCount].section;
                        obj['row'] = listingDetails[listingInfoCount].row;
                        obj['seatdtl'] = listingDetails[listingInfoCount].seatdtl;
                        obj['price'] = listingDetails[listingInfoCount].price;
                        obj['last_price'] = listingDetails[listingInfoCount].last_price;
                        obj['last_price_change'] = listingDetails[listingInfoCount].last_price_change;
                        obj['face_value'] = listingDetails[listingInfoCount].face_value;
                        obj['cost'] = listingDetails[listingInfoCount].cost;
                        obj['margin'] = listingDetails[listingInfoCount].margin;
                        obj['margintype'] = listingDetails[listingInfoCount].margintype;
                        obj['availabletkts'] = listingDetails[listingInfoCount].totalavailable;
                        obj['soldtkts'] = listingDetails[listingInfoCount].totalsold;
                        obj['tags'] = listingDetails[listingInfoCount].tag_name;
                        obj['tickettypename'] = listingDetails[listingInfoCount].tickettypename;
                        obj['active'] = listingDetails[listingInfoCount].active;
                        obj['inactive'] = listingDetails[listingInfoCount].inactive;
                        obj['listing_inactive'] = listingDetails[listingInfoCount].listing_inactive;
                        obj['price_done_by'] = price_by;
                        obj['purchaseid'] = listingDetails[listingInfoCount].purchaseid;
                        obj['seat_type_id'] = listingDetails[listingInfoCount].seat_type_id;

                        if (listingDetails[listingInfoCount].listing_status_id) {
                            obj['listing_status'] = await getListingStatusbyId(listingDetails[listingInfoCount].listing_status_id);
                        }

                        listingsRes.push(obj);
                    }
                }
                eventDetails[eventDetailCount].listingDtls = listingsRes;
            }
        }

        if (eventDetails) {
            return eventDetails;
        } else {
            return 'No Data';
        }
    }
    catch (e) {
        return 'Error';
    }
}




/**
  * Input : User emailId
  * Output : Data From ticketing system account Table
  * Function For : Add ticketing system account data insert to ticketing system account table
  * Ticket No : TIC-425
  */
async function addTicketingSystemAccount(_, { input: { values, userID } }) {
    const userDetails = await User.findOne({ attributes: ['id'], where: { email: values.email.trim() }, raw: true })
    if (!userDetails) {
        return { 'message': 'User not found  !!!', 'data': '', status: 'error' };
    }
    else {

        const isTicketingSystemAccountExist = await Ticketingsystemaccounts.findOne({
            attributes: ['id'], where: {
                ticketing_system_id: parseInt(values.ticket_system_id),
                user_id: userDetails.id
            }, raw: true
        });

        if (isTicketingSystemAccountExist) {
            return { 'message': 'Account already added for selected Ticketing system !!!', 'data': '', status: 'error' };
        }
        else {
            var createTicketingAccountData = {
                ticketing_system_id: parseInt(values.ticket_system_id),
                user_id: userDetails.id,
                parent_id: parseInt(userID),
                account_name: values.email,
                created_at: Date.now(),
                updated_at: Date.now()
            }
            var ticketingsystem = await Ticketingsystemaccounts.create(createTicketingAccountData, { returning: ['id'] });
            if(ticketingsystem && values.typeId && values.lastFourDigits && values.creditCardName){
                var createCreditCardData = {
                    user_id: userDetails.id,
                    type_id: values.typeId,
                    last_4_digits: values.lastFourDigits,
                    creditcard_name: values.creditCardName,
                    address1: values.address1,
                    address2: values.address2,
                    city: values.city,
                    state: values.state,
                    country: values.country,
                    zip_code: values.zipCode,
                    security_code: values.securityCode,
                    ticketing_system_id: parseInt(values.ticket_system_id),
                    ticketing_system_account_id: ticketingsystem.id,
                    created_by: parseInt(userID),
                    createdAt: Date.now(),
                    updatedAt: Date.now()
                }
                await module.exports.addCreditCardDetails(createCreditCardData);
            }
            var data = { 'message': 'Ticketing system account Created !!!', 'data': ticketingsystem, status: 'ok' }
            return data;
        }
    }
}

/**
  * Input : Credit Card Data
  * Output : Data From Ticket Account Form
  * Function For : Add Credit Card data insert to Credit Card table
  * Ticket No : TIC-676
  */
 async function addCreditCardDetails(createCreditCardData) {
    //Channel Fees Start
    if (createCreditCardData) {
        const CreditCard =  await CreditCards.create(createCreditCardData);
        if(CreditCard){
            await Ticketingsystemaccounts.update(
                { credit_card_id: CreditCard.id },
                {where: { id: CreditCard.ticketing_system_account_id }}
            );
        }
    }
    //Channel Fees End
}

/**
 * MyTickets Page - To Get All Credit Card Types Details - TIC 676
 */
 async function allCreditCardTypes() {
    const creditCardTypesDetails = await CreditCardTypes.findAll({
        attributes: ['id', 'type_name'],
        where: {}
    });
    if (creditCardTypesDetails) {
        return JSON.parse(JSON.stringify(creditCardTypesDetails));
    } else {
        return "No data";
    }
}

async function readUploadListingsFile(_, { input: { userId, filePath } }) {
    if (filePath.substring(filePath.lastIndexOf(".")) != ".csv") {
        const file = reader.readFile(filePath)
        let data = [];
        const sheets = file.SheetNames;
        for (let i = 0; i < sheets.length; i++) {
            const temp = reader.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', raw: false })
            temp.forEach(async (res) => {
                var myKeyData = Object.keys(res);
                var myValueData = Object.values(res);
                if (myValueData[2] != "") {
                    myValueData[2] = myValueData[2].split('/').join('-')
                }
                if (myValueData[4] != "") {
                    myValueData[4] = myValueData[4].split('/').join('-')
                }
                if (myValueData[27] != "") {
                    myValueData[27] = myValueData[27].split('/').join('-')
                }

                if (myValueData[6] == seatTypes.generalAdmission) {
                    await module.exports.insertListingGroupDetailsGeneralAdmissionCSVFileUpload(userId, myKeyData, myValueData);
                }
                else {
                    await module.exports.insertListingGroupDetailsCSVFileUpload(userId, myKeyData, myValueData);
                }
            })
        }
    }
    else {
        fs.createReadStream(filePath)
            .pipe(csv())
            .on('data', async function (data) {
                var myKeyData = Object.keys(data);
                var myValueData = Object.values(data);

                if (myValueData[6] == seatTypes.generalAdmission) {
                    await module.exports.insertListingGroupDetailsGeneralAdmissionCSVFileUpload(userId, myKeyData, myValueData);
                }
                else {
                    await module.exports.insertListingGroupDetailsCSVFileUpload(userId, myKeyData, myValueData);
                }
            }).on('end', function (data) {
                fs.unlinkSync(filePath) //Delete temporary file stored in local system
            });
    }
    let resultJSON = {};
    resultJSON["isError"] = false;
    resultJSON["msg"] = "Data stored";
    return resultJSON;
}

/**
 * Page : MyTickets
 * Function For : To Get Primary Id
 * Ticket No : TIC 81
 */
async function getPrimaryIdFromNameDetails(fieldName1, fieldName2, fieldValue1, fieldValue2, primaryIdName, tableName) {
    let whereCondition = '';
    let fieldValue = '';
    if (fieldValue1 != "" && fieldValue1 != null) {
        fieldValue = fieldValue1.toLowerCase();
        fieldValue = fieldValue.replace(/\s/g, "");
    }

    if (tableName == tableNames.event) {
        if (fieldValue2 != "") {
            let myDate = fieldValue2.split("-");
            fieldValue2 = myDate[2] + "-" + myDate[1] + "-" + myDate[0];
        }
        whereCondition = ` and replace(lower(` + fieldName1 + `),' ','') = '` + fieldValue + `' and TO_CHAR(` + fieldName2 + `,'YYYY-MM-DD') = '` + fieldValue2 + `'`;
    }
    else if (tableName == tableNames.ticketType || tableName == tableNames.splits || tableName == tableNames.seatType || tableName == tableNames.ticketingSystem || tableName == tableNames.ticketingSystemAccount || tableName == tableNames.disclosures || tableName == tableNames.attributes || tableName == tableNames.tags || tableName == tableNames.vendor || tableName == tableNames.paymentMethod || tableName == tableNames.paymentType) {
        whereCondition = ` and replace(lower(` + fieldName1 + `),' ','') = '` + fieldValue + `'`;
        if (fieldName2 != "" && fieldValue2 != "") {
            let fieldValueNew2 = fieldValue2.toLowerCase();
            fieldValueNew2 = fieldValueNew2.replace(/\s/g, "");

            whereCondition += ` and replace(lower(` + fieldName2 + `),' ','') = '` + fieldValueNew2 + `'`;
        }
    }
    const resData = await db.sequelize.query(`select ` + primaryIdName + ` from ticket_exchange.` + tableName + ` where 1=1 ` + whereCondition + `;`, { type: QueryTypes.SELECT });

    if (resData && resData.length > 0) {
        return resData[0][primaryIdName];
    } else {
        return 0;
    }
}

/**
 * Fetch the event listing details
 * @param {*} _ 
 * @param {*} param1 
 * @returns List details in JSON string
 */
async function fetchListingGroupDetails(_, { input: { listingGroupIds } }) {
    Events.hasMany(ListingGroup);
    ListingGroup.belongsTo(Events);
    var listing = [];
    if (listingGroupIds && listingGroupIds.length) {
        for (var listingGroupIdCount = 0; listingGroupIdCount < listingGroupIds.length; listingGroupIdCount++) {
            var listings = await ListingGroup.findAll({
                attributes: [
                    [sequelize.literal('listing_group.id'), 'id'],
                    [sequelize.literal('event.name'), 'eventName'],
                    [sequelize.literal('event.date'), 'eventDate'],
                    [sequelize.literal('listing_group.section'), 'section'],
                    [sequelize.literal('listing_group.row'), 'row'],
                    [sequelize.literal('listing_group.quantity'), 'quantity'],
                    [sequelize.literal('listing_group.seat_start'), 'seat_start'],
                    [sequelize.literal('listing_group.seat_end'), 'seat_end'],
                    [sequelize.literal('listing_group.group_cost'), 'group_cost'],
                    [sequelize.literal('listing_group.unit_cost'), 'unit_cost'],
                    [sequelize.literal('listing_group.seat_type_id'), 'seat_type_id'],
                ],
                include: [{
                    model: Events,
                    attributes: [],
                    on: {
                        col1: sequelize.where(sequelize.col('event.id'), '=', sequelize.col('listing_group.event_id')),
                    }
                }],
                where: {
                    id: listingGroupIds[listingGroupIdCount]
                }
            });

            if (listings && listings.length) { listing.push(listings[0]) }
        }
    }

    if (listing && listing.length) {
        var finalResJSON = {
            "isError": false,
            "msg": "Created listing data",
            "value": listing
        };
        return finalResJSON;

    } else {
        var finalResJSON = {
            "isError": true,
            "msg": "No listing data",
            "value": []
        };
        return finalResJSON;
    }
}

async function getRecentListingDetailsBasedOnEventId(_, { input: { eventIds } }) {
    Events.hasMany(ListingGroup);
    ListingGroup.belongsTo(Events);

    eventIds = eventIds.split(",");

    const eventDetails = await db.sequelize.query(`SELECT "event"."id" AS "event_id","event"."name" AS "eventname","listing_group"."ticket_type_id" as tickettype_id,"listing_group"."ticket_system_id","event"."date" AS "eventdate","listing_group"."in_hand","listing_group"."seat_type_id","listing_group"."splits_id","listing_group"."hide_seat_numbers","listing_group"."created_at" FROM "ticket_exchange"."event"AS"event"LEFT OUTER JOIN"ticket_exchange"."listing_group" ON "event"."id" = "listing_group"."event_id" WHERE "event"."id" IN (` + eventIds + `) order by "listing_group"."created_at" DESC limit 1 OFFSET 0 `, { type: QueryTypes.SELECT });


    if (eventDetails) {
        return JSON.parse(JSON.stringify(eventDetails));
    } else {
        return "No Data";
    }
}

/**
 * Page : MyTickets
 * Function For : To Get Listings Based On Listing Group ID
 * Ticket No : TIC 81
 */
async function getListingDetailsBasedOnListingGroupId(listingGroupId) {
    const resultData = await db.sequelize.query(`select id from ticket_exchange.listing where 1=1 and listing_group_id = ` + listingGroupId + `;`, { type: QueryTypes.SELECT });
    return resultData;
}
async function getEventCount(_, { input: { userId, searches, from_date, to_date, day_of_week, ticket_type, genre, tags, in_hand, section, row, quantity, inventory_attached, held, coordinates, radius, days, category, performer, isfutureevents, hold, completedEvents = false } }) {
    try {

        let listingGroupUserIds = '';
        if (userId) {
            let listingGroupUserIdsResponse = await getUserIdsForListingGroupAccess(userId);
            if (listingGroupUserIdsResponse && listingGroupUserIdsResponse.length > 0) {
                listingGroupUserIds = listingGroupUserIdsResponse;
            }
        }
        var paginationCondition = '';
        var operationSelectQueryData = ', ';
        var operationJoinQuery = '';

        let eventIDsArray = [],
            whereStr = '',
            listingWhereStr = '',
            finalWhereStr = '',
            finalListingWhereStr = '';

        if (searches && searches != "" && searches != null && searches != 'null') {
            whereStr += '( ';
            if (Number.isInteger(parseInt(searches))) {
                whereStr += ' "event"."id" = ' + parseInt(searches) + ' or ';
            }
            whereStr += '"event"."name" iLike \'%' + searches + '%\'';
            whereStr += ' or "performer"."name" iLike \'%' + searches + '%\'';
            whereStr += ' or "venue"."name" iLike \'%' + searches + '%\'';
            whereStr += ' or concat("venue"."city", \', \', "venue"."country") iLike \'%' + searches + '%\'';
            whereStr += ' or "venue"."country" iLike \'%' + searches + '%\'';
            whereStr += ' or "eventtag"."tag_name" iLike \'%' + searches + '%\'';
            whereStr += ' or "listingtag"."tag_name" iLike \'%' + searches + '%\'';
            whereStr += ' )';
        }

        if ((from_date && from_date != "" && from_date != null && from_date != 'null')) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += ' ("event"."date" >= \'' + from_date + '\'';
            if (to_date && to_date != "" && to_date != null && to_date != 'null') {
                whereStr += ' and "event"."date" <= \'' + to_date + '\'';
            }
            whereStr += ' )';
        }
        if (isfutureevents) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += ' ("event"."date" >= CURRENT_TIMESTAMP ) ';
        }

        if (day_of_week && day_of_week != "" && day_of_week != null && day_of_week != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += `"event"."eventdate_day" in ( '` + day_of_week.join("','") + `'  )`;
        }

        if (ticket_type && ticket_type != "" && ticket_type != null && ticket_type != 'null' && Number.isInteger(parseInt(ticket_type))) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"listing_group"."ticket_type_id" = ' + ticket_type;
            listingWhereStr += '"listing_group"."ticket_type_id" = ' + ticket_type;
        }

        if (genre && genre != "" && genre != null && genre != 'null' && Number.isInteger(parseInt(genre))) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"event"."genre_id" = ' + genre;
        }

        if (category && category != "" && category != null && category != 'null' && Number.isInteger(parseInt(category))) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"event"."category_id" = ' + category;
        }

        if (performer && performer != "" && performer != null && performer != 'null' && Number.isInteger(parseInt(performer))) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += ' "event"."performer_id" = ' + performer + ' ';
        }

        if (tags && tags != "" && tags != null && tags != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }

            whereStr += '("eventtag"."id" in (' + tags.join() + ')';

            whereStr += ' or "listing_tags"."tag_id" in (' + tags.join() + '))';
            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }

            listingWhereStr += ` "listing_tags"."tag_id" in (${tags.join()}) `;


        }

        if (in_hand && in_hand != "" && in_hand != null && in_hand != 'null') {
            let in_hand_from = in_hand + ' 00:00:00';
            let in_hand_to = in_hand + ' 23:59:59';

            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += ' ("listing"."in_hand" >= \'' + in_hand_from + '\' and "listing"."in_hand" <= \'' + in_hand_to + '\')';
        }

        if (section && section != "" && section != null && section != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"listing_group"."section" iLike \'%' + section + '%\'';
            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += '"listing_group"."section" iLike \'%' + section + '%\'';
        }

        if (row && row != "" && row != null && row != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"listing_group"."row" iLike \'%' + row + '%\'';
            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += '"listing_group"."row" iLike \'%' + row + '%\'';
        }

        if (quantity && quantity != "" && quantity != null && quantity != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"listing_group"."quantity" = ' + quantity;
            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += '"listing_group"."quantity" = ' + quantity;
        }

        if (inventory_attached && inventory_attached != "" && inventory_attached != null && inventory_attached != 'null') {
            if (inventory_attached == commonVariables.yesValue) {
                if (listingWhereStr != "") {
                    listingWhereStr += ' and ';
                }
                listingWhereStr += '"listing"."pdf" != null';
            }
            else if (inventory_attached == commonVariables.noValue) {
                if (listingWhereStr != "") {
                    listingWhereStr += ' and ';
                }
                listingWhereStr += '"listing"."pdf" = null';
            }
        }

        let selectQuery = '';
        let distance = '';
        if (coordinates && coordinates != '' && radius && radius != '') {
            coordinates = JSON.parse(coordinates);
            selectQuery += ", array_agg(3959 * acos( cos( radians('" + coordinates.lat + "') ) * cos( radians( venue.latitude ) ) * cos( radians( longitude ) - radians('" + coordinates.lng + "') ) + sin( radians('" + coordinates.lat + "') ) * sin( radians( latitude ) ) ) ) AS distance";
            distance = " and distance <= '{" + radius + ".0}'  ";
        }

        if (days && days != "") {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += " event.date BETWEEN CURRENT_DATE AND CURRENT_DATE + INTERVAL '" + days + " days' ";
        }

        var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);

        if (whereStr != "") {
            finalWhereStr = ' and (' + whereStr + ')';
        } else {
            finalWhereStr = ' and listing_group.event_id = event.id ';
            if (listingGroupUserIds) {
                finalWhereStr += ' and listing_group.created_by in (' + listingGroupUserIds + ') ';
            }
        }

        if (whereStr == "" && qcListingStatus && qcListingStatus.id > 0) {
            finalWhereStr += ' and NOT( listing_group.listing_status_id = ' + qcListingStatus.id + ')';
        }

        if (!completedEvents || completedEvents == false) {
            finalWhereStr += ' and event.event_status_id != 1 ';
        }

        if (listingWhereStr != "") {
            finalListingWhereStr = ' and (' + listingWhereStr + ')';
        }
        if (hold && hold != '' && hold != null) {
            let holdStatus = hold == commonVariables.yesValue ? true : false;
            finalListingWhereStr = ' and listing.hold =' + holdStatus;
        }

        finalListingWhereStr += ' and is_removed = false ';

        if (listingGroupUserIds) {
            finalListingWhereStr += ' and listing_group.created_by in (' + listingGroupUserIds + ') ';
        }

        if (qcListingStatus && qcListingStatus.id > 0) {
            finalListingWhereStr += ' and NOT( listing_group.listing_status_id = ' + qcListingStatus.id + ')';
        }

        const eventDetails = await getEventDetails(selectQuery, finalWhereStr, paginationCondition, distance);
        for (var eventDtlCount = 0; eventDtlCount < eventDetails.length; eventDtlCount++) {
            eventIDsArray.push(eventDetails[eventDtlCount].event_id);
        }

        if (eventIDsArray.length > 0) {

            const listingDetails = await getListingDetails(operationSelectQueryData, operationJoinQuery, finalListingWhereStr);

            for (var eventDetailCount = 0; eventDetailCount < eventDetails.length; eventDetailCount++) {
                let listingsRes = [];
                for (var listingInfoCount = 0; listingInfoCount < listingDetails.length; listingInfoCount++) {
                    if (listingDetails[listingInfoCount].event_id == eventDetails[eventDetailCount].event_id) {
                        let obj = {};
                        obj['listingGroupId'] = listingDetails[listingInfoCount].listinggroupid;
                        obj['listingId'] = listingDetails[listingInfoCount].listingid;
                        obj['eventId'] = listingDetails[listingInfoCount].event_id;
                        obj['sold'] = listingDetails[listingInfoCount].sold;
                        obj['section'] = listingDetails[listingInfoCount].section;
                        obj['row'] = listingDetails[listingInfoCount].row;
                        obj['seatdtl'] = listingDetails[listingInfoCount].seatdtl;
                        obj['price'] = listingDetails[listingInfoCount].price;
                        obj['last_price'] = listingDetails[listingInfoCount].last_price;
                        obj['last_price_change'] = listingDetails[listingInfoCount].last_price_change;
                        obj['face_value'] = listingDetails[listingInfoCount].face_value;
                        obj['cost'] = listingDetails[listingInfoCount].cost;
                        obj['margin'] = listingDetails[listingInfoCount].margin;
                        obj['margintype'] = listingDetails[listingInfoCount].margintype;
                        obj['availabletkts'] = listingDetails[listingInfoCount].totalavailable;
                        obj['soldtkts'] = listingDetails[listingInfoCount].totalsold;
                        obj['tags'] = listingDetails[listingInfoCount].tag_name;
                        obj['tickettypename'] = listingDetails[listingInfoCount].tickettypename;
                        obj['active'] = listingDetails[listingInfoCount].active;
                        obj['inactive'] = listingDetails[listingInfoCount].inactive;
                        obj['listing_inactive'] = listingDetails[listingInfoCount].listing_inactive;

                        listingsRes.push(obj);
                    }
                }
                eventDetails[eventDetailCount].listingDtls = listingsRes;
            }
        }

        if (eventDetails) {
            return {
                "isError": false,
                "countInfo": {
                    "count": eventDetails.length
                }
            };
        } else {
            return {
                "isError": false,
                "countInfo": {
                    "count": 0
                }
            };
        }
    }
    catch (e) {
        return 'Error';
    }


}

/**
 * Page : MyTickets
 * Function For : Insert Listing Group Details
 * Ticket No : TIC 81
 */
async function insertListingGroupDetailsCSVFileUpload(userId, keyData, valueData) {
    let eventIdKey = keyData[0],
        eventNameKey = keyData[1],
        eventDateKey = keyData[2],
        ticketTypeKey = keyData[3],
        inHandDateKey = keyData[4],
        splitsIdKey = keyData[5],
        seatTypeKey = keyData[6],
        ticketingSystemIdKey = keyData[7],
        ticketingSystemNameKey = keyData[8],
        ticketingSystemAccountKey = keyData[9],
        hideSeatNumberKey = keyData[10],
        sectionKey = keyData[11],
        rowKey = keyData[12],
        quantityKey = keyData[13],
        seatStartKey = keyData[14],
        seatEndKey = keyData[15],
        groupPriceKey = keyData[16],
        faceValueKey = keyData[17],
        groupCostKey = keyData[18],
        unitCostKey = keyData[19],
        disclosuresKey = keyData[20],
        attributesKey = keyData[21],
        tagsKey = keyData[22],
        internalNotesKey = keyData[23],
        externalNotesKey = keyData[24],
        zoneSeatingKey = keyData[25],
        attachmentURLKey = keyData[26],
        purchaseDateKey = keyData[27],
        purchaseAmountKey = keyData[28],
        paymentTypeKey = keyData[29],
        paymentMethodKey = keyData[30],
        last4DigitsKey = keyData[31],
        referenceIdKey = keyData[32],
        salesTaxKey = keyData[33],
        shippingAndHandlingFeeKey = keyData[34],
        otherFeesKey = keyData[35],
        vendorIdKey = keyData[36],
        vendorNameKey = keyData[37],
        vendorEmailKey = keyData[38],
        vendorPhoneKey = keyData[39],
        vendorAddress1Key = keyData[40],
        vendorAddress2Key = keyData[41],
        vendorCityKey = keyData[42],
        vendorStateKey = keyData[43],
        vendorZipKey = keyData[44],
        vendorCountryKey = keyData[45],
        purchaseOrderNotesKey = keyData[46],
        purchaseOrderTagsKey = keyData[47];

    if (eventIdKey == uploadListingsColumnNames.eventId && eventNameKey == uploadListingsColumnNames.eventName && eventDateKey == uploadListingsColumnNames.eventDate && ticketTypeKey == uploadListingsColumnNames.ticketType && inHandDateKey == uploadListingsColumnNames.inHandDate && splitsIdKey == uploadListingsColumnNames.splitsId && seatTypeKey == uploadListingsColumnNames.seatType && ticketingSystemIdKey == uploadListingsColumnNames.ticketingSystemId && ticketingSystemNameKey == uploadListingsColumnNames.ticketingSystemName && ticketingSystemAccountKey == uploadListingsColumnNames.ticketingSystemAccount && hideSeatNumberKey == uploadListingsColumnNames.hideSeatNumber && sectionKey == uploadListingsColumnNames.section && rowKey == uploadListingsColumnNames.row && quantityKey == uploadListingsColumnNames.quantity && seatStartKey == uploadListingsColumnNames.seatStart && seatEndKey == uploadListingsColumnNames.seatEnd && groupPriceKey == uploadListingsColumnNames.groupPrice && faceValueKey == uploadListingsColumnNames.faceValue && groupCostKey == uploadListingsColumnNames.groupCost && unitCostKey == uploadListingsColumnNames.unitCost && disclosuresKey == uploadListingsColumnNames.disclosures && attributesKey == uploadListingsColumnNames.attributes && tagsKey == uploadListingsColumnNames.tags) {
        await module.exports.validateCSVFileUploadFields(userId, keyData, valueData);
    }
}

async function validateCSVFileUploadFields(userId, keyData, valueData) {
    let eventIdValue = valueData[0],
        eventNameValue = valueData[1],
        eventDateValue = valueData[2],
        ticketTypeValue = valueData[3],
        inHandDateValue = valueData[4],
        splitsIdValue = valueData[5],
        seatTypeValue = valueData[6],
        ticketingSystemIdValue = valueData[7],
        ticketingSystemNameValue = valueData[8],
        ticketingSystemAccountValue = valueData[9],
        hideSeatNumberValue = valueData[10],
        sectionValue = valueData[11],
        rowValue = valueData[12],
        quantityValue = valueData[13],
        seatStartValue = valueData[14],
        seatEndValue = valueData[15],
        groupPriceValue = valueData[16],
        faceValueValue = valueData[17],
        groupCostValue = valueData[18],
        unitCostValue = valueData[19],
        disclosuresValue = valueData[20],
        attributesValue = valueData[21],
        tagsValue = valueData[22],
        internalNotesValue = valueData[23],
        externalNotesValue = valueData[24],
        zoneSeatingValue = valueData[25],
        attachmentURLValue = valueData[26],
        purchaseDateValue = valueData[27],
        purchaseAmountValue = valueData[28],
        paymentTypeValue = valueData[29],
        paymentMethodValue = valueData[30],
        last4DigitsValue = valueData[31],
        referenceIdValue = valueData[32],
        salesTaxValue = valueData[33],
        shippingAndHandlingFeeValue = valueData[34],
        otherFeesValue = valueData[35],
        vendorIdValue = valueData[36],
        vendorNameValue = valueData[37],
        vendorEmailValue = valueData[38],
        vendorPhoneValue = valueData[39],
        vendorAddress1Value = valueData[40],
        vendorAddress2Value = valueData[41],
        vendorCityValue = valueData[42],
        vendorStateValue = valueData[43],
        vendorZipValue = valueData[44],
        vendorCountryValue = valueData[45],
        purchaseOrderNotesValue = valueData[46],
        purchaseOrderTagsValue = valueData[47];

    let isFieldValidationSuccess = false;
    if ((eventIdValue != "" || (eventNameValue != "" && eventDateValue != "")) && (ticketTypeValue != "" && ticketTypeValue == ticketTypes.eTicket) && inHandDateValue != "" && splitsIdValue != "" && seatTypeValue != "" && (ticketingSystemIdValue != "" || ticketingSystemNameValue != "") && sectionValue != "" && rowValue != "" && seatStartValue != "" && seatEndValue != "" && groupPriceValue != "" && groupPriceValue != null && parseFloat(groupPriceValue) > 0 && faceValueValue != "" && faceValueValue != null && parseFloat(faceValueValue) > 0)//eTicket
    {
        isFieldValidationSuccess = true;
    }
    else if ((eventIdValue != "" || (eventNameValue != "" && eventDateValue != "")) && (ticketTypeValue != "" && ticketTypeValue == ticketTypes.eTransfer) && inHandDateValue != "" && splitsIdValue != "" && seatTypeValue != "" && (ticketingSystemIdValue != "" || ticketingSystemNameValue != "") && ticketingSystemAccountValue != "" && sectionValue != "" && rowValue != "" && seatStartValue != "" && seatEndValue != "" && groupPriceValue != "" && groupPriceValue != null && parseFloat(groupPriceValue) > 0 && faceValueValue != "" && faceValueValue != null && parseFloat(faceValueValue) > 0)//eTransfer
    {
        isFieldValidationSuccess = true;
    }

    if (isFieldValidationSuccess) {
        var eventId = 0,
            ticketTypeId = 0,
            inHandDate = "",
            splitId = 0,
            seatTypeId = 0,
            ticketSystemId = 0,
            ticketSystemAccount = null,
            hideSeatNumber = false,
            section = sectionValue ? sectionValue : "",
            row = rowValue ? rowValue : "",
            quantity = 0,
            seatStart = seatStartValue,
            seatEnd = seatEndValue,
            groupPrice = parseFloat(groupPriceValue) ? parseFloat(groupPriceValue) : 0,
            faceValue = parseFloat(faceValueValue) ? parseFloat(faceValueValue) : 0,
            groupCost = parseFloat(groupCostValue) ? parseFloat(groupCostValue) : 0,
            unitCost = parseFloat(unitCostValue) ? parseFloat(unitCostValue) : 0,
            disclosures = "",
            attributes = "",
            tags = "",
            disclosureId = 0,
            disclosureIds = [],
            attributeId = 0,
            attributeIds = [],
            tagId = 0,
            tagIds = [];

        //Event Id
        if (eventIdValue == "") {
            eventId = await module.exports.getPrimaryIdFromNameDetails("name", "date", eventNameValue, eventDateValue, "id", "event");
        }
        else {
            eventId = eventIdValue;
        }

        //Ticket Type Id
        if (ticketTypeValue != "") {
            ticketTypeId = await module.exports.getPrimaryIdFromNameDetails("name", "", ticketTypeValue, "", "id", "ticket_types");
        }

        //In Hand Date 
        if (inHandDateValue != "") {
            let inHandDateArray = inHandDateValue.split("-");
            inHandDate = inHandDateArray[2] + "-" + inHandDateArray[1] + "-" + inHandDateArray[0];
        }

        //Split Id
        if (splitsIdValue != "") {
            splitId = await module.exports.getPrimaryIdFromNameDetails("name", "", splitsIdValue, "", "id", "splits");
        }

        //Seat Type Id
        if (seatTypeValue != "") {
            seatTypeId = await module.exports.getPrimaryIdFromNameDetails("name", "", seatTypeValue, "", "id", "seat_type");
        }

        //Ticket System Id
        if (ticketingSystemIdValue == "") {
            ticketSystemId = await module.exports.getPrimaryIdFromNameDetails("ticketing_system_name", "", ticketingSystemNameValue, "", "id", "ticketing_systems");
        }
        else {
            ticketSystemId = ticketingSystemIdValue;
        }

        //Ticket System Account Id
        if (ticketingSystemAccountValue != "" && ticketingSystemAccountValue != null) {
            ticketSystemAccount = await module.exports.getPrimaryIdFromNameDetails("account_name", "", ticketingSystemAccountValue, "", "id", "ticketing_system_accounts");
        }

        //Hide Seat Number
        if (hideSeatNumberValue != "") {
            hideSeatNumber = (hideSeatNumberValue == commonVariables.yesValue ? true : false);
        }

        //Quantity, Seat Start, Seat End
        if (quantityValue == "") {
            quantity = parseInt(seatEnd) - parseInt(seatStart) + 1;
        }
        else {
            quantity = quantityValue;
        }

        //Disclosures
        if (disclosuresValue != "") {
            disclosures = disclosuresValue.split(',');
            if (disclosures.length > 0) {
                for (var disclosureCount = 0; disclosureCount < disclosures.length; disclosureCount++) {
                    disclosureId = await module.exports.getPrimaryIdFromNameDetails("disclosure_name", "", disclosures[disclosureCount], "", "id", "disclosures");
                    if (disclosureId > 0) {
                        disclosureIds.push(disclosureId);
                    }
                }
            }
        }

        //Attributes
        if (attributesValue != "") {
            attributes = attributesValue.split(',');
            if (attributes.length > 0) {
                for (var attributeCount = 0; attributeCount < attributes.length; attributeCount++) {
                    attributeId = await module.exports.getPrimaryIdFromNameDetails("attribute_name", "", attributes[attributeCount], "", "id", "attributes");
                    if (attributeId > 0) {
                        attributeIds.push(attributeId);
                    }
                }
            }
        }

        //Tags
        if (tagsValue != "") {
            tags = tagsValue.split(',');
            if (tags.length > 0) {
                for (var tagCount = 0; tagCount < tags.length; tagCount++) {
                    tagId = await module.exports.getPrimaryIdFromNameDetails("tag_name", "", tags[tagCount], "", "id", "tags");
                    if (tagId > 0) {
                        tagIds.push(tagId);
                    }
                }
            }
        }

        if (eventId > 0 && ticketTypeId > 0 && inHandDate != "" && splitId > 0 && seatTypeId > 0 && ticketSystemId > 0 && ((disclosures != "" && disclosureIds.length > 0) || disclosures == "") && ((attributes != "" && attributeIds.length > 0) || attributes == "") && ((tags != "" && tagIds.length > 0) || tags == "")) {
            //Listing Group Details Starts Here

            var ListingGroupInputData = {
                //Common Data
                eventId: eventId,
                ticket_type_id: ticketTypeId,
                in_hand: inHandDate,
                splits_id: splitId,
                seat_type_id: seatTypeId,
                quantity: quantity,
                ticket_system_id: ticketSystemId,
                ticket_system_account: ticketSystemAccount,
                hide_seat_numbers: hideSeatNumber,

                //JSON Data Starts Here
                section: section,
                row: row,
                seat_start: seatStart,
                seat_end: seatEnd,
                group_price: groupPrice,
                face_value: faceValue,
                group_cost: groupCost,
                unit_cost: unitCost,
                //JSON Data Ends Here

                created_by: userId
            };

            const listingGroupData = await ListingGroup.create(ListingGroupInputData, { returning: ['id'] });
            if (listingGroupData) {
                let listingGroupId = listingGroupData.id;

                //Upload attachment 
                await module.exports.uploadCSVFileUploadListingAttachments(listingGroupId, attachmentURLValue);

                //Insert Listing Details
                await module.exports.insertListingDetailsCSVFileUpload(listingGroupId, seatTypeValue, quantityValue, seatStart, seatEnd, inHandDate, section, row, unitCost, faceValue);

                //Insert Disclosures
                await module.exports.insertListingGroupDisclosuresCSVFileUpload(listingGroupId, disclosures, disclosureIds);

                //Insert Attributes
                await module.exports.insertListingGroupAttributesCSVFileUpload(listingGroupId, attributes, attributeIds);

                //Insert Tags
                await module.exports.insertListingGroupTagsCSVFileUpload(listingGroupId, tags, tagIds);

                //Insert Purchase Order
                await module.exports.insertPurchaseOrderDetails(keyData, valueData, listingGroupId, userId);
            }
        }
    }
}

async function uploadCSVFileUploadListingAttachments(listingGroupId, attachmentURLValue) {
    //Listing Attachment URL
    if (attachmentURLValue != "" && attachmentURLValue != null) {
        var fileExtensionArray = attachmentURLValue.substring(attachmentURLValue.lastIndexOf("."));
        if (fileExtensionArray.indexOf("?") != -1) {
            var stringArray = fileExtensionArray.split('?');
            if (stringArray.length > 0) {
                fileExtensionArray = stringArray[0];
            }
        }

        var download = function (uri, filename, callback) {
            request.head(uri, function (err, res, body) {
                request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
            });
        };

        var temporaryFileName = 'mytestimage' + fileExtensionArray;

        download(attachmentURLValue, 'uploads/' + temporaryFileName, function () {
            //File Download done

            //S3 Storage Starts
            let curDate = new Date();
            let myDate = curDate.getDate();
            let myMonth = curDate.getMonth() + 1;
            let myYear = curDate.getFullYear();
            let myTime = curDate.getTime();

            let myCurDateTime = myDate + "" + myMonth + "" + myYear + "" + myTime;

            let attachmentfile_name = temporaryFileName ? (myCurDateTime + "_" + temporaryFileName) : '';
            fs.readFile('uploads/' + temporaryFileName, async function (err, data) {
                const attachment_file = attachmentfile_name;
                const folderName1 = folderNames.listingsFolder;
                const folderName2 = listingGroupId;//Primary ID"
                var bucketPath = process.env.AWS_BUCKETNAME + uploadURLs.uploadListingsURL + '/' + folderName1;
                if (folderName2 != "") {
                    bucketPath += '/' + folderName2;
                }
                const params = {
                    Body: data,
                    Bucket: bucketPath,
                    Key: attachment_file,
                };

                const upload = await s3.upload(params).promise();
                if (upload) {
                    if (folderName1 == folderNames.listingsFolder) {
                        var attachmentData = {
                            listing_group_id: listingGroupId,
                            attachment_name: temporaryFileName,
                            attachment_url: s3Path.basePath + upload.Bucket + '/' + upload.key,
                            active: 1
                        };

                        await Listingattachments.create(attachmentData, { returning: ['id'] });
                    }

                    fs.unlinkSync('uploads/' + temporaryFileName) //Delete temporary file stored in local system
                }
            });
            //S3 Storage Ends
        });
    }
}

async function insertListingGroupDetailsGeneralAdmissionCSVFileUpload(userId, keyData, valueData) {
    let eventIdKey = keyData[0],
        eventNameKey = keyData[1],
        eventDateKey = keyData[2],
        ticketTypeKey = keyData[3],
        inHandDateKey = keyData[4],
        splitsIdKey = keyData[5],
        seatTypeKey = keyData[6],
        ticketingSystemIdKey = keyData[7],
        ticketingSystemNameKey = keyData[8],
        ticketingSystemAccountKey = keyData[9],
        hideSeatNumberKey = keyData[10],
        sectionKey = keyData[11],
        rowKey = keyData[12],
        quantityKey = keyData[13],
        seatStartKey = keyData[14],
        seatEndKey = keyData[15],
        groupPriceKey = keyData[16],
        faceValueKey = keyData[17],
        groupCostKey = keyData[18],
        unitCostKey = keyData[19],
        disclosuresKey = keyData[20],
        attributesKey = keyData[21],
        tagsKey = keyData[22],
        internalNotesKey = keyData[23],
        externalNotesKey = keyData[24],
        zoneSeatingKey = keyData[25],
        attachmentURLKey = keyData[26],
        purchaseDateKey = keyData[27],
        purchaseAmountKey = keyData[28],
        paymentTypeKey = keyData[29],
        paymentMethodKey = keyData[30],
        last4DigitsKey = keyData[31],
        referenceIdKey = keyData[32],
        salesTaxKey = keyData[33],
        shippingAndHandlingFeeKey = keyData[34],
        otherFeesKey = keyData[35],
        vendorIdKey = keyData[36],
        vendorNameKey = keyData[37],
        vendorEmailKey = keyData[38],
        vendorPhoneKey = keyData[39],
        vendorAddress1Key = keyData[40],
        vendorAddress2Key = keyData[41],
        vendorCityKey = keyData[42],
        vendorStateKey = keyData[43],
        vendorZipKey = keyData[44],
        vendorCountryKey = keyData[45],
        purchaseOrderNotesKey = keyData[46],
        purchaseOrderTagsKey = keyData[47];

    if (eventIdKey == uploadListingsColumnNames.eventId && eventNameKey == uploadListingsColumnNames.eventName && eventDateKey == uploadListingsColumnNames.eventDate && ticketTypeKey == uploadListingsColumnNames.ticketType && inHandDateKey == uploadListingsColumnNames.inHandDate && splitsIdKey == uploadListingsColumnNames.splitsId && seatTypeKey == uploadListingsColumnNames.seatType && ticketingSystemIdKey == uploadListingsColumnNames.ticketingSystemId && ticketingSystemNameKey == uploadListingsColumnNames.ticketingSystemName && ticketingSystemAccountKey == uploadListingsColumnNames.ticketingSystemAccount && hideSeatNumberKey == uploadListingsColumnNames.hideSeatNumber && sectionKey == uploadListingsColumnNames.section && rowKey == uploadListingsColumnNames.row && quantityKey == uploadListingsColumnNames.quantity && seatStartKey == uploadListingsColumnNames.seatStart && seatEndKey == uploadListingsColumnNames.seatEnd && groupPriceKey == uploadListingsColumnNames.groupPrice && faceValueKey == uploadListingsColumnNames.faceValue && groupCostKey == uploadListingsColumnNames.groupCost && unitCostKey == uploadListingsColumnNames.unitCost && disclosuresKey == uploadListingsColumnNames.disclosures && attributesKey == uploadListingsColumnNames.attributes && tagsKey == uploadListingsColumnNames.tags) {
        await module.exports.validateCSVFileUploadFieldsGeneralAdmission(userId, keyData, valueData);
    }
}

async function validateCSVFileUploadFieldsGeneralAdmission(userId, keyData, valueData) {
    let eventIdValue = valueData[0],
        eventNameValue = valueData[1],
        eventDateValue = valueData[2],
        ticketTypeValue = valueData[3],
        inHandDateValue = valueData[4],
        splitsIdValue = valueData[5],
        seatTypeValue = valueData[6],
        ticketingSystemIdValue = valueData[7],
        ticketingSystemNameValue = valueData[8],
        ticketingSystemAccountValue = valueData[9],
        hideSeatNumberValue = valueData[10],
        sectionValue = valueData[11],
        rowValue = valueData[12],
        quantityValue = valueData[13],
        seatStartValue = valueData[14],
        seatEndValue = valueData[15],
        groupPriceValue = valueData[16],
        faceValueValue = valueData[17],
        groupCostValue = valueData[18],
        unitCostValue = valueData[19],
        disclosuresValue = valueData[20],
        attributesValue = valueData[21],
        tagsValue = valueData[22],
        internalNotesValue = valueData[23],
        externalNotesValue = valueData[24],
        zoneSeatingValue = valueData[25],
        attachmentURLValue = valueData[26],
        purchaseDateValue = valueData[27],
        purchaseAmountValue = valueData[28],
        paymentTypeValue = valueData[29],
        paymentMethodValue = valueData[30],
        last4DigitsValue = valueData[31],
        referenceIdValue = valueData[32],
        salesTaxValue = valueData[33],
        shippingAndHandlingFeeValue = valueData[34],
        otherFeesValue = valueData[35],
        vendorIdValue = valueData[36],
        vendorNameValue = valueData[37],
        vendorEmailValue = valueData[38],
        vendorPhoneValue = valueData[39],
        vendorAddress1Value = valueData[40],
        vendorAddress2Value = valueData[41],
        vendorCityValue = valueData[42],
        vendorStateValue = valueData[43],
        vendorZipValue = valueData[44],
        vendorCountryValue = valueData[45],
        purchaseOrderNotesValue = valueData[46],
        purchaseOrderTagsValue = valueData[47];

    let isFieldValidationSuccess = false;
    if ((eventIdValue != "" || (eventNameValue != "" && eventDateValue != "")) && (ticketTypeValue != "" && ticketTypeValue == ticketTypes.eTicket) && inHandDateValue != "" && splitsIdValue != "" && seatTypeValue != "" && (ticketingSystemIdValue != "" || ticketingSystemNameValue != "") && quantityValue != "" && groupPriceValue != "" && groupPriceValue != null && parseFloat(groupPriceValue) > 0 && faceValueValue != "" && faceValueValue != null && parseFloat(faceValueValue) > 0)//eTicket
    {
        isFieldValidationSuccess = true;
    }
    else if ((eventIdValue != "" || (eventNameValue != "" && eventDateValue != "")) && (ticketTypeValue != "" && ticketTypeValue == ticketTypes.eTransfer) && inHandDateValue != "" && splitsIdValue != "" && seatTypeValue != "" && (ticketingSystemIdValue != "" || ticketingSystemNameValue != "") && quantityValue != "" && groupPriceValue != "" && groupPriceValue != null && parseFloat(groupPriceValue) > 0 && faceValueValue != "" && faceValueValue != null && parseFloat(faceValueValue) > 0)//eTransfer
    {
        isFieldValidationSuccess = true;
    }

    if (isFieldValidationSuccess) {
        var eventId = 0,
            ticketTypeId = 0,
            inHandDate = "",
            splitId = 0,
            seatTypeId = 0,
            ticketSystemId = 0,
            ticketSystemAccount = null,
            hideSeatNumber = false,
            section = "NA",//Since it is GA Seat Type
            row = "NA",//Since it is GA Seat Type
            quantity = quantityValue ? quantityValue : 0,//Since it is GA Seat Type
            seatStart = 0,//Since it is GA Seat Type
            seatEnd = 0,//Since it is GA Seat Type
            groupPrice = parseFloat(groupPriceValue) ? parseFloat(groupPriceValue) : 0,
            faceValue = parseFloat(faceValueValue) ? parseFloat(faceValueValue) : 0,
            groupCost = parseFloat(groupCostValue) ? parseFloat(groupCostValue) : 0,
            unitCost = parseFloat(unitCostValue) ? parseFloat(unitCostValue) : 0,
            disclosures = "",
            attributes = "",
            tags = "",
            disclosureId = 0,
            disclosureIds = [],
            attributeId = 0,
            attributeIds = [],
            tagId = 0,
            tagIds = [];

        //Event Id
        if (eventIdValue == "") {
            eventId = await module.exports.getPrimaryIdFromNameDetails("name", "date", eventNameValue, eventDateValue, "id", "event");
        }
        else {
            eventId = eventIdValue;
        }

        //Ticket Type Id
        if (ticketTypeValue != "") {
            ticketTypeId = await module.exports.getPrimaryIdFromNameDetails("name", "", ticketTypeValue, "", "id", "ticket_types");
        }

        //In Hand Date 
        if (inHandDateValue != "") {
            let inHandDateArray = inHandDateValue.split("-");
            inHandDate = inHandDateArray[2] + "-" + inHandDateArray[1] + "-" + inHandDateArray[0];
        }

        //Split Id
        if (splitsIdValue != "") {
            splitId = await module.exports.getPrimaryIdFromNameDetails("name", "", splitsIdValue, "", "id", "splits");
        }

        //Seat Type Id
        if (seatTypeValue != "") {
            seatTypeId = await module.exports.getPrimaryIdFromNameDetails("name", "", seatTypeValue, "", "id", "seat_type");
        }

        //Ticket System Id
        if (ticketingSystemIdValue == "") {
            ticketSystemId = await module.exports.getPrimaryIdFromNameDetails("ticketing_system_name", "", ticketingSystemNameValue, "", "id", "ticketing_systems");
        }
        else {
            ticketSystemId = ticketingSystemIdValue;
        }

        if (ticketingSystemAccountValue != "") {
            ticketSystemAccount = await module.exports.getPrimaryIdFromNameDetails("account_name", "", ticketingSystemAccountValue, "", "id", "ticketing_system_accounts");
        }

        //Hide Seat Number
        if (hideSeatNumberValue != "") {
            hideSeatNumber = (hideSeatNumberValue == commonVariables.yesValue ? true : false);
        }

        //Disclosures
        if (disclosuresValue != "" && disclosuresValue != null) {
            disclosures = disclosuresValue.split(',');
            if (disclosures.length > 0) {
                for (var disclosureCount = 0; disclosureCount < disclosures.length; disclosureCount++) {
                    disclosureId = await module.exports.getPrimaryIdFromNameDetails("disclosure_name", "", disclosures[disclosureCount], "", "id", "disclosures");
                    if (disclosureId > 0) {
                        disclosureIds.push(disclosureId);
                    }
                }
            }
        }

        //Attributes
        if (attributesValue != "" && attributesValue != null) {
            attributes = attributesValue.split(',');
            if (attributes.length > 0) {
                for (var attributeCount = 0; attributeCount < attributes.length; attributeCount++) {
                    attributeId = await module.exports.getPrimaryIdFromNameDetails("attribute_name", "", attributes[attributeCount], "", "id", "attributes");
                    if (attributeId > 0) {
                        attributeIds.push(attributeId);
                    }
                }
            }
        }

        //Tags
        if (tagsValue != "" && tagsValue != null) {
            tags = tagsValue.split(',');
            if (tags.length > 0) {
                for (var tagCount = 0; tagCount < tags.length; tagCount++) {
                    tagId = await module.exports.getPrimaryIdFromNameDetails("tag_name", "", tags[tagCount], "", "id", "tags");
                    if (tagId > 0) {
                        tagIds.push(tagId);
                    }
                }
            }
        }

        if (eventId > 0 && ticketTypeId > 0 && inHandDate != "" && splitId > 0 && seatTypeId > 0 && ticketSystemId > 0 && ((disclosures != "" && disclosureIds.length > 0) || disclosures == "") && ((attributes != "" && attributeIds.length > 0) || attributes == "") && ((tags != "" && tagIds.length > 0) || tags == "")) {
            //Listing Group Details Starts Here

            var ListingGroupInputData = {
                //Common Data
                eventId: eventId,
                ticket_type_id: ticketTypeId,
                in_hand: inHandDate,
                splits_id: splitId,
                seat_type_id: seatTypeId,
                quantity: quantity,
                ticket_system_id: ticketSystemId,
                ticket_system_account: ticketSystemAccount,
                hide_seat_numbers: hideSeatNumber,

                //JSON Data Starts Here
                section: section,
                row: row,
                seat_start: seatStart,
                seat_end: seatEnd,
                group_price: groupPrice,
                face_value: faceValue,
                group_cost: groupCost,
                unit_cost: unitCost,
                //JSON Data Ends Here

                created_by: userId
            };

            const listingGroupData = await ListingGroup.create(ListingGroupInputData, { returning: ['id'] });
            if (listingGroupData) {
                let listingGroupId = listingGroupData.id;

                //Insert Listing Details
                await module.exports.insertListingDetailsCSVFileUpload(listingGroupId, seatTypeValue, quantityValue, seatStart, seatEnd, inHandDate, section, row, unitCost, faceValue);

                //Insert Disclosures
                await module.exports.insertListingGroupDisclosuresCSVFileUpload(listingGroupId, disclosures, disclosureIds);

                //Insert Attributes
                await module.exports.insertListingGroupAttributesCSVFileUpload(listingGroupId, attributes, attributeIds);

                //Insert Tags
                await module.exports.insertListingGroupTagsCSVFileUpload(listingGroupId, tags, tagIds);

                //Insert Purchase Order
                await module.exports.insertPurchaseOrderDetails(keyData, valueData, listingGroupId, userId);
            }
        }
    }
}
async function insertListingDetailsCSVFileUpload(listingGroupId, seatTypeValue, quantity, seatStart, seatEnd, inHandDate, section, row, unitCost, faceValue) {
    if (seatTypeValue == seatTypes.generalAdmission) {
        for (let quantityGACount = 0; quantityGACount < parseInt(quantity); quantityGACount++) {
            let listingInputData = {
                listingGroupId: listingGroupId,
                in_hand: inHandDate,
                section: section,
                row: row,
                seat_number: 0,
                sold: false,
                delivered: false,
                pdf: "yes",//since it is eTicket
                unit_cost: unitCost,
                price: 0,
                face_value: faceValue,
            };
            await Listings.create(listingInputData);
        }
    }
    else {
        if (seatStart != "" && seatEnd != "") {
            for (var seatInfoCount = parseInt(seatStart); seatInfoCount <= parseInt(seatEnd); seatInfoCount++) {
                let listingInputData = {
                    listingGroupId: listingGroupId,
                    in_hand: inHandDate,
                    section: section,
                    row: row,
                    seat_number: seatInfoCount,
                    sold: false,
                    delivered: false,
                    pdf: "yes", //since it is eTicket
                    unit_cost: unitCost,
                    price: 0,
                    face_value: faceValue,
                };
                const listingGroupData = await Listings.create(listingInputData);
            }
        }
    }
}

async function insertListingGroupDisclosuresCSVFileUpload(listingGroupId, disclosures, disclosureIds) {
    if (disclosures != "" && disclosureIds.length > 0) {
        for (var disclosureCount = 0; disclosureCount < disclosureIds.length; disclosureCount++) {
            var disclosuresInputData = {
                listingGroupId: listingGroupId,
                disclosure_id: disclosureIds[disclosureCount]
            };
            await ListingDisclosures.create(disclosuresInputData, { returning: ['id'] });
        }
    }
}

async function insertListingGroupAttributesCSVFileUpload(listingGroupId, attributes, attributeIds) {
    if (attributes != "" && attributeIds.length > 0) {
        for (var attributeCount = 0; attributeCount < attributeIds.length; attributeCount++) {
            var attributesInputData = {
                listingGroupId: listingGroupId,
                attribute_id: attributeIds[attributeCount]
            };
            await ListingAttributes.create(attributesInputData, { returning: ['id'] });
        }
    }
}

async function insertListingGroupTagsCSVFileUpload(listingGroupId, tags, tagIds) {
    if (tags != "" && tagIds.length > 0) {
        for (var tagCount = 0; tagCount < tagIds.length; tagCount++) {
            var tagsInputData = {
                listing_group_id: listingGroupId,
                tag_id: tagIds[tagCount]
            };
            await ListingTags.create(tagsInputData, { returning: ['id'] });
        }
    }
}

async function insertPurchaseOrderDetails(keyData, valueData, listingGroupId, userId) {
    let eventIdKey = keyData[0],
        eventNameKey = keyData[1],
        eventDateKey = keyData[2],
        ticketTypeKey = keyData[3],
        inHandDateKey = keyData[4],
        splitsIdKey = keyData[5],
        seatTypeKey = keyData[6],
        ticketingSystemIdKey = keyData[7],
        ticketingSystemNameKey = keyData[8],
        ticketingSystemAccountKey = keyData[9],
        hideSeatNumberKey = keyData[10],
        sectionKey = keyData[11],
        rowKey = keyData[12],
        quantityKey = keyData[13],
        seatStartKey = keyData[14],
        seatEndKey = keyData[15],
        groupPriceKey = keyData[16],
        faceValueKey = keyData[17],
        groupCostKey = keyData[18],
        unitCostKey = keyData[19],
        disclosuresKey = keyData[20],
        attributesKey = keyData[21],
        tagsKey = keyData[22],
        internalNotesKey = keyData[23],
        externalNotesKey = keyData[24],
        zoneSeatingKey = keyData[25],
        attachmentURLKey = keyData[26],
        purchaseDateKey = keyData[27],
        purchaseAmountKey = keyData[28],
        paymentTypeKey = keyData[29],
        paymentMethodKey = keyData[30],
        last4DigitsKey = keyData[31],
        referenceIdKey = keyData[32],
        salesTaxKey = keyData[33],
        shippingAndHandlingFeeKey = keyData[34],
        otherFeesKey = keyData[35],
        vendorIdKey = keyData[36],
        vendorNameKey = keyData[37],
        vendorEmailKey = keyData[38],
        vendorPhoneKey = keyData[39],
        vendorAddress1Key = keyData[40],
        vendorAddress2Key = keyData[41],
        vendorCityKey = keyData[42],
        vendorStateKey = keyData[43],
        vendorZipKey = keyData[44],
        vendorCountryKey = keyData[45],
        purchaseOrderNotesKey = keyData[46],
        purchaseOrderTagsKey = keyData[47];

    let eventIdValue = valueData[0],
        eventNameValue = valueData[1],
        eventDateValue = valueData[2],
        ticketTypeValue = valueData[3],
        inHandDateValue = valueData[4],
        splitsIdValue = valueData[5],
        seatTypeValue = valueData[6],
        ticketingSystemIdValue = valueData[7],
        ticketingSystemNameValue = valueData[8],
        ticketingSystemAccountValue = valueData[9],
        hideSeatNumberValue = valueData[10],
        sectionValue = valueData[11],
        rowValue = valueData[12],
        quantityValue = valueData[13],
        seatStartValue = valueData[14],
        seatEndValue = valueData[15],
        groupPriceValue = valueData[16],
        faceValueValue = valueData[17],
        groupCostValue = valueData[18],
        unitCostValue = valueData[19],
        disclosuresValue = valueData[20],
        attributesValue = valueData[21],
        tagsValue = valueData[22],
        internalNotesValue = valueData[23],
        externalNotesValue = valueData[24],
        zoneSeatingValue = valueData[25],
        attachmentURLValue = valueData[26],
        purchaseDateValue = valueData[27],
        purchaseAmountValue = valueData[28],
        paymentTypeValue = valueData[29],
        paymentMethodValue = valueData[30],
        last4DigitsValue = valueData[31],
        referenceIdValue = valueData[32],
        salesTaxValue = valueData[33],
        shippingAndHandlingFeeValue = valueData[34],
        otherFeesValue = valueData[35],
        vendorIdValue = valueData[36],
        vendorNameValue = valueData[37],
        vendorEmailValue = valueData[38],
        vendorPhoneValue = valueData[39],
        vendorAddress1Value = valueData[40],
        vendorAddress2Value = valueData[41],
        vendorCityValue = valueData[42],
        vendorStateValue = valueData[43],
        vendorZipValue = valueData[44],
        vendorCountryValue = valueData[45],
        purchaseOrderNotesValue = valueData[46],
        purchaseOrderTagsValue = valueData[47];

    if (purchaseDateKey == uploadListingsColumnNames.purchaseDate && purchaseAmountKey == uploadListingsColumnNames.purchaseAmount && referenceIdKey == uploadListingsColumnNames.referenceId) {
        if (purchaseDateValue != "" && purchaseAmountValue != "" && referenceIdValue != "") {
            let vendorId = 0,
                paymentMethodId = 0,
                paymentTypeId = 0;
            //Vendor ID
            if (vendorIdValue == "") {
                let vendorName = vendorNameValue,
                    vendorAddress1 = vendorAddress1Value;

                if (vendorName != "") {
                    vendorId = await module.exports.getPrimaryIdFromNameDetails("name", "address", vendorName, vendorAddress1, "id", "vendor");
                }
            }

            //Payment Method
            if (paymentMethodValue != "") {
                paymentMethodId = await module.exports.getPrimaryIdFromNameDetails("name", "", paymentMethodValue, "", "id", "payment_method");
            }

            //Payment Type
            if (paymentTypeValue != "") {
                paymentTypeId = await module.exports.getPrimaryIdFromNameDetails("name", "", paymentTypeValue, "", "id", "payment_type");
            }

            var purchaseOrderDetails = {};
            purchaseOrderDetails.user_id = parseInt(userId);
            purchaseOrderDetails.vendor_id = vendorId;
            purchaseOrderDetails.purchase_date = purchaseDateValue;
            purchaseOrderDetails.payment_method_id = paymentMethodId;
            purchaseOrderDetails.sales_tax = salesTaxValue ? parseFloat(salesTaxValue) : 0;
            purchaseOrderDetails.purchase_amount = parseFloat(purchaseAmountValue);
            purchaseOrderDetails.payment_last4_digits = last4DigitsValue;
            purchaseOrderDetails.shipping_and_handling_fee = shippingAndHandlingFeeValue ? parseFloat(shippingAndHandlingFeeValue) : 0;
            purchaseOrderDetails.payment_type_id = paymentTypeId;
            purchaseOrderDetails.external_reference = referenceIdValue;
            purchaseOrderDetails.fees = otherFeesValue ? parseFloat(otherFeesValue) : 0;
            purchaseOrderDetails.notes = purchaseOrderNotesValue ? purchaseOrderNotesValue : "";
            purchaseOrderDetails.currency_id = 1;//TODO
            purchaseOrderDetails.created_by = parseInt(userId);
            purchaseOrderDetails.active = true;
            purchaseOrderDetails.listing_group_id = listingGroupId;

            var purchaseOrderData = await Purchaseorder.create(purchaseOrderDetails, { returning: ['id'] });

            //Insert Purchase Order Listing Details
            await module.exports.insertPurchaseOrderListingDetails(listingGroupId, purchaseOrderData.id);

            //Insert Purchase Order Tags
            await module.exports.insertPurchaseOrderTagDetails(userId, purchaseOrderData.id, purchaseOrderTagsValue);

            /* //Attachment Details: TODO
            if (InvoiceAttachmentInformationsJSON) {
                var url = s3Path.basePath + process.env.AWS_BUCKETNAME + '/' + folderNames.purchaseOrderFolder + '/' + purchaseOrderData.id + '/' + InvoiceAttachmentInformationsJSON.InvoiceAttachName;

                let attachInputData = {
                    purchase_order_id: purchaseOrderData.id,
                    created_by: purchaseOrderData.user_id,
                    attachment_url: url,
                };
                await Purchaseorderinvoiceattachment.create(attachInputData, { returning: ['id'] });
            } */
        }
    }
}

async function insertPurchaseOrderListingDetails(listingGroupId, purchaseOrderId) {
    let listingDetails = await module.exports.getListingDetailsBasedOnListingGroupId(listingGroupId);
    if (listingDetails && listingDetails.length > 0) {
        for (var listingCount = 0; listingCount < listingDetails.length; listingCount++) {
            let purchaseOrderListingData = {
                purchase_order_id: purchaseOrderId,
                listing_id: parseInt(listingDetails[listingCount].id)
            };
            await Purchaseorderlisting.create(purchaseOrderListingData, { returning: ['id'] });
        }
    }
}

async function insertPurchaseOrderTagDetails(userId, purchaseOrderId, purchaseOrderTagsValue) {
    if (purchaseOrderTagsValue != "") {
        let purchaseOrderTagId = await module.exports.getPrimaryIdFromNameDetails("tag_name", "", purchaseOrderTagsValue, "", "id", "tags");//Now tags is single value

        if (purchaseOrderTagId > 0) {
            let tagInputData = {
                purchase_order_id: purchaseOrderId,
                created_by: userId,
                tag_id: purchaseOrderTagId
            };
            await Purchaseordertag.create(tagInputData, { returning: ['id'] });
        }
    }
}

/**
 * Page : MyTickets 
 * Function For : Check in-hand date are equal or not
 * Ticket No : TIC 615
 */
async function checkInhandDateIsEqual(_, { input: { eventId } }) {
    const listingGroupOutput = await ListingGroup.findAll({
        attributes: ['in_hand'],
        where: { event_id: eventId },
        group: ['in_hand']
    });
    var inHandResponse = false; var inHandDate = null;
    if (listingGroupOutput && listingGroupOutput[0].in_hand != null && listingGroupOutput.length === 1) {

        inHandResponse = true; inHandDate = listingGroupOutput[0].in_hand;
    }
    return { response: inHandResponse, inHandDate: inHandDate };
}


/**
 * Page : Opearation 
 * Function For : Update Seat Type
 * Ticket No : TIC 735
 */
async function updateSeatType(_, { input: { selectedTicketTypes, selectedListingId } }) {
    if (selectedListingId) {

        var listingGroupIds = [];
        for (var listingGroupCount = 0; listingGroupCount < selectedListingId.length; listingGroupCount++) {
            const listingGroupDtls = await ListingGroup.findOne({
                attributes: ['id'],
                where: { id: selectedListingId[listingGroupCount] }
            });
            if (listingGroupDtls && listingGroupDtls.id) {
                await listingGroupIds.push(listingGroupDtls.id);
            }
        }

        var finalResJSON = {};
        if (listingGroupIds) {
            const seatTypeDetail = await Seattype.findOne({
                attributes: ['id'],
                where: { name: mailParser.seatTypes.general_admission },
                raw:true
            });

            var updateListData = {
                seat_type_id: selectedTicketTypes,
            };

            if (selectedTicketTypes == seatTypeDetail.id ) {
                updateListData.seat_start = 0,
                updateListData.seat_end = 0,
                updateListData.section = "NA",
                updateListData.row = "NA"
            }

            await ListingGroup.update(
                updateListData,
                { where: { id: { [Op.in]: listingGroupIds } } }
            )
            
            finalResJSON["isError"] = false;
            finalResJSON["msg"] = "Listing Group Seat Type Updated Successfully.";
        } else {
            finalResJSON["isError"] = true;
            finalResJSON["msg"] = "Some Error in Updating Listing Group.";

        }
        return finalResJSON;
    }

}

/**
 * Page : Opearation 
 * Function For : Update Stock Type
 * Ticket No : TIC 736
 */
async function updateStockType(_, { input: { selectedStockTypes, selectedListingId } }) {
    if (selectedListingId) {

        var listingGroupIds = [];
        for (var listingGroupCount = 0; listingGroupCount < selectedListingId.length; listingGroupCount++) {
            const listingGroupDtls = await ListingGroup.findOne({
                attributes: ['id'],
                where: { id: selectedListingId[listingGroupCount] }
            });
            if (listingGroupDtls && listingGroupDtls.id) {
                await listingGroupIds.push(listingGroupDtls.id);
            }
        }
        var finalResJSON = {};
        if (listingGroupIds) {
            await ListingGroup.update(
                { ticket_type_id: selectedStockTypes },
                { where: { id: { [Op.in]: listingGroupIds } } }
            )
            finalResJSON["isError"] = false;
            finalResJSON["msg"] = "Listing Group Stock Type Updated Successfully.";
        } else {
            finalResJSON["isError"] = true;
            finalResJSON["msg"] = "Some Error in Updating Listing Group.";

        }
        return finalResJSON;
    }

}

/**
 * Page : Opearation 
 * Function For : Update unit cost
 * Ticket No : TIC 734
 */
async function updateUnitCost(_, { input: { listingGroupId, unitCost } }) {
    if (listingGroupId && listingGroupId.length) {
        var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);
        var result;
        for (let list of listingGroupId) {
            let listingUpdate = {
                unit_cost: unitCost,
            };
            var quantity = 0;
            const listingGroupDetail = await ListingGroup.findOne({
                attributes: ['id', 'quantity'],
                where: { id: list },
                raw: true
            });
            if (listingGroupDetail) {
                quantity = listingGroupDetail.quantity
            }
            let listingGroupUpdate = {
                unit_cost: unitCost,
                group_cost: unitCost * quantity,
            };
            result = await ListingGroup.update(
                listingGroupUpdate,
                { where: { id: list, listing_status_id: qcListingStatus.id } },
                { returning: ['id'] }
            )

            result = await Listings.update(
                listingUpdate,
                { where: { listing_group_id: list, listing_status_id: qcListingStatus.id } },
                { returning: ['id'] }
            )
        }

        var finalResJSON = {};
        if (result) {
            finalResJSON["isError"] = false;
            finalResJSON["msg"] = "Unit cost Updated Successfully.";
        } else {
            finalResJSON["isError"] = true;
            finalResJSON["msg"] = "Some Error in Updating Unit cost.";

        }
        return finalResJSON;
    }
}

async function bulkUpdateNotesAndTags(_, { input: { listingGroupId, internalNotes, selectedTags } }) {
    var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);
    var result;
    result = await ListingGroup.update(
        {
            internal_notes: internalNotes
        },
        {
            where: { id: { [Op.in]: listingGroupId }, listing_status_id: qcListingStatus.id }
        }
    )

    for (let list of listingGroupId) {
        await module.exports.commonTagUpdate(list, selectedTags)
    }

    if (result) {
        return { 'isError': false, 'msg': "Listing Group Internal Notes Updated Successfully." };
    }
    else {
        return { 'isError': true, 'msg': "Some Error in Updating Listing Group." };
    }

}

async function commonTagUpdate(listingGroupId, selectedTags) {

    if (selectedTags.length && listingGroupId > 0) {
        let listingTagsData = await ListingTags.findAll({ where: { listing_group_id: listingGroupId }, attributes: ['tag_id'], raw: true });
        //Tags Already Saved In Database
        let tagsDataInDb = [];
        listingTagsData.map(tags => {
            tagsDataInDb.push(parseInt(tags.tag_id));
        })

        //Current Tags Selected
        let tagsIds = [];
        selectedTags = JSON.parse(selectedTags);
        selectedTags.map(tags => {
            tagsIds.push(parseInt(tags['value']));
        })

        //Tags To Insert New
        let insertTagData = tagsIds.filter(x => tagsDataInDb.indexOf(x) === -1);
        if (insertTagData.length) {
            for (var tagDataCount = 0; tagDataCount < insertTagData.length; tagDataCount++) {
                var tagsInputData = {
                    listing_group_id: listingGroupId,
                    tag_id: insertTagData[tagDataCount]
                };
                await ListingTags.create(tagsInputData, { returning: ['id'] });
            }
        }

        //Tags to Delete
        let deleteTagData = tagsDataInDb.filter(x => tagsIds.indexOf(x) === -1);
        if (deleteTagData.length) {
            await ListingTags.destroy({ where: { listing_group_id: listingGroupId, tag_id: { [Op.in]: deleteTagData } } });
        }
    }
}

/**
 * Page : Opearation 
 * Function For : Update disclosure and external notes
 * Ticket No : TIC 677
 */
async function updateDisclosureInBulk(_, { input: { listingGroupId, disclosures, externalNotes } }) {
    var listingGroupIds = listingGroupId;
    var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);

    await ListingDisclosures.destroy({ where: { listingGroupId: { [Op.in]: listingGroupIds } } })
    let isError = false;
    for (var listingGroupIdCount = 0; listingGroupIdCount < listingGroupIds.length; listingGroupIdCount++) {
        for (var disclosuresCount = 0; disclosuresCount < disclosures.length; disclosuresCount++) {
            var inputData = {
                listingGroupId: listingGroupIds[listingGroupIdCount],
                disclosure_id: disclosures[disclosuresCount]
            };
            const result = await ListingDisclosures.create(inputData, {
                returning: ['id']
            })

            if (!result) {
                isError = true;
                break;
            }
        }
    }

    if (externalNotes != "") {
        await ListingGroup.update(
            {
                external_notes: externalNotes
            },
            {
                where: { id: { [Op.in]: listingGroupIds }, listing_status_id: qcListingStatus.id }
            }
        )
    }

    if (isError) {
        return { 'isError': true, 'msg': "Some Error in Updating Listing Group." };
    }
    else {
        return { 'isError': false, 'msg': "Listing Group Disclosures Created Successfully." };
    }

}
/**
 * Page : Opearation 
 * Function For : Update disclosure and external notes
 * Ticket No : TIC 677
 */
async function updateSplitsInBulk(_, { input: { listingGroupId, splitId } }) {

    var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);
    if (splitId && splitId > 0) {
        let isError = false;
        for (var listId of listingGroupId) {
            const result = await ListingGroup.update(
                {
                    splits_id: splitId
                },
                {
                    where: { id: listId, listing_status_id: qcListingStatus.id }
                }
            )

            if (!result) {
                isError = true;
                break;
            }
        }

        if (isError) {
            return { 'isError': true, 'msg': "Some Error in Updating Listing Group." };
        }
        else {
            return { 'isError': false, 'msg': "Listing Group Splits Updated Successfully." };
        }
    } else {
        return { 'isError': true, 'msg': "Some Error in Updating Listing Group." };
    }
}


async function updateViagogoStatus(_, { input: { listingGroupId, listing_status_id } }) {
    var finalResJSON = {};
    try {

        await ListingGroup.update({ listing_status_id: listing_status_id },
            { where: { id: { [Op.in]: listingGroupId } } }
        );
        await Listings.update({ listing_group_id: listing_status_id },
            { where: { listing_group_id: { [Op.in]: listingGroupId } } }
        );
        finalResJSON["isError"] = false;
        finalResJSON["msg"] = "Listing Group Viagogo status updated Successfully.";

    } catch (error) {
        finalResJSON["isError"] = true;
        finalResJSON["msg"] = "Listing Group Viagogo status updation failed." + error;
    }
    return finalResJSON;
}

async function allIssueTypes() {
    const allTypes = await IssueTypes.findAll({
        attributes: [
            ['id', 'value'],
            ['name', 'label'],
        ]
    });

    if (allTypes) {
        return JSON.parse(JSON.stringify(allTypes));
    } else {
        return "No data";
    }
}

async function createIssueFlag(_, { input: { listingGroupId, issueValues, event_id } }) {
    var finalResJSON = {};

    //TODO notifications on issue create
    try {
        // const checkIssueExists = await Issues.findAll({
        //     attributes: ['id', 'issue_type_id', 'listing_group_id', 'event_id', 'assignee','created_at'],
        //     where: { listing_group_id: { [Op.in]: listingGroupId }  }
        // });
        let batchInsert = []
        for (const multiInsert of listingGroupId) {
            batchInsert.push({
                issue_type_id: issueValues.issue_type_id,
                assignee: issueValues.assignee,
                assigner: issueValues.assigner,
                listing_group_id: multiInsert,
                event_id: event_id,
                created_by: issueValues.assigner,
                created_at: new Date(),
                updated_at: new Date()
            })
        }
        await Issues.bulkCreate(batchInsert, { returning: ['id'] });
        finalResJSON["isError"] = false;
        finalResJSON["msg"] = "Issue flag created Successfully.";

    } catch (error) {
        finalResJSON["isError"] = true;
        finalResJSON["msg"] = "Issue flag creation failed.";
    }
    return finalResJSON;
}
module.exports = {
    allTicketTypes,
    getAllEventIdPerformerVenueCityCountryTags,
    allGenreDetails,
    allCategoryDetails,
    addListingsFilter,
    allTags,
    allTicketingSystems,
    allDisclosures,
    allAttributes,
    allTicketingSystemAccounts,
    allChannels,
    allSeattypes,
    getListingsDetailsbyEventId,
    fetchListingDetails,
    getAllSplits,
    updateListingGroupStatus,
    getListingGroupBasedOnSplitLogics,
    getEventDetailsBasedOnEventId,
    fetchAttachmentDetails,
    fetchListingIconsId,
    addListing,
    addDisclosure,
    addAttribute,
    selectedTicketingSystemAccounts,
    addTag,
    editListing,
    updateListingPriceById,
    updateMultiListingPriceById,
    updateShareListings,
    updateInternalNotesListings,
    updateDisclosuresInListings,
    updateSplitsInListings,
    addTicketingSystem,
    updateInhandDateInListings,
    removeListing,
    getInHandDateListing,
    getListingData,
    purchaseTicket,
    updateEventLevelListingPrice,
    updateListingGroupMarkAsSold,
    allListings,
    addCreditCardDetails,
    filterListings,
    fetchChannelMarkups,
    addTicketingSystemAccount,
    readUploadListingsFile,
    getPrimaryIdFromNameDetails,
    fetchListingGroupDetails,
    getRecentListingDetailsBasedOnEventId,
    getListingDetailsBasedOnListingGroupId,
    getEventCount,
    insertListingGroupDetailsCSVFileUpload,
    validateCSVFileUploadFields,
    insertListingGroupDetailsGeneralAdmissionCSVFileUpload,
    validateCSVFileUploadFieldsGeneralAdmission,
    uploadCSVFileUploadListingAttachments,
    insertListingDetailsCSVFileUpload,
    insertListingGroupDisclosuresCSVFileUpload,
    insertListingGroupAttributesCSVFileUpload,
    insertListingGroupTagsCSVFileUpload,
    insertPurchaseOrderDetails,
    insertPurchaseOrderListingDetails,
    insertPurchaseOrderTagDetails,
    checkInhandDateIsEqual,
    updateSeatType,
    updateStockType,
    updateUnitCost,
    bulkUpdateNotesAndTags,
    commonTagUpdate,
    updateDisclosureInBulk,
    updateSplitsInBulk,
    updateViagogoStatus,
    allIssueTypes,
    createIssueFlag,
    allCreditCardTypes
};


