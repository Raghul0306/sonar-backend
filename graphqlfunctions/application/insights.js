const db = require('../../models');
const moment = require('moment');
const User = db.user;
const Listings = db.listings;
const Events = db.events;
const Op = db.Sequelize.Op;
const Genre = db.genre;
const ListingGroup = db.listinggroup;
const ListingStatus = db.listingStatus;
const sequelize = require('sequelize');
const queryBuilder = require('../../helpers/queryResponse');
import { listingStatusList } from "../../constants/constants";

const endHour = ' 23:59:59.000 +00:00';
/**
 * Page : home
 * Function For : To get the portfolio insights data
 * Ticket No : TIC-34
**/
async function getCollectionValue(_, { input: { seller_id } }) {

    ListingGroup.hasMany(Listings);
    Listings.belongsTo(ListingGroup);

    var activeListingStatus = await ListingStatus.findOne({
        attributes: ['id','status', 'name'], 
        where: {
            status: listingStatusList.active
        }, 
        raw: true
    });
    
    const inventoryValue = await ListingGroup.sum('listings.price', {
        include: [{
            model: Listings,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listings.listing_group_id'))
            },
            where: {
                sold: false
            }
        }],
        where: {
            seller_id: seller_id
        }
    });

    const marginCost = await ListingGroup.sum('listings.unit_cost', {
        include: [{
            model: Listings,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listings.listing_group_id'))
            },
            where: {
                sold: true
            }
        }],
        where: {
            seller_id: seller_id
        }
    });

    const marginPrice = await ListingGroup.sum('listings.price', {
        include: [{
            model: Listings,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listings.listing_group_id'))
            },
            where: {
                sold: true
            }
        }],
        where: {
            seller_id: seller_id
        }
    });
  
    let whereCondition = '';
    if (seller_id) {
        var listing_status_id = 0;
        if(activeListingStatus && activeListingStatus.id>0){
            listing_status_id = activeListingStatus.id
        }
        whereCondition = {
            seller_id: seller_id,
            listing_status_id: listing_status_id,
            is_removed: false
        }        
    }
    const activeListings = await ListingGroup.count(
        {
            include: [{
                attributes: ['listings.id'],
                model: Listings,
                on: {
                    col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listings.listing_group_id'))
                },
                where: { active: true }

            }],
            where: whereCondition

        });
    var responseData = {};

    responseData.current_inventory_value = inventoryValue;
    responseData.margin_value = marginCost && marginPrice ? marginCost / marginPrice : marginCost;
    //TIC - 34 - To Do : Need to get those values dynamically
    responseData.life_time_sales_value = 0;
    responseData.live_listing_value = activeListings;
    responseData.hold_time_data_value = 0;
    responseData.busted_order_date = 0;
    responseData.average_lisitng_age = 0;
    return responseData;
}

/**
 * Page : home
 * Function For : To get the revenue, sales and listings data
 * Ticket No : TIC-35
*/
async function getRevenueValue(_, { input: { seller_id, day_diff } }) {

    var responseData = {};

    responseData.local_today_rev = queryBuilder.queryRevenueData(seller_id, new Date(day_diff[0] + endHour), new Date(day_diff[1] + endHour));
    responseData.local_week_rev = queryBuilder.queryRevenueData(seller_id, new Date(day_diff[0] + endHour), new Date(day_diff[2] + endHour));
    responseData.local_month_rev = queryBuilder.queryRevenueData(seller_id, new Date(day_diff[0] + endHour), new Date(day_diff[3] + endHour));
    responseData.local_year_to_date_rev = queryBuilder.queryRevenueData(seller_id, new Date(day_diff[0] + endHour), new Date(day_diff[4] + endHour));

    return responseData;
}

async function getSalesValue(_, { input: { seller_id, day_diff } }) {
    var responseData = {};
    responseData.local_today_rev = queryBuilder.querySalesData(seller_id, new Date(day_diff[0] + endHour), new Date(day_diff[1] + endHour));
    responseData.local_week_rev = queryBuilder.querySalesData(seller_id, new Date(day_diff[0] + endHour), new Date(day_diff[2] + endHour));
    responseData.local_month_rev = queryBuilder.querySalesData(seller_id, new Date(day_diff[0] + endHour), new Date(day_diff[3] + endHour));
    responseData.local_year_to_date_rev = queryBuilder.querySalesData(seller_id, new Date(day_diff[0] + endHour), new Date(day_diff[4] + endHour));

    return responseData;
}

async function getListingValue(_, { input: { seller_id, day_diff } }) {
    var responseData = {};
    responseData.local_today_rev = queryBuilder.queryListingData(seller_id, new Date(day_diff[0] + endHour), new Date(day_diff[1] + endHour));
    responseData.local_week_rev = queryBuilder.queryListingData(seller_id, new Date(day_diff[0] + endHour), new Date(day_diff[2] + endHour));
    responseData.local_month_rev = queryBuilder.queryListingData(seller_id, new Date(day_diff[0] + endHour), new Date(day_diff[3] + endHour));
    responseData.local_year_to_date_rev = queryBuilder.queryListingData(seller_id, new Date(day_diff[0] + endHour), new Date(day_diff[4] + endHour));

    return responseData;
}


/**
    * Page : home
    * Function For : To get the top events listings
    * Ticket No : TIC-37
*/

async function getTopFiveSoldListings(_, { input: { seller_id } }) {

    Events.hasMany(ListingGroup);
    ListingGroup.belongsTo(Events);

    ListingGroup.hasMany(Listings);
    Listings.belongsTo(ListingGroup);

    const topRevenue = await ListingGroup.findAll({
        attributes: [
            [sequelize.literal('listing_group.id'), 'eventId'],
            [sequelize.literal('event.name'), 'name'],
            [sequelize.fn('sum', sequelize.col('listings.price')), 'price'],
        ],
        include: [{
            model: Events,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('event.id'), '=', sequelize.col('listing_group.event_id')),
            }
        }, {
            model: Listings,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listings.listing_group_id'))
            },
            where: {
                sold: true
            }
        }],
        where: {
            seller_id: seller_id
        },
        group: [sequelize.literal('listing_group.id'), sequelize.literal('event.name'), sequelize.literal('event.id')],
        order: [
            [sequelize.fn('sum', sequelize.col('listings.price')), 'DESC NULLS LAST']
        ]
    });

    var responseData = {};
    var revenue_list = [];

    for (let revenue = 0; revenue < 5; revenue++) {//topRevenue.length
        var obj = {};
        obj.id = revenue + 1;
        obj.name = topRevenue[revenue].dataValues.name;
        obj.price = topRevenue[revenue].dataValues.price;
        revenue_list.push(obj);
    }

    responseData.revenue_list = revenue_list;

    const topSales = await ListingGroup.findAll({
        attributes: [
            [sequelize.literal('listing_group.id'), 'eventId'],
            [sequelize.literal('event.name'), 'name'],
            [sequelize.fn('count', sequelize.col('listings.id')), 'counts'],
        ],
        include: [{
            model: Events,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('event.id'), '=', sequelize.col('listing_group.event_id')),
            }
        }, {
            model: Listings,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listings.listing_group_id'))
            },
            where: {
                sold: true
            }
        }],
        where: {
            seller_id: seller_id
        },
        group: [sequelize.literal('listing_group.id'), sequelize.literal('event.name'), sequelize.literal('event.id')],
        order: [
            [sequelize.fn('count', sequelize.col('listings.id')), 'DESC NULLS LAST']
        ]
    });

    var sale_list = [];

    for (let sales = 0; sales < 5; sales++) {//topSales.length
        var obj = {};
        obj.id = sales + 1;
        obj.name = topSales[sales].dataValues.name;
        obj.sales = topSales[sales].dataValues.counts;
        sale_list.push(obj);
    }

    responseData.sale_list = sale_list;

    return responseData;

}

/**
    * Page : home
    * Function For : To get the donut chart data
    * Ticket No : TIC-38 & 39
    * TO DO : Need to fetch those data dynamically
*/

async function getPerformersDetails(_, { input: { seller_id } }) {

    var responseData = {};
    var chartData = [];
    chartData.push({ x: "Jan", y: 35 });
    chartData.push({ x: "Feb", y: 1 });
    chartData.push({ x: "Mar", y: 4 });
    chartData.push({ x: "Apr", y: 1 });
    chartData.push({ x: "May", y: 0 });
    responseData.chartData = chartData;
    return responseData;

}

/**
    * Page : home
    * Function For : To get the donut chart data
    * Ticket No : TIC-38 & 39
    * TO DO : Need to fetch those data dynamically
*/

async function getChartDetails(_, { input: { seller_id, pieChartType } }) {

    ListingGroup.hasMany(Events);
    Events.belongsTo(Genre);

    Genre.hasMany(Events);
    Events.belongsTo(Genre);

    Genre.hasMany(ListingGroup);
    ListingGroup.belongsTo(Genre);

    Events.hasMany(ListingGroup);
    ListingGroup.belongsTo(Events);

    ListingGroup.hasMany(Listings);
    Listings.belongsTo(ListingGroup);

    var responseData = {};
    var chartData = [];
    let queryResponse;

    if (pieChartType == 1) {

        queryResponse = await ListingGroup.findAll({
            attributes: [
                [sequelize.literal('event.name'), 'name'],
                [sequelize.fn('sum', sequelize.col('listings.price')), 'price'],
            ],
            include: [{
                model: Events,
                attributes: [],
                on: {
                    col1: sequelize.where(sequelize.col('event.id'), '=', sequelize.col('listing_group.event_id')),
                },
                where: {
                    created_at: { [Op.gte]: moment().subtract(12, "month"), [Op.lte]: moment().format("YYYY-MM-DD") }
                }
            }, {
                model: Listings,
                attributes: [],
                on: {
                    col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listings.listing_group_id'))
                },
                where: {
                    sold: true,
                    active: true,
                }
            }],
            where: {
                seller_id: seller_id
            },
            raw: true,
            group: [sequelize.literal('event.name')]
        });

    } else {

        queryResponse = await ListingGroup.findAll({
            attributes: [
                [sequelize.literal('genre.name'), 'name'],
                [sequelize.fn('sum', sequelize.col('listings.price')), 'price'],
            ],
            include: [{
                model: Events,
                attributes: [],
                on: {
                    col1: sequelize.where(sequelize.col('event.id'), '=', sequelize.col('listing_group.event_id')),
                },
                where: {
                    created_at: { [Op.gte]: moment().subtract(12, "month"), [Op.lte]: moment().format("YYYY-MM-DD") }
                }
            }, {
                model: Genre,
                attributes: [],
                on: {
                    col1: sequelize.where(sequelize.col('genre.id'), '=', sequelize.col('event.genre_id')),
                },
            }, {
                model: Listings,
                attributes: [],
                on: {
                    col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listings.listing_group_id'))
                },
                where: {
                    sold: true,
                    active: true,
                }
            }],
            where: {
                seller_id: seller_id
            },
            raw: true,
            group: [sequelize.literal('genre.name')]
        });

    }

    for (let dataCount = 0; dataCount < queryResponse.length; dataCount++) {
        var obj = {};
        obj.x = queryResponse[dataCount].name;
        obj.y = queryResponse[dataCount].price ? queryResponse[dataCount].price : 10;
        chartData.push(obj);
    }

    responseData.chartData = chartData;
    return responseData;

}


/**
     * Page : home
     * Function For : Function to fetch the broker list
     * Ticket No : TIC-171
     * TO DO : Need to implement the dsahboard data changes based on the broker selected
 */

async function getBrokerUsers() {

    var topRevenue = await User.findAll({
        attributes: ['id', 'first_name', 'last_name'],
        where: {
            'user_role': 2
        }
    });

    var obj = {}
    obj.broker_list = topRevenue;
    return obj;

}

async function orderCardInfo(_, { input: { seller_id } }) {

    var obj = {}
    obj.pastDue = queryBuilder.queryOrderCardData(seller_id, true, false, 'past');
    obj.openOrders = queryBuilder.queryOrderCardData(seller_id, true, false, 'future');
    obj.completedOrders = queryBuilder.queryOrderCardData(seller_id, true, true, 'completed');
    obj.delivery = queryBuilder.queryOrderCardData(seller_id, '', true, 'delivered');

    return obj;
}

/**
     * Page : home
     * Function For : Added API services to get the hot Sellers
     * Ticket No : TIC-136 & 137
*/

async function getHotSellersList() {

    User.hasMany(ListingGroup);
    ListingGroup.belongsTo(User);

    ListingGroup.hasMany(Listings);
    Listings.belongsTo(ListingGroup);

    var responseData = {};

    var sellersData = await ListingGroup.findAll({
        attributes: [
            [sequelize.col('user.id'), 'id'],
            [sequelize.col('user.first_name'), 'first_name'],
            [sequelize.fn('sum', sequelize.col('listings.price')), 'price'],
        ],
        include: [{
            model: User,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('user.id'), '=', sequelize.col('listing_group.seller_id'))
            },
            where: {
                'user_role': {
                    [Op.or]: [2, 3]
                }
            }
        }, {
            model: Listings,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listings.listing_group_id'))
            },
            where: {
                sold: true
            }
        }],
        raw: true,
        group: [sequelize.col('user.id')],
        order: [
            [sequelize.fn('sum', sequelize.col('listings.price')), 'DESC NULLS LAST']
        ]

    });
    var sellerList = [];

    for (let dataCount = 0; dataCount < sellersData.length; dataCount++) {
        var obj = {};
        obj.id = sellersData[dataCount].id;
        obj.name = sellersData[dataCount].first_name;
        obj.price = sellersData[dataCount].price ? sellersData[dataCount].price : 10;
        sellerList.push(obj);
    }

    responseData.seller_list = sellerList;
    return responseData;
}


/**
     * Page : home
     * Function For : Added API services to get the hot buyers
     * Ticket No : TIC-136 & 137
*/

async function getHotBuyersList() {
    User.hasMany(ListingGroup);
    ListingGroup.belongsTo(User);

    ListingGroup.hasMany(Listings);
    Listings.belongsTo(ListingGroup);

    var responseData = {};

    var buyersData = await ListingGroup.findAll({
        attributes: [
            [sequelize.col('user.id'), 'id'],
            [sequelize.col('user.first_name'), 'first_name'],
            [sequelize.fn('sum', sequelize.col('listings.price')), 'price'],
        ],
        include: [{
            model: Listings,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listings.listing_group_id'))
            },
            where: {
                sold: true
            }
        }, {
            model: User,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('user.id'), '=', sequelize.col('listings.buyer_id'))
            },
            where: {
                'user_role': {
                    [Op.or]: [2, 3]
                }
            }
        }],
        raw: true,
        group: [sequelize.col('user.id')],
        order: [
            [sequelize.fn('sum', sequelize.col('listings.price')), 'DESC NULLS LAST']
        ]
    });

    var buyersList = [];

    for (let dataCount = 0; dataCount < buyersData.length; dataCount++) {
        var obj = {};
        obj.id = buyersData[dataCount].id;
        obj.name = buyersData[dataCount].first_name;
        obj.price = buyersData[dataCount].price ? buyersData[dataCount].price : 10;
        buyersList.push(obj);
    }

    responseData.buyer_list = buyersList;
    return responseData;
}

/**
   * Page : home
   * Function For : Added API function to get the sales and revenue data
   * Ticket No : TIC-134
*/

async function getSalesByMonth() {

    var obj = {};
    var sales_list = [];

    var sales = await Listings.findAll({
        attributes: [
            [sequelize.fn('DATE_TRUNC', 'month', sequelize.col('created_at')), 'month'],
            [sequelize.fn('count', sequelize.col('id')), 'sales'],
        ],
        where: {
            created_at: { [Op.gte]: moment().subtract(12, "month").format("YYYY-MM-DD"), [Op.lte]: moment().format("YYYY-MM-DD") },
            sold: true
        },
        raw: true,
        group: [sequelize.fn('DATE_TRUNC', 'month', sequelize.col('created_at'))]
    });

    var revenue = await Listings.findAll({
        attributes: [
            [sequelize.fn('DATE_TRUNC', 'month', sequelize.col('created_at')), 'month'],
            [sequelize.fn('sum', sequelize.col('price')), 'revenue'],
        ],
        where: {
            created_at: { [Op.gte]: moment().subtract(12, "month").format("YYYY-MM-DD"), [Op.lte]: moment().format("YYYY-MM-DD") },
            sold: true
        },
        raw: true,
        group: [sequelize.fn('DATE_TRUNC', 'month', sequelize.col('created_at'))]
    });

    var summarySalesData = [];
    for (var start = 0; sales.length > start; start++) {
        summarySalesData[moment(sales[start].month).format('MMM')] = sales[start].sales;
    }

    var summaryRevenuData = [];
    for (var start = 0; revenue.length > start; start++) {
        summaryRevenuData[moment(sales[start].month).format('MMM')] = revenue[start].revenue;
    }

    var dateStart = moment().subtract(11, "month");
    var growthDate = moment().subtract(12, "month");
    var dateEnd = moment();

    while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {

        sales_list.push({
            month: dateStart.format('MMM YY'),
            sales: summarySalesData[dateStart.format('MMM')] ? summarySalesData[dateStart.format('MMM')] : 0,
            aov: 0,
            revenue: summaryRevenuData[dateStart.format('MMM')] ? summaryRevenuData[dateStart.format('MMM')] : 0,
            takeRate: 0,
            growth: ((summaryRevenuData[dateStart.format('MMM')] > summaryRevenuData[growthDate.format('MMM')]) ? ((summaryRevenuData[dateStart.format('MMM')] - summaryRevenuData[growthDate.format('MMM')]) / summaryRevenuData[dateStart.format('MMM')]) * 100 : 0) + "%"
        });

        growthDate.add(1, 'month');
        dateStart.add(1, 'month');
    }

    obj.sales_list = sales_list;
    return obj;
}

/**
    * Page : home
    * Function For : Added API function to get the sales and revenue data
    * Ticket No : TIC-131
*/

async function getAllOrdersBarChart() {
    var obj = {};
    var sales_list = [];

    var sales = await Listings.findAll({
        attributes: [
            [sequelize.fn('DATE_TRUNC', 'month', sequelize.col('created_at')), 'month'],
            [sequelize.fn('count', sequelize.col('id')), 'sales'],
        ],
        where: {
            created_at: { [Op.gte]: moment().subtract(12, "month").format("YYYY-MM-DD"), [Op.lte]: moment().format("YYYY-MM-DD") },
            sold: true
        },
        raw: true,
        group: [sequelize.fn('DATE_TRUNC', 'month', sequelize.col('created_at'))]
    });

    var revenue = await Listings.findAll({
        attributes: [
            [sequelize.fn('DATE_TRUNC', 'month', sequelize.col('created_at')), 'month'],
            [sequelize.fn('sum', sequelize.col('price')), 'revenue'],
        ],
        where: {
            created_at: { [Op.gte]: moment().subtract(12, "month").format("YYYY-MM-DD"), [Op.lte]: moment().format("YYYY-MM-DD") },
            sold: true
        },
        raw: true,
        group: [sequelize.fn('DATE_TRUNC', 'month', sequelize.col('created_at'))]
    });

    var summarySalesData = [];
    for (var start = 0; sales.length > start; start++) {
        summarySalesData[moment(sales[start].month).format('MMM')] = sales[start].sales;
    }

    var summaryRevenuData = [];
    for (var start = 0; revenue.length > start; start++) {
        summaryRevenuData[moment(sales[start].month).format('MMM')] = revenue[start].revenue;
    }

    var dateStart = moment().subtract(11, "month");
    var growthDate = moment().subtract(12, "month");
    var dateEnd = moment();

    var barChart = [];
    var lineChart = [];
    var salesMax = 0;
    var revenueMax = 0;
    var calendarPeriod = [];

    while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {

        calendarPeriod.push(dateStart.format('MMM YY'));

        if (summarySalesData[dateStart.format('MMM')] > salesMax) {
            salesMax = Number(summarySalesData[dateStart.format('MMM')]);
        }

        if (summaryRevenuData[dateStart.format('MMM')] > revenueMax) {
            revenueMax = Number(summaryRevenuData[dateStart.format('MMM')]);
        }

        barChart.push({
            x: dateStart.format('MMM YY'),
            y: summaryRevenuData[dateStart.format('MMM')] ? summaryRevenuData[dateStart.format('MMM')] : 0
        });

        lineChart.push({
            x: dateStart.format('MMM YY'),
            y: summarySalesData[dateStart.format('MMM')] ? Number(summarySalesData[dateStart.format('MMM')]) : 0
        });

        growthDate.add(1, 'month');
        dateStart.add(1, 'month');
    }


    obj.barChart = barChart;
    obj.lineChart = lineChart;
    obj.salesMax = salesMax;
    obj.revenueMax = revenueMax;
    obj.calendarPeriod = calendarPeriod;


    return obj;

}


module.exports = {
    getCollectionValue,
    getRevenueValue,
    getSalesValue,
    getListingValue,
    getTopFiveSoldListings,
    getPerformersDetails,
    getChartDetails,
    getBrokerUsers,
    orderCardInfo,
    getHotSellersList,
    getHotBuyersList,
    getSalesByMonth,
    getAllOrdersBarChart
};


