const bcrypt = require("bcrypt");
const db = require('../../models');
const User = db.user;
const Useraccount = db.useraccount;
const Userroles = db.userroles;
const Countries = db.countries;
const Companies = db.companies;
const States = db.states;
const Cities = db.cities;
const Companyuser = db.companyuser;
const ChannalFees = db.channelfees;
const Op = db.Sequelize.Op;
const awsHelper = require('../../helpers/awskms');
import { userRolesLower, userRoleRestrictions } from "../../constants/constants";
const sendMail = require('../../helpers/sendMail');
const mailTemplate = require('../../helpers/mailTemplate');
const crypto = require("crypto");
const sequelize = require('sequelize');

async function allUsers() {
    User.hasMany(Userroles, {
        foreignKey: 'user_role'
    });
    User.hasMany(Companies, {
        foreignKey: 'company_id'
    });
    var user_details = await User.findAll({
        attributes: [
            [sequelize.col("user.id"), "id"],
            [sequelize.col("user.first_name"), "first_name"],
            [sequelize.col('user.last_name'), 'last_name'],
            [sequelize.col('user.phone'), 'phone'],
            [sequelize.col('user.email'), 'email'],
            [sequelize.col('user_roles.id'), 'user_role'],
            [sequelize.col('companies.company_name'), 'company_name'],
            [sequelize.col('user.stripe_id'), 'stripeId'],
            [sequelize.col('user.mercuryid'), 'mercuryId'],
            [sequelize.col('user.last_login'), 'last_login'],
            [sequelize.col('user.notes'), 'notes'],
            [sequelize.col('user.street_address_1'), 'street_address_1'],
            [sequelize.col('user.street_address_2'), 'street_address_2'],
            [sequelize.col('user.city'), 'city'],
            [sequelize.col('user.state'), 'state'],
            [sequelize.col('user.zip_code'), 'postalCode'],
            [sequelize.col('user.country'), 'country'],
            [sequelize.col('user.needsToChangePassword'), 'needsToChangePassword'],
            [sequelize.col('user.needsToAcceptTerms'), 'needsToAcceptTerms'],
            [sequelize.col('user.needsToAcceptPrivacy'), 'needsToAcceptPrivacy'],
            [sequelize.col('user.needsToAcceptSeller'), 'needsToAcceptSeller'],
            [sequelize.col('user.needsToSyncBank'), 'needsToSyncBank']
        ],
        include: [
            {
                model: Companies,
                attributes: [],
                on: {
                    col1: sequelize.where(sequelize.col('companies.id'), '=', sequelize.col('user.company_id')),
                }
            },
            {
                model: Userroles,
                attributes: [],
                on: {
                    col1: sequelize.where(sequelize.col('user_roles.id'), '=', sequelize.col('user.user_role')),
                }
            }
        ],
        where: {}
    });

    if (user_details) {
        return JSON.parse(JSON.stringify(user_details));
    } else {
        return "No data";
    }
}

async function allCountries() {
    const country_details = await Countries.findAll(
        {
            attributes: ['id', 'name', 'abbreviation'],
            where: { active: 1 },
            order: [
                ['sorting', 'DESC'],
                ['name', 'ASC'],
            ]
        }
    )
    if (country_details) {
        return JSON.parse(JSON.stringify(country_details));
    } else {
        return "No data";
    }
}

async function allStates(_, { input: { country_id } }) {
    const state_details = await States.findAll(
        {
            attributes: ['id', 'name', 'abbreviation'],
            where: { active: 1, country_id: country_id },
            order: [
                ['name', 'ASC'],
            ]
        }
    )
    if (state_details) {
        let jsonData = JSON.parse(JSON.stringify(state_details));
        return jsonData;
    } else {
        return "No data";
    }
}

async function allCities(_, { input: { country_id, state_id, city_name_search, offset, limit, fields = [] } }) {
    let query = {
        attributes: ['id', 'name'],
        order: [
            ['name', 'ASC'],
        ]
    }
    let whereCondition = { active: 1 }

    let includes = [];

    Cities.belongsTo(States, { foreignKey: 'state_id' });
    Cities.belongsTo(Countries, { foreignKey: 'country_id' });

    fields.forEach(field => {
        switch (field) {
            case 'state':
                includes.push({
                    model: States,
                    attributes: ['abbreviation']
                })
                break;
            case 'country':
                includes.push({
                    model: Countries,
                    attributes: ['abbreviation']
                })
                break;
        }
    });


    if (country_id) whereCondition.country_id = country_id;

    if (state_id) whereCondition.state_id = state_id;

    if (city_name_search) whereCondition.name = { [Op.iLike]: city_name_search + '%' }

    query.where = whereCondition;

    if (includes.length) query.include = includes;

    if (offset) query.offset = offset;
    if (limit) query.limit = limit;


    return Cities.findAll(query);
}

async function citiesCount(_, { input: { country_id, state_id, city_name_search, offset, limit } }) {

    let whereCondition = {}
    if (city_name_search) whereCondition.name = { [Op.iLike]: city_name_search + '%' }

    return { count: await Cities.count({ where: whereCondition }) };

}

async function getCity(_, { input: { id } }) {
    const city_details = await Cities.findOne(
        {
            attributes: ['id', 'name'],
            where: { active: 1, id: id }
        }
    )
    if (city_details) {
        let jsonData = JSON.parse(JSON.stringify(city_details));
        return jsonData;
    } else {
        return "No data";
    }
}

async function getState(_, { input: { id } }) {
    const state_details = await States.findOne(
        {
            attributes: ['id', 'name', 'abbreviation'],
            where: { active: 1, id: id }
        }
    )
    if (state_details) {
        let jsonData = JSON.parse(JSON.stringify(state_details));
        return jsonData;
    } else {
        return "No data";
    }
}

async function getCountry(_, { input: { id } }) {
    const country_details = await Countries.findOne(
        {
            attributes: ['id', 'name', 'abbreviation'],
            where: { active: 1, id: id }
        }
    )
    if (country_details) {
        let jsonData = JSON.parse(JSON.stringify(country_details));
        return jsonData;
    } else {
        return "No data";
    }
}

/**
  * Input : Channel Fees Values
  * Output : Data From Channel Fees Table
  * Function For : Add Channel Fees data insert to Channel Fees table
  * Ticket No : TIC-841
  */
async function addChannelFees(_, { input: { userId, createdBy, channelFeesJSON } }) {

    //Channel Fees Start
    var finalResJSON = {
        "isError": true,
        "message": "Channel Fees not updated"
    };
    var exitedFeeDetails = [];
    if (channelFeesJSON) {
        for (let channelFeesJSONCount = 0; channelFeesJSONCount < channelFeesJSON.length; channelFeesJSONCount++) {
            var channelFeesJSONArray = {
                user_id: userId,
                created_by: createdBy,
                fee: channelFeesJSON[channelFeesJSONCount].markup_amount,
                channel_id: channelFeesJSON[channelFeesJSONCount].channel_id
            };
            exitedFeeDetails = await ChannalFees.findOne({
                attributes: ['id'],
                where: { user_id: userId, channel_id: channelFeesJSON[channelFeesJSONCount].channel_id }
            });
            if (exitedFeeDetails) {
                await ChannalFees.update({ fee: channelFeesJSON[channelFeesJSONCount].markup_amount },
                    {
                        where: {
                            user_id: userId,
                            channel_id: channelFeesJSON[channelFeesJSONCount].channel_id
                        }
                    }, { returning: ['id'] });
            } else {
                await ChannalFees.create(channelFeesJSONArray);
            }
        }
        finalResJSON = {
            "isError": false,
            "message": "Channel Fees updated!"
        };

    }
    return finalResJSON;
    //Channel Fees End
}

/**
  * Input : Channel Fees Values
  * Output : Data From Channel Fees Table
  * Function For : Get Channel Fees data insert to Channel Fees table
  * Ticket No : TIC-841
  */
async function getChannelFee(_, { input: { userId } }) {

    let feeDetails = [];
    if (userId) {
        feeDetails = await ChannalFees.findAll({
            attributes: ['channel_id', ['fee', 'markup_amount']],
            where: { user_id: userId }
        });
    }
    if (feeDetails && feeDetails.length > 0) {
        return { "channelFeesJSON": JSON.parse(JSON.stringify(feeDetails)) };
    } else {
        return "No data";
    }
}

async function allRoleUsers(_, { input: { id } }) {
    let userDetails;
    if (id == 4 || id == 10  || id == 9) {
        userDetails = await User.findAll({ where: { user_role: id } })
    }
    else {
        userDetails = await User.findAll({ where: { created_by: id } })
    }
    if (userDetails) {
        return JSON.parse(JSON.stringify(userDetails));
    } else {
        return "No data";
    }
}

async function userRoles(_, { input: { user_role_name } }) {
    try {
        if (!userRoleRestrictions[user_role_name]) {
            return [{ id: 0, user_role_name: '' }];
        }
        let where = { attributes: ['id', 'user_role_name'], where: { user_role_name: { [Op.in]: userRoleRestrictions[user_role_name] } } };
        const user_details = await Userroles.findAll(where);
        if (user_details) {
            return JSON.parse(JSON.stringify(user_details));
        } else {
            return [{ id: 0, user_role_name: '' }];
        }
    } catch (error) {
        console.log('Getting User Roles failed', error)
        return [{ id: 0, user_role_name: '' }];
    }

}


async function emailExists(_, { input: { email } }) {
    const user_details = await User.findAll({ where: { email: email } });
    if (user_details) {
        return JSON.parse(JSON.stringify(user_details));
    } else {
        return "No data";
    }
}

/* User Detail Section*/
async function getuser(_, { input: { id } }) {
    if (id) {
        User.hasMany(Userroles, {
            foreignKey: 'user_role'
        });
        User.hasMany(Companies, {
            foreignKey: 'company_id'
        });
        var user_details = await User.findOne({
            attributes: [
                [sequelize.col("user.id"), "id"],
                [sequelize.col("user.first_name"), "firstName"],
                [sequelize.col('user.last_name'), 'lastName'],
                [sequelize.fn('concat', sequelize.col('user.first_name'), ' ', sequelize.col('user.last_name')), 'fullName'],
                [sequelize.col('user.phone'), 'phone'],
                [sequelize.col('user.email'), 'email'],
                [sequelize.col('user_roles.user_role_name'), 'userRole'],
                [sequelize.col('companies.company_name'), 'companyName'],
                [sequelize.col('user.stripe_id'), 'stripeId'],
                [sequelize.col('user.mercuryid'), 'mercuryId'],
                [sequelize.col('user.last_login'), 'last_login'],
                [sequelize.col('user.street_address_1'), 'streetAddress'],
                [sequelize.col('user.street_address_2'), 'streetAddress2'],
                [sequelize.col('user.city'), 'city'],
                [sequelize.col('user.state'), 'state'],
                [sequelize.col('user.zip_code'), 'postalCode'],
                [sequelize.col('user.country'), 'country'],
                [sequelize.col('user.needsToChangePassword'), 'needsToChangePassword'],
                [sequelize.col('user.needsToAcceptTerms'), 'needsToAcceptTerms'],
                [sequelize.col('user.needsToAcceptPrivacy'), 'needsToAcceptPrivacy'],
                [sequelize.col('user.needsToAcceptSeller'), 'needsToAcceptSeller'],
                [sequelize.col('user.needsToSyncBank'), 'needsToSyncBank']
            ],
            include: [
                {
                    model: Companies,
                    attributes: [],
                    on: {
                        col1: sequelize.where(sequelize.col('companies.id'), '=', sequelize.col('user.company_id')),
                    }
                },
                {
                    model: Userroles,
                    attributes: [],
                    on: {
                        col1: sequelize.where(sequelize.col('user_roles.id'), '=', sequelize.col('user.user_role')),
                    }
                }
            ],
            where: {
                id: id
            }
        });
        return JSON.parse(JSON.stringify(user_details));
    } else {
        return "No data";
    }
}

/* Register Section*/
/**
     * Input : User Details
     * Output : User Table Response
     * Function For : Adding User account details to user table
 */
async function registerUser(_, { input: { first_name, last_name, email, password, user_role, company_id, phone, created_by } }) {
    let b_password = await bcrypt.hash(password, 10);
    let aws_crypted = await awsHelper.awsEncrypt(b_password);
    const accountExists = await Useraccount.findOne({ where: { email: email } })
    if (accountExists) {   // Check and send response as false if user email already exists
        return { response: false }
    }
    else {
        /* if(user_role >= 1 && user_role <= 3) { 
            const userParentCompanyDetails = await Companies.findOne({ attributes: ['id'], where: { created_by: created_by } })
            if(userParentCompanyDetails && userParentCompanyDetails.id){ company_id = userParentCompanyDetails.id;}
        } */

        const user = await User.create({
            first_name: first_name,
            last_name: last_name,
            email: email,
            password: aws_crypted,
            phone: parseInt(phone),
            zip_code: 0,
            user_role,
            created_by,
            company_id,
            city: "",
            state: "",
            country: "",
            created_at: Date.now(),
            updated_at: Date.now()
        });
        if (user) {

            //Company Insert operation
            const companyusers = await Companyuser.findOne({ where: { user_id: user.dataValues.id, company_id: user.dataValues.company_id } })
            if (!companyusers) {
                var companyUsersInputData = {
                    company_id: user.dataValues.company_id,
                    user_id: user.dataValues.id,
                    created_by: user.dataValues.created_by
                };
                await Companyuser.create(companyUsersInputData);
            }

            //Email Send Oeration
            let verifyToken = crypto.randomBytes(32).toString("hex");
            await User.update({ verification_token: verifyToken }, { where: { email: email } });
            var mailContents = {};
            mailContents.from = process.env.EMAIL_FROM_ADDRESS;
            mailContents.to = email;
            mailContents.subject = "Mail Verification";
            mailContents.message = await mailTemplate.userRegistrationEmail(process.env.APP_URL + 'verify-email/' + verifyToken, email);
            let mailStatus = await sendMail.outgoingMails(mailContents);
            const userPasswordInsert = await Useraccount.create({
                email: email,
                password_hash: aws_crypted,
                user_id: user.dataValues.id

            });
            return { response: true, userId: user.dataValues.id }
        }
        else return { response: false, userId: 0 }
    }
}

/* updatePassword Section - using AWS KMS*/
/**
 * Input : User Credentials
 * Output : User Table Response
 * Function For : Update Password for User Account With AWS KMS
 * Ticket No : TIC-209
 */
async function updatePassword(_, { input: { user, password } }) {
    const useraccount = await Useraccount.findOne({ where: { email: user } })
    if (useraccount) {
        let b_password = await bcrypt.hash(password, 10);
        let aws_password = await awsHelper.awsEncrypt(b_password);
        const result = await Useraccount.update(
            { password_hash: aws_password },
            { where: { id: useraccount.id } }
        )

        if (!result) {

            var data = {
                id: NULL,
                firstName: NULL,
                lastName: NULL,
                fullName: NULL,
                email: NULL,
                phone: NULL,
                userRole: NULL,
                stripeId: NULL,
                last_login: NULL,
                companyName: NULL,
                streetAddress: NULL,
                streetAddress2: NULL,
                city: NULL,
                state: NULL,
                postalCode: NULL,
                country: NULL,
                needsToChangePassword: NULL,
                needsToAcceptTerms: NULL,
                needsToAcceptPrivacy: NULL,
                needsToAcceptSeller: NULL,
                needsToSyncBank: NULL,
                message: " Password not change , try again !!!"
            };
            return data;

        } else {

            await User.update(
                { needsToChangePassword: false },
                { where: { id: useraccount.user_id } }
            )

            User.hasMany(Userroles, {
                foreignKey: 'user_role'
            });
            User.hasMany(Companies, {
                foreignKey: 'company_id'
            });
            var user_details = await User.findOne({
                attributes: [
                    [sequelize.col("user.id"), "id"],
                    [sequelize.col("user.first_name"), "firstName"],
                    [sequelize.col('user.last_name'), 'lastName'],
                    [sequelize.fn('concat', sequelize.col('user.first_name'), ' ', sequelize.col('user.last_name')), 'fullName'],
                    [sequelize.col('user.phone'), 'phone'],
                    [sequelize.col('user.email'), 'email'],
                    [sequelize.col('user_roles.user_role_name'), 'userRole'],
                    [sequelize.col('companies.company_name'), 'companyName'],
                    [sequelize.col('user.stripe_id'), 'stripeId'],
                    [sequelize.col('user.mercuryid'), 'mercuryId'],
                    [sequelize.col('user.last_login'), 'last_login'],
                    [sequelize.col('user.street_address_1'), 'streetAddress'],
                    [sequelize.col('user.street_address_2'), 'streetAddress2'],
                    [sequelize.col('user.city'), 'city'],
                    [sequelize.col('user.state'), 'state'],
                    [sequelize.col('user.zip_code'), 'postalCode'],
                    [sequelize.col('user.country'), 'country'],
                    [sequelize.col('user.needsToChangePassword'), 'needsToChangePassword'],
                    [sequelize.col('user.needsToAcceptTerms'), 'needsToAcceptTerms'],
                    [sequelize.col('user.needsToAcceptPrivacy'), 'needsToAcceptPrivacy'],
                    [sequelize.col('user.needsToAcceptSeller'), 'needsToAcceptSeller'],
                    [sequelize.col('user.needsToSyncBank'), 'needsToSyncBank']
                ],
                include: [
                    {
                        model: Companies,
                        attributes: [],
                        on: {
                            col1: sequelize.where(sequelize.col('companies.id'), '=', sequelize.col('user.company_id')),
                        }
                    },
                    {
                        model: Userroles,
                        attributes: [],
                        on: {
                            col1: sequelize.where(sequelize.col('user_roles.id'), '=', sequelize.col('user.user_role')),
                        }
                    }
                ],
                where: {
                    id: useraccount.user_id
                }
            });
            return JSON.parse(JSON.stringify(user_details));
        }
    } else {
        var data = {
            id: NULL,
            firstName: NULL,
            lastName: NULL,
            fullName: NULL,
            email: NULL,
            phone: NULL,
            userRole: NULL,
            stripeId: NULL,
            last_login: NULL,
            companyName: NULL,
            streetAddress: NULL,
            streetAddress2: NULL,
            city: NULL,
            state: NULL,
            postalCode: NULL,
            country: NULL,
            message: " Email not exit !!! "
        };
        return data;
    }
}


/* updateUserNeedsToAcceptTerms Section */
async function updateUserNeedsToAcceptTerms(_, { input: { id } }) {
    const user_details_row = await User.findOne({ where: { id: id } })
    if (user_details_row.id) {
        const result = await User.update(
            { needsToAcceptTerms: false },
            { where: { id: id } }
        )
        if (!result) {
            var data = {
                message: " Terms and conditions not accepted , try again !!!"
            };
            return data;
        }
        else {
            User.hasMany(Userroles, {
                foreignKey: 'user_role'
            });
            User.hasMany(Companies, {
                foreignKey: 'company_id'
            });
            var user_details = await User.findOne({
                attributes: [
                    [sequelize.col("user.id"), "id"],
                    [sequelize.col("user.first_name"), "firstName"],
                    [sequelize.col('user.last_name'), 'lastName'],
                    [sequelize.fn('concat', sequelize.col('user.first_name'), ' ', sequelize.col('user.last_name')), 'fullName'],
                    [sequelize.col('user.phone'), 'phone'],
                    [sequelize.col('user.email'), 'email'],
                    [sequelize.col('user_roles.user_role_name'), 'userRole'],
                    [sequelize.col('companies.company_name'), 'companyName'],
                    [sequelize.col('user.stripe_id'), 'stripeId'],
                    [sequelize.col('user.mercuryid'), 'mercuryId'],
                    [sequelize.col('user.last_login'), 'last_login'],
                    [sequelize.col('user.street_address_1'), 'streetAddress'],
                    [sequelize.col('user.street_address_2'), 'streetAddress2'],
                    [sequelize.col('user.city'), 'city'],
                    [sequelize.col('user.state'), 'state'],
                    [sequelize.col('user.zip_code'), 'postalCode'],
                    [sequelize.col('user.country'), 'country'],
                    [sequelize.col('user.needsToChangePassword'), 'needsToChangePassword'],
                    [sequelize.col('user.needsToAcceptTerms'), 'needsToAcceptTerms'],
                    [sequelize.col('user.needsToAcceptPrivacy'), 'needsToAcceptPrivacy'],
                    [sequelize.col('user.needsToAcceptSeller'), 'needsToAcceptSeller'],
                    [sequelize.col('user.needsToSyncBank'), 'needsToSyncBank']
                ],
                include: [
                    {
                        model: Companies,
                        attributes: [],
                        on: {
                            col1: sequelize.where(sequelize.col('companies.id'), '=', sequelize.col('user.company_id')),
                        }
                    },
                    {
                        model: Userroles,
                        attributes: [],
                        on: {
                            col1: sequelize.where(sequelize.col('user_roles.id'), '=', sequelize.col('user.user_role')),
                        }
                    }
                ],
                where: {
                    id: id
                }
            });
            return JSON.parse(JSON.stringify(user_details));
        }
    } else {
        var data = {
            message: " Terms and conditions not accepted , try again !!!"
        };
        return data;
    }
}


/* updateUserNeedsToAcceptPrivacy Section */
async function updateUserNeedsToAcceptPrivacy(_, { input: { id } }) {
    const user_details_row = await User.findOne({ where: { id: id } })
    if (user_details_row.id) {

        const result = await User.update(
            { needsToAcceptPrivacy: false },
            { where: { id: id } }
        )
        if (!result) {
            var data = {
                message: "Privacy policy not accepted , try again !!!"
            };
            return data;
        }
        else {
            User.hasMany(Userroles, {
                foreignKey: 'user_role'
            });
            User.hasMany(Companies, {
                foreignKey: 'company_id'
            });
            var user_details = await User.findOne({
                attributes: [
                    [sequelize.col("user.id"), "id"],
                    [sequelize.col("user.first_name"), "firstName"],
                    [sequelize.col('user.last_name'), 'lastName'],
                    [sequelize.fn('concat', sequelize.col('user.first_name'), ' ', sequelize.col('user.last_name')), 'fullName'],
                    [sequelize.col('user.phone'), 'phone'],
                    [sequelize.col('user.email'), 'email'],
                    [sequelize.col('user_roles.user_role_name'), 'userRole'],
                    [sequelize.col('companies.company_name'), 'companyName'],
                    [sequelize.col('user.stripe_id'), 'stripeId'],
                    [sequelize.col('user.mercuryid'), 'mercuryId'],
                    [sequelize.col('user.last_login'), 'last_login'],
                    [sequelize.col('user.street_address_1'), 'streetAddress'],
                    [sequelize.col('user.street_address_2'), 'streetAddress2'],
                    [sequelize.col('user.city'), 'city'],
                    [sequelize.col('user.state'), 'state'],
                    [sequelize.col('user.zip_code'), 'postalCode'],
                    [sequelize.col('user.country'), 'country'],
                    [sequelize.col('user.needsToChangePassword'), 'needsToChangePassword'],
                    [sequelize.col('user.needsToAcceptTerms'), 'needsToAcceptTerms'],
                    [sequelize.col('user.needsToAcceptPrivacy'), 'needsToAcceptPrivacy'],
                    [sequelize.col('user.needsToAcceptSeller'), 'needsToAcceptSeller'],
                    [sequelize.col('user.needsToSyncBank'), 'needsToSyncBank']
                ],
                include: [
                    {
                        model: Companies,
                        attributes: [],
                        on: {
                            col1: sequelize.where(sequelize.col('companies.id'), '=', sequelize.col('user.company_id')),
                        }
                    },
                    {
                        model: Userroles,
                        attributes: [],
                        on: {
                            col1: sequelize.where(sequelize.col('user_roles.id'), '=', sequelize.col('user.user_role')),
                        }
                    }
                ],
                where: {
                    id: id
                }
            });
            return JSON.parse(JSON.stringify(user_details));
        }
    } else {
        var data = {
            message: "Privacy policy not accepted , try again !!!"
        };
        return data;
    }
}

/* updateUserNeedsToAcceptSeller Section */
async function updateUserNeedsToAcceptSeller(_, { input: { id } }) {
    const user_details_row = await User.findOne({ where: { id: id } })
    if (user_details_row.id) {

        const result = await User.update(
            { needsToAcceptSeller: false },
            { where: { id: id } }
        )
        if (!result) {
            var data = {
                message: "Seller Agreement not accepted , try again !!!"
            };
            return data;
        }
        else {
            User.hasMany(Userroles, {
                foreignKey: 'user_role'
            });
            User.hasMany(Companies, {
                foreignKey: 'company_id'
            });
            var user_details = await User.findOne({
                attributes: [
                    [sequelize.col("user.id"), "id"],
                    [sequelize.col("user.first_name"), "firstName"],
                    [sequelize.col('user.last_name'), 'lastName'],
                    [sequelize.fn('concat', sequelize.col('user.first_name'), ' ', sequelize.col('user.last_name')), 'fullName'],
                    [sequelize.col('user.phone'), 'phone'],
                    [sequelize.col('user.email'), 'email'],
                    [sequelize.col('user_roles.user_role_name'), 'userRole'],
                    [sequelize.col('companies.company_name'), 'companyName'],
                    [sequelize.col('user.stripe_id'), 'stripeId'],
                    [sequelize.col('user.mercuryid'), 'mercuryId'],
                    [sequelize.col('user.last_login'), 'last_login'],
                    [sequelize.col('user.street_address_1'), 'streetAddress'],
                    [sequelize.col('user.street_address_2'), 'streetAddress2'],
                    [sequelize.col('user.city'), 'city'],
                    [sequelize.col('user.state'), 'state'],
                    [sequelize.col('user.zip_code'), 'postalCode'],
                    [sequelize.col('user.country'), 'country'],
                    [sequelize.col('user.needsToChangePassword'), 'needsToChangePassword'],
                    [sequelize.col('user.needsToAcceptTerms'), 'needsToAcceptTerms'],
                    [sequelize.col('user.needsToAcceptPrivacy'), 'needsToAcceptPrivacy'],
                    [sequelize.col('user.needsToAcceptSeller'), 'needsToAcceptSeller'],
                    [sequelize.col('user.needsToSyncBank'), 'needsToSyncBank']
                ],
                include: [
                    {
                        model: Companies,
                        attributes: [],
                        on: {
                            col1: sequelize.where(sequelize.col('companies.id'), '=', sequelize.col('user.company_id')),
                        }
                    },
                    {
                        model: Userroles,
                        attributes: [],
                        on: {
                            col1: sequelize.where(sequelize.col('user_roles.id'), '=', sequelize.col('user.user_role')),
                        }
                    }
                ],
                where: {
                    id: id
                }
            });
            return JSON.parse(JSON.stringify(user_details));
        }
    } else {
        var data = {
            message: "Seller Agreement not accepted , try again !!!"
        };
        return data;
    }
}

async function verifyUserToken(_, { input: { token } }) {

    if (token != null) {
        const token_user = await User.findOne({
            where: { verification_token: token }
        });

        if (token_user && token_user.dataValues) {
            var tokenObject = token_user.dataValues;

            //Company Details Script Start
            if (tokenObject.company_id) {
                const companyDetails = await Companies.findOne({
                    attributes: ['id', 'company_name'],
                    where: { id: token_user.dataValues.company_id }
                });
                tokenObject.company_name = companyDetails.company_name;
            }
            //Company Details Script End

            return { status: true, token_user: tokenObject };
        } else {
            return { status: false, token_user: null };
        }

    } else {
        return { status: false, token_user: null };
    }

}


/* updateUserNeedsToSyncBank Section */
async function updateUserNeedsToSyncBank(_, { input: { id } }) {
    const user_details_row = await User.findOne({ where: { id: id } })
    if (user_details_row.id) {

        const result = await User.update(
            { needsToSyncBank: false },
            { where: { id: id } }
        )
        if (!result) {
            var data = {
                message: "Bank Sync not accepted , try again !!!"
            };
            return data;
        }
        else {
            User.hasMany(Userroles, {
                foreignKey: 'user_role'
            });
            User.hasMany(Companies, {
                foreignKey: 'company_id'
            });
            var user_details = await User.findOne({
                attributes: [
                    [sequelize.col("user.id"), "id"],
                    [sequelize.col("user.first_name"), "firstName"],
                    [sequelize.col('user.last_name'), 'lastName'],
                    [sequelize.fn('concat', sequelize.col('user.first_name'), ' ', sequelize.col('user.last_name')), 'fullName'],
                    [sequelize.col('user.phone'), 'phone'],
                    [sequelize.col('user.email'), 'email'],
                    [sequelize.col('user_roles.user_role_name'), 'userRole'],
                    [sequelize.col('companies.company_name'), 'companyName'],
                    [sequelize.col('user.stripe_id'), 'stripeId'],
                    [sequelize.col('user.mercuryid'), 'mercuryId'],
                    [sequelize.col('user.last_login'), 'last_login'],
                    [sequelize.col('user.street_address_1'), 'streetAddress'],
                    [sequelize.col('user.street_address_2'), 'streetAddress2'],
                    [sequelize.col('user.city'), 'city'],
                    [sequelize.col('user.state'), 'state'],
                    [sequelize.col('user.zip_code'), 'postalCode'],
                    [sequelize.col('user.country'), 'country'],
                    [sequelize.col('user.needsToChangePassword'), 'needsToChangePassword'],
                    [sequelize.col('user.needsToAcceptTerms'), 'needsToAcceptTerms'],
                    [sequelize.col('user.needsToAcceptPrivacy'), 'needsToAcceptPrivacy'],
                    [sequelize.col('user.needsToAcceptSeller'), 'needsToAcceptSeller'],
                    [sequelize.col('user.needsToSyncBank'), 'needsToSyncBank']
                ],
                include: [
                    {
                        model: Companies,
                        attributes: [],
                        on: {
                            col1: sequelize.where(sequelize.col('companies.id'), '=', sequelize.col('user.company_id')),
                        }
                    },
                    {
                        model: Userroles,
                        attributes: [],
                        on: {
                            col1: sequelize.where(sequelize.col('user_roles.id'), '=', sequelize.col('user.user_role')),
                        }
                    }
                ],
                where: {
                    id: id
                }
            });
            return JSON.parse(JSON.stringify(user_details));
        }
    } else {
        var data = {
            message: "Bank Sync not accepted , try again !!!"
        };
        return data;
    }
}

/* updateUserById Section */
async function updateUserById(_, {
    input: {
        id,
        firstName,
        lastName,
        email,
        city,
        companyName,
        country,
        phone,
        postalCode,
        state,
        streetAddress,
        streetAddress2
    }
}) {

    const user_details_row = await User.findOne({ where: { id: id } })

    if (user_details_row.id) {
        let companyId;
        //Note - Please dont remove this commented code

        /* if(user_details_row.user_role == 1 ||  user_details_row.user_role == 2 || user_details_row.user_role == 3){
            let userComanyDetails = await Companies.findOne({ attributes: ['id'], where: {created_by : id}});

            if (userComanyDetails && userComanyDetails.id) {
                await Companies.update({ company_name: companyName }, { where: { created_by: id } })
                companyId = userComanyDetails.id;
            } else {
                const updateCompany = await Companies.create({
                    company_name: companyName,
                    created_by: id,
                    created_at: Date.now(),
                    updated_at: Date.now()
                }, { returning: ['id'] });
                companyId = updateCompany.id;
            }
        } */

        const result = await User.update(
            {
                first_name: firstName,
                last_name: lastName,
                email: email,
                city: city,
                country: country,
                phone: phone,
                zip_code: postalCode,
                //company_id:companyId,
                state: state,
                street_address_1: streetAddress,
                street_address_2: streetAddress2
            },
            { where: { id: id } }
        )
        if (!result) {
            var errorMsg = {
                message: "Update Failed, try again !!!"
            };
            return errorMsg;
        }
        else {

            User.hasMany(Userroles, {
                foreignKey: 'user_role'
            });
            User.hasMany(Companies, {
                foreignKey: 'company_id'
            });
            var user_details = await User.findOne({
                attributes: [
                    [sequelize.col("user.id"), "id"],
                    [sequelize.col("user.first_name"), "firstName"],
                    [sequelize.col('user.last_name'), 'lastName'],
                    [sequelize.fn('concat', sequelize.col('user.first_name'), ' ', sequelize.col('user.last_name')), 'fullName'],
                    [sequelize.col('user.phone'), 'phone'],
                    [sequelize.col('user.email'), 'email'],
                    [sequelize.col('user_roles.user_role_name'), 'userRole'],
                    [sequelize.col('companies.company_name'), 'companyName'],
                    [sequelize.col('user.stripe_id'), 'stripeId'],
                    [sequelize.col('user.mercuryid'), 'mercuryId'],
                    [sequelize.col('user.last_login'), 'last_login'],
                    [sequelize.col('user.street_address_1'), 'streetAddress'],
                    [sequelize.col('user.street_address_2'), 'streetAddress2'],
                    [sequelize.col('user.city'), 'city'],
                    [sequelize.col('user.state'), 'state'],
                    [sequelize.col('user.zip_code'), 'postalCode'],
                    [sequelize.col('user.country'), 'country'],
                    [sequelize.col('user.needsToChangePassword'), 'needsToChangePassword'],
                    [sequelize.col('user.needsToAcceptTerms'), 'needsToAcceptTerms'],
                    [sequelize.col('user.needsToAcceptPrivacy'), 'needsToAcceptPrivacy'],
                    [sequelize.col('user.needsToAcceptSeller'), 'needsToAcceptSeller'],
                    [sequelize.col('user.needsToSyncBank'), 'needsToSyncBank']
                ],
                include: [
                    {
                        model: Companies,
                        attributes: [],
                        on: {
                            col1: sequelize.where(sequelize.col('companies.id'), '=', sequelize.col('user.company_id')),
                        }
                    },
                    {
                        model: Userroles,
                        attributes: [],
                        on: {
                            col1: sequelize.where(sequelize.col('user_roles.id'), '=', sequelize.col('user.user_role')),
                        }
                    }
                ],
                where: {
                    id: id
                }
            });
            return JSON.parse(JSON.stringify(user_details));
        }
    } else {
        var resData = {
            message: "Update Failed , try again !!!"
        };
        return resData;
    }
}

async function updateStripId(_, { input: { id, stripeId } }) {

    var timestamp = Math.round((new Date()).getTime() / 1000);
    let status = await User.update({ needsToSyncBank: false, stripe_id: stripeId + '_' + timestamp, verification_token: null }, { where: { id: id } });

    if (status[0]) {
        User.hasMany(Userroles, {
            foreignKey: 'user_role'
        });
        User.hasMany(Companies, {
            foreignKey: 'company_id'
        });
        var user_details = await User.findOne({
            attributes: [
                [sequelize.col("user.id"), "id"],
                [sequelize.col("user.first_name"), "firstName"],
                [sequelize.col('user.last_name'), 'lastName'],
                [sequelize.fn('concat', sequelize.col('user.first_name'), ' ', sequelize.col('user.last_name')), 'fullName'],
                [sequelize.col('user.phone'), 'phone'],
                [sequelize.col('user.email'), 'email'],
                [sequelize.col('user_roles.user_role_name'), 'userRole'],
                [sequelize.col('companies.company_name'), 'companyName'],
                [sequelize.col('user.stripe_id'), 'stripeId'],
                [sequelize.col('user.mercuryid'), 'mercuryId'],
                [sequelize.col('user.last_login'), 'last_login'],
                [sequelize.col('user.street_address_1'), 'streetAddress'],
                [sequelize.col('user.street_address_2'), 'streetAddress2'],
                [sequelize.col('user.city'), 'city'],
                [sequelize.col('user.state'), 'state'],
                [sequelize.col('user.zip_code'), 'postalCode'],
                [sequelize.col('user.country'), 'country'],
                [sequelize.col('user.needsToChangePassword'), 'needsToChangePassword'],
                [sequelize.col('user.needsToAcceptTerms'), 'needsToAcceptTerms'],
                [sequelize.col('user.needsToAcceptPrivacy'), 'needsToAcceptPrivacy'],
                [sequelize.col('user.needsToAcceptSeller'), 'needsToAcceptSeller'],
                [sequelize.col('user.needsToSyncBank'), 'needsToSyncBank']
            ],
            include: [
                {
                    model: Companies,
                    attributes: [],
                    on: {
                        col1: sequelize.where(sequelize.col('companies.id'), '=', sequelize.col('user.company_id')),
                    }
                },
                {
                    model: Userroles,
                    attributes: [],
                    on: {
                        col1: sequelize.where(sequelize.col('user_roles.id'), '=', sequelize.col('user.user_role')),
                    }
                }
            ],
            where: {
                id: id
            }
        });
        let userDetailsJSON = JSON.parse(JSON.stringify(user_details));
        return {
            status: true,
            userInfo: userDetailsJSON
        }
    } else {
        return {
            status: false,
            userInfo: null
        }
    }
}

async function getAllSuperAdminDetails() {
    User.hasMany(Userroles, {
        foreignKey: 'user_role'
    });

    const superAdminDetails = await User.findAll(
        {
            attributes: ['id', 'first_name', 'last_name', 'email', 'company_id', 'created_at', 'updated_at'],
            include: [
                {
                    model: Userroles,
                    attributes: [],
                    on: {
                        col1: sequelize.where(sequelize.col('user.user_role'), '=', sequelize.col('user_roles.id')),
                    }
                }
            ],
            where: { 'active': 1, '$user_roles.user_role_name$': userRolesLower.super_admin }
        }
    )
    if (superAdminDetails) {
        return JSON.parse(JSON.stringify(superAdminDetails));
    } else {
        return "No data";
    }
}

module.exports = {
    allUsers,
    allCountries,
    allStates,
    allCities,
    citiesCount,
    getCountry,
    getCity,
    getState,
    allRoleUsers,
    userRoles,
    emailExists,
    getuser,
    registerUser,
    updatePassword,
    updateUserNeedsToAcceptTerms,
    updateUserNeedsToAcceptPrivacy,
    updateUserNeedsToAcceptSeller,
    updateUserNeedsToSyncBank,
    updateUserById,
    verifyUserToken,
    updateStripId,
    getAllSuperAdminDetails,
    addChannelFees,
    getChannelFee
};


