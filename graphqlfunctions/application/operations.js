const db = require('../../models');
import { seat_type, commonVariables, inputType, userRolesLower, listingStatusList, operationGridConstant } from "../../constants/constants";
const {getUserIdsForListingGroupAccess, getUserRole} = require('../../helpers/userAccess.js');
const {getEventDetails, getListingDetails, getListingStatusbyName} = require('../../helpers/queryResponse');
const { QueryTypes } = require('sequelize');
const {isValidInput} = require('../../helpers/inputValidate.js');
const Op = db.Sequelize.Op;
const ListingGroup = db.listinggroup;
const ListingStatus = db.listingStatus;
const TicketingSystemAccounts = db.ticketingsystemaccounts;
const PurchaseOrder = db.purchaseorder;
const User = db.user;
const Disclosures = db.disclosures;
const Tags = db.tags;
const Listings = db.listings;

/**
 * Page : Operation Listings Filter
 * Function For : To Get Default & Filtered Listings
 * Ticket No : TIC 657
 */
async function operationFilterListings(_, { input: { userId, searches, from_date, to_date, day_of_week, ticket_type, genre, tags, in_hand, section, row, quantity, inventory_attached, held, coordinates, radius, days, category, performer, isfutureevents, hold, page, limit,tagIds = [] ,venue, purchaseDate, split, company, userAssignedTo, ticketingSystemAccount} }) {
    try {
        let paginationCondition = '';
        if (limit && page) {
            paginationCondition = ` limit ${limit} OFFSET ${(page - 1) * limit} `;
        }
        let listingGroupUserIds = '';
        if (userId) {
            let listingGroupUserIdsResponse = await getUserIdsForListingGroupAccess(userId);
            if (listingGroupUserIdsResponse && listingGroupUserIdsResponse.length > 0) {
                listingGroupUserIds = listingGroupUserIdsResponse;
            }
        }
        let eventIDsArr = [],
            whereStr = '',
            listingWhereStr = '',
            finalWhereStr = '',
            finalListingWhereStr = '';

        if (searches && searches != "" && searches != null && searches != 'null') {
            whereStr += '( ';
            if(Number(searches)){
                if (Number.isInteger(parseInt(searches))) {
                    whereStr += ' "event"."id" = ' + parseInt(searches) + ' or ';
                    whereStr += ' "purchase_order"."id" = ' + parseInt(searches) + ' or ';
                }
            }
            whereStr += '"event"."name" iLike \'%' + searches + '%\'';
            whereStr += ' or "performer"."name" iLike \'%' + searches + '%\'';
            whereStr += ' or "venue"."name" iLike \'%' + searches + '%\'';
            whereStr += ' or concat("venue"."city", \', \', "venue"."country") iLike \'%' + searches + '%\'';
            whereStr += ' or "venue"."country" iLike \'%' + searches + '%\'';
            whereStr += ' or "eventtag"."tag_name" iLike \'%' + searches + '%\'';
            whereStr += ' or "listingtag"."tag_name" iLike \'%' + searches + '%\'';
            whereStr += ' or "companies"."company_name" iLike \'%' + searches + '%\'';
            whereStr += ' or "purchase_order"."external_reference" iLike \'%' + searches + '%\'';
            whereStr += ' )';
        }

        if ((from_date && from_date != "" && from_date != null && from_date != 'null')) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += ' ("event"."date" >= \'' + from_date + '\'';
            if (to_date && to_date != "" && to_date != null && to_date != 'null') {
                whereStr += ' and "event"."date" <= \'' + to_date + '\'';
            }
            whereStr += ' )';
        }
        if (isfutureevents) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += ' ("event"."date" >= CURRENT_TIMESTAMP ) ';
        }

        if (tagIds && tagIds.length) {
            whereStr += ` ${whereStr != ""?' and ':''} listing_tags.tag_id in (${tagIds.join()})`;
        }

        if (day_of_week && day_of_week != "" && day_of_week != null && day_of_week != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += `"event"."eventdate_day" in ( '` + day_of_week.join("','") + `'  )`;
        }

        if (ticket_type && ticket_type != "" && ticket_type != null && ticket_type != 'null' && Number.isInteger(parseInt(ticket_type))) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"listing_group"."ticket_type_id" = ' + ticket_type;
            listingWhereStr += '"listing_group"."ticket_type_id" = ' + ticket_type;
        }

        if (genre && genre != "" && genre != null && genre != 'null' && Number.isInteger(parseInt(genre))) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"event"."genre_id" = ' + genre;
        }

        if (category && category != "" && category != null && category != 'null' && Number.isInteger(parseInt(category))) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"event"."category_id" = ' + category;
        }

        if (performer && performer != "" && performer != null && performer != 'null' && Number.isInteger(parseInt(performer))) {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += ' "event"."performer_id" = ' + performer + ' ';
        }
       
        if(await isValidInput(inputType.id,[venue])){ 
            whereStr += ` ${ whereStr != '' ? 'and' : '' } "venue"."id" = ${parseInt(venue)} `
        }

        if (tags && tags != "" && tags != null && tags != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '("eventtag"."tag_name" iLike \'%' + tags + '%\'';
            whereStr += ' or "listingtag"."tag_name" iLike \'%' + tags + '%\')';
            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += '"listingtag"."tag_name" iLike \'%' + tags + '%\'';
        }

        if (in_hand && in_hand != "" && in_hand != null && in_hand != 'null') {
            let in_hand_from = in_hand + ' 00:00:00';
            let in_hand_to = in_hand + ' 23:59:59';

            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += ' ("listing"."in_hand" >= \'' + in_hand_from + '\' and "listing"."in_hand" <= \'' + in_hand_to + '\')';
        }

        if (purchaseDate && purchaseDate != "" && purchaseDate != null && purchaseDate != 'null') {
            let purchaseDateFrom = purchaseDate + ' 00:00:00';
            let purchaseDateTo = purchaseDate + ' 23:59:59';

            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += ' ("purchase_order"."purchase_date" >= \'' + purchaseDateFrom + '\' and "purchase_order"."purchase_date" <= \'' + purchaseDateTo + '\')';
        }

        if (section && section != "" && section != null && section != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"listing_group"."section" iLike \'%' + section + '%\'';
            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += '"listing_group"."section" iLike \'%' + section + '%\'';
        }

        if (row && row != "" && row != null && row != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"listing_group"."row" iLike \'%' + row + '%\'';
            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += '"listing_group"."row" iLike \'%' + row + '%\'';
        }

        if (quantity && quantity != "" && quantity != null && quantity != 'null') {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += '"listing_group"."quantity" = ' + quantity;
            if (listingWhereStr != "") {
                listingWhereStr += ' and ';
            }
            listingWhereStr += '"listing_group"."quantity" = ' + quantity;
        }

        if (split && split != "" && split != null && split != 'null') {
            if(Number(split)){
                if (whereStr != "") {
                    whereStr += ' and ';
                }
                whereStr += '"listing_group"."splits_id" = ' + parseInt(split);
                if (listingWhereStr != "") {
                    listingWhereStr += ' and ';
                }
                listingWhereStr += '"listing_group"."splits_id" = ' + parseInt(split);
            }
        }
        if (ticketingSystemAccount && ticketingSystemAccount != "" && ticketingSystemAccount != null && ticketingSystemAccount != 'null') {
            const accountIdList = await TicketingSystemAccounts.findAll({
                attributes: ['id'],
                where: { account_name: ticketingSystemAccount },
                raw:true
            });
            
            let listingAccountId = '';
            if(accountIdList && accountIdList.length){                   
                for (var accountId of accountIdList) {
                    listingAccountId += ',' + accountId.id;
                }
                if (listingAccountId != "") {
                    listingAccountId = listingAccountId.substr(1);
                }                
            }
            
            if(listingAccountId != "") {
                if (whereStr != "") {
                    whereStr += ' and ';
                }
                whereStr += '"listing_group"."ticket_system_account" in (' + listingAccountId +')';
                if (listingWhereStr != "") {
                    listingWhereStr += ' and ';
                }
                listingWhereStr += '"listing_group"."ticket_system_account" in (' + listingAccountId +')';
            }
        }
        
        if (inventory_attached && inventory_attached != "" && inventory_attached != null && inventory_attached != 'null') {
            if (inventory_attached == commonVariables.yesValue) {
                if (listingWhereStr != "") {
                    listingWhereStr += ' and ';
                }
                listingWhereStr += '"listing"."pdf" is not null';
            }
            else if (inventory_attached == commonVariables.noValue) {
                if (listingWhereStr != "") {
                    listingWhereStr += ' and ';
                }
                listingWhereStr += '"listing"."pdf" is null';
            }
        }

        let selectQuery = '';
        let distance = '';
        if (coordinates && coordinates != '' && radius && radius != '') {
            coordinates = JSON.parse(coordinates);
            selectQuery += ", array_agg(3959 * acos( cos( radians('" + coordinates.lat + "') ) * cos( radians( venue.latitude ) ) * cos( radians( longitude ) - radians('" + coordinates.lng + "') ) + sin( radians('" + coordinates.lat + "') ) * sin( radians( latitude ) ) ) ) AS distance";
            distance = " and distance <= '{" + radius + ".0}'  ";
        }

        if (days && days != "") {
            if (whereStr != "") {
                whereStr += ' and ';
            }
            whereStr += " event.date BETWEEN CURRENT_DATE AND CURRENT_DATE + INTERVAL '" + days + " days' ";
        }

        var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);
        if (whereStr != "") {
            finalWhereStr = ' and (' + whereStr + ')';
        } else {
            finalWhereStr = ' and listing_group.event_id = event.id ';
            if (listingGroupUserIds) {
                finalWhereStr += ' and listing_group.created_by in (' + listingGroupUserIds + ')';
            }
        }

        if(qcListingStatus && qcListingStatus.id >0){
            finalWhereStr += ' and listing_group.listing_status_id =' + qcListingStatus.id;
        }

        if (listingWhereStr != "") {
            finalListingWhereStr = ' and (' + listingWhereStr + ')';
        }
        if (hold && hold != '' && hold != null) {
            let holdStatus = hold == commonVariables.yesValue ? true : false;
            finalListingWhereStr = ' and listing.hold =' + holdStatus;
        }
        if (company && company != "" && company != null && company != 'null') {
            if(Number(company)){
                if (finalListingWhereStr == "") {
                    finalListingWhereStr = `and companyDetail.company_id=` + parseInt(company);
                } else {
                    finalListingWhereStr += ' and ';
                    finalListingWhereStr += `companyDetail.company_id=` + parseInt(company);
                }
                
            }
        }

        var operationSelectQueryData = `, listing_group.ticket_system_account, listing_group.quantity, array_agg(distinct seat_type.name) as seat_type_name, array_agg(distinct purchase_order.id) as purchase_order_id, array_agg(distinct purchase_order.purchase_date) as purchase_date, array_agg(distinct purchase_order.payment_last4_digits) as payment_last4_digits, array_agg(distinct purchase_order.external_reference) as external_id, array_agg(distinct ticketing_system_accounts.account_name) as account, array_remove( array_agg(distinct listingdisc.disclosure_name), null ) as disclosure, array_agg(distinct listing_disclosures.disclosure_id) as listingdisclosure, array_agg(distinct splits.name) as split_name, array_remove( array_agg(distinct companyDetail.company_id), null ) as company_id, array_remove( array_agg(distinct companyDetail.company_name), null ) as company_name ,`;

        var operationJoinQuery = `left outer join ticket_exchange.seat_type on listing_group.seat_type_id = seat_type.id left outer join ticket_exchange.purchase_order on purchase_order.listing_group_id = listing_group.id left outer join ticket_exchange.ticketing_system_accounts on listing_group.ticket_system_account = ticketing_system_accounts.id left outer join ticket_exchange.splits on listing_group.splits_id = splits.id left outer join ticket_exchange.listing_disclosures on listing_disclosures.listing_group_id = listing_group.id left outer join ( select listing_disclosures.listing_group_id, listing_disclosures.disclosure_id, disclosures.disclosure_name from ticket_exchange.listing_disclosures inner join ticket_exchange.disclosures on listing_disclosures.disclosure_id = disclosures.id where 1 = 1 ) listingdisc on listingdisc.listing_group_id = listing_group.id left outer join ( select "user".id, companies.id as company_id, companies.company_name from ticket_exchange.companies inner join ticket_exchange."user" on companies.id = "user".company_id  where 1 = 1 ) companyDetail on companyDetail.id = listing_group.created_by`;
        
        const eventDetails = await getEventDetails(selectQuery,finalWhereStr,paginationCondition,distance);
        for (var eventList of eventDetails) {
            eventIDsArr.push(eventList.event_id);
        }
        
        if (searches && searches != "" && searches != null && searches != 'null') {
            let listingEventId = '';
            if (eventIDsArr.length > 0) {        
                for (var eventId of eventIDsArr) {
                    listingEventId += ',' + eventId;
                }
                if (listingEventId != "") {
                    listingEventId = listingEventId.substr(1);
                }
            }
            
            finalListingWhereStr += `and listing_group.event_id in (` + listingEventId + `) `;
            finalListingWhereStr += `and ( purchase_order.external_reference iLike '%` + searches + `%'`
            finalListingWhereStr += `or companyDetail.company_name iLike '%` + searches + `%'`
            finalListingWhereStr += `or tags.tag_name iLike '%` + searches + `%' or 1 = 1)`

            if(Number(searches)){
                if (Number.isInteger(parseInt(searches))) {
                    finalListingWhereStr += 'or purchase_order.id =' + parseInt(searches) ;
                }
            }
        }

        finalListingWhereStr += ' and is_removed = false';

        if(qcListingStatus && qcListingStatus.id >0){
            finalListingWhereStr += ' and listing_group.listing_status_id =' + qcListingStatus.id;
        }

        if (listingGroupUserIds) {
            finalListingWhereStr += ' and listing_group.created_by in (' + listingGroupUserIds + ') ';
        }

        if (eventIDsArr.length > 0) {
            const listingDetails = await getListingDetails(operationSelectQueryData,operationJoinQuery,finalListingWhereStr);

            for (var eventDetailCount = 0; eventDetailCount < eventDetails.length; eventDetailCount++) {
                let listingsRes = [];
                for (var listingInfoCount = 0; listingInfoCount < listingDetails.length; listingInfoCount++) {
                    if (listingDetails[listingInfoCount].event_id == eventDetails[eventDetailCount].event_id) {
                        let obj = {};
                        obj['listingGroupId'] = listingDetails[listingInfoCount].listinggroupid;
                        obj['listingId'] = listingDetails[listingInfoCount].listingid;
                        obj['eventId'] = listingDetails[listingInfoCount].event_id;
                        obj['sold'] = listingDetails[listingInfoCount].sold;
                        obj['section'] = listingDetails[listingInfoCount].section;
                        obj['row'] = listingDetails[listingInfoCount].row;
                        obj['seatdtl'] = listingDetails[listingInfoCount].seatdtl;
                        obj['price'] = listingDetails[listingInfoCount].price;
                        obj['last_price'] = listingDetails[listingInfoCount].last_price;
                        obj['last_price_change'] = listingDetails[listingInfoCount].last_price_change;
                        obj['face_value'] = listingDetails[listingInfoCount].face_value;
                        obj['cost'] = listingDetails[listingInfoCount].cost;
                        obj['margin'] = listingDetails[listingInfoCount].margin;
                        obj['margintype'] = listingDetails[listingInfoCount].margintype;
                        obj['availabletkts'] = listingDetails[listingInfoCount].totalavailable;
                        obj['soldtkts'] = listingDetails[listingInfoCount].totalsold;
                        obj['tags'] = listingDetails[listingInfoCount].tag_name;
                        obj['ticketTypeName'] = listingDetails[listingInfoCount].tickettypename;
                        obj['active'] = listingDetails[listingInfoCount].active;
                        obj['inactive'] = listingDetails[listingInfoCount].inactive;
                        obj['listing_inactive'] = listingDetails[listingInfoCount].listing_inactive;
                        obj['price_done_by'] = listingDetails[listingInfoCount].price_by;
                        obj['in_hand'] = listingDetails[listingInfoCount].in_hand;
                        obj['seat_type'] = listingDetails[listingInfoCount].seat_type_name;
                        obj['external_id'] = listingDetails[listingInfoCount].external_id;
                        obj['purchaseOrder'] = listingDetails[listingInfoCount].purchase_order_id;
                        obj['purchaseDate'] = listingDetails[listingInfoCount].purchase_date;
                        obj['account'] = listingDetails[listingInfoCount].account;
                        obj['last4_digits'] = listingDetails[listingInfoCount].payment_last4_digits;
                        obj['disclosure'] = listingDetails[listingInfoCount].disclosure;
                        obj['splits'] = listingDetails[listingInfoCount].split_name;
                        obj['listing_status_id'] = listingDetails[listingInfoCount].listing_status_id;
                        obj['company'] = listingDetails[listingInfoCount].company_name;
                        obj['seatStart'] = listingDetails[listingInfoCount].seat_start;
                        obj['seatEnd'] = listingDetails[listingInfoCount].seat_end;
                        obj['ticketTypeId'] = listingDetails[listingInfoCount].ticket_type_id;
                        obj['seatTypeId'] = listingDetails[listingInfoCount].seat_type_id;
                        obj['splitId'] = listingDetails[listingInfoCount].splits_id;
                        obj['companyId'] = listingDetails[listingInfoCount].company_id;
                        obj['ticketSystemAccountId'] = listingDetails[listingInfoCount].ticket_system_account;
                        obj['disclosureValue'] = [];
                        obj['tagValue'] = [];
                        obj['quantity'] = listingDetails[listingInfoCount].quantity;

                        if (listingDetails[listingInfoCount].disclosure && listingDetails[listingInfoCount].disclosure.length) {
                            var disclosureArray = []
                            for(var data of listingDetails[listingInfoCount].disclosure){
                                var disclosure = await Disclosures.findOne({ attributes: [['id', 'value'], ['disclosure_name','label']], where: { disclosure_name: data }, raw:true });
                                disclosureArray.push(disclosure)
                            }
                            obj['disclosureValue'] = disclosureArray;
                        }
                        if (listingDetails[listingInfoCount].tag_name && listingDetails[listingInfoCount].tag_name.length) {
                            var tagsArray = []
                            for(var name of listingDetails[listingInfoCount].tag_name){
                                var tag = await Tags.findOne({ attributes: [['id', 'value'], ['tag_name','label']], where: { tag_name: name }, raw:true });
                                tagsArray.push(tag)
                            }
                            obj['tagValue'] = tagsArray;
                        }

                        listingsRes.push(obj);
                    }
                }
                eventDetails[eventDetailCount].listingDetails = listingsRes;
            }
        }
        var operationJson = [];
        if (eventDetails && eventDetails.length ) {

            for (let event of eventDetails) {
                var eventObject = {};
                eventObject.section = 'big-title';
                eventObject.eventName = event.eventname;
                eventObject.eventDate = event.eventdate;
                eventObject.timezone = event.timezone[0];
                eventObject.location = event.venuename[0] + " , " + event.city[0] + " , " + event.state[0] + " , " + event.country[0] ;
                eventObject.locationValue = event.eventname + " | " + event.eventdate + " | " + event.venuename[0] + " , " + event.city[0] + " , " + event.state[0] + " , " + event.country[0];
                eventObject.eventId= event.event_id;

                if(event.listingDetails && event.listingDetails.length ){
                    operationJson.push(eventObject);

                    for (let listing of event.listingDetails) {
                        var listingObject = {};
                        listingObject.listingGroupId = listing.listingGroupId;
                        listingObject.inHandDate = listing.in_hand;
                        listingObject.sec = listing.section;
                        listingObject.row = listing.row;
                        listingObject.seat = listing.seatdtl;
                        listingObject.cost = listing.cost;
                        listingObject.seatType = listing.seat_type && listing.seat_type.length ? listing.seat_type[0] : "" ;
                        listingObject.stockType = listing.ticketTypeName && listing.ticketTypeName.length ? listing.ticketTypeName[0] : "" ;
                        listingObject.externalId = listing.external_id && listing.external_id.length ? listing.external_id[0] : "" ;
                        listingObject.purchaseOrder = listing.purchaseOrder && listing.purchaseOrder.length ? listing.purchaseOrder[0] : "" ;
                        listingObject.purchaseDate = listing.purchaseDate && listing.purchaseDate.length ? listing.purchaseDate[0] : "" ;
                        listingObject.account= listing.account && listing.account.length ? listing.account[0] : "" ;
                        listingObject.assign= '';
                        listingObject.owner= listing.company && listing.company.length ? listing.company[0] : "" ;
                        listingObject.lastDigits= listing.last4_digits && listing.last4_digits.length ? listing.last4_digits[0] : "" ;
                        listingObject.tags= listing.tags;
                        listingObject.disclosure= listing.disclosure;
                        listingObject.splits= listing.splits;
                        listingObject.listing_status_id= listing.listing_status_id;
                        listingObject.eventId= listing.eventId;
                        listingObject.seatStart= listing.seatStart;
                        listingObject.seatEnd= listing.seatEnd;
                        listingObject.ticketType= {value:listing.ticketTypeId, label:listing.ticketTypeName && listing.ticketTypeName.length ? listing.ticketTypeName[0] : ""};
                        listingObject.seatTypeValue= {value:listing.seatTypeId, label:listing.seat_type && listing.seat_type.length ? listing.seat_type[0] : "" };
                        listingObject.splitValue= {value:listing.splitId, label:listing.splits && listing.splits.length ? listing.splits[0] : ""};
                        listingObject.companyValue= {value:listing.companyId && listing.companyId.length ? listing.companyId[0] : "", label:listing.company.length ? listing.company[0] : "" };
                        listingObject.ticketingSystemAccount= {value:listing.ticketSystemAccountId, label:listing.account && listing.account.length ? listing.account[0] : "" };
                        listingObject.disclosureValue= listing.disclosureValue;
                        listingObject.tagValue= listing.tagValue;
                        listingObject.inHandDateValue= listing.in_hand;
                        var inHandDateTime = listing.in_hand ? new Date(listing.in_hand) : '';
                        let inHandDate = '';
                        if(inHandDateTime != ''){
                            var year = inHandDateTime.getFullYear();
                            var month = (inHandDateTime.getMonth() + 1) < 10 ? '0' + (inHandDateTime.getMonth() + 1) : (inHandDateTime.getMonth() + 1);
                            var date = (inHandDateTime.getDate()) < 10 ? '0' + inHandDateTime.getDate() : inHandDateTime.getDate();
                            inHandDate = date + '-' + month + '-' + year;
                        }
                        listingObject.inHandDateValue = inHandDate;
                        listingObject.quantity = listing.quantity;

                        operationJson.push(listingObject)
                    }
                }
            }
            
            
            let finalResJSON = {};
            if (operationJson) {
                let jsonData = JSON.parse(JSON.stringify(operationJson));
                finalResJSON['isError'] = false;
                finalResJSON['msg'] = 'Data Returned Successfully.';
                finalResJSON['data'] = jsonData;
            }
            else {
                finalResJSON['isError'] = true;
                finalResJSON['msg'] = 'Some Error in Retrieving Data.';
                finalResJSON['data'] = [];
            }
            return finalResJSON;

        } else {
            return {
                'isError' : true,
                'msg' : 'Some Error in Retrieving Data.',
                'data' : []
            };
        }
    }
    catch (e) {
        console.log(e)
        return {
            'isError' : true,
            'msg' : 'Some Error in Retrieving Data.'
        };
    }
}

/**
 * To change qc hold status to inactive status 
 * @param {*} [listingGroupId] 
 * @param {*} 
 * @returns <promise> array{object}
 */

async function changeQCStatus(_, { input:
    { listingGroupId }
}) {

    var inActiveListingStatus = await getListingStatusbyName(listingStatusList.inactive);
    var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);

    var result;
    if(inActiveListingStatus && qcListingStatus){
        result = await ListingGroup.update({ listing_status_id:inActiveListingStatus.id }, { where: { id: { [Op.in]: listingGroupId }, listing_status_id:qcListingStatus.id } });        
    }

    if(result){
        return {
            "isError": false,
            "msg": "QC tag removed successfully."
        };
    }else {
        return {
            "isError": true,
            "msg": "Some error in updating qc status."
        }
    }
}

async function allListingStatus() {
    const allListingStatusInfo = await ListingStatus.findAll({
        attributes: ['id', 'name', 'status']
    });
    
    if (allListingStatusInfo) {
        return JSON.parse(JSON.stringify(allListingStatusInfo));
    } else {
        return "No data";
    }
}

/**
 * To change qc hold status to inactive status 
 * @param {*} [listingGroupId] 
 * @param {*} 
 * @returns <promise> array{object}
 */

async function updateListingFromGrid(_, { input:
    { listingGroupId, columnName, section, row, owner, ticketingSystemAccount, externalReference, inHandDate, purchaseOrderDate, last4Digits, seatStart, seatEnd, quantity }
}) {
    var qcListingStatus = await getListingStatusbyName(listingStatusList.qc_hold);
    var listingGroupDetail = await ListingGroup.findOne({ attributes: ['id', 'created_by', 'listing_status_id'], where: { id: listingGroupId }, raw:true });

    var result;
    
    if(columnName == operationGridConstant.section){
       result = await ListingGroup.update({ section:section }, { where: { id: listingGroupId, listing_status_id:qcListingStatus.id } });        
    }
    if(columnName == operationGridConstant.quantity){
       result = await ListingGroup.update({ quantity:quantity }, { where: { id: listingGroupId, listing_status_id:qcListingStatus.id } });        
    }
    if(columnName == operationGridConstant.row){        
       result = await ListingGroup.update({ row:row }, { where: { id: listingGroupId, listing_status_id:qcListingStatus.id } });        
    }
    if(columnName == operationGridConstant.ticketingSystemAccount){
       result = await ListingGroup.update({ ticket_system_account:ticketingSystemAccount }, { where: { id: listingGroupId, listing_status_id:qcListingStatus.id } });
    }
    if(columnName == operationGridConstant.externalReference){
       result = await PurchaseOrder.update({ external_reference:externalReference }, { where: { listing_group_id: listingGroupId } });
    }
    if(columnName == operationGridConstant.owner && listingGroupDetail && listingGroupDetail.created_by){
       result = await User.update({ company_id:owner }, { where: { id: listingGroupDetail.created_by } });
    }
    if(columnName == operationGridConstant.inHandDate){
        result = await ListingGroup.update({ in_hand:inHandDate }, { where: { id: listingGroupId, listing_status_id:qcListingStatus.id } }); 
    }
    if(columnName == operationGridConstant.purchaseOrderDate){
        result = await PurchaseOrder.update({ purchase_date:purchaseOrderDate }, { where: { listing_group_id: listingGroupId } });
    }
    if(columnName == operationGridConstant.last4Digits){
        result = await PurchaseOrder.update({ payment_last4_digits:last4Digits }, { where: { listing_group_id: listingGroupId } });
    }
    if(columnName == operationGridConstant.seatNumberInfo){
        var quantityCount = (seatEnd - seatStart) + 1;

        var updateListData = {
            quantity: quantityCount,
            seat_start: seatStart,
            seat_end: seatEnd
        };

        result = await ListingGroup.update( updateListData, { where: { id: listingGroupId } } );

        if (seatStart != "" && seatEnd != "") {
            let listingsData = await Listings.findAll({ where: { listing_group_id: listingGroupId }, attributes: ['id', 'unit_cost', 'in_hand', 'face_value', 'seat_number', 'price', 'last_price'], raw: true });
            
            //Seat Numbers Already Saved In Database
            let seatNumbersDataInDb = [];
            listingsData.map(listings => {
                seatNumbersDataInDb.push(parseInt(listings.seat_number));
            })

            //Current Seat Numbers Selected
            let selectedSeatNumbers = [];
            for (var seatCount = seatStart; seatCount <= seatEnd; seatCount++) {
                selectedSeatNumbers.push(parseInt(seatCount));
            }

            //Attributes To Insert New
            let insertListingsData = selectedSeatNumbers.filter(x => seatNumbersDataInDb.indexOf(x) === -1);
            if (insertListingsData.length) {
                for (var listingDataCount = 0; listingDataCount < insertListingsData.length; listingDataCount++) {
                    var listingsInputData = {
                        listingGroupId: listingGroupId,
                        in_hand: listingsData[0].in_hand,
                        face_value: listingsData[0].face_value,
                        seat_number: insertListingsData[listingDataCount],
                        unit_cost: listingsData[0].unit_cost,
                        price: listingsData[0].price
                    };
                    await Listings.create(listingsInputData, { returning: ['id'] });
                }
            }

            //Listings to Delete
            let deleteListingsData = seatNumbersDataInDb.filter(x => selectedSeatNumbers.indexOf(x) === -1);
            if (deleteListingsData.length) {
                for (var deleteListingCount = 0; deleteListingCount < deleteListingsData.length; deleteListingCount++) {
                    let seatNumber = '' + deleteListingsData[deleteListingCount];
                    await Listings.destroy({ where: { listing_group_id: parseInt(listingGroupId), seat_number: seatNumber } });
                }
            }
        }
    }

    if(result){
        return {
            "isError": false,
            "msg": "Listing updated successfully."
        };
    }else {
        return {
            "isError": true,
            "msg": "Some error in updating listing."
        }
    }
    
}

module.exports = {
    operationFilterListings,
    changeQCStatus,
    allListingStatus,
    updateListingFromGrid
};


