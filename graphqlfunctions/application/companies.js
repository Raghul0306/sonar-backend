const db = require('../../models');
const User = db.user;
const Op = db.Sequelize.Op;
const Company = db.companies;
const Companyuser = db.companyuser;
import axios from "axios";
import { s3Path, folderNames, tableNames, uploadListingsColumnNames, splitTypes, shareTypes, seat_type, seatTypes, ticketTypes, commonVariables, levelNames, listingVariables, uploadURLs, userRolesLower, inputType } from "../../constants/constants";
const csv = require('csv-parser');
const fs = require('fs')
var request = require('request');
var FormData = require('form-data');
const reader = require('xlsx');
const {getUserIdsForListingGroupAccess, getUserRole} = require('../../helpers/userAccess.js');

const AWS = require('aws-sdk');
const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});
const sequelize = require('sequelize');
const { QueryTypes, where } = require('sequelize');
const { table } = require('console');
const {isValidInput} = require('../../helpers/inputValidate.js');

/**
 * Page : MyTickets 
 * Function For : To Get All EventID, Performer, Venue & Listings Details For Auto Suggest
 * Ticket No : TIC 17, 104, 105 & 106
 */
 async function getAllCompanies() {
    const companyDetails = await db.sequelize.query(`SELECT distinct companies.id, companies.parent_company_id, parentcompany.company_name as parentcompanyname, companies.company_name, companies.address1, companies.address2, companies.city, companies.state, companies.country, companies.zip_code, companies.created_at, companies.updated_at, users.company_id AS usercompany, users.user_role as userroleid, users.user_role_name FROM ticket_exchange.companies LEFT OUTER JOIN (select users.company_id, users.user_role, user_roles.user_role_name from ticket_exchange.user as users left outer join ticket_exchange.user_roles on users.user_role = user_roles.id where user_roles.user_role_name = 'super_admin') as users ON companies.id = users.company_id left outer join ticket_exchange.companies as parentcompany on companies.parent_company_id = parentcompany.id where 1 = 1 order by companies.company_name;`, { type: QueryTypes.SELECT });
    
    if (companyDetails) {
        return companyDetails;
    } else {
        return "No data";
    } 
}

/**
 * Page : MyTickets 
 * Function For : To get assigned company list
 * Ticket No : TIC 647
 */
 async function getAssignedCompanies(_, { input: { userId } }) {

    let childCompanyIDs = [];
    /* const companyusers = await Companyuser.findAll({ 
        attributes: ['id','company_id'],
        where: { user_id: userId } }); */

        const childCompanyDetails = await db.sequelize.query(`select users.id, users.company_id, users.user_role, user_roles.user_role_name, COALESCE(companies.id,users.company_id) as childcompanyid from ticket_exchange.user as users inner join ticket_exchange.user_roles on users.user_role = user_roles.id left outer join ticket_exchange.companies on users.company_id = companies.parent_company_id where users.id = `+ userId + ` order by users.user_role;`, { type: QueryTypes.SELECT });

    if (childCompanyDetails && childCompanyDetails.length) {
        for(var childCompanyDataCount=0; childCompanyDataCount<childCompanyDetails.length; childCompanyDataCount++)
        {
            childCompanyIDs.push(childCompanyDetails[childCompanyDataCount].childcompanyid)
        }
    }

    const companyDetails = await Company.findAll({
        attributes: ['id', 'company_name', 'address1', 'address2', 'city', 'state', 'country', 'zip_code','created_at', 'updated_at'],
        where: { id: childCompanyIDs }
    });
    if (companyDetails) {
        return JSON.parse(JSON.stringify(companyDetails));
    } else {
        return "No data";
    }
}

async function saveCompanyDetails(_, { input: { userId, companyId, parentCompanyId, companyName,  address1, address2, city, state, zip_code, country } }) {
    let companyDetails = [];
    let formData = {
        company_name: companyName,
        parent_company_id: parentCompanyId, 
        address1: address1,
        address2: address2,
        city: city,
        state: state,
        zip_code: zip_code,
        country: country
    }
    if(companyId > 0)
    {
        formData.updated_by = userId;
        companyDetails = await Company.update(
            formData,
            { where: { id: companyId } }, 
            { returning: ['id'] }
        )
    }
    else
    {
        formData.created_by = userId;
        companyDetails = await Company.create(
            formData, 
            { returning: ['id'] }
        );
        companyId = companyDetails.id;
    }

    var data = { 'isError': false, 'message': 'Company Data Saved !!!',  'companyId': companyId}
    return data;
}

async function saveCompanyAssignedSuperAdmin(_, { input: { userId, companyId, superAdminId } }) {
    
    var data;
    const companyusers = await Companyuser.findOne({ where: { user_id: superAdminId, company_id: companyId } })
    if (companyusers && companyusers.dataValues) {
        data = { 'isError': true, 'message': 'Company User Already Exists  !!!',  'companyId': companyId}
    }
    else {
        var companyUsersInputData = {
            company_id: companyId,
            user_id: superAdminId,
            created_by: userId
        };
        await Companyuser.create(companyUsersInputData);
        await User.update(
            { parent_id: superAdminId },
            { where: { company_id: companyId } }, 
            { returning: ['id'] }
        )
        data = { 'isError': false, 'message': 'Company Data Saved !!!',  'companyId': companyId}
    }
    return data;
    
}

module.exports = {
    getAllCompanies, 
    saveCompanyDetails, 
    saveCompanyAssignedSuperAdmin,
    getAssignedCompanies
};


