const db = require('../../models');
const Listings = db.listings;
const BuyerUsers = db.buyerUsers;
const Transaction = db.transaction;
const TransactionListings  = db.transactionlisting;
const _ = require('lodash');
const Op = db.Sequelize.Op;
/**
 * Page : buyer
 * Function For : To get the buyers data
 * Ticket No : TIC-350
**/

async function allBuyers() {

    const buyer_user_details = await BuyerUsers.findAll();

    const buyers = [];
    for(var buyer of buyer_user_details){
        const data = {};
        data.id = buyer.dataValues.id;
        data.first_name = buyer.dataValues.first_name;
        data.last_name = buyer.dataValues.last_name;
        data.email = buyer.dataValues.email;
        data.phone_number = buyer.dataValues.phone_number;
        data.address1 = buyer.dataValues.address1;
        data.address2 = buyer.dataValues.address2;
        data.zip_code = buyer.dataValues.zip_code;
        data.country = buyer.dataValues.country;
        data.state = buyer.dataValues.state;
        data.city = buyer.dataValues.city;
        data.notes = buyer.dataValues.notes;

        var transactionDatas = await Transaction.findAll({ where: { buyer_user_id: buyer.dataValues.id }, attributes:['id','payment_id']});

        let transactionIds = transactionDatas.map(transaction=>_.get(transaction,'dataValues.id'));
        
        let transactionListings = await TransactionListings.findAll({
            attributes:['id','transaction_id','listing_id'],
            where: {
                transaction_id:{
                    [Op.in]:transactionIds
                }
            }
        });

        data.transactionsByBuyerId = await Promise.all(transactionListings.map( async (transaction)=>{
            return  {
                id: _.get(transaction,'dataValues.transaction_id'),
                listing_id: _.get(transaction,'dataValues.listing_id'),
                listingByListingId:  await Listings.findAll({ where: { id: _.get(transaction,'dataValues.listing_id') }, attributes: ['price'], raw: true })
            }
        }));
        
        buyers.push(data);
    }
    if (buyers) {
        return JSON.parse(JSON.stringify(buyers));
    } else {
        return "No data";
    }
}

module.exports = {
    allBuyers
};