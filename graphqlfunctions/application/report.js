const db = require('../../models');

const {reports} = require('../../constants/constants')
const {get:lodashGet,toString:lodashToString} = require('lodash');
const moment = require('moment');

module.exports = {
    getPurchaseReport : async (_,{ input }, context) => {
        
        const {startDate,endDate} = input;
        let purchaseReportData = await db.sequelize.query(`select
            "PO"."id" as "po_id",
            to_char("PO"."created_at", 'mm-dd-yyyy') as "po_created_at",
            to_char("PO"."purchase_date", 'mm-dd-yyyy') as "po_date",
            "PO"."purchase_amount" as "po_amount",
            "PO"."external_reference" as "po_external_reference",
            CONCAT ("POCUSER"."first_name",' ',"POCUSER"."last_name") as "po_created_by",
            CONCAT ("POUUSER"."first_name",' ',"POUUSER"."last_name") as "po_updated_by",
            "PO"."updated_at" as "po_updated_at",
            null As "purchase_type",
            "LG"."event_id",
            "EV"."name" as "event_name",
            to_char("EV"."date", 'mm-dd-yyyy HH24-MI') as "event_date",
            "EVS"."event_status",
            "CAT"."name" as "event_type",
            "GEN"."name" as "event_sub_type",
            "PER"."name" as "performer",
            "VEN"."name" as "venue",
            COALESCE("cities"."name","VEN"."city") as "city",
            COALESCE("states"."name","VEN"."state") as "state",
            "CON"."name" as "country",
            "PO"."listing_group_id" as "po_listing_id",
            "LG"."section",
            "LG"."row",
            "LG"."seat_start",
            "LG"."seat_end",
            "LG"."quantity",
            "LG"."group_cost",
            "LG"."group_cost",
            "COMP"."company_name",
            "TTYPE"."name" as "ticket_type", 
            case when "LG"."qc_hold" then 'Yes' else 'No' end as "qc_hold",
            case when "LG"."in_hand"<now() then 'Yes' else 'No' end as "is_in_hand",
            to_char("LG"."in_hand", 'mm-dd-yyyy HH24-MI') as "in_hand",
            "LG"."internal_notes",
            case when "LGS"."status" = 'active' then 'Yes' else 'No' end as "status",
            "LG"."group_price",
            "LG"."last_price_change",
            1 - "LG"."group_cost"/"LG"."group_price" as "inventory_margin",
            "LG"."group_price" - "LG"."group_cost" as "inventory_profit_loss",
            "LG"."tags",
            "LG"."external_notes",
            "LG"."is_attachments",
            "LGS"."name" as "listing_group_status",
            to_char("LGS"."updated_at", 'mm-dd-yyyy HH24-MI-SS') as "listing_group_status_update_at",
            "LG"."zone_seating",
            "PO"."payment_last4_digits",
            "PAYMENTTYPE"."name" as "credit_card_type",
            "CURR"."currency" as "currency_name",
            "TSACC"."account_name",
            "VENDOR"."name" as "vendor_name",
            "TICKSYS"."ticketing_system_name" as "ticketing_system_name",
            "BRORDER"."id" as "order_number",
            "BRORDER"."created_at" as "invoice_date",
            "CHANNEL"."channel_name" as "customer",
            "BRORDER"."delivery_at" as "delivery_date",
            "DSTATUS"."name" as "delivery_status",
            "PO"."fees" as "po_charge_amount",
            "BOCHANNELS"."channel_name" as "sale_external_reference",
            "CHANNELFEES"."fee" as "channel_fees",
            "LG"."group_price" - "CHANNELFEES"."fee" as "net_ticket_sale_amount"
        from
            "ticket_exchange"."purchase_order" as "PO"
            left join "ticket_exchange"."listing_group" as "LG" on "LG"."id" = "PO"."listing_group_id"
            left join "ticket_exchange"."event" as "EV" on "EV"."id" = "LG"."event_id"
            left join "ticket_exchange"."event_status" as "EVS" on "EVS"."id" = "EV"."event_status_id"
            left join "ticket_exchange"."category" as "CAT" on "CAT"."id" = "EV"."category_id"
            left join "ticket_exchange"."genre" as "GEN" on "GEN"."id" = "EV"."genre_id"
            left join "ticket_exchange"."performer" as "PER" on "PER"."id" = "EV"."performer_id"
            left join "ticket_exchange"."venue" as "VEN" on "VEN"."id" = "EV"."venue_id"
            left join "ticket_exchange"."countries" as "CON" on "CON"."id" = "VEN"."country" :: INTEGER
            left join "ticket_exchange"."user" as "USER" on "USER"."id" = "PO"."user_id"
            left join "ticket_exchange"."companies" as "COMP" on "COMP"."id" = "USER"."company_id"
            left join "ticket_exchange"."ticket_types" as "TTYPE" on "TTYPE"."id" = "LG"."ticket_type_id"
            left join "ticket_exchange"."listing_status" as "LGS" on "LGS"."id" = "LG"."listing_status_id"
            left join "ticket_exchange"."currency" as "CURR" on "CURR"."id" = "PO"."currency_id"
            left join "ticket_exchange"."ticketing_system_accounts" as "TSACC" on "TSACC"."id" = "PO"."account_id"
            left join "ticket_exchange"."user" as "POCUSER" on "POCUSER"."id" = "PO"."created_by"
            left join "ticket_exchange"."user" as "POUUSER" on "POUUSER"."id" = "PO"."updated_by"
            left join ticket_exchange.states on ( states.id :: VARCHAR = "VEN"."state" or "states"."name" = "VEN"."state") 
            left join ticket_exchange.cities on ( cities.id :: VARCHAR = "VEN"."city" or "cities"."name" = "VEN"."city" )
            left join "ticket_exchange"."vendor" as "VENDOR" on "VENDOR"."id" = "PO"."vendor_id"
            left join "ticket_exchange"."ticketing_systems" as "TICKSYS" on "TICKSYS"."id" = "PO"."ticketing_system_id"
            left join "ticket_exchange"."broker_order" as "BRORDER" on "BRORDER"."id" = "PO"."broker_order_id"
            left join "ticket_exchange"."payment_type" as "PAYMENTTYPE" on "PAYMENTTYPE"."id" = "PO"."payment_type_id"
            left join "ticket_exchange"."channels" as "CHANNEL" on "CHANNEL"."id" = "BRORDER"."channel_id"
            left join "ticket_exchange"."delivery_status" as "DSTATUS" on "DSTATUS"."id" = "BRORDER"."delivery_status_id"
            left join "ticket_exchange"."channels" as "BOCHANNELS" on "BOCHANNELS"."id" = "BRORDER"."channel_id"
            left join "ticket_exchange"."channel_fees" as "CHANNELFEES" on "CHANNELFEES"."id" = "BRORDER"."channel_id"
            where "PO"."purchase_date" >= '${moment(startDate).format('YYYY-MM-DD')}'
            AND  "PO"."purchase_date" <  '${moment(endDate).add(1,'days').format('YYYY-MM-DD')}'
        `);

        purchaseReportData[0] = await Promise.all(purchaseReportData[0].map(async (data,index) => {

            let listingGroup = await db.sequelize.query(`select CONCAT ("USER"."first_name",' ',"USER"."last_name") as username from "ticket_exchange"."price_log" as "PLOG" left join "ticket_exchange"."user" as "USER" on "USER"."id" =  "PLOG"."created_by" where listing_group_id = ${data.po_listing_id} order by "PLOG"."created_at" desc limit 1`);
            let query = `select "disclosures"."disclosure_name" from "ticket_exchange"."listing_disclosures" as listing_disclosures  ,"ticket_exchange"."disclosures" as disclosures  where listing_disclosures.disclosure_id = disclosures.id and "listing_disclosures"."listing_group_id" = ${data.po_listing_id} `;
            
            let disclosures = await db.sequelize.query(query);
            purchaseReportData[0][index].disclosures = lodashToString(disclosures[0].map(disclosure=>disclosure.disclosure_name),',')
            purchaseReportData[0][index].last_price_update_by = lodashGet(listingGroup,'0.0.username');

            let eventTags = await db.sequelize.query(`select tag_name from ticket_exchange.event_tags as event_tags,ticket_exchange.tags as tags where tags.id = event_tags.tag_id and event_tags.event_id = ${data.event_id} `);
            purchaseReportData[0][index].event_notes = lodashToString(eventTags[0].map(tags=>tags.tag_name),',');

            let issues = await db.sequelize.query(`select issue_types.name from ticket_exchange.issues as issues,ticket_exchange.issue_types as issue_types where issue_types.id = issues.issue_type_id and issues.listing_group_id = ${data.po_listing_id}`)
            purchaseReportData[0][index].issue_names = lodashToString(issues[0].map(issue=>issue.name),',');

            return purchaseReportData[0][index];
        }));

        return {
            purchaseReportData,
            headers: reports.purchaseReport.xlsHeaders
        }

    },
    getInventoryReport : async (_, { input }, context) => {
        const {startDate,endDate} = input;
        let inventoryReportData = await db.sequelize.query(`select
            "PO"."id" as "po_id",
            to_char("PO"."created_at", 'mm-dd-yyyy') as "po_created_at",
            to_char("PO"."purchase_date", 'mm-dd-yyyy') as "po_date",
            "PO"."purchase_amount" as "po_amount",
            "PO"."external_reference" as "po_external_reference",
            CONCAT ("POCUSER"."first_name",' ',"POCUSER"."last_name") as "po_created_by",
            CONCAT ("POUUSER"."first_name",' ',"POUUSER"."last_name") as "po_updated_by",
            "PO"."updated_at" as "po_updated_at",
            null As "purchase_type",
            "LG"."event_id",
            "EV"."name" as "event_name",
            to_char("EV"."date", 'mm-dd-yyyy HH24-MI') as "event_date",
            "EVS"."event_status",
            "CAT"."name" as "event_type",
            "GEN"."name" as "event_sub_type",
            "PER"."name" as "performer",
            "VEN"."name" as "venue",
            COALESCE("cities"."name","VEN"."city") as "city",
            COALESCE("states"."name","VEN"."state") as "state",
            "CON"."name" as "country",
            "PO"."listing_group_id" as "po_listing_id",
            "LG"."section",
            "LG"."row",
            "LG"."seat_start",
            "LG"."seat_end",
            "LG"."quantity",
            "LG"."group_cost",
            "LG"."group_cost",
            "COMP"."company_name",
            "TTYPE"."name" as "ticket_type", 
            case when "LG"."qc_hold" then 'Yes' else 'No' end as "qc_hold",
            case when "LG"."in_hand"<now() then 'Yes' else 'No' end as "is_in_hand",
            to_char("LG"."in_hand", 'mm-dd-yyyy HH24-MI') as "in_hand",
            "LG"."internal_notes",
            case when "LGS"."status" = 'active' then 'Yes' else 'No' end as "status",
            "LG"."group_price",
            "LG"."last_price_change",
            "LG"."tags",
            "LG"."external_notes",
            "LG"."is_attachments",
            "LGS"."name" as "listing_group_status",
            to_char("LGS"."updated_at", 'mm-dd-yyyy HH24-MI-SS') as "listing_group_status_update_at",
            "LG"."zone_seating",
            "CURR"."currency" as "currency_name",
            "TSACC"."account_name",
            "VENDOR"."name" as "vendor_name",
            "TICKSYS"."ticketing_system_name" as "ticketing_system_name",
            "BRORDER"."created_at" as "invoice_date",
            now()::DATE - "LG"."created_at"::DATE as "old_days"
        from
            "ticket_exchange"."purchase_order" as "PO"
            left join "ticket_exchange"."listing_group" as "LG" on "LG"."id" = "PO"."listing_group_id"
            left join "ticket_exchange"."event" as "EV" on "EV"."id" = "LG"."event_id"
            left join "ticket_exchange"."event_status" as "EVS" on "EVS"."id" = "EV"."event_status_id"
            left join "ticket_exchange"."category" as "CAT" on "CAT"."id" = "EV"."category_id"
            left join "ticket_exchange"."genre" as "GEN" on "GEN"."id" = "EV"."genre_id"
            left join "ticket_exchange"."performer" as "PER" on "PER"."id" = "EV"."performer_id"
            left join "ticket_exchange"."venue" as "VEN" on "VEN"."id" = "EV"."venue_id"
            left join "ticket_exchange"."countries" as "CON" on "CON"."id" = "VEN"."country" :: INTEGER
            left join "ticket_exchange"."user" as "USER" on "USER"."id" = "PO"."user_id"
            left join "ticket_exchange"."companies" as "COMP" on "COMP"."id" = "USER"."company_id"
            left join "ticket_exchange"."ticket_types" as "TTYPE" on "TTYPE"."id" = "LG"."ticket_type_id"
            left join "ticket_exchange"."listing_status" as "LGS" on "LGS"."id" = "LG"."listing_status_id"
            left join "ticket_exchange"."currency" as "CURR" on "CURR"."id" = "PO"."currency_id"
            left join "ticket_exchange"."ticketing_system_accounts" as "TSACC" on "TSACC"."id" = "PO"."account_id"
            left join "ticket_exchange"."user" as "POCUSER" on "POCUSER"."id" = "PO"."created_by"
            left join "ticket_exchange"."user" as "POUUSER" on "POUUSER"."id" = "PO"."updated_by"
            left join ticket_exchange.states on ( states.id :: VARCHAR = "VEN"."state" or "states"."name" = "VEN"."state") 
            left join ticket_exchange.cities on ( cities.id :: VARCHAR = "VEN"."city" or "cities"."name" = "VEN"."city" )
            left join "ticket_exchange"."vendor" as "VENDOR" on "VENDOR"."id" = "PO"."vendor_id"
            left join "ticket_exchange"."ticketing_systems" as "TICKSYS" on "TICKSYS"."id" = "PO"."ticketing_system_id"
            left join "ticket_exchange"."broker_order" as "BRORDER" on "BRORDER"."id" = "PO"."broker_order_id"
            where "PO"."purchase_date" >= '${moment(startDate).format('YYYY-MM-DD')}'
            AND  "PO"."purchase_date" <  '${moment(endDate).add(1,'days').format('YYYY-MM-DD')}'
        `);
        
        inventoryReportData[0] = await Promise.all(inventoryReportData[0].map(async (data,index) => {

            let listingGroup = await db.sequelize.query(`select CONCAT ("USER"."first_name",' ',"USER"."last_name") as username from "ticket_exchange"."price_log" as "PLOG" left join "ticket_exchange"."user" as "USER" on "USER"."id" =  "PLOG"."created_by" where listing_group_id = ${data.po_listing_id} order by "PLOG"."created_at" desc limit 1`);
            
            let query = `select "disclosures"."disclosure_name" from "ticket_exchange"."listing_disclosures" as listing_disclosures  ,"ticket_exchange"."disclosures" as disclosures  where listing_disclosures.disclosure_id = disclosures.id and "listing_disclosures"."listing_group_id" = ${data.po_listing_id} `;
            let disclosures = await db.sequelize.query(query);
            inventoryReportData[0][index].disclosures = lodashToString(disclosures[0].map(disclosure=>disclosure.disclosure_name),',')
            inventoryReportData[0][index].last_price_update_by = lodashGet(listingGroup,'0.0.username');

            let eventTags = await db.sequelize.query(`select tag_name from ticket_exchange.event_tags as event_tags,ticket_exchange.tags as tags where tags.id = event_tags.tag_id and event_tags.event_id = ${data.event_id} `);
            inventoryReportData[0][index].event_notes = lodashToString(eventTags[0].map(tags=>tags.tag_name),',');

            let issues = await db.sequelize.query(`select issue_types.name from ticket_exchange.issues as issues,ticket_exchange.issue_types as issue_types where issue_types.id = issues.issue_type_id and  issues.listing_group_id = ${data.po_listing_id}`)
            inventoryReportData[0][index].issue_names = lodashToString(issues[0].map(issue=>issue.name),',');

            return inventoryReportData[0][index];
        }));
            
        return {
            inventoryReportData,
            headers: reports.inventoryReport.xlsHeaders
        }
    },
    getSalesReport  : async (_, { input }, context) => {

       const {startDate,endDate} = input;

       let salesReportData = await db.sequelize.query(`select
            "PO"."id" as "po_id",
            to_char("PO"."created_at", 'mm-dd-yyyy') as "po_created_at",
            to_char("PO"."purchase_date", 'mm-dd-yyyy') as "po_date",
            "PO"."purchase_amount" as "po_amount",
            "PO"."external_reference" as "po_external_reference",
            CONCAT ("POCUSER"."first_name",' ',"POCUSER"."last_name") as "po_created_by",
            CONCAT ("POUUSER"."first_name",' ',"POUUSER"."last_name") as "po_updated_by",
            "PO"."updated_at" as "po_updated_at",
            null As "purchase_type",
            "LG"."event_id",
            "EV"."name" as "event_name",
            to_char("EV"."date", 'mm-dd-yyyy HH24-MI') as "event_date",
            "EVS"."event_status",
            "CAT"."name" as "event_type",
            "GEN"."name" as "event_sub_type",
            "PER"."name" as "performer",
            "VEN"."name" as "venue",
            COALESCE("cities"."name","VEN"."city") as "city",
            COALESCE("states"."name","VEN"."state") as "state",
            "CON"."name" as "country",
            "PO"."listing_group_id" as "po_listing_id",
            "LG"."section",
            "LG"."row",
            "LG"."seat_start",
            "LG"."seat_end",
            "LG"."quantity",
            "LG"."group_cost",
            "LG"."group_cost",
            "COMP"."company_name",
            "TTYPE"."name" as "ticket_type", 
            case when "LG"."qc_hold" then 'Yes' else 'No' end as "qc_hold",
            case when "LG"."in_hand"<now() then 'Yes' else 'No' end as "is_in_hand",
            to_char("LG"."in_hand", 'mm-dd-yyyy HH24-MI') as "in_hand",
            "LG"."internal_notes",
            case when "LGS"."status" = 'active' then 'Yes' else 'No' end as "status",
            "LG"."group_price",
            "LG"."last_price_change",
            1 - "LG"."group_cost"/"LG"."group_price" as "inventory_margin",
            "LG"."group_price" - "LG"."group_cost" as "inventory_profit_loss",
            "LG"."tags",
            "LG"."external_notes",
            "LG"."is_attachments",
            "LGS"."name" as "listing_group_status",
            to_char("LGS"."updated_at", 'mm-dd-yyyy HH24-MI-SS') as "listing_group_status_update_at",
            "LG"."zone_seating",
            "PO"."payment_last4_digits",
            "PAYMENTTYPE"."name" as "credit_card_type",
            "CURR"."currency" as "currency_name",
            "TSACC"."account_name",
            "VENDOR"."name" as "vendor_name",
            "TICKSYS"."ticketing_system_name" as "ticketing_system_name",
            "BRORDER"."id" as "order_number",
            "BRORDER"."created_at" as "invoice_date",
            "CHANNEL"."channel_name" as "customer",
            "BRORDER"."delivery_at" as "delivery_date",
            "DSTATUS"."name" as "delivery_status",
            "PO"."fees" as "po_charge_amount",
            "BOCHANNELS"."channel_name" as "sale_external_reference",
            "CHANNELFEES"."fee" as "channel_fees",
            "LG"."group_price" - "CHANNELFEES"."fee" as "net_ticket_sale_amount"
        from
            "ticket_exchange"."purchase_order" as "PO"
            left join "ticket_exchange"."listing_group" as "LG" on "LG"."id" = "PO"."listing_group_id"
            left join "ticket_exchange"."event" as "EV" on "EV"."id" = "LG"."event_id"
            left join "ticket_exchange"."event_status" as "EVS" on "EVS"."id" = "EV"."event_status_id"
            left join "ticket_exchange"."category" as "CAT" on "CAT"."id" = "EV"."category_id"
            left join "ticket_exchange"."genre" as "GEN" on "GEN"."id" = "EV"."genre_id"
            left join "ticket_exchange"."performer" as "PER" on "PER"."id" = "EV"."performer_id"
            left join "ticket_exchange"."venue" as "VEN" on "VEN"."id" = "EV"."venue_id"
            left join "ticket_exchange"."countries" as "CON" on "CON"."id" = "VEN"."country" :: INTEGER
            left join "ticket_exchange"."user" as "USER" on "USER"."id" = "PO"."user_id"
            left join "ticket_exchange"."companies" as "COMP" on "COMP"."id" = "USER"."company_id"
            left join "ticket_exchange"."ticket_types" as "TTYPE" on "TTYPE"."id" = "LG"."ticket_type_id"
            left join "ticket_exchange"."listing_status" as "LGS" on "LGS"."id" = "LG"."listing_status_id"
            left join "ticket_exchange"."currency" as "CURR" on "CURR"."id" = "PO"."currency_id"
            left join "ticket_exchange"."ticketing_system_accounts" as "TSACC" on "TSACC"."id" = "PO"."account_id"
            left join "ticket_exchange"."user" as "POCUSER" on "POCUSER"."id" = "PO"."created_by"
            left join "ticket_exchange"."user" as "POUUSER" on "POUUSER"."id" = "PO"."updated_by"
            left join ticket_exchange.states on ( states.id :: VARCHAR = "VEN"."state" or "states"."name" = "VEN"."state") 
            left join ticket_exchange.cities on ( cities.id :: VARCHAR = "VEN"."city" or "cities"."name" = "VEN"."city" )
            left join "ticket_exchange"."vendor" as "VENDOR" on "VENDOR"."id" = "PO"."vendor_id"
            left join "ticket_exchange"."ticketing_systems" as "TICKSYS" on "TICKSYS"."id" = "PO"."ticketing_system_id"
            left join "ticket_exchange"."broker_order" as "BRORDER" on "BRORDER"."id" = "PO"."broker_order_id"
            left join "ticket_exchange"."payment_type" as "PAYMENTTYPE" on "PAYMENTTYPE"."id" = "PO"."payment_type_id"
            left join "ticket_exchange"."channels" as "CHANNEL" on "CHANNEL"."id" = "BRORDER"."channel_id"
            left join "ticket_exchange"."delivery_status" as "DSTATUS" on "DSTATUS"."id" = "BRORDER"."delivery_status_id"
            left join "ticket_exchange"."channels" as "BOCHANNELS" on "BOCHANNELS"."id" = "BRORDER"."channel_id"
            left join "ticket_exchange"."channel_fees" as "CHANNELFEES" on "CHANNELFEES"."id" = "BRORDER"."channel_id"
            where "PO"."purchase_date" >= '${moment(startDate).format('YYYY-MM-DD')}'
            AND  "PO"."purchase_date" <  '${moment(endDate).add(1,'days').format('YYYY-MM-DD')}'
        `);
      
   
       salesReportData[0] = await Promise.all(salesReportData[0].map(async (data,index) => {

           let listingGroup = await db.sequelize.query(`select CONCAT ("USER"."first_name",' ',"USER"."last_name") as username from "ticket_exchange"."price_log" as "PLOG" left join "ticket_exchange"."user" as "USER" on "USER"."id" =  "PLOG"."created_by" where listing_group_id = ${data.po_listing_id} order by "PLOG"."created_at" desc limit 1`);
           let query = `select "disclosures"."disclosure_name" from "ticket_exchange"."listing_disclosures" as listing_disclosures  ,"ticket_exchange"."disclosures" as disclosures  where listing_disclosures.disclosure_id = disclosures.id and "listing_disclosures"."listing_group_id" = ${data.po_listing_id} `;
           
           let disclosures = await db.sequelize.query(query);
           salesReportData[0][index].disclosures = lodashToString(disclosures[0].map(disclosure=>disclosure.disclosure_name),',')
           salesReportData[0][index].last_price_update_by = lodashGet(listingGroup,'0.0.username');

           let eventTags = await db.sequelize.query(`select tag_name from ticket_exchange.event_tags as event_tags,ticket_exchange.tags as tags where tags.id = event_tags.tag_id and event_tags.event_id = ${data.event_id} `);
           salesReportData[0][index].event_notes = lodashToString(eventTags[0].map(tags=>tags.tag_name),',');

           let issues = await db.sequelize.query(`select issue_types.name from ticket_exchange.issues as issues,ticket_exchange.issue_types as issue_types where issue_types.id = issues.issue_type_id and issues.listing_group_id = ${data.po_listing_id}`)
           salesReportData[0][index].issue_names = lodashToString(issues[0].map(issue=>issue.name),',');

           return salesReportData[0][index];
       }));

       return {
           salesReportData,
           headers: reports.salesReport.xlsHeaders
       }
    }
}