const AWS = require('aws-sdk');
const db = require('../../models');
const Events = db.events;
const Op = db.Sequelize.Op;
const Purchaseorder = db.purchaseorder;
const Paymenttype = db.paymenttype;
const Paymentmethod = db.paymentmethod;
const Vendor = db.vendor;
const ListingGroup = db.listinggroup;
const Purchaseorderlisting = db.purchaseorderlisting;
const Purchaseordertag = db.purchaseordertag;
const Purchaseorderinvoiceattachment = db.purchaseorderinvoiceattachment
const TicketingSystemAccount = db.ticketingsystemaccounts;

const sequelize = require('sequelize');
const sendMail = require('../../helpers/sendMail');
const mailTemplate = require('../../helpers/mailTemplate');
const {getUserIdsForListingGroupAccess, getUserRole} = require('../../helpers/userAccess.js');
const Tags = db.tags;
const { QueryTypes, where } = require('sequelize');

import { userTypes, s3Path, folderNames, userRolesLower } from "../../constants/constants";

/**
 * Page : Add Purchase Order 
 * Function For : Add Purchase Order  Section
 * Ticket No : TIC 529
 */
 async function addPurchaseOrder(_,
    { input:
        {
            user_id,
            vendor_id,
            purchase_date,
            payment_method_id,
            sales_tax,
            purchase_amount,
            payment_last4_digits,
            shipping_and_handling_fee,
            external_reference,
            payment_type_id,
            fees,
            notes,
            currency_id,
            created_by,
            tagsInformationsJSON,
            vendorInformationsJSON,
            listingInformationsJSON,
            InvoiceAttachmentInformationsJSON
        }
    }) {
    //Vendor Details:
    var vendorID = "";
    if (vendorInformationsJSON.vendorID == "") {
        var vendorData = {
            name: vendorInformationsJSON.vendorName,
            address: vendorInformationsJSON.vendorAddress1,
            address2: vendorInformationsJSON.vendorAddress2,
            city: vendorInformationsJSON.vendorCity,
            country: vendorInformationsJSON.vendorCountry,
            email: vendorInformationsJSON.vendorEmail,
            phone: vendorInformationsJSON.vendorPhone,
            state: vendorInformationsJSON.vendorState,
            zip: vendorInformationsJSON.vendorZipcode,
        };
        var vendorResponseData = await Vendor.create(vendorData, {
            returning: [
                'id']
        });
        if (vendorResponseData) {
            vendorID = vendorResponseData.id;
        }

    } else {
        var vendorData = {
            name: vendorInformationsJSON.vendorName,
            address: vendorInformationsJSON.vendorAddress1,
            address2: vendorInformationsJSON.vendorAddress2,
            city: vendorInformationsJSON.vendorCity,
            country: vendorInformationsJSON.vendorCountry,
            email: vendorInformationsJSON.vendorEmail,
            phone: vendorInformationsJSON.vendorPhone,
            state: vendorInformationsJSON.vendorState,
            zip: vendorInformationsJSON.vendorZipcode,
        };
        await Vendor.update(vendorData, { where: { id: vendor_id } });
        vendorID = vendor_id;
    }


    var purchaseOrderDetails = {};
    purchaseOrderDetails.user_id = user_id;
    purchaseOrderDetails.vendor_id = vendorID;
    purchaseOrderDetails.purchase_date = purchase_date;
    purchaseOrderDetails.payment_method_id = payment_method_id;
    purchaseOrderDetails.sales_tax = sales_tax;
    purchaseOrderDetails.purchase_amount = purchase_amount;
    purchaseOrderDetails.payment_last4_digits = payment_last4_digits;
    purchaseOrderDetails.shipping_and_handling_fee = shipping_and_handling_fee;
    purchaseOrderDetails.payment_type_id = payment_type_id;
    purchaseOrderDetails.external_reference = external_reference;
    purchaseOrderDetails.fees = fees;
    purchaseOrderDetails.notes = notes;
    purchaseOrderDetails.currency_id = currency_id;
    purchaseOrderDetails.created_by = created_by;
    purchaseOrderDetails.active = true;

    var response = {};

    if (listingInformationsJSON && listingInformationsJSON.length > 0) {
        for (var listingInfoCount = 0; listingInfoCount < listingInformationsJSON.length; listingInfoCount++) {

            if (listingInformationsJSON[listingInfoCount].listingId) {
                purchaseOrderDetails.listing_group_id = listingInformationsJSON[listingInfoCount].listingId;
                var purchaseOrderData = await Purchaseorder.create(purchaseOrderDetails, {
                    returning: [
                        'id',
                        'user_id',
                        'vendor_id',
                        'purchase_date',
                        'payment_method_id',
                        'sales_tax',
                        'purchase_amount',
                        'payment_last4_digits',
                        'shipping_and_handling_fee',
                        'external_reference',
                        'payment_type_id',
                        'listing_group_id',
                        'fees',
                        'notes',
                        'currency_id',
                        'created_at',
                        'updated_at']
                });
                if (purchaseOrderData) {
                    let listingInputData = {
                        purchase_order_id: purchaseOrderData.id,
                        listing_id: parseInt(listingInformationsJSON[listingInfoCount].listingId)
                    };
                    await Purchaseorderlisting.create(listingInputData, { returning: ['id'] });
                    if (tagsInformationsJSON && tagsInformationsJSON.length > 0) {
                        for (var tagInfoCount = 0; tagInfoCount < tagsInformationsJSON.length; tagInfoCount++) {
                            if (tagsInformationsJSON[tagInfoCount].value) {
                                let tagInputData = {
                                    purchase_order_id: purchaseOrderData.id,
                                    created_by: purchaseOrderData.user_id,
                                    tag_id: parseInt(tagsInformationsJSON[tagInfoCount].value)
                                };
                                await Purchaseordertag.create(tagInputData, { returning: ['id'] });
                            }
                        }
                    }
                    //Attachment Details:
                    if (InvoiceAttachmentInformationsJSON) {
                        var url = s3Path.basePath + process.env.AWS_BUCKETNAME + '/' + folderNames.purchaseOrderFolder + '/' + purchaseOrderData.id + '/' + InvoiceAttachmentInformationsJSON.InvoiceAttachName;

                        let attachInputData = {
                            purchase_order_id: purchaseOrderData.id,
                            created_by: purchaseOrderData.user_id,
                            attachment_url: url,
                        };
                        await Purchaseorderinvoiceattachment.create(attachInputData, { returning: ['id'] });
                    }
                }
            }
        }
    }


    if (purchaseOrderData) {
        response = {
            "isError": false,
            "msg": "Purchase Order Created Successfully",
            "data": purchaseOrderData
        };
    }
    else {
        response = {
            "isError": true,
            "msg": "Something went wrong",
            "data": null
        };
    }
    return response;
}

async function allPaymentTypes() {
    const paymentTypes = await Paymenttype.findAll({
        attributes: ['id', 'name']
    });
    if (paymentTypes) {
        return JSON.parse(JSON.stringify(paymentTypes));
    } else {
        return "No data";
    }
}

async function allPaymentGroups() {
    const paymentGroups = await Paymentmethod.findAll({
        attributes: ['id', 'name']
    });
    if (paymentGroups) {
        return JSON.parse(JSON.stringify(paymentGroups));
    } else {
        return "No data";
    }
}

async function getvendorFilter(_, { input: { searches } }) {
    const vendors = await Vendor.findAll({ attributes: ['id', 'name', 'email', 'phone', 'address', 'address2', 'city', 'state', 'zip', 'country', 'created_at', 'updated_at'], where: { name: searches }, raw: true })
    if (vendors) {
        return JSON.parse(JSON.stringify(vendors));
    } else {
        return "No data";
    }
}


async function vendorSearchFilter(_, { input: { searches, type } }) {
    let myResArr = [];
    if (type == userTypes.vendor) {
        let vendorNameDtls = await Vendor.findAll({
            attributes: [[sequelize.fn('DISTINCT', sequelize.col('name')), 'name']],
            where: { name: { [Op.iLike]: '%' + searches + '%' } }
        });
        let vendorNameDtlsJSON = JSON.parse(JSON.stringify(vendorNameDtls));

        vendorNameDtlsJSON.forEach(row => {
            if (row.name != null && row.name != 'null' && row.name != "")
                myResArr.push(row.name);
        });

        return JSON.parse(JSON.stringify(myResArr));
    }
    else {
        return myResArr;
    }

}

async function getListingFilter(_, { input: { searches } }) {

    Events.hasMany(ListingGroup);
    ListingGroup.belongsTo(Events);

    const ListingGroupRow = await ListingGroup.findAll({
        attributes: [
            [sequelize.literal('listing_group.id'), 'id'],
            [sequelize.literal('event.name'), 'eventName'],
            [sequelize.literal('event.date'), 'eventDate'],
            [sequelize.literal('listing_group.section'), 'section'],
            [sequelize.literal('listing_group.row'), 'row'],
            [sequelize.literal('listing_group.quantity'), 'quantity'],
            [sequelize.literal('listing_group.seat_start'), 'seat_start'],
            [sequelize.literal('listing_group.seat_end'), 'seat_end'],
            [sequelize.literal('listing_group.group_cost'), 'group_cost'],
            [sequelize.literal('listing_group.unit_cost'), 'unit_cost'],
            [sequelize.literal('listing_group.seat_type_id'), 'seat_type_id'],
        ],
        include: [{
            model: Events,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('event.id'), '=', sequelize.col('listing_group.event_id')),
            }
        }],
        where: {
            id: searches
        }
    });

    if (ListingGroupRow) {
        return JSON.parse(JSON.stringify(ListingGroupRow));
    } else {
        return "No data";
    }
}

async function listingSearchFilter(_, { input: { searches, userId } }) {
    let myResArr = [];
    if (searches) {
        let listingGroupUserIds = '';
        let whereCondition = { 
                [Op.or]: [
                    sequelize.where(
                    sequelize.cast(sequelize.col('id'), 'varchar'),
                    { [Op.iLike]: `%${searches}%` }
                    ),
                ],
                listing_status_id: {
                    [sequelize.Op.not]: 1
                },
                is_removed:false
            }
            if (userId) {
            let listingGroupUserIdsResponse = await getUserIdsForListingGroupAccess(userId);
            if(listingGroupUserIdsResponse && listingGroupUserIdsResponse.length > 0)
            {
                whereCondition.created_by = listingGroupUserIdsResponse;
            }
        }
        let vendorIdDtls = await ListingGroup.findAll({
            attributes: [[sequelize.fn('DISTINCT', sequelize.col('id')), 'id']],
            where: whereCondition,
            order: [
                ['id', 'ASC']
            ],
            limit: 50
        });
        let vendorIdDtlsJSON = JSON.parse(JSON.stringify(vendorIdDtls));
        vendorIdDtlsJSON.forEach(row => {
            if (row.id != null && row.id != 'null' && row.id != "")
                myResArr.push(row.id);
        });
        return JSON.parse(JSON.stringify(myResArr));
    }
    else {
        return myResArr;
    }
}

async function getPurchaseOrderDetails(_, { input: { id } }) {
    try {
        let eventDetails = await db.sequelize.query(`SELECT po.id, po.purchase_date, po.purchase_amount, po.sales_tax, po.shipping_and_handling_fee, po.fees, po.external_reference, po.payment_last4_digits, ts.ticketing_system_name, ts.id as ticket_system_id, po.notes, concat(usr.first_name,' ', usr.last_name) as userName, pm.id as payment_method_id, pm."name"as payment_method_name, pt.id as payment_type_id, pt."name"as payment_type_name, vn.id as vendor_id, vn.name as vendor_name, vn.email as vendor_email, vn.phone as vendor_phone, concat( vn.address,', ', vn.address2,', ', vn.city,', ', vn."state",', ', vn.country,', ', vn.zip ) as vendor_address, vn.address, vn.address, vn.address2, vn.city, vn."state", vn.country, vn.zip, vn.country_id, ptag.tagids, ptag.tags, tsa.id as ticket_system_acc_id, tsa.account_name as ticket_system_acc_name, poia.id AS attachment_id, poia.attachment_url, poia.attachment_name FROM ticket_exchange.purchase_order as po LEFT JOIN ticket_exchange.listing_group as lg on lg.id = po.listing_group_id LEFT JOIN ticket_exchange.ticketing_systems as ts on ts.id = po.ticketing_system_id LEFT JOIN ticket_exchange."user"as usr on usr.id = lg.ticket_system_account LEFT JOIN ticket_exchange.payment_method as pm on pm.id = po.payment_method_id LEFT JOIN ticket_exchange.payment_type as pt on pt.id = po.payment_type_id LEFT JOIN ticket_exchange.vendor as vn on vn.id = po.vendor_id LEFT JOIN ( select pot.purchase_order_id, ARRAY_AGG(distinct pot.tag_id) as tagids, array_agg(distinct t.tag_name) as tags from ticket_exchange.purchase_order_tag pot inner join ticket_exchange.tags t on pot.tag_id = t.id group by pot.purchase_order_id ) as ptag on ptag.purchase_order_id = po.id LEFT JOIN ticket_exchange.ticketing_system_accounts as tsa on tsa.id = po.account_id LEFT JOIN ticket_exchange.purchase_order_invoice_attachment AS poia ON poia.purchase_order_id = po.id WHERE po.id = ` + id, { type: QueryTypes.SELECT });
        return { purchaseOrderDetails: eventDetails };

    } catch (error) {
        return { purchaseOrderDetails: '' };
    }



}
async function purchaseOrderDetailsUpdate(_, { input: { id, purchaseOrderDetails } }) {

    try {
        delete purchaseOrderDetails.id;
        var data = await Purchaseorder.update(purchaseOrderDetails, { where: { id: id } });
        if (data[0]) { return { status_data: true }; }
        else { return { status_data: false }; }
    } catch (error) { console.log(error); }

}

async function purchaseOrderDetailsVendorUpdate(_, { input: { id, purchaseOrderDetails } }) {
    try {
        var data = await Vendor.update(purchaseOrderDetails, { where: { id: id } });
        if (data[0]) { return { status_data: true }; }
        else { return { status_data: false }; }
    } catch (error) { console.log(error); }

}


async function purchaseorderlistings(_, { input: { id } }) {
    try {
        let eventDetails = await db.sequelize.query(`SELECT po.id as purchase_order_id, po.listing_group_id, lg.event_id, ev."name"as eventname, ev."date"as eventdate, lg."section"as sec, po.external_reference as referenceid, lg."row", (lg.seat_start ||'-'|| lg.seat_end) as seat, lg.unit_cost as unitcost, lg.group_cost as cost, lg.quantity, po.sales_tax as tax, po.fees FROM ticket_exchange.purchase_order as po left outer join ticket_exchange.listing_group lg on po.listing_group_id = lg.id left outer join ticket_exchange."event"ev on lg.event_id = ev.id where po.id = ` + id, { type: QueryTypes.SELECT });
        return { purchaseOrderGrouplist: eventDetails };
    }
    catch (err) {
        return { purchaseOrderGrouplist: '' };
    }
}
async function getTagsPurchaseOrder() {
    const paymentTagsAll = await Tags.findAll({
        attributes: [
            ['tag_name', 'label'],
            ['id', 'value']
        ]
    });
    if (paymentTagsAll) {
        return JSON.parse(JSON.stringify(paymentTagsAll));
    } else {
        return "No data";
    }
}

async function allPurchaseOrders(_, { input: { searchText, startDate, endDate, userId } }) {
    try {
        var where_condition = '';
        if (userId) {
            let listingGroupUserIdsResponse = await getUserIdsForListingGroupAccess(userId);
            if(listingGroupUserIdsResponse && listingGroupUserIdsResponse.length > 0)
            {
                where_condition = ' and "purchase_order"."created_by" IN ('+ listingGroupUserIdsResponse +') ';
            }
        }

        if (searchText == parseInt(searchText)) {
            where_condition = `  AND ("purchase_order"."id" = ${searchText} 
        OR "purchase_order"."listing_group_id" = ${searchText}  )`;
        }
        else if (searchText) {
            where_condition = `  AND "purchase_order_tag"."tag_name" iLike  '%` + searchText + `%'`;
        }
        if (startDate && !endDate) {
            where_condition += ` AND "purchase_order"."purchase_date" >= '${startDate}'`;
        }
        if (startDate && endDate) {
            where_condition += ` AND "purchase_order"."purchase_date" BETWEEN '${startDate}'  AND '${endDate}'`;
        }





        var purchase_order_list_sql = `SELECT  purchase_order.id AS "id",     listing_group.id AS "listingId",     listing_group.event_id AS "eventId",     purchase_order.purchase_date AS "purchaseDate",     purchase_order.purchase_amount AS "amount",     payment_method.name AS "purchaseMethod",     purchase_order.payment_last4_digits AS "lastFourDigits",     listing_group.id AS "invoice",     STRING_AGG ( purchase_order_tag.tag_name, ', ') AS "tags",poia.attachment_url  ,poia.id as attachment_id FROM     "ticket_exchange"."purchase_order" AS "purchase_order"     LEFT OUTER JOIN "ticket_exchange"."listing_group" AS "listing_group" ON "listing_group"."id" = "purchase_order"."listing_group_id"     LEFT OUTER JOIN "ticket_exchange"."payment_method" AS "payment_method" ON "payment_method"."id" = "purchase_order"."payment_method_id"     left outer join (select purchase_order_tag.purchase_order_id, tags.tag_name         from             ticket_exchange.purchase_order_tag             inner join ticket_exchange.tags on purchase_order_tag.tag_id = tags.id         where             1 = 1     ) purchase_order_tag on purchase_order_tag.purchase_order_id = purchase_order.id LEFT JOIN ticket_exchange.purchase_order_invoice_attachment as poia on poia.purchase_order_id = purchase_order.id WHERE     1 = 1 ` + where_condition + 'GROUP BY   purchase_order.id , listing_group.id, payment_method.name,poia.attachment_url,poia.id ORDER BY     purchase_order.id ASC';


        var PurchaseOrderList = await db.sequelize.query(purchase_order_list_sql, { type: QueryTypes.SELECT });
        if (PurchaseOrderList) {
            return JSON.parse(JSON.stringify(PurchaseOrderList));
        } else {
            return "No data";
        }
    }
    catch (err) {
        return "No data";
    }
}

async function purchaseOrderDetailsTagsUpdate(_, { input: { id, purchaseOrderDetails } }) {

    const paymentTagsAllOthers = await Purchaseordertag.findAll({
        attributes: ['tag_id'],
        where: {
            purchase_order_id: id
        },
    });
    let returnings = false;


    //Tags Already Saved In Database
    let tagsDataInDb = [];
    paymentTagsAllOthers.map(tags => {
        tagsDataInDb.push(parseInt(tags.tag_id));
    })

    //Current Tags Selected
    let tagsIds = [];

    purchaseOrderDetails.map(tags => {
        tagsIds.push(tags);
    })

    //Tags To Insert New
    let insertTagData = tagsIds.filter(x => tagsDataInDb.indexOf(x) === -1);
    if (insertTagData.length) {
        for (var tagDataCount = 0; tagDataCount < insertTagData.length; tagDataCount++) {
            var tagsInputData = {
                purchase_order_id: id,
                tag_id: insertTagData[tagDataCount]
            };
            await Purchaseordertag.create(tagsInputData, { returning: ['id'] });
        }
        returnings = true;
    }

    //Tags to Delete
    let deleteTagData = tagsDataInDb.filter(x => tagsIds.indexOf(x) === -1);
    if (deleteTagData.length) {
        await Purchaseordertag.destroy({ where: { purchase_order_id: id, tag_id: { [Op.in]: deleteTagData } } });
        returnings = true;
    }
    if (returnings) {
        return { status_data: true };
    }
    else {
        return { status_data: false };
    }
}
async function allTicketingSystemAccountsById(_, { input: { id } }) {
    const Userlistdetailsoutput = await TicketingSystemAccount.findAll({
        attributes: ['id',
            ['account_name', 'email']],
        where: { ticketing_system_id: id }
    });
    if (Userlistdetailsoutput) {
        return JSON.parse(JSON.stringify(Userlistdetailsoutput));
    } else {
        return "No data";
    }
}
async function updateAttachmentDetails(_, { input: { id, po_id, attachment_url, attachment_name } }) {

    var url = s3Path.basePath + process.env.AWS_BUCKETNAME + '/' + folderNames.purchaseOrderFolder + '/' + po_id + '/' + attachment_url;

    let attachInputData = {
        attachment_url: url
    };
    if (id) {
        await Purchaseorderinvoiceattachment.update(attachInputData, { where: { id: id } });
    }
    else {
        attachInputData.purchase_order_id = po_id;
        attachInputData.attachment_name = attachment_name;
        await Purchaseorderinvoiceattachment.create(attachInputData, { returning: ['id'] });
    }


    return { status: true }
}
module.exports = {
    addPurchaseOrder,
    allPaymentTypes,
    allPaymentGroups,
    getvendorFilter,
    vendorSearchFilter,
    listingSearchFilter,
    getListingFilter,
    getPurchaseOrderDetails,
    purchaseOrderDetailsUpdate,
    purchaseorderlistings,
    allPurchaseOrders,
    purchaseOrderDetailsVendorUpdate,
    getTagsPurchaseOrder,
    purchaseOrderDetailsTagsUpdate,
    allTicketingSystemAccountsById,
    updateAttachmentDetails
};


