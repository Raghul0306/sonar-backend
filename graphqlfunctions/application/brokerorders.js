const db = require('../../models');
const Op = db.Sequelize.Op;
const sequelize = require('sequelize');
const { json } = require('sequelize');
const BrokerOrders = db.brokerorder;
const Channels = db.channels;
const ListingGroup = db.listinggroup;
const Listing = db.listings;
const ChannelReference = db.channelreference;
const Events = db.events;
const DeliveryStatus = db.deliverystatus;
const Venue = db.venue;
const ListingTag = db.listingtags;
const Tag = db.tags;
const TicketingSystem = db.ticketingsystems;
const ListingAttachments = db.listingattachments;
const BuyerUsers = db.buyerUsers;
const TicketingSystemAccount = db.ticketingsystemaccounts;
const { QueryTypes } = require('sequelize');
/**
 * Page : MyTickets 
 * Function For : To Get All Ticket Types
 * Ticket No : TIC 337
 */


async function allBrokerOrders(_, { input: { id, start_date, end_date, orderStatus } }) {

    var where_condition = '';
    if (id == parseInt(id)) {
        where_condition = `  AND (broker_order.channel_reference_id = ${id} 
        OR broker_order.listing_group_id = ${id} 
        OR broker_order.channel_reference_id = ${id} 
        OR broker_order.id = ${id}   )`;
    }
    else if(id) {
        where_condition = `  AND channels.channel_name iLike  '%`+id+`%'`;
    }
    if (start_date && !end_date) {
        where_condition += ` AND broker_order.created_at >= '${start_date + ' 00:00:00'}'`;
    }
    if (start_date && end_date) {
        where_condition += ` AND broker_order.created_at BETWEEN '${start_date + ' 00:00:00'}'  AND '${end_date + ' 23:59:59'}'`;
    }
    if (orderStatus && orderStatus.length > 0) {
        if (orderStatus && orderStatus.length > 0) {
            let delivery_status;
            delivery_status = orderStatus.filter(function (item) {
                return (parseInt(item) == item);
            });
            if (delivery_status.length > 0) {
                where_condition += ' AND broker_order.delivery_status_id IN (' + delivery_status.toString() + ')';
            }
        }
    }

    var broker_list_sql = `SELECT "broker_order"."id","broker_order"."buyer_user_id","broker_order"."order_status_id","broker_order"."cancelled_order_type_id","broker_order"."delivery_status_id","broker_order"."listing_group_id","broker_order"."created_at","broker_order"."channel_id","channels"."channel_name"AS"channel_name", "channel_references"."external_channel_reference_id", concat("buyerUsers"."first_name",' ',"buyerUsers"."last_name") AS"buyer_name","buyerUsers"."email"AS"buyer_email",to_char("broker_order"."delivery_at" :: timestamp, 'mm/dd/yyyy') as "delivery_at","buyerUsers"."phone_number"AS"phone_number"FROM"ticket_exchange"."broker_order"AS"broker_order" LEFT OUTER JOIN"ticket_exchange"."channels"AS"channels"ON"channels"."id"="broker_order"."channel_id" LEFT OUTER JOIN"ticket_exchange"."listing_group"AS"listing_groups"ON"listing_groups"."id"="broker_order"."listing_group_id" LEFT OUTER JOIN"ticket_exchange"."channel_reference"AS"channel_references"ON"channel_references"."id"="broker_order"."channel_reference_id" LEFT OUTER JOIN"ticket_exchange"."buyerUsers"AS"buyerUsers"ON"buyerUsers"."id"="broker_order"."buyer_user_id" WHERE 1=1 ` + where_condition + ' ORDER BY broker_order.id ASC;';

    var broker_orders = await db.sequelize.query(broker_list_sql, { type: QueryTypes.SELECT });

    if (broker_orders) {
        return JSON.parse(JSON.stringify(broker_orders));
    } else {
        return "No data";
    }
}
/**
 * Page : Buyer orders details page
 * Function For : Change Order status
 * TIC-405
 */

async function changeOrderStatus(_, { input: { id, status } }) {
    if (id && status) {
        await BrokerOrders.update({ order_status_id: status },
            {
                where: {
                    id: id
                }
            },
            { returning: ['id'] }
        );
        return JSON.parse(JSON.stringify({ status: true }));
    }
    else {
        return JSON.parse(JSON.stringify({ status: false }));
    }
}
/**
 * Page : MyTickets 
 * Function For : To get specific broker order details by id
 * Ticket No : TIC 409
 */

async function viewOrderDetails(_, { input: { orderId } }) {

    let broker_orders = await db.sequelize.query(`select 
	"broker_order"."id", to_char("broker_order"."created_at" :: timestamp, 'dd-mm-yyyy') as orderdate, "broker_order"."event_id", "broker_order"."listing_group_id", "broker_order"."channel_reference_id", "broker_order"."transaction_id", "broker_order"."buyer_user_id", "broker_order"."order_status_id", "broker_order"."delivery_status_id", to_char("broker_order"."delivery_at" :: timestamp, 'dd-mm-yyyy') as "delivery_at", "broker_order"."cancelled_order_type_id", "channels"."channel_name" as "channelname", 
    "buyerUsers"."first_name"|| ' ' || "buyerUsers"."last_name" as "buyerusername", "buyerUsers"."email" as "buyeremail", 
    "buyerUsers"."phone_number" as "buyerphonenumber", "event"."performer_id", "event"."venue_id", "event"."tickettype_id", "event"."genre_id", "event"."event_status_id", "event"."name" as "eventname", to_char("event"."date" :: timestamp, 'Day MM DD, YYYY @ HH:MI') as "eventdate", "performer"."name" as "performername", "venue"."name" as "venuename", "venue"."city" as "venuecity", "venue"."state" as "venuestate", 
	"venue"."country" as "venuecountry", "venue"."timezone" as "timezone", "event_status"."event_status" as "event_status", "listing_group"."section", 
	"listing_group"."row", "listing_group"."seat_start", "listing_group"."seat_end", "listing_group"."seat_start" || '-' || "listing_group"."seat_end" as "seats", 
    to_char("listing_group"."in_hand" :: timestamp, 'dd-mm-yyyy') as "inhand", 
	"listing_group"."ticket_system_id" as "ticketing_system_id", 
    "listing_group"."ticket_system_account" as "ticketing_system_account_id", 
	"ticketing_systems"."ticketing_system_name" as "ticketingsystemname", 
    "ticketing_system_accounts"."account_name" as "ticketingsystemaccountname", "delivery_status"."name" as "deliverystatusname", 
	"listing_group"."ticket_type_id","ticket_types"."name" as "tickettypename"
from 
    "ticket_exchange"."broker_order"
    left outer join "ticket_exchange"."channels" on "broker_order"."channel_id" = "channels"."id"
    left outer join "ticket_exchange"."buyerUsers" on "broker_order"."buyer_user_id" = "buyerUsers"."id"
    left outer join "ticket_exchange"."event" on "broker_order"."event_id" = "event"."id"
    left outer join "ticket_exchange"."performer" on "event"."performer_id" = "performer"."id"
    left outer join "ticket_exchange"."venue" on "event"."venue_id" = "venue"."id"
    left outer join "ticket_exchange"."event_status" on "event"."event_status_id" = "event_status"."id"
    left outer join "ticket_exchange"."listing_group" on "broker_order"."listing_group_id" = "listing_group"."id"
    left outer join "ticket_exchange"."ticketing_systems" on "listing_group"."ticket_system_id" = "ticketing_systems"."id"
    left outer join "ticket_exchange"."ticketing_system_accounts" on "listing_group"."ticket_system_account" = "ticketing_system_accounts"."id"
    left outer join "ticket_exchange"."delivery_status" on "broker_order"."delivery_status_id" = "delivery_status"."id"
    left outer join "ticket_exchange"."ticket_types" on "listing_group"."ticket_type_id" = "ticket_types"."id"
    where 1=1 AND "broker_order"."id" = ` + orderId, { type: QueryTypes.SELECT });

    let broker_info = {};

    if (broker_orders && broker_orders[0]) {

        const listingData = await db.sequelize.query(`select "listing"."listing_group_id", 
        sum("listing"."price") as "price",
        sum(case when "listing"."sold" and "listing"."price" > 0 then trunc(("listing"."price"/"listing"."unit_cost") :: numeric, 2) else 0 end) as margin,
        sum(case when "listing"."sold" is true then 1 else 0 end) as "soldcount"
        from
        "ticket_exchange"."broker_order_listing"
        inner join "ticket_exchange"."listing" on "listing"."id" = "broker_order_listing"."listing_id"
        where "listing"."listing_group_id" = ` + broker_orders[0].listing_group_id + `
        group by "listing"."listing_group_id";`, { type: QueryTypes.SELECT });
        let row_info = broker_orders[0];

        broker_info.id = row_info.id;
        broker_info.order_date = row_info.orderdate;
        broker_info.event_id = row_info.event_id;
        broker_info.listing_group_id = row_info.listing_group_id;
        broker_info.channel_reference_id = row_info.channel_reference_id;
        broker_info.transaction_id = row_info.transaction_id;
        broker_info.buyer_user_id = row_info.buyer_user_id;
        broker_info.order_status_id = row_info.order_status_id;
        broker_info.delivery_status_id = row_info.delivery_status_id;
        broker_info.delivery_at = row_info.delivery_at;
        broker_info.cancelled_order_type_id = row_info.cancelled_order_type_id;
        broker_info.events = [];
        broker_info.listing_groups = [];
        broker_info.delivery_statuses = [];
        broker_info.channels = [];
        broker_info.buyerUsers = [];
        broker_info.ticket_type = [];

        let events = {};
        events.id = row_info["event_id"];
        events.name = row_info["eventname"];
        events.performer_id = row_info["performer_id"];
        events.venue_id = row_info["venue_id"];
        events.date = row_info["eventdate"];
        events.tickettype_id = row_info["tickettype_id"];
        events.genre_id = row_info["genre_id"];
        events.event_status_id = row_info["event_status_id"];
        events.event_status_name = row_info["event_status"];
        events.venues = [];

        let venues = {};
        venues.id = row_info["venue_id"];
        venues.name = row_info["venuename"];
        venues.city = row_info["venuecity"];
        venues.state = row_info["venuestate"];
        venues.country = row_info["venuecountry"];
        venues.timezone = row_info["timezone"];

        let ticket_type = {};
        ticket_type.id = row_info["tickettype_id"];
        ticket_type.name = row_info["tickettypename"];

        broker_info.ticket_type.push(ticket_type);

        let listing_groups = {};
        listing_groups.id = row_info["listing_group_id"];
        listing_groups.seller_id = row_info["user_id"];
        listing_groups.in_hand = row_info["inhand"];
        listing_groups.ticket_type_id = row_info["ticket_type_id"];
        listing_groups.ticket_system_id = row_info["ticketing_system_id"];
        listing_groups.ticket_system_account = row_info["ticketing_system_account_id"];
        listing_groups.section = row_info["section"];
        listing_groups.row = row_info["row"];
        listing_groups.seat_start = row_info["seat_start"];
        listing_groups.seat_end = row_info["seat_end"];

        if (listingData.length > 0) {
            listing_groups.quantity = listingData[0].soldcount;
            listing_groups.price = listingData[0].price;
            listing_groups.margin = listingData[0].margin;
        }
        else {
            listing_groups.quantity = 0;
            listing_groups.price = 0;
            listing_groups.margin = 0;
        }
        listing_groups.listing_attachments = [];
        listing_groups.listing_tags = [];
        listing_groups.ticketing_systems = [];

        let listing_attachments = await db.sequelize.query(`SELECT * FROM "ticket_exchange"."listing_attachments" WHERE "ticket_exchange"."listing_attachments"."listing_group_id" = ` + row_info["listing_group_id"] + ` ORDER BY "listing_attachments"."id" ASC `, { type: QueryTypes.SELECT });

        listing_groups.listing_attachments = listing_attachments;

        let listing_tags = await db.sequelize.query(`SELECT "ticket_exchange"."tags".*, "ticket_exchange"."listing_tags"."id" AS "listing_tag_id"   FROM "ticket_exchange"."listing_tags" LEFT OUTER JOIN  "ticket_exchange"."tags" ON  "ticket_exchange"."tags"."id" = "ticket_exchange"."listing_tags"."tag_id"  WHERE "ticket_exchange"."listing_tags"."listing_group_id" = ` + row_info["listing_group_id"], { type: QueryTypes.SELECT });

        if (listing_tags) {
            for (let listingTagCount = 0; listingTagCount < listing_tags.length; listingTagCount++) {
                let tag = {};
                tag.id = listing_tags[listingTagCount].listing_tag_id;
                tag.tags = {};
                tag.tags.id = listing_tags[listingTagCount].id;
                tag.tags.name = listing_tags[listingTagCount].tag_name;
                listing_groups.listing_tags.push(tag);
            }
        }
        let ticketing_systems = {};
        ticketing_systems.id = row_info["ticketing_system_id"];
        ticketing_systems.ticketing_system_name = row_info["ticketingsystemname"];

        ticketing_systems.ticketing_system_accounts = {};
        ticketing_systems.ticketing_system_accounts.id = row_info["ticketing_system_account_id"];
        ticketing_systems.ticketing_system_accounts.account_name = row_info["ticketingsystemaccountname"];
        listing_groups.ticketing_systems.push(ticketing_systems);

        events.venues.push(venues);
        broker_info.events.push(events);
        broker_info.listing_groups.push(listing_groups);

        let delivery_status = {};
        delivery_status.id = row_info["delivery_status_id"];
        delivery_status.name = row_info["deliverystatusname"];

        broker_info.delivery_statuses.push(delivery_status);

        let channels = {};
        channels.id = row_info["channel_id"];
        channels.channel_name = row_info["channelname"];

        broker_info.channels.push(channels);

        let buyerUsers = {};
        buyerUsers.id = row_info["buyer_user_id"];
        buyerUsers.first_name = row_info["buyerusername"];
        buyerUsers.email = row_info["buyeremail"];
        buyerUsers.phone_number = row_info["buyerphonenumber"];

        broker_info.buyerUsers.push(buyerUsers);


    }

    return {
        broker_order_detail: broker_info
    }
}

module.exports = {
    allBrokerOrders,
    changeOrderStatus,
    viewOrderDetails
};
