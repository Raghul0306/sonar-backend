const db = require('../../models');
const Countries = db.countries;
const Events = db.events;
const Performer = db.performer;
const Venue = db.venue;
const Op = db.Sequelize.Op;
const Ticketingsystems = db.ticketingsystems;
const EventStatus = db.eventstatus;
const States = db.states;
const Cities = db.cities;
const https = require('https');
const sequelize = require('sequelize');
const sendMail = require('../../helpers/sendMail');
const mailTemplate = require('../../helpers/mailTemplate');
const { DateTime } = require("luxon");
const { QueryTypes, where } = require('sequelize');
import { userRolesLower, searchFiltersFor, weekDaysArray } from "../../constants/constants";
const tzlookup = require("tz-lookup");


/**
 * Page : Add Event 
 * Function For : Add Event Section
 * Ticket No : TIC 97
 */
async function addEvent(_, { input: { eventName, eventStatus, eventDate, eventTime, eventLink, performer, opponent, city, state, cityName, stateName, country, ticketingSystem, category, genre, venueName, venueAddress, venueAddress2, zip_code, userDetails } }) {
    try {
        var days = weekDaysArray;
        var currentDate = new Date(eventDate);
        var date = currentDate.getDate();
        var month = currentDate.getMonth();
        var year = currentDate.getFullYear();
        var getDay = currentDate.getDay();
        let eventdate_day = days[getDay];
        month = month + 1;
        eventDate = year + "-" + (month < 10 ? "0" + month : month) + "-" + (date < 10 ? "0" + date : date);
        if (userDetails.userRole == userRolesLower.broker || userDetails.userRole == userRolesLower.broker_user) {

            const ticketing_system_details = await Ticketingsystems.findAll({
                attributes: ['ticketing_system_name'],
                where: { id: ticketingSystem },
                raw: true
            });

            const event_status_details = await EventStatus.findAll({
                attributes: ['event_status'],
                where: { id: eventStatus },
                raw: true
            });

            var eventDetails = {};
            eventDetails.name = eventName;
            eventDetails.date = eventDate + " " + eventTime;
            eventDetails.eventTime = eventTime;
            eventDetails.performer = performer;
            eventDetails.opponent_id = opponent;
            eventDetails.event_link = eventLink;
            eventDetails.ticketSystemId = ticketingSystem;
            eventDetails.eventStatusId = eventStatus;
            eventDetails.eventdate_day = eventdate_day;
            eventDetails.venueName = venueName;
            eventDetails.address = venueAddress;
            eventDetails.address2 = venueAddress2;
            eventDetails.city = city;
            eventDetails.zip_code = zip_code;
            eventDetails.state = state;
            eventDetails.country = country;//country_details_name;
            eventDetails.eventStatusName = event_status_details[0].event_status;
            eventDetails.ticketSystemName = ticketing_system_details[0].ticketing_system_name;

            var mailContents = {};
            mailContents.from = process.env.EMAIL_FROM_ADDRESS;
            mailContents.to = process.env.EMAIL_SUPPORT_TO;
            mailContents.subject = "Request to create a new event";
            mailContents.message = await mailTemplate.addEventMailTemplate(eventDetails);
            sendMail.outgoingMails(mailContents);

            var finalResJSON = {
                "isError": false,
                "msg": "Event Details mail has been sent to " + mailContents.to,
            };
            return finalResJSON;
        }

        if (userDetails.userRole == userRolesLower.admin || userDetails.userRole == userRolesLower.marketplace_admin || userDetails.userRole == userRolesLower.marketplace_user || userDetails.userRole == userRolesLower.marketplace_user || userDetails.userRole == userRolesLower.super_super_admin) {
            var venueDetails = {};
            var venueId = "";
            var performerId = "";
            var overrideZone = '';
            const venues = await Venue.findAll({ attributes: ['id', 'name', 'address', 'address2', 'city', 'state', 'country', 'timezone', 'created_at', 'updated_at'], where: { name: venueName }, raw: true })

            if (venues[0]) {
                venueId = venues[0].id;
                if (!venues[0].timezone || city != venues[0].city) {
                    let timeZone = await getEventVenueLatLong(cityName + ',' + stateName);
                    let parsedData = JSON.parse(timeZone);
                    let position = parsedData && parsedData.items ? parsedData.items[0].position : '';
                    if (position.lat && position.lng) {
                        let timeZonelookup = await tzlookup(position.lat, position.lng);
                        venueDetails.latitude = position.lat;
                        venueDetails.longitude = position.lng;
                        venueDetails.timezone = timeZonelookup;
                        try {
                            overrideZone = DateTime.fromISO(eventDate + "T" + eventTime, { zone: timeZonelookup });
                            eventDate = overrideZone.toString();
                        } catch (error) {
                            console.log(error)
                        }
                    }
                    await Venue.update(venueDetails, { where: { id: venueId }, returning: ['id'] });
                }
            }
            else {
                venueDetails.name = venueName;
                venueDetails.address = venueAddress;
                venueDetails.address2 = venueAddress2;
                venueDetails.city = city;
                venueDetails.state = state;
                venueDetails.country = country;//country_details_name;
                venueDetails.zip_code = zip_code;
                let timeZone = await getEventVenueLatLong(cityName + ',' + stateName);
                let parsedData = JSON.parse(timeZone);

                let position = parsedData && parsedData.items ? parsedData.items[0].position : '';
                if (position.lat && position.lng) {
                    let timeZonelookup = await tzlookup(position.lat, position.lng);
                    venueDetails.latitude = position.lat;
                    venueDetails.longitude = position.lng;
                    venueDetails.timezone = timeZonelookup;
                    try {
                        overrideZone = DateTime.fromISO(eventDate + "T" + eventTime, { zone: timeZonelookup });
                        eventDate = overrideZone.toString();
                    } catch (error) {
                        console.log(error)
                    }
                }
                var venueData = await Venue.create(venueDetails, { returning: ['id', 'name', 'address', 'address2', 'city', 'state', 'country', 'zip_code', 'created_at', 'updated_at'] });

                if (venueData.dataValues)
                    venueId = venueData.dataValues.id;
            }

            const performerDetail = await Performer.findAll({ attributes: ['id', 'name', 'category_id', 'tevoid', 'mercuryid', 'popularity_score', 'genre_id', 'created_at', 'updated_at'], where: { name: performer }, raw: true })
            if (performerDetail[0]) {
                performerId = performerDetail[0].id;
            }
            else {
                var performerDetails = {}
                performerDetails.name = performer;
                performerDetails.genre_id = 1;

                var performerData = await Performer.create(performerDetails, { returning: ['id', 'name', 'category_id', 'tevoid', 'mercuryid', 'popularity_score', 'genre_id', 'created_at', 'updated_at'] });

                if (performerData.dataValues)
                    performerId = performerData.dataValues.id;
            }

            var eventDetails = {};
            eventDetails.name = eventName;
            eventDetails.date = eventDate;
            eventDetails.opponent_id = null;
            eventDetails.performerId = performerId;
            eventDetails.venueId = venueId;
            eventDetails.event_link = eventLink;
            eventDetails.ticketSystemId = ticketingSystem;
            eventDetails.categoryId = category;
            eventDetails.genre_id = genre;
            eventDetails.eventStatusId = eventStatus;
            eventDetails.eventdate_day = eventdate_day;
            eventDetails.tickettype_id = 0;
            var eventData = await Events.create(eventDetails, { returning: ['id', 'name', 'venue_id', 'event_status_id', 'ticket_system_id', 'date', 'eventdate_day', 'event_link', 'tickettype_id', 'created_at', 'updated_at'] });

            var finalResJSON = {
                "isError": true,
                "msg": "Something went wrong"
            };
            if (eventData) {
                finalResJSON = {
                    "isError": false,
                    "msg": "Event Created Successfully",
                    "attachmentData": eventData
                };
            }
            return finalResJSON;
        } else {
            finalResJSON = {
                "isError": true,
                "msg": "Permission denied"
            };
            return finalResJSON;
        }
    } catch (error) {
        return { "isError": true, "msg": "Something went wrong" };
    }
}

async function getEventVenueLatLong(queryParams) {
    return new Promise((resolve, reject) => {
        try {
            const options = {
                hostname: process.env.HERE_API_HOST,
                port: 443,
                path: process.env.HERE_API_PATH + process.env.HERE_API_KEY + '&q=' + encodeURI(queryParams),
                method: 'GET'
            };
            const req = https.request(options, res => {
                res.on('data', d => {
                    resolve(JSON.stringify(JSON.parse(d.toString())));
                });
            })
            req.on('error', error => {
                console.error(error)
            });
            req.end()
        }
        catch (error) {
            reject(error);
        }
    })
}
async function allEventStatus() {
    const eventStatus = await EventStatus.findAll();
    if (eventStatus) {
        return JSON.parse(JSON.stringify(eventStatus));
    } else {
        return "No data";
    }
}

async function getVenueFilter(_, { input: { searches } }) {
    const venues = await Venue.findAll({ attributes: ['id', 'name', 'address', 'address2', 'city', 'state', 'zip_code', 'country', 'created_at', 'updated_at'], where: { name: searches }, raw: true })
    if (venues) {
        return JSON.parse(JSON.stringify(venues));
    } else {
        return "No data";
    }
}


async function venueSearchFilter(_, { input: { searches, type } }) {
    let myResArr = [];
    if (type == searchFiltersFor.venue || type == searchFiltersFor.venueName) {
        let venueNameDtls = await Venue.findAll({
            attributes: [[sequelize.fn('DISTINCT', sequelize.col('name')), 'name']],
            where: { name: { [Op.iLike]: '%' + searches + '%' } }
        });
        let venueNameDtlsJSON = JSON.parse(JSON.stringify(venueNameDtls));

        venueNameDtlsJSON.forEach(row => {
            if (row.name != null && row.name != 'null' && row.name != "")
                myResArr.push(row.name);
        });

        return JSON.parse(JSON.stringify(myResArr));
    }

    if (type == searchFiltersFor.performer) {
        let performerDtls = await Performer.findAll({
            attributes: [[sequelize.fn('DISTINCT', sequelize.col('name')), 'name']],
            where: { name: { [Op.iLike]: '%' + searches + '%' } }
        });
        let performerDtlsJSON = JSON.parse(JSON.stringify(performerDtls));

        performerDtlsJSON.forEach(row => {
            if (row.name != null && row.name != 'null' && row.name != "")
                myResArr.push(row.name);
        });
        return JSON.parse(JSON.stringify(myResArr));
    }
    else {
        return myResArr;
    }

}

//Edit event function
async function getEventDetail(_, { input: { eventIds } }) {
    let eventsDtls = await Events.findOne({
        attributes: ["id", "name", "category_id", "performer_id", "opponent_id", "venue_id", "date", "eventdate_day", "tickettype_id", "genre_id", "event_status_id", "ticket_system_id", "event_link", "image", "created_at", "created_by", "updated_at"],
        where: { id: eventIds },
        raw: true
    });
    if (eventsDtls) {
        let eventsInformation;
        let venueDtls;
        eventsInformation = { "eventsDtls": eventsDtls };
        if (eventsDtls.venue_id) {
            venueDtls = await Venue.findOne({
                attributes: [["id", "venue_id"], ["name", "venue_name"], "address", "address2", "city", "zip_code", "state", "country", "timezone"],
                where: { id: eventsDtls.venue_id },
                raw: true
            });
            eventsInformation = { "eventsDtls": eventsDtls, "venueDtls": venueDtls };
        }
        let jsonData = JSON.parse(JSON.stringify(eventsInformation));
        return { data: jsonData };
    } else {
        return "No data";
    }
}

async function getEventFilter(_, { input: { searchText } }) {

    var where_condition = '';
    if (searchText == parseInt(searchText)) {
        where_condition = `  AND "event"."id" = ${searchText}`;
    }
    else if (searchText) {

        where_condition = ` AND ("event"."name" iLike  '%` + searchText + `%' 
            OR "performer"."name" iLike '%`+ searchText + `%'
            OR  "venue"."name" iLike  '%`+ searchText + `%')`;
    }
    var event_filter_list_sql = `SELECT
        event.id AS "id",
        venue.id AS "venueId",
        TO_CHAR(event.date,'Dy') AS "eventDay",
        TO_CHAR(event.date,'dd/mm') AS "eventDate",
        event.date AS "fullEventDate",
        event.name AS "eventName",
        event.event_status_id,
        venue.name AS "venueName",
        venue.address AS "venueAddress",
        venue.address2 AS "venueAddress2",
        venue.zip_code AS "venueZipcode",
        cities.name AS "venueCity",
        states.name AS "venueState",
        countries.name AS "venueCountry",
        venue.timezone AS "timezone"         
        FROM
        "ticket_exchange"."event" AS "event"
        LEFT OUTER JOIN "ticket_exchange"."venue" AS "venue" ON "venue"."id" = "event"."venue_id"   
        LEFT OUTER JOIN "ticket_exchange".performer as performer on performer.id = "event".performer_id
        LEFT OUTER JOIN "ticket_exchange".countries as countries on venue.country::VARCHAR = countries.id ::VARCHAR
        LEFT OUTER JOIN "ticket_exchange".states as states on venue.state::VARCHAR = states.id ::VARCHAR
        LEFT OUTER JOIN "ticket_exchange".cities as cities on venue.city::VARCHAR = cities.id ::VARCHAR
        WHERE
        1 = 1`  + where_condition + ` ORDER BY     event.id ASC`;

    var EventFilterList = await db.sequelize.query(event_filter_list_sql, { type: QueryTypes.SELECT });
    if (EventFilterList) {
        let jsonData = JSON.parse(JSON.stringify(EventFilterList));
        return { data: jsonData };
    } else {
        return "No data";
    }


}

async function updateEvent(_, { input: { eventId, eventName, eventDate, eventTime, eventStatus, venueId, venueName, venueAddress, venueAddress2, venueCity, venueZipcode, venueState, venueCountry, ticketSystemId, category, genre, eventLink, eventImagelink, userDetails, cityName, stateName } }) {
    try {


        var days = weekDaysArray;
        var currentDate = new Date(eventDate);
        var date = currentDate.getDate();
        var month = currentDate.getMonth();
        var year = currentDate.getFullYear();
        var getDay = currentDate.getDay();
        let eventDay = days[getDay];
        eventDate = year + "-" + ((month + 1) < 10 ? '0' + (month + 1) : (month + 1)) + "-" + (date < 10 ? "0" + date : date) + 'T' + eventTime;
        const country_details = await Countries.findAll({
            attributes: ['id'],
            where: { id: venueCountry },
            raw: true
        });
        var country_details_id = "";
        var venueDetails = {};
        if (country_details[0]) { country_details_id = country_details[0].id } else { country_details_id = "" }

        //Venue
        const venues = await Venue.findAll({ attributes: ['id', 'name', 'address', 'address2', 'city', 'state', 'country', 'created_at', 'updated_at'], where: { name: venueName }, raw: true })
        if (venues[0]) {
            venueId = venues[0].id;
            venueDetails.name = venueName;
            venueDetails.address = venueAddress;
            venueDetails.address2 = venueAddress2;
            venueDetails.city = venueCity;
            venueDetails.state = venueState;
            venueDetails.country = country_details_id;
            venueDetails.zip_code = venueZipcode;
            if (!venues[0].timezone || venueCity != venues[0].city) {
                let timeZone = await getEventVenueLatLong(cityName + ',' + stateName);
                let parsedData = JSON.parse(timeZone);
                let position = parsedData && parsedData.items ? parsedData.items[0].position : '';
                if (position.lat && position.lng) {
                    let timeZonelookup = await tzlookup(position.lat, position.lng);
                    venueDetails.latitude = position.lat;
                    venueDetails.longitude = position.lng;
                    venueDetails.timezone = timeZonelookup;
                    var overrideZone = DateTime.fromISO(eventDate, { zone: timeZonelookup });
                    eventDate = overrideZone.toString();
                }
                await Venue.update(venueDetails, { where: { id: venueId }, returning: ['id'] });
            }
            await Venue.update(venueDetails, { where: { id: venueId }, returning: ['id'] });
        }
        else {
            venueDetails.name = venueName;
            venueDetails.address = venueAddress;
            venueDetails.address2 = venueAddress2;
            venueDetails.city = venueCity;
            venueDetails.state = venueState;
            venueDetails.country = country_details_id;
            venueDetails.zip_code = venueZipcode;
            let timeZone = await getEventVenueLatLong(cityName + ',' + stateName);
            let parsedData = JSON.parse(timeZone);

            let position = parsedData && parsedData.items ? parsedData.items[0].position : '';

            if (position.lat && position.lng) {
                let timeZonelookup = await tzlookup(position.lat, position.lng);
                venueDetails.latitude = position.lat;
                venueDetails.longitude = position.lng;
                venueDetails.timezone = timeZonelookup;
                var overrideZone = DateTime.fromISO(eventDate, { zone: timeZonelookup });
                eventDate = overrideZone.toString();
            }
            var venueData = await Venue.create(venueDetails, { returning: ['id', 'name', 'address', 'address2', 'city', 'state', 'country', 'zip_code', 'created_at', 'updated_at'] });
            if (venueData.dataValues)
                venueId = venueData.dataValues.id;
        }

        var eventDetails = {};
        eventDetails.name = eventName;
        eventDetails.date = eventDate;
        eventDetails.venueId = venueId;
        eventDetails.event_link = eventLink;
        eventDetails.ticketSystemId = ticketSystemId;
        eventDetails.category_id = category;
        eventDetails.genre_id = genre;
        eventDetails.eventStatusId = eventStatus;
        eventDetails.eventdate_day = eventDay;
        eventDetails.tickettype_id = 0;
        eventDetails.image = eventImagelink;

        var eventData = await Events.update(eventDetails, { where: { id: eventId }, returning: ['id', 'name', 'venue_id', 'event_status_id', 'ticket_system_id', 'date', 'eventdate_day', 'event_link', 'tickettype_id', 'image', 'created_at', 'updated_at'] });
        let jsonEventData = JSON.parse(JSON.stringify(eventData));
        let eventJsonDataDetails = jsonEventData && jsonEventData.length && jsonEventData[0] == 1 && jsonEventData[1] ? jsonEventData[1][0] : false;
        var finalResJSON = {};
        if (eventData) {
            finalResJSON = {
                "isError": false,
                "msg": "Event Updated Successfully",
                "attachmentData": eventJsonDataDetails
            };
        }
        else {
            finalResJSON = {
                "isError": true,
                "msg": "Something went wrong"
            };
        }
        return finalResJSON;
    } catch (error) {
        return { "isError": true, "msg": "Something went wrong" }
    }
}

async function updateEventStatus(_, { input: { eventId, eventStatusId } }) {
    var eventDetails = {};
    eventDetails.eventStatusId = eventStatusId;

    var eventData = await Events.update(eventDetails, { where: { id: eventId }, returning: ['id', 'event_status_id'] });

    let jsonEventData = JSON.parse(JSON.stringify(eventData));
    let eventJsonDataDetails = jsonEventData && jsonEventData.length && jsonEventData[0] == 1 && jsonEventData[1] ? jsonEventData[1][0] : false;
    var finalResJSON = {};
    if (eventData) {
        finalResJSON = {
            "isError": false,
            "msg": "Event Updated Successfully",
            "data": eventJsonDataDetails
        };
    }
    else {
        finalResJSON = {
            "isError": true,
            "msg": "Something went wrong",
            "data": []
        };
    }
    return finalResJSON;
}

module.exports = {
    addEvent,
    allEventStatus,
    getVenueFilter,
    venueSearchFilter,
    getEventDetail,
    getEventFilter,
    updateEvent,
    updateEventStatus
};


