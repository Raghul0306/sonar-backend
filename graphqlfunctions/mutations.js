const logins = require("../graphqlfunctions/authentication/logins");
const users = require("../graphqlfunctions/application/users");
const listings = require("../graphqlfunctions/application/listings");
const events = require("../graphqlfunctions/application/events");
const purchaseorders = require("../graphqlfunctions/application/purchaseorders");
const companies = require("../graphqlfunctions/application/companies");

// Login
const login = logins.login;

//User
const registerUser = users.registerUser;
const updatePassword = users.updatePassword;
const updateUserNeedsToAcceptTerms = users.updateUserNeedsToAcceptTerms;
const updateUserNeedsToAcceptPrivacy = users.updateUserNeedsToAcceptPrivacy;
const updateUserNeedsToAcceptSeller = users.updateUserNeedsToAcceptSeller;
const updateUserNeedsToSyncBank = users.updateUserNeedsToSyncBank;
const updateUserById = users.updateUserById;
const addChannelFees = users.addChannelFees;

//Listings
const addListing = listings.addListing;
const addDisclosure = listings.addDisclosure;
const addAttribute = listings.addAttribute;
const addTag = listings.addTag;
const editListing = listings.editListing;
const updateListingPriceById = listings.updateListingPriceById;
const updateShareListings = listings.updateShareListings;
const updateInternalNotesListings = listings.updateInternalNotesListings;
const updateDisclosuresInListings = listings.updateDisclosuresInListings;
const updateSplitsInListings = listings.updateSplitsInListings;
const addTicketingSystem = listings.addTicketingSystem;
const updateInhandDateInListings = listings.updateInhandDateInListings;
const removeListing = listings.removeListing;
const getInHandDateListing = listings.getInHandDateListing;
const getListingData = listings.getListingData;
const purchaseTicket = listings.purchaseTicket;
const updateEventLevelListingPrice = listings.updateEventLevelListingPrice;
const updateMultiListingPriceById = listings.updateMultiListingPriceById;
const updateListingGroupMarkAsSold = listings.updateListingGroupMarkAsSold;
const addTicketingSystemAccount = listings.addTicketingSystemAccount;
const updateSeatType = listings.updateSeatType;
const updateStockType = listings.updateStockType;
const updateUnitCost = listings.updateUnitCost;
const bulkUpdateNotesAndTags = listings.bulkUpdateNotesAndTags;
const updateDisclosureInBulk = listings.updateDisclosureInBulk;
const updateSplitsInBulk = listings.updateSplitsInBulk;
const updateViagogoStatus = listings.updateViagogoStatus;
const createIssueFlag = listings.createIssueFlag;

//Events
const addEvent = events.addEvent;
const updateEvent = events.updateEvent;
const updateEventStatus = events.updateEventStatus;
//Purchase Orders
const addPurchaseOrder = purchaseorders.addPurchaseOrder;

//Company
const saveCompanyDetails = companies.saveCompanyDetails;
const saveCompanyAssignedSuperAdmin = companies.saveCompanyAssignedSuperAdmin;

module.exports = {
    login,

    registerUser,
    updatePassword,
    updateUserNeedsToAcceptTerms,
    updateUserNeedsToAcceptPrivacy,
    updateUserNeedsToAcceptSeller,
    updateUserNeedsToSyncBank,
    updateUserById,

    addListing,
    addDisclosure,
    addAttribute,
    addTag,
    editListing,
    updateListingPriceById,
    updateShareListings,
    updateInternalNotesListings,
    updateDisclosuresInListings,
    updateSplitsInListings,
    addTicketingSystem,
    updateInhandDateInListings,
    removeListing,
    getInHandDateListing,
    getListingData,
    purchaseTicket,
    updateEventLevelListingPrice,
    updateMultiListingPriceById,
    updateListingGroupMarkAsSold,
    addEvent,
    updateEvent,
    addTicketingSystemAccount,
    addPurchaseOrder,
    updateEventStatus,
    updateSeatType,
    updateStockType,
    updateUnitCost,
    bulkUpdateNotesAndTags,
    updateDisclosureInBulk,
    saveCompanyDetails, 
    saveCompanyAssignedSuperAdmin,
    updateSplitsInBulk,
    updateViagogoStatus,
    createIssueFlag,
    addChannelFees

};

