const { gql } = require("apollo-server-express");

const typeDefs = gql`
    input Loginput {
        login: String!
        password: String!
    }
    type Loginoutput {
        id: ID!
    }
    type userregoutput {
        response: Boolean!,
        userId:Int
    }
    type Logindata {
        user_details: [Loginoutput]
    }

    input Getuserinput {
        id: ID!
    }
    type Getuseroutput {
        id: ID!
        firstName: String!
        lastName: String
        fullName: String!
        companyName: String
        email: String!
        phone: String!
        userRole: String!
        last_login: String
        stripeId: String
        streetAddress: String
        streetAddress2: String
        city: String
        state: String
        postalCode: String
        country: String
        needsToAcceptTerms: Boolean!
        needsToAcceptPrivacy: Boolean!
        needsToAcceptSeller: Boolean!
        needsToChangePassword: Boolean!
        needsToSyncBank: Boolean!
        message: String
    }

    type Getallusersoutput {
        first_name: String!
        id: Int!
        last_name: String
        company_name: String
        email: String!
        phone: String!
        user_role: String!
        last_login: String
        notes: String
        stripeId: String!
        street_address_1: String!
        street_address_2: String!
        city: String!
        state: String!
        postalCode: String!
        country: String!
        needsToAcceptTerms: Boolean!
        needsToAcceptPrivacy: Boolean!
        needsToChangePassword: Boolean!
        needsToSyncBank: Boolean!
        message: String
    }

    input UserRegistration{
        first_name: String!
        last_name: String!
        email: String!
        password: String!
        user_role:Int!
        company_id: ID
        phone:String
        created_by:ID!
         
    }
    input updateListingPriceInput{
        listingGroupId: ID!
        price: Float
        lastPrice: Float
        eventId: ID
        userId: ID
    }
    type updateListingPriceOutput {
        response: Boolean!
    }
    input updateListingInput{
        id: ID!
        price: Float
    }
    type listingPriceResult {
        response: Boolean!
    }
    input UserPasswordinput {
        user: String!
        password: String!
    }

    type UserPasswordoutput {
        firstName: String!
        id: ID!
        lastName: String!
        fullName: String!
        companyName: String
        email: String!
        phone: String!
        userRole: String
        last_login: String
        stripeId: String
        streetAddress: String
        streetAddress2: String
        city: String
        state: String
        postalCode: String
        country: String
        message: String
        needsToAcceptTerms: Boolean!
        needsToAcceptPrivacy: Boolean!
        needsToAcceptSeller: Boolean!
        needsToChangePassword: Boolean!
        needsToSyncBank: Boolean!
    }

    type User {
        id: Int!
        login: String!
    }
    type Userdetail {
        id: ID!
        email: String!
        phone: String!
    }


    type Getupdateuseroutput {
        id: ID!
        firstName: String!
        lastName: String!
        fullName: String!
        email: String!
        city: String!
        companyName: String
        country: String!
        phone: String!
        postalCode: String!
        state: String!
        streetAddress: String!
        streetAddress2: String
        needsToAcceptTerms: Boolean!
        needsToAcceptPrivacy: Boolean!
        needsToAcceptSeller: Boolean!
        needsToChangePassword: Boolean!
        needsToSyncBank: Boolean!
        message: String
    }
    

    input updateUserByIdinput {
        id: ID!
        firstName: String!
        lastName: String!
        email: String!
        city: String!
        companyName: String
        country: String!
        phone: String!
        postalCode: String!
        state: String!
        streetAddress: String!
        streetAddress2: String
    }

    input Getuserroleinput {
        id: ID!
    }

    type GetUserRoleOutput {
        id:Int!
        user_role_name:String!
    }

    input GetUserRole {
        user_role_name: String!
    }

    input checkEMail {
        email: String!
    }
    type checkEMailoutput {
        id:Int!
        email:String!
    }
    input stateInput {
        country_id: ID!
    }
    input cityInput {
        country_id: ID
        state_id: ID
        city_name_search: String
        limit: Int
        offset: Int
        fields: [String]
    }
    input getStateInput {
        id: ID!
    }
    input getCityInput {
        id: ID!
    }
    input getCountryInput {
        id: ID!
    }
    type countryOutput {
        id:ID!
        name:String!
        abbreviation: String
    }
    type stateOutput {
        id:ID!
        name:String!
        abbreviation: String
    }
    type cityOutput {
        id:ID!
        name:String!
        country: JSON
        state: JSON
    }
    type citiesCountOutput {
        count: Int
    }
    input GetListings {
        seller_id: String!
    }
    scalar JSON
    scalar Date
    type GetListingOutput {
        id:Int!
        buyer_id:Int
        seller_id:Int!
        cost:String!
        sold:Boolean!
        inactive:Boolean!
        
        quantity:Int!
        tags:String
        createdAt:String!
       
    }
    input GetFilterListings {
        userId: ID
        searches: String
        from_date: String
        to_date: String
        day_of_week: [String]
        ticket_type: String
        genre: String
        tags: [Int]
        in_hand: String
        section: String
        row: String
        quantity: Int
        inventory_attached: String
        held: String
        coordinates: JSON
        radius: Int
        days: Int,
        category: Int,
        hold:String
        performer: Int
        isfutureevents: Boolean
        page: Int
        limit: Int
        tagIds: [Int]
        venue: Int
        eventSearch : String
        cities: [String]
        completedEvents: Boolean
        purchaseDate:String
        split: String
        company: String
        userAssignedTo: String
        ticketingSystemAccount: String
    }

    type GetFilterListingOutput {
        event_id:Int
        eventname:String!
        eventdate:String
        totalseats:Int
        venuename:[String]
        timezone:[String]
        city:[String]
        state:[String]
        country:[String]
        totalavailabletkts:Int
        totalsoldtkts:Int
        last7days:Int
        awaitingdelivery:Int
        daystoevent:Int
        totalsoldtktsinlast7days:Int
        inactive: [Boolean]
        listingDtls: JSON,
        categoryname: [String],
        genrename: [String]
        performername: [String]
        categoryid: Int
        genreid: Int
        countrycode: [String]
        eventimage: String
    } 

    type GetAllTicketTypesOutput {
        id: Int!
        name: String!
        created_at: String
        updated_at: String
    }

    input GetEventPerformerVenueTagsInput {
        searchStr: String
        isOperationFilter: Boolean
    }

    type GetAllGenreDetailsOutput {
        id: Int!
        name: String!
        parent_category: String
        child_category: String
        tevo_id: String
        mercury_id: String
        created_at: String
        updated_at: String
    }

    type GetAllCategoryDetailsOutput {
        id: Int!
        name: String!
    }

    input GetAddFilterListings {
        searches: String
        from_date: String
        to_date: String
        day_of_week: [String]
        genre: String
        tags: [Int]
        page: Int
        limit: Int
    }
    
    type GetAddFilterListingsOutput {
        event_id:Int!
        eventname:String!
        eventdate:String
        venuename:[String]
        city:[String]
        state:[String]
        country:[String]
        timezone:[String]
    }
    
    input addListinginput {
        seller_id: ID!
        buyer_id: ID
        eventIds: String
        ticket_type_id: ID
        in_hand: String
        splitId: Int
        seat_type_id: ID
        quantity: String
        quantity_ga: String
        ticket_system_id: ID
        ticket_system_account: String
        hide_seat_number: Boolean
        seatInformationsJSON: JSON
        channelmarkupsJSON: JSON
        attachmentsJSON: JSON
    }
    type addListingoutput {
        isError: Boolean
        msg: String
        listingGroupIds: [Int]
        attachmentData: JSON
    }

    type editListingoutput {
        isError: Boolean
        msg: String
        attachmentData: JSON
    }

    input GetAllTagsInput {
        userId: ID
    }

    input GetAllDisclosuresInput {
        userId: ID
    }

    input GetAllAttributesInput {
        userId: ID
    }
    
    type GetAllTagsOutput {
        id: ID!
        tag_name: String!
        created_at: String
        updated_at: String
    }
    
    type getAllSplitsOutput {
        id: ID!
        name: String!
        created_at: String
        updated_at: String
    }

    type allCompaniesOutput {
        id: ID!
        company_name: String!
        parent_company_id: ID
        parentcompanyname: String
        address1: String
        address2: String
        city: ID
        state: ID
        zip_code: String
        country: ID
        created_at: String
        updated_at: String
        usercompany: Int
    }

    input allCompaniesInput {
        userId: ID!
    }
    
    type superAdminDetailsOutput {
        id: ID!
        first_name: String
        last_name: String
        email: String!
        company_id: ID
        created_at: String
        updated_at: String
    }

    type GetSeattypesOutput {
        id: ID!
        name: String!
        created_at: String
        updated_at: String
    }

    type GetAllTicketingSystemsOutput {
        id: ID!
        ticketing_system_name: String!
        created_at: String
        updated_at: String
    }

    type GetAllDisclosuresOutput {
        id: ID!
        disclosure_name: String!
        created_at: String
        updated_at: String
    }

    type GetAllAttributesOutput {
        id: ID!
        attribute_name: String!
        created_at: String
        updated_at: String
    }

    type GetAllChannelsOutput {
        id: ID!
        channel_name: String!
        created_at: String
        updated_at: String
    }

    input GetAllTicketingSystemAccountsInput {
        id: ID!
        userId: ID
        isOperationFilter:Boolean
    }
    type GetAllTicketingSystemAccountsOutput {
        id: ID!
        email: String!
    }

    input getListingsDetailsbyEventIdInput {
        event_id: Int!
    }

    input fetchListing {
        listingGroupId: Int!
    }
    type listingsplitOutput {
        splits_id:String
        internal_notes:String
        in_hand:String
        external_notes:String
        disclosure_id:[String]

    }
    type listingsOutput {
        event_id:Int!
        event_date: [String]
        in_hand:String
        tagids:[Int]
        attributeids:[Int]
        disclosureids:[Int]
        channel_ids:[Int]
        ticket_type_id:Int
        seat_type_id:Int
        splits_id:String
        ticket_system_id:Int
        hide_seat_numbers:Boolean
        ticket_system_account:Int
        quantity:Int
        row:String
        seat_start:Int
        seat_end:Int
        price:[Float]
        face_value:Float
        group_cost:Float
        unit_cost:Float
        internal_notes:String
        external_notes:String
        section:String
        PO_ID:String 
        event_name:[String]
        venue_name:[String]
        city:[String]
        state:[String]
        country:[String]
        eventdate:[String]
        dateDiff:String
        chm_ids:[String]
        channelManagement:[String]
        sold : [Boolean]
        days_old: String
        timezone: [String]
    }
    type channelOutput {
        values:JSON
    }
    type getListingsDetailsbyEventIdOutput {
        id: Int
        event_id: Int
        ticket_type_id: Int
        in_hand: String
        splits: String
        seat_type_id: Int
        quantity: Int
        ticket_system_id: Int
        ticket_system_account: String
        hide_seat_numbers: Boolean
        section: String
        row: String
        seat_start: String
        seat_end: String
        price: String
        face_value: String
        cost: String
        unit_cost: String
        attribute_id: Int
        PO_ID: Int
        zone_seating: String
        external_notes: String
        internal_notes: String
    }
input editListing{
        userId: ID
        isPriceChanged: Boolean
        listingGroupId: ID!
        ticket_type_id: ID
        in_hand: String
        splits: String
        seat_type_id: ID
        quantity: Int
        quantity_ga: String
        ticket_system_id: ID
        ticket_system_account: String
        disclosureids:[String]
        attrbtids:[String]
        selectedTags:[String]
        external_notes:String
        internal_notes:String
        hide_seat_number: String
        seatInformationsJSON: JSON
        face_value:String
        po_id:String 
        price:String
        row:String
        seat_end:Int
        seat_start:Int
        section:String
        unit_cost:String
        group_cost:String
        ticketing_system:String
        ticketing_system_accounts:String
        channelData:JSON
        attachmentsJSON:JSON
        channelmarkupsJSON: JSON
        eventId: Int
    }
    input addDisclosureInput {
        values: JSON
        userID: ID!
    }
    type addDisclosureOutput {
        message: String!
        data: JSON
        status:String
    }

    input addAttributeInput {
        values: JSON
        userID: ID!
    }
    type addAttributeOutput {
        message: String!
        data: JSON
        status:String
    }
    input addTagInput {
        values: JSON
        userID: ID!
    }
    type addTagOutput {
        message: String!
        data: JSON
        status:String
    }
    
    input updateShareListingsInput{
        listingGroupId: ID
        shareType: String!
        levelName: String!
        eventId: ID
        inActive: Boolean!
    }

    type updateShareListingsOutput {
        isError: Boolean
        msg: String
    }

    input updateInternalNotesListingsInput{
        listingGroupId: ID!
        internalNotes: String!
        levelName: String!
        selectedTags: JSON
        eventId: ID
    }

    input updateSeatTypeListingsInput{
        selectedTicketTypes: ID!
        selectedListingId: JSON
    }

    input updateStockTypeListingsInput{
        selectedStockTypes: ID!
        selectedListingId: JSON
    }

    type updateInternalNotesListingsOutput {
        isError: Boolean
        msg: String
    }

    input updateDisclosuresInListingsInput{
        listingGroupId: ID!
        disclosures: [String!]
        externalNotes: String
        levelName: String!
        eventId: ID
    }

    type updateDisclosuresInListingsOutput {
        isError: Boolean
        msg: String
    }

    input updateSplitsInListingsInput{
        listingGroupId: ID!
        splitId: Int
        levelName: String!
        eventId: ID
    }

    type updateSplitsInListingsOutput {
        isError: Boolean
        msg: String
    }
    
    input addTicketingSystemInput {
        values: JSON
    }
    type addTicketingSystemOutput {
        message: String!
        data: JSON
        status:String
    }
    
    input updateInhandDateInListingsInput{
        inHand: [String]
        eventId: ID
        levelName: String!
        listingGroupId: [ID!]
    }

    type updateInhandDateInListingsOutput {
        isError: Boolean
        msg: String
    }

    input removeListing{         
        listingGroupId: [ID!]
    }

    input getInHandDateListingListing{         
        eventId: ID!
        type: String!
    }

    type removeListingoutput {
        isError: Boolean
        msg: String
    }

    type getInHandDateListingoutput {
        isError: Boolean
        msg: String
        listingData: JSON
    }

    input getListingDataInput{         
        listingGroupId: ID
        eventId: ID
        level: String!
    }

    type getListingDataOutput {
        isError: Boolean
        msg: String
        listingData: JSON
    }
    
    input splitsLogicInput{
        eventId: ID!
        ticketsRequired: Int!
    }

    type splitsLogicOutput{
        isError: Boolean
        msg: String
        listingGroupIds: [String]
        splitName: [String]
    }

    input purchaseTicketInput{
        listingGroupId: ID!
        ticketsRequired: Int!
    }

    type purchaseTicketOutput{
        isError: Boolean
        msg: String
        listingGroupId: Int
        listingIds: [String]
        section: [String]
        row: [String]
        seatNumbers: [String]
    }
    
     input getSellerId {
        seller_id:ID
    }

    input getPieChartType {
        seller_id:ID,
        pieChartType: ID
    }

    type getColleactionData{
        current_inventory_value: String,
        margin_value: String,
        life_time_sales_value: String,
        live_listing_value: String,
        hold_time_data_value: String,
        busted_order_date: String,
        average_lisitng_age: String
    }

    input getFilterData {
        seller_id: ID,
        day_diff: JSON
    }

    type getFilterOutputData {
        local_today_rev: String,
        local_week_rev: String,
        local_month_rev: String,
        local_year_to_date_rev: String
    }

    input getMailData{
        subject: String,
        message: String,
        user_role: String
    }
    input getMailDataBroker{
        subject: String,
        message: String,         
        from: String
    }

    type sendMailResponse{
        status: String
    }

    type getTopLisitings{
        revenue_list: JSON,
        sale_list: JSON
    }

    type getChartData{
        chartData: JSON
    }

    type getJsonList{
        broker_list: JSON
    }
    
    type getCardData{
        pastDue: String
        openOrders: String
        completedOrders: String
        delivery: String
    }

    type getHotSellersData{
        seller_list: JSON
    }

    type getHotBuyersData{
        buyer_list: JSON
    }

    type getSalesByMonthData{
        sales_list: JSON
    }

    type getAllOrdersBarChartData{
        lineChart: JSON,
        barChart: JSON,
        salesMax: String,
        revenueMax: String,
        calendarPeriod: JSON
    }

    input getEventDetailsInput{
        eventIds: String!
    }

    input filterEventDetailsInput{
        searchText: String!
    }

    type getEventDetailsOutput{
        event_id: ID
        eventname: String
        eventdate: String
        venuename: [String]
        city: [String]
        state: [String]
        country: [String]
        timezone: [String]
    }

    type getEditEventDetailsOutput{
        data: JSON
    }

    input updateEventInput {
        eventId: ID!
        eventName: String
        eventDate: String
        eventTime: String
        eventStatus: String
        venueId: ID
        venueName: String
        venueAddress: String
        venueAddress2: String
        venueCity: String
        venueZipcode: String
        venueState: String
        venueCountry: String
        ticketSystemId: ID
        category: ID
        genre: ID
        eventLink: String
        eventImagelink: String
        userDetails:JSON
        cityName: String
        stateName: String
    }

    type updateEventOutput {
        isError: Boolean
        msg: String
        attachmentData: JSON
    }

    input updateEventLevelListingPriceInput{
        eventId: ID!
        price: Float
        userId: String
        updateSymbol: String
        listingIds: [Int]
    }

    type updateEventLevelListingPriceOutput{
        isError: Boolean!
        msg: String
    }

    input updateMultiListingPriceInput{
        listIds: [Int]
        eventId: Int
        price: Float
        userId: String
        updateSymbol: String
    }
    type updateMultiListingPriceOutput{
        isError: Boolean!
        msg: String
    }
    
    
    type attachmentOutput{
        id:Int
        attachment_name:String
        attachment_url:String
    }

    input insertPriceLogDetailsInput{
        eventId: ID!
        oldPrice: Float
        price: Float
    }

    type insertPriceLogDetailsOutput{
        isError: Boolean!
        msg: String
    }
    
    input getStatusChangeInput{
        listing_group_id: [ID]
    }

    type getStatusChangeValue{
        status_data: Boolean
    }

    type GetallBuyersOutput {
        first_name: String!
        id: Int!
        last_name: String!
        email: String!
        phone_number: String!
        address1: String!
        address2: String!
        city: String!
        state: String!
        zip_code: String!
        country: String!
        notes: String!
        transactionsByBuyerId:JSON
    }

    type allEventStatusOutput{
        id: Int!
        event_status: String!
    }
    
    input sendMagicLink{
        email: String!
    }

    type getRestStatus{
        reset_status: Boolean!
    }

    input changePassword{
        passwordDetails: JSON!
    }

    type getChangePasswordStatus{
        change_password_status: Boolean!
    }
    
    input updateListingGroupMarkAsSoldInput{
        listingGroupId: [ID!]
    }

    type updateListingGroupMarkAsSoldOutput{
        isError: Boolean!
        msg: String
    }

    input addEventInput {
        eventName: String
        eventStatus: String
        eventDate: String
        eventTime: String
        eventLink: String
        performer: String
        opponent: String
        city: String
        state: String 
        cityName: String
        stateName: String 
        country: ID   
        ticketingSystem: ID
        category: ID
        genre: ID
        venueName: String 
        venueAddress: String
        venueAddress2: String
        zip_code: String       
        userDetails:JSON
    }

    type addEventOutput {
        isError: Boolean
        msg: String
        attachmentData: JSON
    }

    input GetVenueFilterInput {
        searches: String
    }
    
    input GetVenueSearchFilterInput {
        searches: String
        type: String
    }
    
    type GetVenueFilterOutput {
        id:Int!
        name:String!
        address:String
        address2:String
        city:String
        state:String
        country:String
        zip_code:String
    }
    input GetAllMarketPlaceOrdersInput {
        searches: String
        orderStartDate: String
        orderEndDate: String
        deliveryStatusId: [String]
    }
    
    type GetAllMarketPlaceOrdersOutput{
        isError: Boolean
        msg: String
        data: JSON
    }
input getSingleMarketPlaceOrderDetailsInput {
        marketPlaceOrderId: Int!
    }

    type getSingleMarketPlaceOrderDetailsOutput {
        isError: Boolean
        msg: String
        data: JSON
    }
    
    input brokerOrdersInput{
        id:String 
        start_date:String
        end_date:String,
        orderStatus:[String]
    }    
    type allBrokerOrdersOutput {
        id:Int!       
        channel_id:Int
        cancelled_order_type_id:String
        created_at:String
        delivery_at:String
        channel_name:String
        listing_group_id:Int
        delivery_status_id:Int
        buyer_name:String
        buyer_email:String
        phone_number:String        
        external_channel_reference_id:ID
    }
    input brokerOrdersStatusInput{
        id:Int 
        status:Int
    }
    type brokerOrdersStatusOutput {
        status:Boolean
    }
    input brokerOrderId {
        orderId:String
    }

    type getBrokerOrderDetails {
        broker_order_detail: JSON
    }

    input addTicketingSystemAccountInput {
        values: JSON
        userID: ID!
    }
    type addTicketingSystemAccountOutput {
        message: String!
        data: JSON
        status:String
    }
    type allPaymentTypesOutput{
        id: Int!
        name: String
    }

    type allPaymentGroupsOutput{
        id: Int!
        name: String
    }
    type allPaymentTagsOutput{
        value:Int!
        label:String!
    }

    input addPurchaseOrderInput {
        user_id: ID
        vendor_id: ID
        purchase_date: String
        payment_method_id: ID
        sales_tax: String
        purchase_amount: String
        payment_last4_digits: String
        shipping_and_handling_fee: String
        external_reference: String
        payment_type_id: ID
        fees: String
        notes: String
        currency_id: ID
        created_by: ID
        tagsInformationsJSON: JSON
        vendorInformationsJSON: JSON
        listingInformationsJSON: JSON
        InvoiceAttachmentInformationsJSON: JSON
    }

    type addPurchaseOrderOutput {
        isError: Boolean
        msg: String
        data: JSON
    }

    input GetVendorSearchFilterInput {
        searches: String
        type: String
    }

    input GetListingSearchFilterInput {
        searches: String
        userId: ID
    }
    
    type GetVendorFilterOutput {
        id:Int!
        name:String!
        email:String
        phone: String
        address:String
        address2:String
        city:String
        state:String
        country:String
        zip:String
    }

    type GetListingFilterOutput {
        id:ID!
        eventName: String
        eventDate: String
        section: String
        row: String
        quantity: String
        seat_start: String
        seat_end: String
        group_cost: String
        unit_cost: String
        seat_type_id:Int
    }
    input purchaseDetailsInput {
        id:ID!
    }
    type purchaseDetailsOutput {
        purchaseOrderDetails:JSON       
    }
    type purchaseDetailsListOutput {
        purchaseOrderGrouplist:JSON       
    }

    input purchaseDetailsUpdate{
        id: ID!
        purchaseOrderDetails:JSON
    }
    input purchaseDetailsPOTUpdate{
        id: ID!
        purchaseOrderDetails:[Int]
    }
    
    type purchaseDetailsUpdateReturn{
        status_data: Boolean
    }

    input purchaseOrderSearchInput{
        searchText:String 
        startDate:String
        endDate:String,
        userId:ID
    }    
    type allPurchaseOrderListOutput {
        id:ID!       
        listingId:ID
        eventId:ID
        purchaseDate:String
        amount:String
        purchaseMethod:String
        lastFourDigits:String
        invoice:String
        tags:String
        attachment_url:String
    }
    
    input csvFileReadingInput{
        userId: ID
        filePath: String!
    }

    type csvFileReadingOutput{
        isError: Boolean
        msg: String
        listingGroupIds: String
    }

    input fetchListingGroupInput {
        listingGroupIds: [Int]
    }
    type listingGroupDetailsListOutput {
        isError: Boolean
        msg: String
        value:JSON
    }

    input updatePoAttachment{
        id: ID
        po_id: ID!
        attachment_url: String,
        attachment_name:String
    }

    type updatePoAttachmentStatus {
        status: Boolean
    }
    
    input getUserData {
        token: String
    }

    type getTokenUser {
        status: Boolean
        token_user: JSON
    }

    input getBankInfo {
        id: ID !
        stripeId: String
    }

    type getBankSyncDetails {
        status: Boolean
        userInfo: JSON
    }
    type getRecentListingDetailsOutput{
        event_id: ID
        eventname: String
        eventdate: String
        tickettype_id: ID
        ticket_system_id: ID
        seat_type_id: ID
        splits_id: ID
        in_hand: String
        hide_seat_numbers: Boolean
    }

    type updateEventStatusOutput{
        isError: Boolean
        msg: String
        data: JSON
    }

    input updateEventStatusInput {
        eventId: ID!
        eventStatusId: ID
    }    
    
    input companyNameInput {
        userId: ID
        companyId: ID
        parentCompanyId: ID
        companyName: String!
        address1: String
        address2: String
        city: ID
        state: ID
        zip_code: String
        country: ID
    }    

    type companyNameOutput {
        isError: Boolean
        message: String
        companyId: ID
    }
    input assignSuperAdminInput {
        userId: ID
        companyId: ID
        superAdminId: ID!
    }    

    type assignSuperAdminOutput {
        isError: Boolean
        message: String
        companyId: ID
    }
    type eventCountDetails {
        isError: Boolean
        countInfo: JSON
    }   

    type checkInhandDateOutput {
        response: Boolean
        inHandDate: String
    }
    input checkInhandDateInput {
        eventId: ID
    }

    type GetAllListingStatusOutput {
        id: ID!
        name: String
        status: String
    }  

    type GetAllIssueTypesOutput {
        value: ID!
        label: String
         
    } 

    input updateUnitCostInput {
        listingGroupId: [ID]
        unitCost:String
    }  

    input bulkUpdateNotesAndTagsInput{
        listingGroupId: [ID]
        internalNotes: String!
        selectedTags: JSON
    }
    input updateDisclosureInBulkInput{
        listingGroupId: [ID]
        disclosures: [String!]
        externalNotes: String
    }
    input updateViagogoInput{
        listingGroupId: [Int]
        listing_status_id: ID         
    }
    type updateViagogoOutput {
        isError: Boolean
        msg: String
    }
    input createIssueFlagInput{
        listingGroupId: [Int]
        issueValues:JSON
        event_id:ID!
    }
    type createIssueFlagOutput {
        isError: Boolean
        msg: String
    }
    input updateSplitsInBulkInput{
        listingGroupId: [ID]
        splitId: Int
    }

    type inventoryReportOutput {
        inventoryReportData: JSON,
        headers: JSON
    }
    
    input getResetToken{
        token: String
    }

    type resetTokenId{
        id: Int
    }

    type purchaseReportOutput {
        purchaseReportData: JSON,
        headers: JSON
    }

    type salesReportOutput {
        salesReportData: JSON,
        headers: JSON
    }    

    input inventoryReportInput {
        startDate: Date,
        endDate: Date,
    }

    input salesReportInput {
        startDate: Date,
        endDate: Date,
    }
    
    input purchaseReportInput {
        startDate: Date,
        endDate: Date,
    }

    input updateListingFromGridInput{
        listingGroupId: ID!
        columnName:String
        section:String
        row:String
        owner: String
        ticketingSystemAccount: String
        externalReference:String
        inHandDate:String
        purchaseOrderDate:String
        last4Digits:String
        seatStart:Int
        seatEnd:Int
        quantity:Int
    }
    type creditCardTypeOutput {
        id:ID!
        type_name:String
    }
    input channelFeeInput {
        userId:ID!
        createdBy:ID!
        channelFeesJSON:JSON
    }
    type channelFeeOutput {
        isError: Boolean
        message: String
    }
    input getChannelFeeInput {
        userId:ID!
    }
    type getChannelFeeOutput {
        channelFeesJSON:JSON
    }

    type Query {
        getuser(input: Getuserinput!): Getuseroutput
        allCountries : [countryOutput]
        allStates(input: stateInput!) : [stateOutput]
        allCities(input: cityInput!) : [cityOutput]
        citiesCount(input: cityInput!) : citiesCountOutput
        getCountry(input: getCountryInput!) : countryOutput
        getState(input: getStateInput!) : stateOutput
        getCity(input: getCityInput!) : cityOutput
        allUsers : [Getallusersoutput]
        allRoleUsers(input: Getuserroleinput!): [Getallusersoutput]
        emailExists(input: checkEMail!): [checkEMailoutput]
        userRoles(input: GetUserRole!): [GetUserRoleOutput]
        allListings(input: GetListings!): [GetListingOutput]
        filterListings(input: GetFilterListings!): [GetFilterListingOutput]
        allTicketTypes : [GetAllTicketTypesOutput ]
        getAllEventIdPerformerVenueCityCountryTags(input: GetEventPerformerVenueTagsInput): [String]
        allGenreDetails: [GetAllGenreDetailsOutput]
        allCategoryDetails: [GetAllCategoryDetailsOutput]
        addListingsFilter(input: GetAddFilterListings): [GetAddFilterListingsOutput]
        allTags(input: GetAllTagsInput) : [GetAllTagsOutput ]
        allChannels : [GetAllChannelsOutput]
        allTicketingSystems : [GetAllTicketingSystemsOutput ]
        allDisclosures(input: GetAllDisclosuresInput) : [GetAllDisclosuresOutput ]
        allAttributes(input: GetAllAttributesInput) : [GetAllAttributesOutput ]
        allSeattypes : [GetSeattypesOutput]
        allTicketingSystemAccounts(input: GetAllTicketingSystemAccountsInput!): [GetAllTicketingSystemAccountsOutput]
        selectedTicketingSystemAccounts(input: GetAllTicketingSystemAccountsInput!): [GetAllTicketingSystemAccountsOutput]
        getListingsDetailsbyEventId(input: getListingsDetailsbyEventIdInput!): [getListingsDetailsbyEventIdOutput]
        fetchListingDetails(input: fetchListing!): [listingsOutput]
        fetchAttachmentDetails(input: fetchListing!): [attachmentOutput]
        fetchChannelMarkups(input: fetchListing!): channelOutput
        getAllSplits : [getAllSplitsOutput]
        getListingGroupBasedOnSplitLogics(input: splitsLogicInput!): splitsLogicOutput
        getEventDetailsBasedOnEventId(input: getEventDetailsInput!): [getEventDetailsOutput]
	    fetchListingIconsId(input: fetchListing!): [listingsplitOutput]
        getCollectionValue(input: getSellerId!): getColleactionData
        getRevenueValue(input: getFilterData!): getFilterOutputData
        getSalesValue(input: getFilterData!): getFilterOutputData
        getListingValue(input: getFilterData!): getFilterOutputData
        sendSupportMail(input: getMailData!): sendMailResponse
        brokerSupportMail(input: getMailDataBroker!): sendMailResponse
        getTopFiveSoldListings(input: getSellerId!): getTopLisitings
        getPerformersDetails(input: getSellerId!): getChartData
        getChartDetails(input: getPieChartType!): getChartData
        getBrokerUsers : getJsonList
        orderCardInfo(input: getSellerId!): getCardData
        getHotSellersList : getHotSellersData
        getHotBuyersList : getHotBuyersData
        getSalesByMonth : getSalesByMonthData
        getAllOrdersBarChart: getAllOrdersBarChartData
        updateListingGroupStatus(input: getStatusChangeInput!): getStatusChangeValue
        allBuyers : [GetallBuyersOutput]
        sendResetMail(input: sendMagicLink!): getRestStatus
        sendChangePassword(input: changePassword!): getChangePasswordStatus
        allEventStatus : [allEventStatusOutput]
        getVenueFilter(input: GetVenueFilterInput): [GetVenueFilterOutput]
        venueSearchFilter(input: GetVenueSearchFilterInput): [String]       
        allBrokerOrders(input: brokerOrdersInput): [allBrokerOrdersOutput]
        changeOrderStatus(input: brokerOrdersStatusInput): brokerOrdersStatusOutput
        viewOrderDetails(input: brokerOrderId): getBrokerOrderDetails
        allMarketPlaceOrders(input: GetAllMarketPlaceOrdersInput): GetAllMarketPlaceOrdersOutput
        getSingleMarketPlaceOrderDetails(input: getSingleMarketPlaceOrderDetailsInput!): getSingleMarketPlaceOrderDetailsOutput
        allPaymentTypes : [allPaymentTypesOutput]
        allPaymentGroups : [allPaymentGroupsOutput]
        getTagsPurchaseOrder: [allPaymentTagsOutput]
        vendorSearchFilter(input: GetVendorSearchFilterInput): [String]
        listingSearchFilter(input: GetListingSearchFilterInput): [String]
        getvendorFilter(input: GetVendorSearchFilterInput): [GetVendorFilterOutput]
        getListingFilter(input: GetListingSearchFilterInput): [GetListingFilterOutput]
        getPurchaseOrderDetails(input: purchaseDetailsInput): purchaseDetailsOutput
        purchaseOrderDetailsUpdate(input: purchaseDetailsUpdate): purchaseDetailsUpdateReturn
        purchaseorderlistings(input: purchaseDetailsInput): purchaseDetailsListOutput
        allPurchaseOrders(input: purchaseOrderSearchInput): [allPurchaseOrderListOutput]
        readUploadListingsFile(input: csvFileReadingInput): csvFileReadingOutput 
        fetchListingGroupDetails(input: fetchListingGroupInput!): listingGroupDetailsListOutput
        purchaseOrderDetailsVendorUpdate(input: purchaseDetailsUpdate): purchaseDetailsUpdateReturn
        purchaseOrderDetailsTagsUpdate(input: purchaseDetailsPOTUpdate): purchaseDetailsUpdateReturn
        allTicketingSystemAccountsById(input: GetAllTicketingSystemAccountsInput!): [GetAllTicketingSystemAccountsOutput]
        updateAttachmentDetails(input: updatePoAttachment): updatePoAttachmentStatus
        getEventDetail(input: getEventDetailsInput!): getEditEventDetailsOutput
        getEventFilter(input: filterEventDetailsInput!): getEditEventDetailsOutput
        verifyUserToken(input: getUserData): getTokenUser
        updateStripId(input: getBankInfo): getBankSyncDetails
        getRecentListingDetailsBasedOnEventId(input: getEventDetailsInput!): [getRecentListingDetailsOutput]
        getEventCount(input: GetFilterListings!): eventCountDetails
        checkInhandDateIsEqual(input: checkInhandDateInput): checkInhandDateOutput
        operationFilterListings(input: GetFilterListings!): GetAllMarketPlaceOrdersOutput
        changeQCStatus(input:removeListing!): removeListingoutput
        allListingStatus : [GetAllListingStatusOutput]
        getAllCompanies : [allCompaniesOutput]
        getAllSuperAdminDetails : [superAdminDetailsOutput]
        getAssignedCompanies(input: allCompaniesInput): [allCompaniesOutput]
        checkResetToken(input: getResetToken!): resetTokenId
        getInventoryReport(input : inventoryReportInput ) : inventoryReportOutput
        getPurchaseReport(input : purchaseReportInput ) : purchaseReportOutput
        getSalesReport(input : salesReportInput ) : salesReportOutput
        allIssueTypes : [GetAllIssueTypesOutput]
        updateListingFromGrid(input: updateListingFromGridInput!): updateSplitsInListingsOutput
        allCreditCardTypes : [creditCardTypeOutput]
        getChannelFee(input: getChannelFeeInput!): getChannelFeeOutput

    }

    type Mutation {
        registerUser( input:UserRegistration!): userregoutput
        login(input: Loginput!): Loginoutput
        updatePassword(input: UserPasswordinput!): UserPasswordoutput
        updateUserNeedsToAcceptTerms(input: Getuserinput!): Getuseroutput
        updateUserNeedsToAcceptPrivacy(input: Getuserinput!): Getuseroutput
        updateUserNeedsToAcceptSeller(input: Getuserinput!): Getuseroutput
        updateUserNeedsToSyncBank(input: Getuserinput!): Getuseroutput
        updateUserById(input: updateUserByIdinput!): Getupdateuseroutput
        addListing(input:addListinginput!): addListingoutput
        editListing(input:editListing!): editListingoutput
        updateListingById(input: updateListingInput!): listingPriceResult
        addDisclosure(input:addDisclosureInput!): addDisclosureOutput
        addAttribute(input:addAttributeInput!): addAttributeOutput
        addTag(input:addTagInput!): addTagOutput
        updateListingPriceById(input: updateListingPriceInput!): updateListingPriceOutput
        updateShareListings(input: updateShareListingsInput!): updateShareListingsOutput
        updateInternalNotesListings(input: updateInternalNotesListingsInput!): updateInternalNotesListingsOutput
        updateDisclosuresInListings(input: updateDisclosuresInListingsInput!): updateDisclosuresInListingsOutput
        updateSplitsInListings(input: updateSplitsInListingsInput!): updateSplitsInListingsOutput
        addTicketingSystem(input:addTicketingSystemInput!): addTicketingSystemOutput
        updateInhandDateInListings(input: updateInhandDateInListingsInput!): updateInhandDateInListingsOutput
        removeListing(input:removeListing!): removeListingoutput
        getInHandDateListing(input:getInHandDateListingListing!): getInHandDateListingoutput
        getListingData(input:getListingDataInput!): getListingDataOutput
        purchaseTicket(input:purchaseTicketInput!): purchaseTicketOutput
        updateEventLevelListingPrice(input: updateEventLevelListingPriceInput!): updateEventLevelListingPriceOutput
        updateMultiListingPriceById(input: updateMultiListingPriceInput!): updateMultiListingPriceOutput
        insertPriceLogDetails(input: insertPriceLogDetailsInput!): insertPriceLogDetailsOutput
        addEvent(input:addEventInput!): addEventOutput
        updateEvent(input:updateEventInput!): updateEventOutput
        updateListingGroupMarkAsSold(input: updateListingGroupMarkAsSoldInput!): updateListingGroupMarkAsSoldOutput
        addTicketingSystemAccount(input:addTicketingSystemAccountInput!): addTicketingSystemAccountOutput
        addPurchaseOrder(input:addPurchaseOrderInput!): addPurchaseOrderOutput
        updateEventStatus(input:updateEventStatusInput!): updateEventStatusOutput 
        updateSeatType(input:updateSeatTypeListingsInput!): updateInternalNotesListingsOutput
        updateStockType(input:updateStockTypeListingsInput!): updateInternalNotesListingsOutput
        updateUnitCost(input:updateUnitCostInput!): updateInternalNotesListingsOutput
        bulkUpdateNotesAndTags(input: bulkUpdateNotesAndTagsInput!): updateInternalNotesListingsOutput
        updateDisclosureInBulk(input: updateDisclosureInBulkInput!): updateDisclosuresInListingsOutput
        saveCompanyDetails(input: companyNameInput!): companyNameOutput
        saveCompanyAssignedSuperAdmin(input: assignSuperAdminInput!): assignSuperAdminOutput
        updateSplitsInBulk(input: updateSplitsInBulkInput!): updateSplitsInListingsOutput
        updateViagogoStatus(input: updateViagogoInput!): updateViagogoOutput
        createIssueFlag(input: createIssueFlagInput!): createIssueFlagOutput
        addChannelFees(input: channelFeeInput!): channelFeeOutput
    }

`;

module.exports = typeDefs;
