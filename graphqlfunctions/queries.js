const resetpasswords = require("./authentication/resetpasswords");
const insights = require("./application/insights.js");
const buyers = require("./application/buyers");
const users = require("./application/users");
const listings = require("./application/listings");
const events = require("./application/events");
const brokerorders = require("./application/brokerorders");
const marketplaceorders = require("./application/marketplaceorders");
const purchaseorders = require("./application/purchaseorders");
const operations = require("./application/operations");
const companies = require("./application/companies");
const reports = require("./application/report.js");
// Reset password
const sendSupportMail = resetpasswords.sendSupportMail;
const sendChangePassword = resetpasswords.sendChangePassword;
const sendResetMail = resetpasswords.sendResetMail;
const checkResetToken = resetpasswords.checkResetToken;

// Insight
const getCollectionValue = insights.getCollectionValue;
const getRevenueValue = insights.getRevenueValue;
const getSalesValue = insights.getSalesValue;
const getListingValue = insights.getListingValue;
const getTopFiveSoldListings = insights.getTopFiveSoldListings;
const getPerformersDetails = insights.getPerformersDetails;
const getChartDetails = insights.getChartDetails;
const getBrokerUsers = insights.getBrokerUsers;
const orderCardInfo = insights.orderCardInfo;
const getHotSellersList = insights.getHotSellersList;
const getHotBuyersList = insights.getHotBuyersList;
const getSalesByMonth = insights.getSalesByMonth;
const getAllOrdersBarChart = insights.getAllOrdersBarChart;

//Buyer
const allBuyers = buyers.allBuyers;

//User
const allUsers = users.allUsers;
const allCountries = users.allCountries;
const allStates = users.allStates;
const allCities = users.allCities;
const citiesCount = users.citiesCount;
const getCountry = users.getCountry;
const getState = users.getState;
const getCity = users.getCity;
const allRoleUsers = users.allRoleUsers;
const userRoles = users.userRoles;
const emailExists = users.emailExists;
const getuser = users.getuser;
const verifyUserToken = users.verifyUserToken;
const updateStripId = users.updateStripId;
const getAllSuperAdminDetails = users.getAllSuperAdminDetails;
const getChannelFee = users.getChannelFee;

//Listings
const allTicketTypes = listings.allTicketTypes;
const getAllEventIdPerformerVenueCityCountryTags = listings.getAllEventIdPerformerVenueCityCountryTags;
const allGenreDetails = listings.allGenreDetails;
const allCategoryDetails = listings.allCategoryDetails;
const addListingsFilter = listings.addListingsFilter;
const allTags = listings.allTags;
const allTicketingSystems = listings.allTicketingSystems;
const allDisclosures = listings.allDisclosures;
const allAttributes = listings.allAttributes;
const allTicketingSystemAccounts = listings.allTicketingSystemAccounts;
const allChannels = listings.allChannels;
const allSeattypes = listings.allSeattypes;
const getListingsDetailsbyEventId = listings.getListingsDetailsbyEventId;
const fetchListingDetails = listings.fetchListingDetails;
const getAllSplits = listings.getAllSplits;
const updateListingGroupStatus = listings.updateListingGroupStatus;
const getListingGroupBasedOnSplitLogics = listings.getListingGroupBasedOnSplitLogics;
const getEventDetailsBasedOnEventId = listings.getEventDetailsBasedOnEventId;
const fetchAttachmentDetails = listings.fetchAttachmentDetails;
const fetchListingIconsId = listings.fetchListingIconsId;
const allListings = listings.allListings;
const filterListings = listings.filterListings;
const fetchChannelMarkups = listings.fetchChannelMarkups;
const selectedTicketingSystemAccounts = listings.selectedTicketingSystemAccounts;
const readUploadListingsFile = listings.readUploadListingsFile;
const checkInhandDateIsEqual = listings.checkInhandDateIsEqual;
const allIssueTypes = listings.allIssueTypes;

//CSV
const testCSVFileReading = listings.testCSVFileReading;
const fetchListingGroupDetails = listings.fetchListingGroupDetails;
const getRecentListingDetailsBasedOnEventId = listings.getRecentListingDetailsBasedOnEventId;
const getEventCount = listings.getEventCount;

//Credit Card
const allCreditCardTypes = listings.allCreditCardTypes;

//Events
const allEventStatus = events.allEventStatus;
const getVenueFilter = events.getVenueFilter;
const venueSearchFilter = events.venueSearchFilter;
const allBrokerOrders = brokerorders.allBrokerOrders;
const changeOrderStatus = brokerorders.changeOrderStatus;
const viewOrderDetails = brokerorders.viewOrderDetails;
const allTicketingSystemAccountsById = purchaseorders.allTicketingSystemAccountsById;
const updateAttachmentDetails = purchaseorders.updateAttachmentDetails;
const getEventDetail = events.getEventDetail;
const getEventFilter = events.getEventFilter;

//Market Place Orders
const { allMarketPlaceOrders, getSingleMarketPlaceOrderDetails } = marketplaceorders;

//Purchase Orders
const { allPaymentTypes, allPaymentGroups, vendorSearchFilter, getvendorFilter, listingSearchFilter, getListingFilter, getPurchaseOrderDetails, purchaseOrderDetailsUpdate, purchaseorderlistings, allPurchaseOrders, getTagsPurchaseOrder, purchaseOrderDetailsVendorUpdate, purchaseOrderDetailsTagsUpdate } = purchaseorders;

//Broker Order Details
const brokerSupportMail = resetpasswords.brokerSupportMail;

//Operations
const operationFilterListings = operations.operationFilterListings;
const changeQCStatus = operations.changeQCStatus;
const allListingStatus = operations.allListingStatus;

//Company
const getAllCompanies = companies.getAllCompanies;
const getAssignedCompanies = companies.getAssignedCompanies;
const updateListingFromGrid = operations.updateListingFromGrid;

module.exports = {

    sendSupportMail,
    sendChangePassword,
    sendResetMail,

    getCollectionValue,
    getRevenueValue,
    getSalesValue,
    getListingValue,
    getTopFiveSoldListings,
    getPerformersDetails,
    getChartDetails,
    getBrokerUsers,
    orderCardInfo,
    getHotSellersList,
    getHotBuyersList,
    getSalesByMonth,
    getAllOrdersBarChart,

    allBuyers,

    allUsers,
    allCountries,
    allStates,
    allCities,
    citiesCount,
    getCountry,
    getState,
    getCity,
    allRoleUsers,
    userRoles,
    emailExists,
    getuser,
    verifyUserToken,
    updateStripId,
    getAllSuperAdminDetails, 
    getChannelFee,
    
    allTicketTypes,
    getAllEventIdPerformerVenueCityCountryTags,
    allGenreDetails,
    allCategoryDetails,
    addListingsFilter,
    allTags,
    allTicketingSystems,
    allDisclosures,
    allAttributes,
    allTicketingSystemAccounts,
    allChannels,
    allSeattypes,
    getListingsDetailsbyEventId,
    fetchListingDetails,
    getAllSplits,
    updateListingGroupStatus,
    getListingGroupBasedOnSplitLogics,
    getEventDetailsBasedOnEventId,
    fetchAttachmentDetails,
    fetchListingIconsId,
    allListings,
    filterListings,
    fetchChannelMarkups,
    selectedTicketingSystemAccounts,
    readUploadListingsFile,
    checkInhandDateIsEqual,
    allEventStatus,
    getVenueFilter,
    venueSearchFilter,
    allMarketPlaceOrders,
    allBrokerOrders,
    changeOrderStatus,
    viewOrderDetails,
    getEventDetail,
    getEventFilter,


    getSingleMarketPlaceOrderDetails,

    allPaymentTypes,
    allPaymentGroups,
    vendorSearchFilter,
    getvendorFilter,
    listingSearchFilter,
    getListingFilter,
    getPurchaseOrderDetails,
    purchaseOrderDetailsUpdate,
    purchaseorderlistings,
    allPurchaseOrders,
    updateAttachmentDetails,
    fetchListingGroupDetails,
    purchaseOrderDetailsVendorUpdate,
    getTagsPurchaseOrder,
    purchaseOrderDetailsTagsUpdate,
    allTicketingSystemAccountsById,

    brokerSupportMail,
    getRecentListingDetailsBasedOnEventId,
    getEventCount,
    operationFilterListings,
    changeQCStatus,
    allListingStatus,
    getAllCompanies,
    getAssignedCompanies,
    checkResetToken,
    getInventoryReport: reports.getInventoryReport,
    getPurchaseReport: reports.getPurchaseReport,
    getSalesReport: reports.getSalesReport,
    allIssueTypes,
    updateListingFromGrid,
    allCreditCardTypes
};
