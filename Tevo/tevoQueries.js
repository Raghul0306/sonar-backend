const { makeExtendSchemaPlugin, gql } = require("graphile-utils");
const fetch = require("node-fetch");
const TevoClient = require("ticketevolution-node");
const apiToken = "4183d2b779e9a19af42872ffa3a5d5a3";
const apiSecret = "aRnjkWDOczYfCkm8g2mjtR9ElLZVpjmomVrHRJm+";

const tevoClient = new TevoClient({
  apiToken: apiToken,
  apiSecretKey: apiSecret
});

export const ExternalAPIExample = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      # 1. Create Type based on the fields that come back from external API Request
      type SampleUser {
        title: String
        id: Int
        userId: Int
        completed: Boolean
      }
      # 2. Extend the Query (or Mutation) type with our newly created external type
      extend type Query {
        externalApiSample(todoID: Int): SampleUser
      }
    `,
    resolvers: {
      Query: {
        // 3. Using our new type and query extention, reach out to your external API to fetch data
        externalApiSample: async (obj, args, context, info) => {
          return fetch(
            `https://jsonplaceholder.typicode.com/todos/${args.todoID}`
          ).then(res => res.json());
        }
      }
    }
  };
});


export const TevoPerformersByCategory = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      type TevoPerformer {
        name: String
        tevoid: Int
        popularity_score: Float
        primary: Boolean
      }
      extend type Query {
        TevoPerformers(categoryID: Int): [TevoPerformer]
      }
    `,
    resolvers: {
      Query: {
        TevoPerformers: async (obj, args, context, info) => {
          return tevoClient
            .getJSON(
              `https://api.sandbox.ticketevolution.com/v9/performers?category_id=${args.categoryID}`
            )
            .then(json => {
              json.performers.forEach(performer => {
                performer.tevoid = performer.id;
              });
              return json.performers;
            });
        }
      }
    }
  };
});

export const TevoEventsByPerformer = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      type TevoSeatingChart {
        medium: String
        large: String
      }
      type EventPerformer{
        performer: TevoPerformer
        primary: Boolean
      }
      type TevoEventConfig {
        id: Int
        url: String
        name: String
        ticket_utils_id: Int
        fanvenues_key: String
        seating_chart: TevoSeatingChart
      }
      type TevoEvent {
        name: String
        tevoid: Int
        popularity_score: Float
        long_term_popularity_score: Float
        occurs_at: Date
        configuration: TevoEventConfig
        available_count: Int
        performances:[EventPerformer]
      }
      extend type Query {
        TevoEvents(performerID: Int): [TevoEvent]
      }
    `,
    resolvers: {
      Query: {
        TevoEvents: async (obj, args, context, info) => {
          return tevoClient
            .getJSON(
              `https://api.sandbox.ticketevolution.com/v9/events?performer_id=${args.performerID}&primary_performer=true&only_with_available_tickets=true`
            )
            .then(json => {
              json.events.forEach(element => {
                element.tevoid = element.id;
              });
              return json.events;
            });
        }
      }
    }
  };
});

export const TevoListingsByEvent = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      type TevoListing {
        id: Int
        wholesale_price: Float
        row: String
        section: String
        available_quantity: Int
        instant_delivery: Boolean
        in_hand: String
        in_hand_on: Date
        event: TevoEvent
        retail_price: Float
        splits: [Int]
        format: String
        seats: [Int]
      }
      input TevoListingInput {
        id: Int
        wholesale_price: Float
        row: String
        section: String
        available_quantity: Int
        instant_delivery: Boolean
        in_hand: String
        in_hand_on: Date
        retail_price: Float
        splits: [Int]
        format: String
      }
      extend type Query {
        TevoListings(eventID: Int): [TevoListing]

      }
    `,
    resolvers: {
      Query: {
        TevoListings: async (obj, args, context, info) => {
          return tevoClient
            .getJSON(
              `https://api.sandbox.ticketevolution.com/v9/ticket_groups?event_id=${args.eventID}&lightweight=false&format=Eticket,Flash_seats,TM_mobile`
            )
            .then(json => {
              return json.ticket_groups;
            });
        }
      }
    }
  };
});

export const TevoCategories = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      type TevoCategory {
        id: Int
        name: String
      }
      extend type Query {
        TevoCategories: [TevoCategory]
      }
    `,
    resolvers: {
      Query: {
        TevoCategories: async (obj, args, context, info) => {
          return tevoClient
            .getJSON(`https://api.sandbox.ticketevolution.com/v9/categories`)
            .then(json => {
              return json.categories;
            });
        }
      }
    }
  };
});

export const TevoListingByID = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      extend type Query {
        TevoListingByID(tevoListingID: Int): TevoListing
      }
    `,
    resolvers: {
      Query: {
        TevoListingByID: async (obj, args, context, info) => {
          return tevoClient
            .getJSON(
              `https://api.sandbox.ticketevolution.com/v9/ticket_groups/${args.tevoListingID}?ticket_list=true`
            )
            .then(json => {
              return json;
            });
        }
      }
    }
  };
});

export const TevoCreateClient = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      type TevoClientAddress {
        id: Int
        region: String
        longitude: String
        extended_address: String
        label: String
        locality: String
        street_address: String
        name: String
        primary: Boolean
        primary_billing_address: Boolean
        country_code: String
        latitude: String
        postal_code: String
        po_box: Boolean
      }
      input TevoClientAddressInput {
        id: Int
        region: String
        id: Int
        longitude: String
        extended_address: String
        label: String
        locality: String
        street_address: String
        name: String
        primary: Boolean
        is_primary_shipping: Boolean
        is_primary_billing: Boolean
        country_code: String
        latitude: String
        postal_code: String
        po_box: Boolean
      }
      type TevoClientEmail {
        id: Int
        address: String
        label: String
        primary: Boolean
        instant_delivery: Boolean
      }
      input TevoClientEmailInput {
        id: Int
        address: String
        label: String
        id: Int
        primary: Boolean
        instant_delivery: Boolean
      }
      type TevoClientPhoneNumber {
        id: Int
        number: String
        label: String
        country_code: String
        extension: String
        primary: Boolean
      }
      input TevoClientPhoneNumberInput {
        id: Int
        number: String
        label: String
        id: Int
        country_code: String
        extension: String
        primary: Boolean
      }
      type TevoClientCompany {
        id: Int
        name: String
      }
      input TevoClientCompanyInput {
        name: String
      }
      type TevoClient {
        id: Int
        addresses: [TevoClientAddress]
        company: TevoClientCompany
        email_addresses: [TevoClientEmail]
        phone_numbers: [TevoClientPhoneNumber]
        primary_shipping_address: TevoClientAddress
        primary_billing_address: TevoClientAddress
        primary_phone_number: TevoClientPhoneNumber
        primary_email_address: TevoClientEmail
        name: String
        tags: [String]
        updatedAt: Date
        balance: Float
      }
      input TevoClientInput {
        id: Int
        name: String
        addresses: [TevoClientAddressInput]
        company: TevoClientCompanyInput
        email_addresses: [TevoClientEmailInput]
        phone_numbers: [TevoClientPhoneNumberInput]
        primary_shipping_address: TevoClientAddressInput
        primary_billing_address: TevoClientAddressInput
        primary_phone_number: TevoClientPhoneNumberInput
        primary_email_address: TevoClientEmailInput
        tags: [String]
        balance: Float
      }
      extend type Query {
        TevoCreateClient(tevoClient: TevoClientInput): TevoClient
      }
    `,
    resolvers: {
      Query: {
        TevoCreateClient: async (obj, args, context, info) => {
          let client = {
            clients: [args.tevoClient]
          };
          return tevoClient
            .postJSON(
              `https://api.sandbox.ticketevolution.com/v9/clients`,
              client
            )
            .then(json => {
              return json.clients[0]; //We are only creating one user at a time. No need for all of it.
            });
        }
      }
    }
  };
});

export const TevoClients = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      extend type Query {
        TevoClients: [TevoClient]
      }
    `,
    resolvers: {
      Query: {
        TevoClients: async (obj, args, context, info) => {
          return tevoClient
            .getJSON(`https://api.sandbox.ticketevolution.com/v9/clients`)
            .then(json => {
              return json.clients;
            });
        }
      }
    }
  };
});

export const TevoClientsByName = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      extend type Query {
        TevoClientsByName(name:String): [TevoClient]
      }
    `,
    resolvers: {
      Query: {
        TevoClientsByName: async (obj, args, context, info) => {
          return tevoClient
            .getJSON(`https://api.sandbox.ticketevolution.com/v9/clients?name=${args.name}`)
            .then(json => {
              return json.clients;
            });
        }
      }
    }
  };
});

export const TevoClientById = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      extend type Query {
        TevoClientByID(clientId: Int): TevoClient
      }
    `,
    resolvers: {
      Query: {
        TevoClientByID: async (obj, args, context, info) => {
          return tevoClient
            .getJSON(
              `https://api.sandbox.ticketevolution.com/v9/clients/${args.clientId}`
            )
            .then(json => {
              return json;
            });
        }
      }
    }
  };
});

export const TevoCreateOrder = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      type TevoOrderItem{
        
        eticket_delivery: Boolean
        eticket_available: Boolean
        price: Float
        ticket_group: TevoListing
        eticket_downloaded_at: Date
        eticket_downloaded_by: String
        id: Int
        quantity: Int
      }
      type TevoOrder {
        id: Int
        oid: String
        balance: Float
        total: Float
        subtotal: Float
        was_auto_pended: Boolean
        was_auto_accepted: Boolean
        was_auto_canceled: Boolean
        instructions: String
        state: String
        client: TevoClient
        buyer: TevoClient
        external_notes: String
        internal_notes: String
        items: [TevoOrderItem]
      }
      input SplitOptionInput {
        split_value: Int
      }
      extend type Query {
         """
          This query creates orders on TEVO
        """
        TevoCreateOrder(
          listing: TevoListingInput
          client: TevoClientInput
          splitOption: SplitOptionInput,
          stripeInformation: String
        ): TevoOrder
      }
    `,
    resolvers: {
      Query: {
        TevoCreateOrder: async (obj, args, context, info) => {
          let client = args.client; // TEVO CLIENT OBJECT
          let listing = args.listing;
          let useSplit = args.splitOption.split_value; // A split value for the ticket
          let stripeInformation = args.stripeInformation; // Transactional information for seller notes on TEVO
          let orderJson = {
            orders: [
              {
                shipped_items: [
                  {
                    items: [
                      {
                        ticket_group_id: 660818301, // listing.id
                        quantity: useSplit
                          ? 2
                          : 2, // if there is a truthy split value, use it.
                        price: listing.retail_price // listing.priceretail_price
                      }
                    ],
                    ship_to_name: client.name,
                    
                    type:args.listing.format,  // FlashSeats, "TM_mobile", "Eticket"
                    email_address_id: client.primary_email_address.id //client.primary_email_address.id
                    /**
                     * TODO: Make a separate shipping method with info below
                     */
                    // service_type: "LEAST_EXPENSIVE",
                    // signature_type: "INDIRECT",
                    // residential: true,
                    // ship_to_company_name: "Phunnel Inc",
                    // address_attributes: {
                    //   street_address: "123 Fake St",
                    //   extended_address: "Suite 230",
                    //   locality: "Austin",
                    //   region: "TX",
                    //   postal_code: "78759",
                    //   country_code: "US"
                    // },
                    
                  }
                ],
                email_address_id: client.primary_email_address.id,
                billing_address_id: client.primary_billing_address.id, //client.primary_billing_address.id
                payments: [
                  {
                    type: "offline" //We've handled this via stripe
                  }
                ],
                seller_id: 3464, // office.id This is hardcoded to the ticketevo account.
                client_id: client.id,
                service_fee: 0, //this will be determined by Tim
                shipping: 0,
                tax: 0,
                additional_expense: 0,
                discount: 0,
                external_notes: "These notes will be visible to all parties",
                internal_notes: `Stripe Transactional ID: blah blah blah`
              }
            ]
          };
       
          return tevoClient
            .postJSON(
              `https://api.sandbox.ticketevolution.com/v9/orders`,
              orderJson
            )
            .then(json => {
               if (json.error){
                throw json.message
            }
            else{
             return json.orders[0]
            }
            });
        }
      }
    }
  };
});

export const TevoOrders = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      extend type Query {
        TevoOrders: [TevoOrder]
      }
    `,
    resolvers: {
      Query: {
        TevoOrders: async (obj, args, context, info) => {
          return tevoClient
            .getJSON(`https://api.sandbox.ticketevolution.com/v9/orders`)
            .then(json => {
              return json.orders;
            });
        }
      }
    }
  };
});

export const TevoGetEtickets = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
     extend type Query {
      TevoGetEtickets(orderID: String): [TevoPDF]
      }
      type TevoPDF {
        item_id: Int
        content: String
      }
    `,
    resolvers: {
      Query: {
        TevoGetEtickets: async (obj, args, context, info) => {
          return tevoClient
            .getJSON(`https://api.sandbox.ticketevolution.com/v9/orders/${args.orderID}/print_etickets`)
            .then(json => {
              return json.files;
            });
        }
      }
    }
  };
});

export const TevoDeliverEtickets = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
     extend type Query {
      TevoDeliverEtickets(orderID: String, email: String ): TevoOrder
      }
    `,
    resolvers: {
      Query: {
        TevoDeliverEtickets: async (obj, args, context, info) => {
          let orderJson =
          {
             recipients:[
                args.email
             ]
          }
          return tevoClient
          .postJSON(
            `https://api.sandbox.ticketevolution.com/v9/orders/${args.orderID}/email`,
            orderJson
          )
          .then(json => {
            return json; // We will only do one at a time, but it returns an array.
          });
        }
      }
    }
  };
});

export const TevoOrdersByClient = makeExtendSchemaPlugin(build => {
  return {
    typeDefs: gql`
      extend type Query {
        TevoOrdersByClient(buyerID: Int): [TevoOrder]
      }
    `,
    resolvers: {
      Query: {
        TevoOrdersByClient: async (obj, args, context, info) => {
          return tevoClient
            .getJSON(`https://api.sandbox.ticketevolution.com/v9/orders?buyer_id=${args.buyerID}`)
            .then(json => {
              return json.orders;
            });
        }
      }
    }
  };
});