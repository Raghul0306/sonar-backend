const reportConstants = require('./reportConstants.js');

module.exports = {

    s3Path: {
        basePath: "s3://"
    },
    uploadURLs: {
        listingsAttachmentURL: "/listing-attachments",
        uploadListingsURL: "/uploads",
        imageUploadURL: "/imageupload"
    },
    folderNames: {
        listingsFolder: "listing-attachments",
        eventsFolder: "events",
        channelsFolder: "channels",
        venuesFolder: "venues",
        caegoryFolder: "categories",
        genreFolder: "genre",
        uploadListingsFolder: "uploadlistings",
        uploadsFolder: "uploads",
        purchaseOrderFolder: "purchase_order"
    },
    userRolesLower: {
        super_super_admin: "super_super_admin",
        super_admin: "super_admin",
        admin: "admin",
        broker: "broker",
        broker_user: "broker_user",
        accounting_admin:"accounting_admin",
        accounting_user:"accounting_user",
        processing_admin:"processing_admin",
        processing_user:"processing_user",
        marketplace_admin: "marketplace_admin",
        marketplace_user: "marketplace_user",
        buyer: "buyer", 
        support: "support", 
        support_admin: "support_admin", 
        support_user: "support_user" 
    },
    userTypes: {
        vendor: "vendor"
    },
    tableNames: {
        event: "event",
        ticketType: "ticket_types",
        splits: "splits",
        seatType: "seat_type",
        ticketingSystem: "ticketing_systems",
        ticketingSystemAccount: "ticketing_system_accounts",
        disclosures: "disclosures",
        attributes: "attributes",
        tags: "tags",
        vendor: "vendor",
        paymentMethod: "payment_method",
        paymentType: "payment_type"
    },
    uploadListingsColumnNames: {
        eventId: "Event ID",
        eventName: "Event Name",
        eventDate: "Event Date",
        ticketType: "Ticket Type",
        inHandDate: "In Hand Date",
        splitsId: "Splits ID",
        seatType: "Seat Type",
        ticketingSystemId: "Ticket System ID",
        ticketingSystemName: "Ticket System Name",
        ticketingSystemAccount: "Ticket System Account",
        hideSeatNumber: "Hide Seat Number",
        section: "Section",
        row: "Row",
        quantity: "Quantity",
        seatStart: "Seat Start",
        seatEnd: "Seat End",
        groupPrice: "Group Price",
        faceValue: "Face Value",
        groupCost: "Group Cost",
        unitCost: "Unit Cost",
        disclosures: "Disclosures",
        attributes: "Attributes",
        tags: "Tags",
        purchaseDate: "Purchase Date",
        purchaseAmount: "Purchase Amount",
        referenceId: "Reference ID"
    },
    splitTypes: {
        neverLeaveOne: "Never Leave 1",
        any: "Any",
        noSplits: "No-Splits"
    },
    searchFiltersFor: {
        venue: "venue",
        venueName: "venueName",
        performer: "performer"
    },
    seat_type: {
        general_admission: 2
    },
    shareTypes: {
        share: "share",
        pauseShare: "pauseShare"
    },
    seatTypes: {
        generalAdmission: 'General Admission (GA)'
    },
    ticketTypes: {
        eTicket: 'eTicket',
        eTransfer: 'eTransfer'
    },
    commonVariables: {
        yesLabel: 'Yes',
        noLabel: 'No',
        yesValue: 'yes',
        noValue: 'no',
    },
    responseStatus: {
        ok: "ok",
        error: "error",
        success: "success",
        failed: 'failed'
    },
    weekDaysArray: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    levelNames: {
        listing: "listing",
        event: "event"
    },
    listingVariables: {
        inHand: "in_hand",
        internalNotes: "internal_notes",
        externalNotes: "external_notes",
    },
    userRoles: {
        buyerUser: {
            userRoleName: "buyer"
        }
    },
    yadara: {
        url: process.env.APP_YADARA_URL,
        channelName: 'Yadara'
    },
    inputType: {
        id: 'ID',
        string: 'String'
    },
    listingStatusList: {
        qc_hold: "qc_hold",
        active: "active",
        inactive: "inactive",
        sold: "sold",
        viagogo_hold: "viagogo_hold"
    },
    authType: {
        signIn: 'signIn',
        signUp: 'signUp'
    },
    googleOAuthKeys: {
        clientId: process.env.GOOGLE_OAUTH_CLIENT_ID,
        clientSecret: process.env.GOOGLE_OAUTH_CLIENT_SECRET,
        grantType: 'authorization_code',
        yadaraRedirectURI: process.env.APP_YADARA_URL
    },
    googleAPI: {
        getTokenByCode: 'https://oauth2.googleapis.com/token',
        revokeToken: 'https://oauth2.googleapis.com/revoke',
        userInfo: 'https://www.googleapis.com/oauth2/v1/userinfo'
    },
    facebookAPI: {
        getProfile: `https://graph.facebook.com/v11.0/me`
    },
    tokenTypes:['refreshToken','accessToken'],
    sampleChannelFees:[{
        fees: 10,
        channel_id: 1
    },
    {
        fees: 15,
        channel_id: 2
    },
    {
        fees: 17,
        channel_id: 3
    }],
    mailParser:{
        response:{
            failed:{
                result: "failed",
                status: 400,
                addParser: "parser already exists",
                eventNotMatch: "Can't match the events",
                ticketSystemNotMatch: "Cant't match the ticket system",
                vendorNotMach: "Can't match the venodr details"
            },
            success: {
                result: "success",
                status: 200,
                addParser: "parser data added successfully",
                listingGroupAdd: "listing group added successfully",
                listingAdd: "listing added successfully",
                purchaseOrderAdd: "purchase order added successfully",
                purchaseOrderListing: "purchase order listing added successfully",
                parsingCompleted: "email parsing process completed"
            }
        },
        seatTypes:{
            seated_ticket : "Seated Ticket",
            general_admission : "General Admission",
            full_price_ticket : "Full Price Ticket",
        },
        seatTypesID: {
            'Seated Ticket': 1,
            'General Admission': 2,
            'Full Price Ticket' : 1,
        }
    },
    userRoleRestrictions: { //fetch only given user roles
        super_super_admin: ['super_admin', 'admin', 'broker', 'broker_user', 'accounting_admin', 'accounting_user', 'processing_admin', 'processing_user', 'marketplace_admin', 'marketplace_user', 'support_admin', 'support_user'],
        super_admin: ['admin', 'broker', 'broker_user', 'accounting_admin', 'accounting_user', 'processing_admin', 'processing_user', 'marketplace_admin', 'marketplace_user', 'support_admin', 'support_user'],

        admin: ['broker', 'broker_user', 'accounting_admin', 'accounting_user', 'processing_admin', 'processing_user'],
        broker: ['broker_user'],
        accounting_admin: ['accounting_user'],
        processing_admin: ['processing_user'],
        marketplace_admin: ['broker', 'broker_user', 'accounting_admin', 'accounting_user', 'marketplace_user', 'support_admin', 'support_user'],
        support_admin: ['support_user']
    },
    operationGridConstant:{
        section:"section",
        row:"row",
        owner:"owner",
        externalReference:"externalReference",
        ticketingSystemAccount:"ticketingSystemAccount",
        purchaseOrderDate:"purchaseOrderDate",
        last4Digits:"last4Digits",
        inHandDate:"inHandDate",
        seatNumberInfo:"seatNumberInfo",
        quantity:"quantity"
    },
    reports: reportConstants
};
