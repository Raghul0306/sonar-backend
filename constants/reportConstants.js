module.exports = {
    salesReport:{
        xlsHeaders:[
            {
                label:'Purchase ID',
                key:'po_id'
            },{
                label:'PO Date',
                key:'po_created_at'
            },{
                label:'Purchase Date',
                key:'po_date'
            },{
                label:'Purchase Amount',
                key:'po_amount'
            },{
                label:'Purchase External Reference',
                key:'po_external_reference'
            },{
                label:'Company Name',
                key:'company_name'
            },{
                label:'Purchasing Ticketing System',
                key:'ticketing_system_name'
            },{
                label:'Purchasing Account',
                key:'account_name'
            },{
                label:'PO Created By',
                key:'po_created_by'
            },{
                label:'Last Modified By',
                key:'po_updated_by'
            },{
                label:'Last Modified Date',
                key:'po_updated_at'
            },{
                label:'Purchase Type',
                key:''
            },{
                label:'Event ID',
                key:'event_id'
            },{
                label:'Event',
                key:'event_name'
            },{
                label:'Event Date',
                key:'event_date'
            },{
                label:'Event Status',
                key:'event_status'
            },{
                label:'Event Type',
                key:'event_type'
            },{
                label:'Event Sub-Type',
                key:'event_sub_type'
            },{
                label:'Event Tag',
                key:'event_notes'
            },{
                label:'Performer',
                key:'performer'
            },{
                label:'Venue',
                key:'venue'
            },{
                label:'City',
                key:'city'
            },{
                label:'State',
                key:'state'
            },{
                label:'Country',
                key:'country'
            },{
                label:'Number of Events',
                key:''
            },{
                label:'Listing ID',
                key:'po_listing_id'
            },{
                label:'Section',
                key:'section'
            },{
                label:'Row',
                key:'row'
            },{
                label:'Seats',
                key:'seat_start'
            },{
                label:'Seats',
                key:'seat_end'
            },{
                label:'QTY',
                key:'quantity'
            },{
                label:'Cost of Tickets in Foreign Currency',
                key:'group_cost'
            },{
                label:'Cost of Tickets in USD',
                key:''
            },{
                label:'Consignment',
                key:'company_name'
            },{
                label:'Stock Type',
                key:'ticket_type'
            },{
                label:'In-Hand Date',
                key:'in_hand'
            },{
                label:'Internal Notes',
                key:'internal_notes'
            },{
                label:'Last price update by user',
                key:'last_price_update_by'
            },{
                label:'Inventory Margin',
                key:'inventory_margin'
            },{
                label:'Inventory P/L',
                key:'inventory_profit_loss'
            },{
                label:'Inventory Tags',
                key:'tags'
            },{
                label:'Disclosures',
                key:'disclosures'
            },{
                label:'External Notes',
                key:'external_notes'
            },{
                label:'Ticket Status', 
                key:'listing_group_status'
            },{
                label:'Ticket Status Change Date',
                key:'listing_group_status_update_at'
            },{
                label:'Zone Seating',
                key:'zone_seating'
            },{
                label:'Vendor',
                key:'vendor_name'
            },{
                label:'Sale External Reference',
                key:'sale_external_reference'
            },{
                label:'Invoice Number',
                key:'order_number'
            },{
                label:'Invoice Date',
                key:'invoice_date'
            },{
                label:'Customer',
                key:'customer'
            },{
                label:'Delivery Date',
                key:'delivery_date'
            },{
                label:'Delivery Status',
                key:'delivery_status'
            },{
                label:'Gross Ticket Sales Amount in Foreign Currency',
                key:'group_price'
            },{
                label:'Gross Ticket Sales Amount in USD',
                key:''
            },{
                label:'Marketplace Selling Fee in Foreign Currency',
                key:'channel_fees'
            },{
                label:'Marketplace Selling Fee in USD',
                key:''
            },{
                label:'Net Ticket Sales Amount in Foreign Currency',
                key:'net_ticket_sale_amount'
            },{
                label:'Net Ticket Sales Amount in USD',
                key:''
            },{
                label:'Purchase Charge Creation Date',
                key:'po_updated_at'
            },{
                label:'Purchase Charge Type',
                key:''
            },{
                label:'Invoice Charge Amount',
                key:''
            },{
                label:'Invoice Charge Created Date',
                key:''
            },{
                label:'Invoice Charge Payment Date',
                key:''
            },{
                label:'Invoice Charge Status',
                key:''
            },{
                label:'Invoice Charge Type',
                key:''
            },{
                label:'Invoice Created By',
                key:''
            },{
                label:'Invoice Date',
                key:''
            },{
                label:'Invoice Days Aged',
                key:''
            },{
                label:'Invoice Due Date',
                key:''
            },{
                label:'Invoice number',
                key:''
            },{
                label:'Invoice Outstanding Balance',
                key:''
            },{
                label:'Invoice Payment Date',
                key:''
            },{
                label:'Invoice Payment Reference Number',
                key:''
            },{
                label:'Invoice Payment Status',
                key:''
            },{
                label:'Invoice Status',
                key:''
            },{
                label:'Invoice Status Change Date',
                key:''
            }
        ]
    },
    purchaseReport:{
        xlsHeaders:[
            {
                label:'Purchase ID',
                key:'po_id'
            },{
                label:'PO Date',
                key:'po_created_at'
            },{
                label:'Purchase Date',
                key:'po_date'
            },{
                label:'Purchase Amount',
                key:'po_amount'
            },{
                label:'Foreign Currency',
                key:'currency_name'
            },{
                label:'Purchase External Reference',
                key:'po_external_reference'
            },{
                label:'Company Name',
                key:'company_name'
            },{
                label:'Purchasing Ticketing System',
                key:'ticketing_system_name'
            },{
                label:'Purchasing Account',
                key:'account_name'
            },{
                label:'PO Created By',
                key:'po_created_by'
            },{
                label:'Last Modified By',
                key:'po_updated_by'
            },{
                label:'Last Modified Date',
                key:'po_updated_at'
            },{
                label:'Purchase Type',
                key:''
            },{
                label:'Event ID',
                key:'event_id'
            },{
                label:'Event',
                key:'event_name'
            },{
                label:'Event Date',
                key:'event_date'
            },{
                label:'Event Status',
                key:'event_status'
            },{
                label:'Event Type',
                key:'event_type'
            },{
                label:'Event Sub-Type',
                key:'event_sub_type'
            },{
                label:'Event Tag',
                key:'event_notes'
            },{
                label:'Performer',
                key:'performer'
            },{
                label:'Venue',
                key:'venue'
            },{
                label:'City',
                key:'city'
            },{
                label:'State',
                key:'state'
            },{
                label:'Country',
                key:'country'
            },{
                label:'Number of Events',
                key:''
            },{
                label:'Listing ID',
                key:'po_listing_id'
            },{
                label:'Section',
                key:'section'
            },{
                label:'Row',
                key:'row'
            },{
                label:'Seats',
                key:'seat_start'
            },{
                label:'Seats',
                key:'seat_end'
            },{
                label:'QTY',
                key:'quantity'
            },{
                label:'Cost of Tickets in Foreign Currency',
                key:'group_cost'
            },{
                label:'Cost of Tickets in USD',
                key:''
            },{
                label:'Consignment',
                key:'company_name'
            },{
                label:'Stock Type',
                key:'ticket_type'
            },{
                label:'Held',
                key:'qc_hold'
            },{
                label:'In hand',
                key:'is_in_hand'
            },{
                label:'In-Hand Date',
                key:'in_hand'
            },{
                label:'Internal Notes',
                key:'internal_notes'
            },{
                label:'Listed',
                key:'status'
            },{
                label:'Listing Amount',
                key:'group_price'
            },{
                label:'Last price update',
                key:'last_price_change'
            },{
                label:'Last price update by user',
                key:'last_price_update_by'
            },{
                label:'Inventory Margin',
                key:'inventory_margin'
            },{
                label:'Inventory P/L',
                key:'inventory_profit_loss'
            },{
                label:'Inventory Tags',
                key:'tags'
            },{
                label:'Disclosures',
                key:'disclosures'
            },{
                label:'External Notes',
                key:'external_notes'
            },{
                label:'Electronic transfer',
                key:'ticket_type'
            },{
                label:'Files uploaded',
                key:'is_attachments'
            },{
                label:'Ticket Status', 
                key:'listing_group_status'
            },{
                label:'Ticket Status Change Date',
                key:'listing_group_status_update_at'
            },{
                label:'Days Old',
                key:'old_days'
            },{
                label:'Barcodes',
                key:''
            },{
                label:'Zone Seating',
                key:'zone_seating'
            },{
                label:'Held Notes',
                key:'issue_names'
            },{
                label:'Credit Card Account',
                key:''
            },{
                label:'Credit Card Name',
                key:''
            },{
                label:'Credit Card Last 4 digits',
                key:'payment_last4_digits'
            },{
                label:'Credit Card Type',
                key:'credit_card_type'
            },{
                label: 'Credit Card Account Level',
                key: ''
            },{
                label:'Vendor',
                key:'vendor_name'
            },{
                label:'Sale External Reference',
                key:'sale_external_reference'
            },{
                label:'Invoice Number',
                key:'order_number'
            },{
                label:'Invoice Date',
                key:'invoice_date'
            },{
                label:'Customer',
                key:'customer'
            },{
                label:'Delivery Date',
                key:'delivery_date'
            },{
                label:'Delivery Status',
                key:'delivery_status'
            },{
                label:'Gross Ticket Sales Amount in Foreign Currency',
                key:'group_price'
            },{
                label:'Gross Ticket Sales Amount in USD',
                key:''
            },{
                label:'Marketplace Selling Fee in Foreign Currency',
                key:'channel_fees'
            },{
                label:'Marketplace Selling Fee in USD',
                key:''
            },{
                label:'Net Ticket Sales Amount in Foreign Currency',
                key:'net_ticket_sale_amount'
            },{
                label:'Net Ticket Sales Amount in USD',
                key:''
            },{
                label:'Purchase Charge Amount',
                key:'po_charge_amount'
            },{
                label:'Purchase Charge Creation Date',
                key:'po_updated_at'
            },{
                label:'Purchase Charge Type',
                key:''
            },{
                label:'Purchase Payment Date',
                key:''
            },{
                label:'Purchase Payment Status',
                key:''
            },{
                label:'Purchase Outstanding Balance',
                key:''
            }
        ]
    },
    inventoryReport:{
        xlsHeaders:[
            {
                label:'Purchase ID',
                key:'po_id'
            },{
                label:'PO Date',
                key:'po_created_at'
            },{
                label:'Purchase Date',
                key:'po_date'
            },{
                label:'Purchase Amount',
                key:'po_amount'
            },{
                label:'Foreign Currency',
                key:'currency_name'
            },{
                label:'Purchase External Reference',
                key:'po_external_reference'
            },{
                label:'Company Name',
                key:'company_name'
            },{
                label:'Purchasing Ticketing System',
                key:'ticketing_system_name'
            },{
                label:'Purchasing Account',
                key:'account_name'
            },{
                label:'PO Created By',
                key:'po_created_by'
            },{
                label:'Last Modified By',
                key:'po_updated_by'
            },{
                label:'Last Modified Date',
                key:'po_updated_at'
            },{
                label:'Purchase Type',
                key:''
            },{
                label:'Event ID',
                key:'event_id'
            },{
                label:'Event',
                key:'event_name'
            },{
                label:'Event Date',
                key:'event_date'
            },{
                label:'Event Status',
                key:'event_status'
            },{
                label:'Event Type',
                key:'event_type'
            },{
                label:'Event Sub-Type',
                key:'event_sub_type'
            },{
                label:'Event Tag',
                key:'event_notes'
            },{
                label:'Performer',
                key:'performer'
            },{
                label:'Venue',
                key:'venue'
            },{
                label:'City',
                key:'city'
            },{
                label:'State',
                key:'state'
            },{
                label:'Country',
                key:'country'
            },{
                label:'Number of Events',
                key:''
            },{
                label:'Listing ID',
                key:'po_listing_id'
            },{
                label:'Section',
                key:'section'
            },{
                label:'Row',
                key:'row'
            },{
                label:'Seats',
                key:'seat_start'
            },{
                label:'Seats',
                key:'seat_end'
            },{
                label:'QTY',
                key:'quantity'
            },{
                label:'Cost of Tickets in Foreign Currency',
                key:'group_cost'
            },{
                label:'Cost of Tickets in USD',
                key:''
            },{
                label:'Consignment',
                key:'company_name'
            },{
                label:'Stock Type',
                key:'ticket_type'
            },{
                label:'Held',
                key:'qc_hold'
            },{
                label:'In hand',
                key:'is_in_hand'
            },{
                label:'In-Hand Date',
                key:'in_hand'
            },{
                label:'Internal Notes',
                key:'internal_notes'
            },{
                label:'Listed',
                key:'status'
            },{
                label:'Listing Amount',
                key:'group_price'
            },{
                label:'Last price update',
                key:'last_price_change'
            },{
                label:'Last price update by user',
                key:'last_price_update_by'
            },{
                label:'Inventory Tags',
                key:'tags'
            },{
                label:'Disclosures',
                key:'disclosures'
            },{
                label:'External Notes',
                key:'external_notes'
            },{
                label:'Electronic transfer',
                key:'ticket_type'
            },{
                label:'Files uploaded',
                key:'is_attachments'
            },{
                label:'Ticket Status',
                key:'listing_group_status'
            },{
                label:'Ticket Status Change Date',
                key:'listing_group_status_update_at'
            },{
                label:'Days Old',
                key:'old_days'
            },{
                label:'Barcodes',
                key:''
            },{
                label:'Zone Seating',
                key:'zone_seating'
            },{
                label:'Held Notes',
                key:'issue_names'
            },{
                label:'Vendor',
                key:'vendor_name'
            },{
                label:'Invoice Date',
                key:'invoice_date'
            }
        ]
    }
}