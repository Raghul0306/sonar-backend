
const db = require('../models');
const { QueryTypes } = require('sequelize');
const tzlookup = require("tz-lookup");
const https = require('https');
const Op = db.Sequelize.Op;
const moment = require('moment-timezone');
const eventTable = db.events;
module.exports = {
    async getEventVenueLatLong(queryParams) {

        return new Promise((resolve, reject) => {
            try {
                const options = {
                    hostname: process.env.HERE_API_HOST,
                    path: process.env.HERE_API_PATH + process.env.HERE_API_KEY + '&q=' + encodeURI(queryParams),
                    method: 'GET'
                };
                const req = https.request(options, res => {
                    res.on('data', d => {
                        resolve(JSON.parse(d.toString()));
                    });
                })
                req.on('error', error => {
                    console.error(error)
                });
                req.end()
            }
            catch (error) {
                reject(error);
            }
        })
    },
    async updateVenueTimeZone() {
        var fetchVenue = `SELECT venue.id, venue.name, venue.address, venue.city, venue."state", venue.country, venue.timezone, cities."name"as city_name,"states"."name"as state_name, countries."name"as country_name from ticket_exchange.venue LEFT JOIN ticket_exchange.countries on ( countries.id :: VARCHAR = venue.country or countries."name"= venue.country ) LEFT JOIN ticket_exchange.states on ( states.id :: VARCHAR = venue."state"or states."name"= venue."state") LEFT JOIN ticket_exchange.cities on ( cities.id :: VARCHAR = venue.city or cities."name"= venue.city ) WHERE TRIM(venue.timezone) ='' or venue.timezone is null ORDER BY venue.id ASC`;
        var venueDetails = await db.sequelize.query(fetchVenue, { type: QueryTypes.SELECT });
        for (const venue of venueDetails) {
            let position = '';
            let locationDetails = [venue.city_name, venue.city, venue.state_name, venue.state, venue.country_name, venue.country];
            for (const locationAddress of locationDetails) {
                if (locationAddress && isNaN(locationAddress) && position == '') {
                    parsedData = await this.getEventVenueLatLong(locationAddress);
                    position = parsedData && parsedData.items && parsedData.items.length ? parsedData.items[0].position : '';
                    if (position != '' && position.lat && position.lng) {
                        let timeZonelookup = await tzlookup(position.lat, position.lng);
                        await db.sequelize.query(`UPDATE "ticket_exchange"."venue" SET "timezone" = '${timeZonelookup}', latitude= '${position.lat}' , longitude= '${position.lng}' WHERE "id" =${venue.id}`);
                        break;
                    }
                }
            }
        }

    },
    async changeEventStatus() { // to change the completed event status based on local time 
        var fetchEvents = `SELECT event.id, "event"."date", venue.timezone    FROM        ticket_exchange.event  LEFT JOIN ticket_exchange.venue on venue.id = event.venue_id WHERE "event"."date" < now() and event_status_id = 2`;
        var venueDetails = await db.sequelize.query(fetchEvents, { type: QueryTypes.SELECT });
        let currentDate = new Date();
        let eventCompletedEvent = [];
        for (const eventDetails of venueDetails) {
            if (eventDetails.timezone && eventDetails.date) {
                let localCurrentDate = moment.utc(currentDate).tz(eventDetails.timezone).format('YYYY-MM-DD');
                let localEventDate = moment.utc(eventDetails.date).tz(eventDetails.timezone).format('YYYY-MM-DD');
                if (new Date(localCurrentDate) > new Date(localEventDate)) {
                    eventCompletedEvent.push(eventDetails.id);
                }
            }
        }
        if (eventCompletedEvent && eventCompletedEvent.length) {
            try {
                var set_data = 1; //completed status
                var updateQuery = `UPDATE ticket_exchange.event SET event_status_id = ` + set_data + ` WHERE id in (` + eventCompletedEvent + `)`;
                await db.sequelize.query(updateQuery, { type: QueryTypes.UPDATE });
            } catch (error) {
                console.log(error)
            }
        }
    }
}