
const axios = require('axios');
const constants = require('./../constants/constants.js');

module.exports = {
    getProfile : async ({token,fields}) => {
        return axios.get(`${constants.facebookAPI.getProfile}?access_token=${token}&fields=${fields}`);
    }
}