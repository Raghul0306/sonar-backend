
const axios = require('axios');
const constants = require('./../constants/constants.js');

module.exports = {
    getUser : async ({token}) => {
        return axios.get(`${constants.googleAPI.userInfo}?access_token=${token}`);
    },
    getTokens : async (formData) => {
        return axios({
            method: "post",
            url: constants.googleAPI.getTokenByCode,
            data: formData,
        });
    },
    revokeToken : async ({token}) => {
        return axios({
            method: "post",
            url: `${constants.googleAPI.revokeToken}?token=${token}`,
        });
    }  
}