const db = require('../models');
const { QueryTypes, where,Op } = require('sequelize');
const BuyerUsers = db.buyerUsers;
const Userrole = db.userroles;
const _ = require('lodash');
const constants = require('../constants/constants.js');
const crypto = require("crypto");
const jwt = require('jsonwebtoken');

module.exports = {
    async getBuyerUserRole(){
        return Userrole.findOne({
            attributes:['id'],
            where:{
                user_role_name: _.get(constants,'userRoles.buyerUser.userRoleName')
            }
        });
    },
    async getBuyerUsers(query = {}){
        return BuyerUsers.findAll(query)
    },
    async formDataToBuyerUserData(formData, isDeleteNonExitField = false){
        let buyerUserData = {
            first_name: '',
            last_name: '',
            email: '',
            phone_number: '',
            zip_code: 0,
            company_name: "",
            address1: "",
            city: "",
            state: "",
            country: "",
        }
        for(let key in buyerUserData){
            
            let currentKey = key;
            let value = _.get(formData,`${key}.0`);

            if(currentKey == 'phone_number' && !value){
                value = _.get(formData,`phone.0`);
            } 
            
            if(value)  buyerUserData[currentKey] = value;
            else if(isDeleteNonExitField && !buyerUserData[currentKey]) delete buyerUserData[currentKey];

        }
        return buyerUserData;
    },
    async createBuyerUser(bueryUserData){
        return BuyerUsers.create(bueryUserData);
    },
    async updateBuyerUsers(data,query){
        return BuyerUsers.update(data, query);
    },
    async getCryptoRandonToken(){
        return crypto.randomBytes(32).toString("hex");
    },
    async formatUserDetailsForToken(buyerUser = {}){
        return {
            firstName: buyerUser.dataValues && buyerUser.dataValues.first_name,
            lastName: buyerUser.dataValues && buyerUser.dataValues.last_name,
            userId: buyerUser.dataValues && buyerUser.dataValues.id,
            userRole: buyerUser.dataValues && buyerUser.dataValues.user_role_id,          
        }
    },
    async getToken(userDetails,type,key){
        if(!constants.tokenTypes.includes(type)) throw Error('Invalid token type');
        let token = '';
        switch(type){
            case 'refreshToken':
                token = jwt.sign( userDetails,  key)
                break;
            case 'accessToken':
                token = jwt.sign( 
                    userDetails,
                    key,{
                        expiresIn: 3600,
                    }
                );
                break;
        }
        return token;
    }    
}
    
