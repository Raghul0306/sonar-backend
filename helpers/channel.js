const db = require('../models');
const constants = require('../constants/constants.js');
const _ = require('lodash');

module.exports = {
    async getCurrentUserChannel(request){
        let referer = request.header('referer');
        if(`${_.get(constants,'yadara.url')}/` === referer){
            return db.channels.findOne({
                attributes:['id'],
                where:{
                    channel_name:_.get(constants,'yadara.channelName')
                }
            });
        } else {
            return null;
        }
    }
}