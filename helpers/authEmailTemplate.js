const baseURL = process.env.APP_YADARA_URL;
const yadaraPasswordSetMailTemplate = async (setUrl,email) => {

	return `<!doctype html>
	<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

		<head>
			<title>
			</title>
			<!--[if !mso]><!-->
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<!--<![endif]-->
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
			<style type="text/css">
				#outlook a {
					padding: 0;
				}

				body {
					margin: 0;
					padding: 0;
					-webkit-text-size-adjust: 100%;
					-ms-text-size-adjust: 100%;
				}

				table,
				td {
					border-collapse: collapse;
					mso-table-lspace: 0pt;
					mso-table-rspace: 0pt;
				}

				img {
					border: 0;
					height: auto;
					line-height: 100%;
					outline: none;
					text-decoration: none;
					-ms-interpolation-mode: bicubic;
				}

				p {
					display: block;
					margin: 13px 0;
				}

			</style>
			<!--[if mso]>
				<noscript>
				<xml>
				<o:OfficeDocumentSettings>
				<o:AllowPNG/>
				<o:PixelsPerInch>96</o:PixelsPerInch>
				</o:OfficeDocumentSettings>
				</xml>
				</noscript>
				<![endif]-->
			<!--[if lte mso 11]>
				<style type="text/css">
				.mj-outlook-group-fix { width:100% !important; }
				</style>
				<![endif]-->
			<style type="text/css">
				@media only screen and (min-width:480px) {
					.mj-column-per-100 {
						width: 100% !important;
						max-width: 100%;
					}
				}

			</style>
			<style media="screen and (min-width:480px)">
				.moz-text-html .mj-column-per-100 {
					width: 100% !important;
					max-width: 100%;
				}

			</style>
			<style type="text/css">
				@media only screen and (max-width:480px) {
					table.mj-full-width-mobile {
						width: 100% !important;
					}

					td.mj-full-width-mobile {
						width: auto !important;
					}
				}

			</style>
		</head>

		<body style="word-spacing:normal;">
			<div style="">
				<!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:500px;" width="500" bgcolor="#f8f8f8" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
				<div style="margin:0px auto;max-width:500px;">
					<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
						<tbody>
							<tr>
								<td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
									<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:500px;" ><![endif]-->
									<div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
										<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
											<tbody>
												<tr>
													<td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
														<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
															<tbody>
																<tr>
																	<td style="width:150px;">
																		<img height="auto" src="https://i.ibb.co/PDS8gxb/yadara-logo-Blue-Black.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="150" />
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--[if mso | IE]></td></tr></table><![endif]-->
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--[if mso | IE]></td></tr></table><![endif]-->

				<!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:500px;" width="500" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
				<div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:500px;">
					<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
						<tbody>
							<tr>
								<td style="direction:ltr;font-size:0px;padding:20px 0px 20px 0px;text-align:center;">
									<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:500px;" ><![endif]-->
									<div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
										<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
											<tbody>
												<tr>
													<td align="center" style="font-size:0px;padding:0px 25px 30px 25px;word-break:break-word;">
														<div style="font-family:Nunito,sans-serif;font-size:28px;line-height:24px;color:#23AAE1;font-weight:bold; text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);">
															Welcome to Yadara!
														</div>
													</td>
												</tr>
												<tr>
													<td align="center" style="font-size:0px;padding:0px 0px 30px 0px;word-break:break-word;">
														<div style="
														font-family: 'Open Sans', sans-serif;
														font-style: normal;
														font-weight: normal;
														font-size: 17px;
														line-height: 24px;
														color: #23AAE1">
															Please verify your email address, <span style="color:#23AAE1">${email}</span>
														</div>
													</td>
												</tr>
												<tr>
													<td align="center" style="font-size:0px;padding-bottom: 20px; word-break:break-word;">
														<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
															<tbody>
																<tr>
																	<td style="width: 248px; height: 50px;">
																		<a href="${setUrl}" style=" color: #FFFFFF;font-family:Nunito,sans-serif;font-style: normal; font-weight: 800;font-size: 20px;line-height: 24px;text-decoration: none;outline: 0;text-decoration: none;outline: 0;width: 248px;max-height: 50px;background: #23aae1;display: block;border-radius: 10px;color: #fff;font-size: 18px;text-align: center;padding: 12px;"> Verify your email </a>
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--[if mso | IE]></td></tr></table><![endif]-->
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--[if mso | IE]></td></tr></table><![endif]-->


				<!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:500px;" width="500" bgcolor="#f8f8f8" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
				<div style="background:#f8f8f8;background-color:#f8f8f8;margin:0px auto;max-width:500px;">
					<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f8f8f8;background-color:#f8f8f8;width:100%;">
						<tbody>
							<tr>
								<td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
									<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:500px;" ><![endif]-->
									<div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
										<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
											<tbody>
												<tr>
													<td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
														<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
															<tbody>
																<tr>
																	<td style="width:80px;padding-bottom: 10px;">
																		<img height="auto" src="https://i.ibb.co/PDS8gxb/yadara-logo-Blue-Black.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="80" />
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
												<tr>
													<td align="center" style="font-size:0px;padding:0px 25px;word-break:break-word;">
														<div style="font-family:Calibri,Arial,sans-serif;font-size:12px;line-height:22px;text-align:center;color:#050505;">
															Copyright &copy; 2021. All Rights Reserved on Yadara
														</div>
													</td>
												</tr>
												<tr>
													<td align="center" style="font-size:0px;padding:0px 25px;word-break:break-word;">
														<div style="font-family:Calibri,Arial,sans-serif;font-size:12px;line-height:22px;text-align:center;color:#050505;">
															<a style="color:#050505" href="${process.env.APP_YADARA_URL}/terms-and-conditions" target="_blank">
																Terms and Conditions
															</a>&nbsp;&nbsp;&nbsp;

															<a style="color:#050505" href="${process.env.APP_YADARA_URL}/privacy-policy" target="_blank">
																Privacy Policy
															</a>&nbsp;&nbsp;&nbsp;

															<a style="color:#050505" href="https://yadara.zendesk.com/" target="_blank">
																Support
															</a>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--[if mso | IE]></td></tr></table><![endif]-->
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--[if mso | IE]></td></tr></table><![endif]-->
			</div>
		</body>
	</html>`;
}


const yadaraForgetPasswordMailTemplate = (setUrl,emailType) => {
	var title;
	var description;
	var buttonValue;
	if(emailType==="resetPassword")
	{
		title="Reset Your Password";
		description="We received a request to reset your Yadara password.";
		buttonValue="Reset My Password";
	}
	else{
		title="Forgot Password?";
		description="We received a request that you forgot your Yadara password.";
		buttonValue="Change My Password";
	}

	if(title && description && buttonValue)
		return `<!doctype html>
	<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

	<head>
		<title>
		</title>
		<!--[if !mso]><!-->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!--<![endif]-->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
		<style type="text/css">
			#outlook a {
				padding: 0;
			}

			body {
				margin: 0;
				padding: 0;
				-webkit-text-size-adjust: 100%;
				-ms-text-size-adjust: 100%;
			}

			table,
			td {
				border-collapse: collapse;
				mso-table-lspace: 0pt;
				mso-table-rspace: 0pt;
			}

			img {
				border: 0;
				height: auto;
				line-height: 100%;
				outline: none;
				text-decoration: none;
				-ms-interpolation-mode: bicubic;
			}

			p {
				display: block;
				margin: 13px 0;
			}

		</style>
		<!--[if mso]>
			<noscript>
			<xml>
			<o:OfficeDocumentSettings>
			<o:AllowPNG/>
			<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
			</xml>
			</noscript>
			<![endif]-->
		<!--[if lte mso 11]>
			<style type="text/css">
			.mj-outlook-group-fix { width:100% !important; }
			</style>
			<![endif]-->
		<style type="text/css">
			@media only screen and (min-width:480px) {
				.mj-column-per-100 {
					width: 100% !important;
					max-width: 100%;
				}
			}

		</style>
		<style media="screen and (min-width:480px)">
			.moz-text-html .mj-column-per-100 {
				width: 100% !important;
				max-width: 100%;
			}

		</style>
		<style type="text/css">
			@media only screen and (max-width:480px) {
				table.mj-full-width-mobile {
					width: 100% !important;
				}

				td.mj-full-width-mobile {
					width: auto !important;
				}
			}

		</style>
	</head>

	<body style="word-spacing:normal;">
		<div style="">
			<!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:500px;" width="500" bgcolor="#f8f8f8" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
		 
			<div style="margin:0px auto;max-width:500px;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
					<tbody>
						<tr>
							<td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
								<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:500px;" ><![endif]-->
								<div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
									<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
										<tbody>
											<tr>
												<td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
													<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
														<tbody>
															<tr>
																<td style="width:150px;">
																	<img height="auto" src="https://i.ibb.co/PDS8gxb/yadara-logo-Blue-Black.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="150" />
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!--[if mso | IE]></td></tr></table><![endif]-->
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<!--[if mso | IE]></td></tr></table><![endif]-->

			<!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:500px;" width="500" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
			<div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:500px;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
					<tbody>
						<tr>
							<td style="direction:ltr;font-size:0px;padding:20px 0px 20px 0px;text-align:center;">
								<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:500px;" ><![endif]-->
								<div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
									<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
										<tbody>
											<tr>
												<td align="center" style="font-size:0px;padding:0px 25px 30px 25px;word-break:break-word;">
													<div style="font-family: Nunito, sans-serif;font-style: normal;font-weight: 800;font-size: 28px;line-height: 24px;text-align: center;color: #23AAE1;">
													 ${title}
													</div>
												</td>
											</tr>
											<tr>
												<td align="center" style="font-size:0px;padding:0px 25px 30px 25px;word-break:break-word;">
													<div style="font-family: 'Open Sans', sans-serif;font-style: normal;font-weight: normal;font-size: 18px;line-height: 24px;text-align: center;color: #141414;">
													${description}
													</div>
												</td>
											</tr>
											<tr>
												<td align="center" style="font-size:0px;padding-bottom: 20px; word-break:break-word;">
													<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
														<tbody>
															<tr>
																<td style="width:220px;">
																	<a href="${setUrl}" style="text-decoration: none;outline: 0;text-decoration: none;outline: 0;width: 248px;max-height: 50px;background: #23aae1;display: block;border-radius: 10px;color: #fff;font-style: normal;font-weight: 800; font-family: Nunito, sans-serif;font-size: 20px;text-align: center;padding: 12px;"> ${buttonValue} </a>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" style="font-size:0px;padding:0px 25px 30px 25px;word-break:break-word;">
													<div style="font-family: 'Open Sans', sans-serif;font-style: normal;font-weight: normal;font-size: 18px;line-height: 24px;text-align: center;color: #141414;">
													If you didn't make this request, please contact us at <a href= "mailto:support@yadara.com" style="font-family: Nunito, sans-serif;font-style: normal;font-weight: normal;font-size: 18px;line-height: 24px;text-align: center;color: #23AAE1; text-decoration: underline;">support@yadara.com</a>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!--[if mso | IE]></td></tr></table><![endif]-->
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--[if mso | IE]></td></tr></table><![endif]-->


			<!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:500px;" width="500" bgcolor="#f8f8f8" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
			
			<div style="background:#f8f8f8;background-color:#f8f8f8;margin:0px auto;max-width:500px;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f8f8f8;background-color:#f8f8f8;width:100%;">
					<tbody>
						<tr>
							<td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
								<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:500px;" ><![endif]-->
								<div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
									<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
										<tbody>
											<tr>
												<td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
													<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
														<tbody>
															<tr>
																<td style="width:80px;padding-bottom: 10px;">
																	<img height="auto" src="https://i.ibb.co/PDS8gxb/yadara-logo-Blue-Black.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="80" />
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center" style="font-size:0px;padding:0px 25px;word-break:break-word;">
													<div style="font-family: 'Open Sans', sans-serif;font-size:12px;line-height:22px;text-align:center;color:#050505;">
														Copyright &copy; 2021. All Rights Reserved on Yadara
													</div>
												</td>
											</tr>
											<tr>
												<td align="center" style="font-size:0px;padding:0px 25px;word-break:break-word;">
													<div style="font-family: 'Open Sans', sans-serif;font-size:12px;line-height:22px;text-align:center;color:#050505;">
														<a style="color:#050505" href="${process.env.APP_YADARA_URL}/terms-and-conditions" target="_blank">
															Terms and Conditions
														</a>&nbsp;&nbsp;&nbsp;

														<a style="color:#050505" href="${process.env.APP_YADARA_URL}/privacy-policy" target="_blank">
															Privacy Policy
														</a>&nbsp;&nbsp;&nbsp;

														<a style="color:#050505" href="https://yadara.zendesk.com/" target="_blank">
															Support
														</a>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!--[if mso | IE]></td></tr></table><![endif]-->
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			 
			<!--[if mso | IE]></td></tr></table><![endif]-->
		</div>
	</body>
	</html>`;
}

module.exports = {
	yadaraPasswordSetMailTemplate,
	yadaraForgetPasswordMailTemplate,
}