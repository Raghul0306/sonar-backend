const db = require('../models');
const ListingGroup = db.listinggroup;
const Listings = db.listings;
const Purchaseorder = db.purchaseorder;
const Currency = db.currency;
const PurchaseOrderListings = db.purchaseorderlisting;
const moment = require('moment');
const constants = require('../constants/constants');
const webHookHelper = require('./emailParserHelper');

const ListingGroupInputFormat = async (parsedData, ticketSystemAccount, eventDetails) => {

    let responseStatus = {};
    let seatEnd = parsedData.ticket_range.length;
    let firstSeat = await webHookHelper.ticketTypeMaping(parsedData, parsedData.ticket_range[0])
    let lastSeat = await webHookHelper.ticketTypeMaping(parsedData, parsedData.ticket_range[seatEnd-1])
    var ListingGroupInputData = {
        seller_id: 1,
        buyer_id: ticketSystemAccount.dataValues.user_id,
        eventId: eventDetails.id,
        ticket_type_id: 1,
        in_hand: moment().format("YYYY-MM-DD HH:mm:ss"),
        splits_id: 3,
        seat_type_id:parsedData.seat_type_id,
        quantity: parsedData.no_of_tickets,
        ticket_system_id: ticketSystemAccount.dataValues.ticketing_system_id,
        ticket_system_account: ticketSystemAccount.dataValues.id,
        hide_seat_numbers: true,

        //JSON Data Starts Here
        section: firstSeat.section,
        row: firstSeat.row,
        seat_start: firstSeat.seat_no,
        seat_end: lastSeat.seat_no,
        group_price: parsedData.grand_total,
        face_value: 0,
        group_cost: parsedData.grand_total,
        unit_cost: parsedData.ticket_unit_price,
        zone_seating: null,
        internal_notes: 'ticket master internal notes',
        external_notes: 'ticket master external notes',
        is_attachments: Boolean(parsedData.mail_attachments),
        created_by: 1
    };

    try{
        responseStatus.data = await ListingGroup.create(ListingGroupInputData, { returning: ['id'] });
        responseStatus.status = true;
        responseStatus.message = constants.mailParser.response.success.listingGroupAdd;
    }catch(err){
        responseStatus.status = false;
        responseStatus.message = err.message;
    }

    return responseStatus;

}

const ListingInputFormat = async (parsedData, listingGroupData, seat) => {
    let seatProperties = await webHookHelper.ticketTypeMaping(parsedData,parsedData.ticket_range[seat])
    let responseStatus = {};
    let listingInputData = {
        listingGroupId: listingGroupData.dataValues.id,
        buyer_id: 1,
        in_hand: moment().format("YYYY-MM-DD HH:mm:ss"),
        section: seatProperties.section,
        row: seatProperties.row,
        seat_number: seatProperties.seat_no,
        sold: false,
        delivered: false,
        pdf: "Yes",
        unit_cost: parsedData.ticket_unit_price,
        price: parsedData.ticket_unit_price,
        face_value: 0,
    };

    console.log("====listing input format===");
    console.log(listingInputData);
    console.log(listingGroupData);
          
    try{
        responseStatus.data = await Listings.create(listingInputData, { returning: ['id'] });
        responseStatus.status = true;
        responseStatus.message = constants.mailParser.response.success.listingAdd;
    }catch(err){
        responseStatus.status = false;
        responseStatus.message = err.message;
    }
    
    console.log("===input data===")
    console.log(responseStatus);
    
    return responseStatus;

}

const purchaseOrderInputFormat = async (parsedData, vendorDetails, listingGroupDetails) => {

    let currency = await Currency.findOne({
        attributes : ['id'],
        where: {symbol : parsedData.currency},
        raw:true,
    })

    let responseStatus = {};
    let purchaseOrderDetails = {};
    purchaseOrderDetails.user_id = 1;
    purchaseOrderDetails.vendor_id = vendorDetails.dataValues.id;
    purchaseOrderDetails.listing_group_id = listingGroupDetails.dataValues.id;
    purchaseOrderDetails.purchase_date = moment().format("YYYY-MM-DD HH:mm:ss");
    purchaseOrderDetails.payment_method_id = null;
    purchaseOrderDetails.sales_tax = parsedData.service_charge_per_unit*parsedData.no_of_tickets;
    purchaseOrderDetails.purchase_amount = parsedData.grand_total;
    purchaseOrderDetails.payment_last4_digits = null;
    purchaseOrderDetails.shipping_and_handling_fee = parsedData.order_processing_fees;
    purchaseOrderDetails.payment_type_id = null;
    purchaseOrderDetails.external_reference = parsedData.order_number;
    purchaseOrderDetails.fees = 0;
    purchaseOrderDetails.notes = 'ticket master email parser';
    purchaseOrderDetails.currency_id = currency.id||231;
    purchaseOrderDetails.created_by = 1;
    purchaseOrderDetails.active = true;

    try{
        responseStatus.data = await Purchaseorder.create(purchaseOrderDetails, { returning: ['id'] });
        await ListingGroup.update({
            PO_ID: responseStatus.data.dataValues.id,
          }, {
            where: { id : listingGroupDetails.dataValues.id},
            returning: true,
            plain: true
        });   
        responseStatus.status = true;
        responseStatus.message = constants.mailParser.response.success.purchaseOrderAdd;
    }catch(err){
        responseStatus.status = false;
        responseStatus.message = err.message;
    }

    return responseStatus;
}

const purchaseOrderListingsInputs = async (purchaseOrderData, listingsIds) => {

    let responseStatus = {};
    let purchaseListingsInputs = {};
    purchaseListingsInputs.purchase_order_id = purchaseOrderData.dataValues.id;
    purchaseListingsInputs.listing_id = listingsIds;

    try{
        responseStatus.data = await PurchaseOrderListings.create(purchaseListingsInputs, { returning: ['id'] });
        responseStatus.status = true;
        responseStatus.message = constants.mailParser.response.success.purchaseOrderListing;
    }catch(err){
        responseStatus.status = false;
        responseStatus.message = err.message;
    }

    return responseStatus;

}

module.exports = {
    ListingGroupInputFormat,
    ListingInputFormat,
    purchaseOrderInputFormat,
    purchaseOrderListingsInputs
}