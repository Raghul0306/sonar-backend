const db = require('../models');
const Listings = db.listings;
const ListingGroup = db.listinggroup;
const ListingStatus = db.listingStatus;
const sequelize = require('sequelize');
const Op = sequelize.Op;
ListingGroup.hasMany(Listings);
Listings.belongsTo(ListingGroup);
import { listingStatusList, seat_type } from "../constants/constants";
import { listinggroup } from "../models";
const { QueryTypes } = require('sequelize');

/**
    * Page : home
    * Function For : To get the revenue, sales and listing data on the insights page
    * Ticket No : TIC-35
*/

const queryRevenueData = async (seller_id, startDate, toDate) => {

    const queryData = await ListingGroup.sum('listings.price', {
        include: [{
            model: Listings,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listings.listing_group_id'))
            },
            where: {
                sold: true,
                created_at: { [Op.gt]: toDate, [Op.lt]: startDate }
            }
        }],
        where: {
            seller_id: seller_id
        }
    });

    return queryData;
}

const querySalesData = async (seller_id, startDate, toDate) => {

    var queryData = await ListingGroup.count({
        include: [{
            model: Listings,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listings.listing_group_id'))
            },
            where: {
                sold: true,
                created_at: { [Op.gt]: toDate, [Op.lt]: startDate }
            }
        }],
        where: {
            seller_id: seller_id
        }
    });

    return queryData;
}

const queryListingData = async (seller_id, startDate, toDate) => {
    var queryData = await ListingGroup.count({
        include: [{
            model: Listings,
            attributes: [],
            on: {
                col1: sequelize.where(sequelize.col('listing_group.id'), '=', sequelize.col('listings.listing_group_id'))
            },
            where: {
                sold: false,
                created_at: { [Op.gt]: toDate, [Op.lt]: startDate }
            }
        }],
        where: {
            seller_id: seller_id
        }
    });

    return queryData;
}

const getEventDetails = async (selectQuery, finalWhereStr, paginationCondition, distance) => {
    var activeListingStatus = await module.exports.getListingStatusbyName(listingStatusList.active);
    var inactiveListingStatus = await module.exports.getListingStatusbyName(listingStatusList.inactive);

    return db.sequelize.query(`SELECT * FROM ( select event.image as eventimage, event.genre_id as genreid, event.category_id as categoryid, array_agg(distinct category.name) as categoryname, event.id as event_id, array_agg(distinct genre.name) as genrename, event.name as eventname, TO_CHAR(event.date,'YYYY-MM-DD HH24:MI') as eventdate, array_agg(distinct venue.name) as venuename, array_agg(distinct venue.timezone) as timezone, array_agg(distinct venue.city) as city, array_agg(distinct venue.state) as state, array_agg(distinct countries.name) as country, array_agg(distinct countries.abbreviation) as countrycode, DATE_PART('day', event.date :: TIMESTAMP - NOW() :: TIMESTAMP ) as daystoevent, avg(listcnt.totalseats) :: numeric(20) as totalseats, avg(listcnt.soldtkts) :: numeric(20) as totalsoldtkts, avg(listcnt.totalavailabletkts) :: numeric(20) as totalavailabletkts, avg(listcnt.last7days) :: numeric(20) as totalsoldtktsinlast7days, avg(listcnt.awaitingdelivery) :: numeric(20) as awaitingdelivery, array_agg(distinct performer.name) as performername, array_agg(distinct companies.company_name) as company_name, array_agg(distinct purchase_order.id) as purchase_order_id, array_remove(array_agg(distinct purchase_order.external_reference), null) as external_reference, array_remove( array_agg( distinct CASE WHEN listing_group.listing_status_id = ` + inactiveListingStatus.id + ` THEN true WHEN listing_group.listing_status_id = ` + activeListingStatus.id + ` THEN false ELSE false END ), null ) as inactive, array_remove( array_agg(distinct listing_group.listing_status_id), null ) as listing_status ` + selectQuery + ` from ticket_exchange.event left outer join ticket_exchange.category on event.category_id = category.id left outer join ticket_exchange.genre on event.genre_id = genre.id left outer join ticket_exchange.listing_group on event.id = listing_group.event_id left outer join ticket_exchange.listing_tags on listing_group.id = listing_tags.listing_group_id left outer join ticket_exchange.event_tags on event.id = event_tags.event_id left outer join ticket_exchange.tags eventtag on event_tags.tag_id = eventtag.id left outer join ticket_exchange.tags listingtag on listing_tags.tag_id = listingtag.id left outer join ticket_exchange.venue on event.venue_id = venue.id left outer join ticket_exchange.countries on venue.country :: INTEGER = countries.id left outer join ticket_exchange.performer on event.performer_id = performer.id left outer join ticket_exchange."user" on listing_group.created_by = "user".id left outer join ticket_exchange.companies on companies.id = "user".company_id left outer join ticket_exchange.purchase_order on listing_group.id = purchase_order.listing_group_id left outer join ( select listing_group.event_id, sum(st.quantity) as totalseats, sum(st.soldtkts) as soldtkts, sum(st.last7days) as last7days, sum(st.awaitingdelivery) as awaitingdelivery, sum(st.quantity) - sum(st.soldtkts) as totalavailabletkts from ticket_exchange.listing_group left outer join ( select listing_group_id, count(listing_group_id) as quantity, sum( case when sold then 1 else 0 end ) as soldtkts, sum( case when sold and delivered is false then 1 else 0 end ) as awaitingdelivery, sum( case when sold and updated_at between current_date - interval'7 days'and current_date then 1 else 0 end ) as last7days from ticket_exchange.listing where 1 = 1 group by listing_group_id ) st on listing_group.id = st.listing_group_id where 1 = 1 AND listing_group.listing_status_id != 1 AND is_removed = FALSE group by listing_group.event_id ) as listcnt on event.id = listcnt.event_id where 1 = 1 ` + finalWhereStr + ` group by event.id order by event.date ASC ${paginationCondition} ) q WHERE 1 = 1 ` + distance + `;`, { type: QueryTypes.SELECT });
}

const getListingDetails = async (operationSelectQueryData, operationJoinQuery, finalListingWhereStr) => {
    //TODO - for viagogo hold -listing_group.listing_status_id != 1
    var activeListingStatus = await module.exports.getListingStatusbyName(listingStatusList.active);
    var inactiveListingStatus = await module.exports.getListingStatusbyName(listingStatusList.inactive);

    // const listingDetails = await db.sequelize.query(`select listing_group.id as listinggroupid, listing_group.listing_status_id, array_agg(distinct listing.id) as listingid, listing_group.event_id, array_agg( distinct ( case when listing.sold then 'yes' else 'no' end ) ) as sold, listing_group.section, listing_group.row, listing_group.seat_start || '-' || listing_group.seat_end as seatdtl, listing_group.in_hand, listing_group.unit_cost as cost, ( case when sum( case when listing.sold is false then 1 else 0 end ) > 0 then sum( case when listing.sold is false then listing.price else 0 end ) / sum( case when listing.sold is false then 1 else 0 end ) else 0 end ) as price, ( case when sum( case when listing.sold is false then 1 else 0 end ) > 0 then sum( case when listing.sold is false then listing.last_price else 0 end ) / sum( case when listing.sold is false then 1 else 0 end ) else 0 end ) as last_price, array_remove( array_agg(distinct listing.last_price_change), null ) as last_price_change, avg(st.price) as soldprice, avg(st.soldtkts) :: numeric(20) as totalsold, ( case when listing_group.seat_type_id = ` + seat_type.general_admission + ` then avg(listing_group.quantity) :: numeric(20) - avg(st.soldtkts) :: numeric(20) else avg( listing_group.seat_end :: integer - listing_group.seat_start :: integer + 1 ) :: numeric(20) - avg(st.soldtkts) :: numeric(20) end ) as totalavailable, listing_group.face_value, CASE WHEN listing_group.listing_status_id=` + inactiveListingStatus.id + ` THEN true WHEN listing_group.listing_status_id=` + activeListingStatus.id + ` THEN false ELSE false END as listing_inactive, array_remove(array_agg(distinct listing.active), null) as active, array_remove(array_agg(distinct listing.inactive), null) as inactive, listing_group.ticket_type_id, array_agg(distinct ticket_types.name) as tickettypename, avg(st.last7days) :: numeric(20) as last7days, avg(st.awaitingdelivery) :: numeric(20) as awaitingdelivery, avg(st.margin) :: numeric(20, 2) as margin, avg(st.margintype) :: numeric(20) as margintype, array_remove(array_agg(distinct listingtag.tag_name), null) as tag_name, array_agg(price_log.user_name) as price_by`+ operationSelectQueryData +` from ticket_exchange.listing_group left outer join ( select listing_group_id, sum( case when sold then 1 else 0 end ) as soldtkts, sum( case when sold then price else 0 end ) as price, sum( case when sold and delivered is false then 1 else 0 end ) as awaitingdelivery, avg( case when sold and price > 0 then trunc((price / unit_cost) :: numeric, 2) else 0 end ) as margin, avg( case when sold then price else 0 end ) - avg( case when sold then unit_cost else 0 end ) as margintype, sum( case when sold and updated_at between current_date - interval'7 days'and current_date then 1 else 0 end ) as last7days from ticket_exchange.listing where 1 = 1 group by listing_group_id ) st on listing_group.id = st.listing_group_id left outer join ticket_exchange.listing on listing_group.id = listing.listing_group_id left outer join ticket_exchange.ticket_types on listing_group.ticket_type_id = ticket_types.id left outer join ( select listing_tags.listing_group_id, tags.tag_name from ticket_exchange.listing_tags inner join ticket_exchange.tags on listing_tags.tag_id = tags.id where 1 = 1 ) listingtag on listingtag.listing_group_id = listing_group.id left outer join ( select DISTINCT price_log.listing_group_id, price_log.id, concat("user".first_name,' ',"user".last_name) as user_name from ticket_exchange.price_log inner join ticket_exchange."user"on"user".id = price_log.created_by where 1 = 1 ORDER BY price_log.id DESC ) price_log on price_log.listing_group_id = listing_group.id `+ operationJoinQuery +` where 1 = 1 ` + finalListingWhereStr + ` group by listing_group.id;`, { type: QueryTypes.SELECT }); // Note: old query dont

     //return db.sequelize.query(`select listing_group.id as listinggroupid, listing_group.listing_status_id, array_agg(distinct listing.id) as listingid, listing_group.event_id, listing_group.section, listing_group.row, listing_group.seat_start ||'-'|| listing_group.seat_end as seatdtl, listing_group.in_hand, listing_group.unit_cost as cost, listing_group.face_value, listing_group.ticket_type_id, array_agg(distinct ticket_types.name) as tickettypename, array_agg( distinct ( case when listing.sold then'yes'else'no'end ) ) as sold, ( case when sum( case when listing.sold is false then 1 else 0 end ) > 0 then sum( case when listing.sold is false then listing.price else 0 end ) / sum( case when listing.sold is false then 1 else 0 end ) else 0 end ) as price, ( case when sum( case when listing.sold is false then 1 else 0 end ) > 0 then sum( case when listing.sold is false then listing.last_price else 0 end ) / sum( case when listing.sold is false then 1 else 0 end ) else 0 end ) as last_price `+ operationSelectQueryData +` array_remove( array_agg(distinct listing.last_price_change), null ) as last_price_change, ( case when listing_group.seat_type_id = ` + seat_type.general_admission + ` then avg(listing_group.quantity) :: numeric(20) - sum( case when ticket_exchange.listing.sold then 1 else 0 end ) :: numeric(20) else avg( listing_group.seat_end :: integer - listing_group.seat_start :: integer + 1 ) :: numeric(20) - sum( case when ticket_exchange.listing.sold then 1 else 0 end ) :: numeric(20) end ) as totalavailable, ( case when listing_group.listing_status_id = ` + inactiveListingStatus.id + ` then true when listing_group.listing_status_id = ` + activeListingStatus.id + ` then false else false end ) as listing_inactive, array_remove(array_agg(distinct listing.active), null) as active, array_remove(array_agg(distinct listing.inactive), null) as inactive, sum( case when ticket_exchange.listing.sold then 1 else 0 end ) as totalsold, sum( case when ticket_exchange.listing.sold then ticket_exchange.listing.price else 0 end ) as soldprice, sum( case when ticket_exchange.listing.sold and ticket_exchange.listing.delivered is false then 1 else 0 end ) as awaitingdelivery, round( avg( case when ticket_exchange.listing.sold and ticket_exchange.listing.price > 0 then trunc( ( ticket_exchange.listing.price / ticket_exchange.listing.unit_cost ) :: numeric, 2 ) else 0 end ), 2 ) as margin, avg( case when ticket_exchange.listing.sold then ticket_exchange.listing.price else 0 end ) - avg( case when ticket_exchange.listing.sold then ticket_exchange.listing.unit_cost else 0 end ) as margintype, sum( case when ticket_exchange.listing.sold and ticket_exchange.listing.updated_at between current_date - interval'7 days'and current_date then 1 else 0 end ) as last7days, array_remove(array_agg(distinct listingtag.tag_name), null) as tag_name from ticket_exchange.listing_group left outer join ticket_exchange.listing on listing_group.id = listing.listing_group_id left outer join ticket_exchange.ticket_types on listing_group.ticket_type_id = ticket_types.id LEFT outer JOIN ticket_exchange.listing_tags on listing_tags.listing_group_id = listing.listing_group_id left outer join ticket_exchange.tags on listing_tags.tag_id = tags.id left outer join ( select listing_tags.listing_group_id, tags.tag_name from ticket_exchange.listing_tags inner join ticket_exchange.tags on listing_tags.tag_id = tags.id where 1 = 1 ) listingtag on listingtag.listing_group_id = listing_group.id `+ operationJoinQuery + ` where 1 = 1 ` + finalListingWhereStr + ` group by listing_group.id; `, { type: QueryTypes.SELECT });

    // 21/10/2021
     return db.sequelize.query(`SELECT listing_group.id as listinggroupid, listing_group.listing_status_id, array_agg(distinct listing.id) as listingid, listing_group.event_id, listing_group."section", listing_group."row", listing_group.seat_start ||'-'|| listing_group.seat_end as seatdtl, listing_group.in_hand, listing_group.unit_cost as cost, listing_group.face_value, listing_group.ticket_type_id, array_agg(distinct ticket_types.name) as tickettypename,listing_group.seat_type_id,array_agg( distinct ( case when listing.sold then'yes'else'no'end ) ) as sold, ( case when sum( case when listing.sold is false then 1 else 0 end ) > 0 then sum( case when listing.sold is false then listing.price else 0 end ) / sum( case when listing.sold is false then 1 else 0 end ) else 0 end ) as price, ( case when sum( case when listing.sold is false then 1 else 0 end ) > 0 then sum( case when listing.sold is false then listing.last_price else 0 end ) / sum( case when listing.sold is false then 1 else 0 end ) else 0 end ) as last_price ` + operationSelectQueryData + ` array_remove( array_agg(distinct listing.last_price_change), null ) as last_price_change, ( CASE WHEN count (DISTINCT listing_tags.tag_id) > 0 THEN( sum ( CASE WHEN listing.sold ='FALSE'THEN 1 ELSE 0 END ) / count (DISTINCT listing_tags.tag_id) ) ELSE sum ( CASE WHEN listing.sold ='FALSE'THEN 1 ELSE 0 END ) END ) as totalavailable, ( case when listing_group.listing_status_id = ` + inactiveListingStatus.id + ` then true when listing_group.listing_status_id = ` + activeListingStatus.id + ` then false else false end ) as listing_inactive, listing_group.seat_start,listing_group.seat_end, listing_group.seat_type_id,listing_group.splits_id, array_remove(array_agg(distinct listing.active), null) as active, array_remove(array_agg(distinct listing.inactive), null) as inactive, ( CASE WHEN count (DISTINCT listing_tags.tag_id) > 0 THEN( sum ( CASE WHEN listing.sold = TRUE THEN 1 ELSE 0 END ) / count (DISTINCT listing_tags.tag_id) ) ELSE sum ( CASE WHEN listing.sold ='TRUE'THEN 1 ELSE 0 END ) END ) as totalsold, sum( case when listing.sold then listing.price else 0 end ) as soldprice, sum( case when listing.sold and listing.delivered is false then 1 else 0 end ) as awaitingdelivery, round( avg( case when listing.sold and listing.price > 0 then trunc( (listing.price / listing.unit_cost) :: numeric, 2 ) else 0 end ), 2 ) as margin, avg( case when listing.sold then listing.price else 0 end ) - avg( case when listing.sold then listing.unit_cost else 0 end ) as margintype, sum( case when listing.sold and listing.updated_at between current_date - interval'7 days'and current_date then 1 else 0 end ) as last7days, array_remove(array_agg(distinct tags.tag_name), null) as tag_name from ticket_exchange.listing_group left outer join ticket_exchange.listing on listing_group.id = listing.listing_group_id left outer join ticket_exchange.ticket_types on listing_group.ticket_type_id = ticket_types.id LEFT outer JOIN ticket_exchange.listing_tags on listing_tags.listing_group_id = listing.listing_group_id left outer join ticket_exchange.tags on listing_tags.tag_id = tags.id  ` + operationJoinQuery + ` where 1 = 1 ` + finalListingWhereStr + ` group by listing_group.id; `, { type: QueryTypes.SELECT });
}

const getListingStatusbyName = async (status_name) => {
    var listingStatus = await ListingStatus.findOne({
        attributes: ['id', 'status', 'name'],
        where: {
            status: status_name
        },
        raw: true
    });
    return listingStatus;
}

const getListingStatusbyId = async (status_id) => {
    var listingStatus = await ListingStatus.findOne({
        attributes: ['id', 'status', 'name'],
        where: {
            id: status_id
        },
        raw: true
    });
    return listingStatus;
}

const getRecentPricelogChanger = async (listingGroupId) => {
    var query = `select users.id, users.first_name, users.last_name FROM ticket_exchange.price_log as price_logs inner join ticket_exchange.user as users on price_logs.created_by = users.id where price_logs.listing_group_id = ` + listingGroupId + ` order by price_logs.id asc limit 1 `;
    const pricelogChangerDetails = await db.sequelize.query(query, { type: QueryTypes.SELECT });
    if (pricelogChangerDetails != "") { return pricelogChangerDetails; } else { return null; }
}

module.exports = {
    queryRevenueData, querySalesData, queryListingData, getEventDetails, getListingDetails, getListingStatusbyName, getListingStatusbyId, getRecentPricelogChanger
}