const db = require('../models');
const User = db.user;
import { userRolesLower } from "../constants/constants";
const { QueryTypes, where } = require('sequelize');
let userAccess = module.exports = {};
/**
 * Page : MyTickets
 * Function For : To Get User Ids for Listings Access
 * Ticket No : TIC 647
 */
    userAccess.getUserIdsForListingGroupAccess = async (userId) => {
        /* //Check whether this user is parent or child
        const userDetails = await db.sequelize.query(`select  parent.id, parent.user_role, user_roles.user_role_name, parent.created_by, parent.company_id from ticket_exchange.user as parent inner join ticket_exchange.user_roles on parent.user_role = user_roles.id where 1=1 and parent.id = `+ userId, { type: QueryTypes.SELECT });
        var companyUserIds = []; var companyFinalUserIds = [];
        if(userDetails && userDetails.length > 0)
        {
            var userRole = null;
            var userId = null;
            var where = `where 1=1 `;
            if(userDetails[0].user_role_name){ userRole = userDetails[0].user_role_name; }
            if(userDetails[0].id){ userId = userDetails[0].id; }
            if(userRole != userRolesLower.super_super_admin){
                where += ` and user_id  = `+userId;
            }
            else{
                return [];
            }
            var sql = `select DISTINCT company_id  from ticket_exchange.company_user `+where;
            const companyUserData = await db.sequelize.query(sql, { type: QueryTypes.SELECT });
            if(companyUserData && companyUserData.length > 0){
                for(var companyUserDataCount=0; companyUserDataCount<companyUserData.length; companyUserDataCount++) {
                companyUserIds.push(companyUserData[companyUserDataCount].company_id)
                }
                var sqlUser = `select DISTINCT id  from ticket_exchange.user where company_id IN (`+companyUserIds+`)`;
                const companyUserFinalData = await db.sequelize.query(sqlUser, { type: QueryTypes.SELECT });
                for(var companyUserFinalDataCount=0; companyUserFinalDataCount<companyUserFinalData.length; companyUserFinalDataCount++) {
                    companyFinalUserIds.push(companyUserFinalData[companyUserFinalDataCount].id)
                }
            }
        } */
        
        const companyDetails = await db.sequelize.query(`select users.id, users.company_id, users.user_role, user_roles.user_role_name, COALESCE(companies.id,users.company_id) as childcompanyid from ticket_exchange.user as users inner join ticket_exchange.user_roles on users.user_role = user_roles.id left outer join ticket_exchange.companies on users.company_id = companies.parent_company_id where users.id = `+ userId + ` order by users.user_role;`, { type: QueryTypes.SELECT });

        var childCompanyIDs = [];
        var companyFinalUserIds = [];

        if(companyDetails && companyDetails.length > 0)
        {
            var userRole = companyDetails[0].user_role_name;
            if(userRole != userRolesLower.super_super_admin)
            {
                for(var childCompanyDataCount=0; childCompanyDataCount<companyDetails.length; childCompanyDataCount++)
                {
                    childCompanyIDs.push(companyDetails[childCompanyDataCount].childcompanyid)
                }
                
                const userDetails = await db.sequelize.query(`select DISTINCT company_id, user_id  from ticket_exchange.company_user where 1=1 and company_user.company_id in (`+ childCompanyIDs +`) order by company_id, user_id;`, { type: QueryTypes.SELECT });

                if(userDetails && userDetails.length > 0)
                {
                    for(var companyUserFinalDataCount=0; companyUserFinalDataCount<userDetails.length; companyUserFinalDataCount++)
                    {
                        companyFinalUserIds.push(userDetails[companyUserFinalDataCount].user_id);
                    }
                }
            }
            else
            {
                return [];
            }
        }

        return companyFinalUserIds;
    }

/**
 * Function For : To Get User role
 * Used For : Common Function
 */
    userAccess.getUserRole = async (userId) => {
        //Get User Role Details 
        const userDetails = await db.sequelize.query(`select users.id, user_roles.user_role_name FROM ticket_exchange.user as users inner join ticket_exchange.user_roles as user_roles on users.user_role = user_roles.id where users.id = `+ userId, { type: QueryTypes.SELECT });
        var userRole = null;
        var userId = null;
        if(userDetails && userDetails.length > 0)
        {
            if(userDetails[0].user_role_name){ userRole = userDetails[0].user_role_name; }
            if(userDetails[0].id){ userId = userDetails[0].id; }
        }
        return {userId: userId, userRole: userRole};
    }
