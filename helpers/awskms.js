var AWS = require('aws-sdk');
var aws_config = require('./awskms-config')
/**
 * TIC 209
 * AWS KMS Key configs, loading from env file
 */
const kmsClient = new AWS.KMS({
    accessKeyId: aws_config.AWS_ACCESS_ID,
    secretAccessKey: aws_config.AWS_SECRET_ACCESS,
    region: aws_config.REGION,
});
const KeyId = aws_config.AWS_KMS_ID;

/**
 * TIC 209
 * @param {*} plainText to be encrypted
 * @returns Encoded data
 */

const awsEncrypt = async (plainText) => {
    const Plaintext = await new Buffer.from(plainText);
    try {
        let encrypt = await kmsClient.encrypt({ KeyId, Plaintext }).promise();
        if (encrypt) {
            const { CiphertextBlob } = encrypt;
            if (Buffer.isBuffer(CiphertextBlob)) {
                return await Buffer.from(CiphertextBlob).toString('base64');
            } else {
                throw new Error('Failed to Encrypt');
            }
        }
        else {
            return false;
        }
    }
    catch (err) {
        console.log(err);
    }
}

/**
 * TIC 209
 * @param {*} encoded to decode the encoded data/password
 * @returns Decoded/Plaintext data
 */
const awsDecrypt = async (encoded) => {
    let CiphertextBlob = Buffer.from(encoded, 'base64');
    try {
        let decrypt = await kmsClient.decrypt({ CiphertextBlob, KeyId }).promise();
        if (decrypt) {
            const { Plaintext } = decrypt;
            if (Buffer.isBuffer(Plaintext)) {
                var decryptedText = new Buffer.from(Plaintext).toString('base64');
                return new Buffer.from(decryptedText, 'base64').toString('ascii');
            } else {
                throw new Error('Failed to Decrypt');
            }
        }
        else {
            return false;
        }
    }
    catch (error) {
        console.log(error)
    }
}

module.exports = {
    awsEncrypt, awsDecrypt
}