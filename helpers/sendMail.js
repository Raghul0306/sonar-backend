var nodeMailer = require('nodemailer');

const outgoingMails = async (mailContents) => {

    let transporter = nodeMailer.createTransport({
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        secure: false,
        auth: {
            user: process.env.EMAIL_USERNAME,
            pass: process.env.EMAIL_PASSWORD
        }
    });

    let mailOptions = {
        from: mailContents.from,
        to: mailContents.to,
        subject: mailContents.subject,
        html: mailContents.message,
    };

    let mailStatus = await transporter.sendMail(mailOptions);

    if(mailStatus && mailStatus.messageId !== undefined){
        return true;
    }else{
        return false;
    }

    
}

module.exports = {
    outgoingMails
}