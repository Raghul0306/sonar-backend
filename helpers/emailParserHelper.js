const db = require('../models');
const { QueryTypes, where } = require('sequelize');
const emailparser=db.emailparser;
const Event = db.events;
const TicketingSystemAccount = db.ticketingsystemaccounts;
const Vendor = db.vendor;
const moment = require('moment');
const constants = require('../constants/constants');
const seatTypes = constants.mailParser.seatTypes;

const addParserData = async (webhookData) => {
    let obj = {};
    let responseStatus = {};
    obj.ticketing_system_id = 1;
    obj.message_id = webhookData.id;
    obj.parsed_data = JSON.stringify(webhookData);
    obj.sync_status = false;
    obj.sync_at = null;
    obj.no_of_attempts = 0;
    obj.status = true;

    let parsedData = await emailparser.count({
        where:{
            message_id: obj.message_id
        }
    });

    if(parsedData == 0){

        try{
            responseStatus.data = await emailparser.create(obj, { returning: ['id'] });
            responseStatus.status = true;
            responseStatus.message = constants.mailParser.response.success.addParser;
        }catch(err){
            responseStatus.status = false;
            responseStatus.message = err.message;
        }

    }else{
        responseStatus.status = false;
        responseStatus.message = constants.mailParser.response.failed.addParser;
    }
    
    return responseStatus;

}

const checkEventExists = async (eventDetails) => {

    let eventInfo = await Event.findOne({ 
        attributes : [
            'id',               'name',
            'categoryId',       'performerId',
            'opponent',         'venueId',
            'eventStatusId',    'ticketSystemId',
            'tickettype_id',    'date',
            'tevoid',           'mercuryid',
            'popularity_score', 'eventdate_day',
            'event_link',       'image',
            'created_by',       'genre_id',
            'disclosures',      'notes',
            'createdAt',        'updatedAt'
          ],
        where:{
            name: eventDetails.event_name
            },
        raw:true
    });

    if(eventInfo){
        return eventInfo;
    }
    else{
        return null;
    }

}

const getTicketSystemAccount = async (accountDetails) => {
     
    let ticketSystem = await TicketingSystemAccount.findOne({
                            where:{
                                account_name: accountDetails.mail_to
                            }
                        });

    return ticketSystem;

}

const getVendorDetails = async (vendorDetails) => {
     
    let vendorInfo = await Vendor.findOne({
                            where:{
                                email: vendorDetails.mail_from
                            }
                        });

    return vendorInfo;
}

const updateEmailParser = async (parserDetails, data) => {

    if(!parserDetails.status){return false;}

    let emailParserData = {};
    emailParserData.sync_status = data.status == 200 ? true:false;
    emailParserData.message = data.message;
    emailParserData.sync_at = moment().format("YYYY-MM-DD HH:mm:ss");
    emailParserData.no_of_attempts = parserDetails.data.dataValues.no_of_attempts + 1;
    emailParserData.status = parserDetails.data.dataValues.no_of_attempts<4?true:false;

    let parserUpdateStatus = await emailparser.update(emailParserData, {where:{id: parserDetails.data.dataValues.id}});

    let returnStatus;
    if(parserUpdateStatus){
        returnStatus = true;
    }else{
        returnStatus = false;
    }

    return returnStatus;

}

//Function which extracts section,row and seat_no from seatType and ticket object for different ticket templetes
const ticketTypeMaping = async (parsedData, ticket) => {
    let section = "NA", row = "NA", seat_no = "0";

    switch(parsedData.seat_type){

        case seatTypes.seated_ticket :            
                section = ticket[2];
                row = ticket[4];
                seat_no = ticket[6];
             break;

        case seatTypes.general_admission:
                section = "NA"; 
                row = "NA"; 
                seat_no = "0";
                break;

        case seatTypes.full_price_ticket:
                section = ticket[2];
                row = ticket[4];
                seat_no = ticket[6];
            break;

        default : 
            console.log("ALERT : No tables found");
        
    }
    console.log("TICKET ==> ",{section,row,seat_no})
    return {section,row,seat_no};
}

const findSeatType = async (parsedData)=>{
    if(parsedData.ticket_range.length > 0){
        let schema = Object.values(parsedData.ticket_range[0])
        if(
            (schema.indexOf("SECTION") !== -1) &&
            (schema.indexOf("ROW") !== -1) &&
            (schema.indexOf("SEAT") !== -1)
        ) { return "Seated Ticket" }  

        else { return "General Admission" }          
     }
}
module.exports = {
    addParserData,
    checkEventExists,
    getTicketSystemAccount,
    getVendorDetails,
    updateEmailParser,
    ticketTypeMaping,
    findSeatType
}