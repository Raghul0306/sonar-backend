const db = require('../models');
const {inputType} = require('../constants/constants.js');
const _ = require('lodash');

let inputValidate = module.exports = {};

inputValidate.isValidInput = async (type,values = []) => {
    let result = true;
    for(let value of values){
        if(result){
            switch(type){
                case inputType.id:
                    result = value && value != "null" && value != "undefined" && !isNaN(value)
                    continue;
                case inputType.string:
                    result = value && value.trim() && value != "null" && value != "null" && value != "undefined"
                    continue;
            }
        }
    }
    
    return !!result && !!values.length;
}

